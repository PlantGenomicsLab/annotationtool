import numpy as np
import tensorflow
from tensorflow.keras import layers, models, losses, regularizers

train_data = np.loadtxt("train_data.txt")
test_data = np.loadtxt("test_data.txt")

y_col = train_data.shape[1] - 1

train_x = np.delete(train_data, y_col, axis = 1)
train_y = train_data[:, y_col]

test_x = np.delete(test_data, y_col, axis = 1)
test_y = test_data[:, y_col]

model = models.Sequential([
        layers.InputLayer(train_x.shape[1]),
        layers.Dense(35, activation="relu", kernel_regularizer=regularizers.L2(0.001)),
        layers.Dense(20, activation="sigmoid", kernel_regularizer=regularizers.L2(0.001)),
        layers.Dense(11, activation="sigmoid", kernel_regularizer=regularizers.L2(0.001)),
        layers.Dense(5, activation="sigmoid")
])

loss_fn = losses.SparseCategoricalCrossentropy()
model.compile(optimizer='adam',
              loss=loss_fn,
              metrics=['accuracy'])

model.fit(train_x, train_y, epochs=100, verbose=1)

res = model.evaluate(test_x, test_y, verbose=1)

#print(test_y[0:10])
#model.predict(test_x[0:10, :])
#print(res[1])
