import numpy as np
from collections import Counter

files={
	1 : "../data_folder/sample_intergene_atg_data.txt",
	2 : "../data_folder/sample_intragene_atg_cds_data.txt",
	3 : "../data_folder/sample_intragene_atg_non_cds_data.txt",
	4 : "../data_folder/sample_pseudo_atg_data.txt",
	0 : "../data_folder/sample_true_start_sites_data.txt"
}

data_arr=[]
for n in files.keys():
	x = np.loadtxt(files[n])
	data = np.insert(x, x.shape[1], n, axis = 1)
	data_arr.append(data)

lens = list(map(lambda data : data.shape[0] , data_arr))
smaller = data_arr.pop(lens.index(min(lens)))
for i in range(len(data_arr)):
	data = data_arr[i]
	np.random.shuffle(data)
	data_arr[i] = data[0:smaller.shape[0], :]

data_arr.append(smaller)	

data = np.vstack(data_arr)
np.random.shuffle(data)

frac=0.8
cutoff_ind = round(data.shape[0] * frac)
np.savetxt("train_data.txt", data[:cutoff_ind, :])
np.savetxt("test_data.txt", data[cutoff_ind:, :])
