#!/bin/bash
#SBATCH --job-name=makeData
#SBATCH -o makeData-%j.output
#SBATCH -e makeData-%j.error
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=5G

echo `hostname`

for f in *.out; do

pref=${f%_entropies.out}
out=${pref}_data.txt

if [[ -f $out ]]; then rm $out; fi

awk '{if (NR%2==0)print $0}' $f |
while read LINE; do

echo ${LINE#,} | sed "s/|/ /g" >> $out

done

if [[ ! -d data_folder ]]; then mkdir data_folder; fi
mv $out data_folder/

done
