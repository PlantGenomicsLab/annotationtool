#!/bin/bash
#SBATCH --job-name=makeData
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=5G
#SBATCH -o makeData_%j.out
#SBATCH -e makeData_%j.err

module load anaconda
source activate py_new
module unload anaconda


datadir=/labs/Wegrzyn/annotationtool/folding_testing/elegans/window_size/40_20/new
for f in $datadir/*.rna.out; do



f_name=$(basename $f | sed 's/\..*\.out//')
out=${f_name}_sl_data.txt
fold_data_file=${f_name}_fold_data.txt

awk '{if (NR%3==0)print $1}' $f > $fold_data_file
if [[ -f $out ]]; then rm $out; fi


python make_data.py --filename $fold_data_file --out $out

rm $fold_data_file

outdir=data_folder
if [[ ! -d $outdir ]]; then mkdir $outdir; fi
mv $out $outdir/





done
