import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--filename', type=str, required=True)
parser.add_argument('--out', type=str, required=True)

args = parser.parse_args()
filename = args.filename
out = args.out


file_arr = np.loadtxt(filename, dtype="str")
n_row=file_arr.shape[0]
sl_arr = np.empty([n_row, len(file_arr[0])])

change = {"." : 0, "(" : 1, ")" : 2}
for r in range(n_row):
	folds = file_arr[r]
	sl_arr[r, :]  = list(map(lambda x : change[x], folds))

np.savetxt(out, np.array(sl_arr))
