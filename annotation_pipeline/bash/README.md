# Annotation pipeline

* Steps and scripts are taken from https://gitlab.com/PlantGenomicsLab/annotationtool/-/tree/master/data-gathering and modified to work within an automatic pipeline
* An example analysis can be run via `bash run_pipeline` for the species Rosa Chinensis and related species Rosa Rugosa
* Input validation is performed, and if the pipeline is re-run after an error it will not repeat steps if it sees expected completion files
