#!/bin/bash
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G
#SBATCH --cpus-per-task=1

### This sickle script is designed for PAIRED end reads and is path dependent!!!
### variable myarr tries to capture all of the accession numbers (eg. SRR3723923_1.fastq & SRR3723923_2.fastq)
### variable uniq_file tries to capture all of the UNIQUE accession numbers (eg. SRR3723923)
### the unique accession numbers are then used to specify the forward (var f1) files and reverse (var f2) files
### the forward and reverse file names are used to run sickle 


rawreadsdir="$1"
outdir="$2"

echo "Current Directory: $PWD"

module load sickle/1.33

myarr=()
for f in $rawreadsdir/*.fastq
do 
	myarr+=($(echo "$f" | sed "s|.*/||g" | sed "s|_.*||g"))
done

uniq_file=($(echo "${myarr[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

cd $outdir

for i in ${uniq_file[@]}
do
	f1="$i"_1.fastq
  f2="$i"_2.fastq
	if [ -f $rawreadsdir/$f2 ]; then
		script="sickle pe -f $rawreadsdir/$f1 -r $rawreadsdir/$f2 -t sanger -o trimmed_"$f1" -p trimmed_"$f2" -s single_trimmed_"$i".fastq -q 30 -l 50"
    eval "$script"
    if [[ "$?" != 0 ]]; then
      echo "Current directory = $PWD"
      echo "Running $script failed"
      exit 1
    fi
	else
		script="sickle se -f $rawreadsdir/$f1 -t sanger -o trimmed_"$f1" -q 30 -l 50"
    eval "$script"
    if [[ "$?" != 0 ]]; then
      echo "Current directory = $PWD"
      echo "Running $script failed"
      exit 1
    fi
	fi
done

touch "pipeline_completed"
