#!/bin/bash
#SBATCH --job-name=validateSRA
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH -o validateSRA_%j.out
#SBATCH -e validateSRA_%j.err

echo `hostname`

module load  sratoolkit/2.8.1

FILENAME="fetchSRA_input.txt"

while read LINE
do
    vdb-validate $LINE
    echo "$LINE"

done < $FILENAME
