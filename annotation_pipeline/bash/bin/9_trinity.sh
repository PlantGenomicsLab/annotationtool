#!/bin/bash
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=150G
#SBATCH --cpus-per-task=10

module load trinity/2.8.5
module load bowtie2/2.3.4.3
module load samtools/1.10

PE_1="$1"
PE_2="$2"
out="$3"

cd $out

outname=trinity

script="Trinity --seqType fq --left $PE_1 --right $PE_2 --min_contig_length 300 --output $outname --full_cleanup --max_memory 150G --CPU 10"

eval "$script"

if [[ "$?" != 0 ]]; then
  echo "Current directory = $PWD"
  echo "Running $script failed"
  exit 1
else
  touch "pipeline_completed"
fi
