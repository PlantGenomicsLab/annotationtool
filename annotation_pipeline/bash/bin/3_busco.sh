#!/bin/bash
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G
#SBATCH --cpus-per-task=1

module load busco/4.1.2

# These variables should make sense with your individual home directory
# Change them if you named things differently

export PATH=$HOME/Augustus/bin:$HOME/Augustus/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/Augustus/config

gen="$1"
busco_db="$2"

cd metrics/busco

script="busco -i $gen -l $busco_db -o busco_o -m geno"

eval "$script"

if [[ "$?" != 0 ]]; then
  echo "Current directory = $PWD"
  echo "Running $script failed"
  exit 1
else
  touch "pipeline_completed"
fi
