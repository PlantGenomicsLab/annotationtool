#!/bin/bash
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=30G
#SBATCH --cpus-per-task=1

module load biopython/1.70
module load numpy/1.6.2

genome="$1"
out=genome/$(basename $genome)-filtered

script="bin/./filterLen.py --fasta $genome --length 500 --out $out"

eval "$script"

if [[ "$?" != 0 ]]; then
  echo "Current directory = $PWD"
  echo "Running $script failed"
  exit 1
fi

