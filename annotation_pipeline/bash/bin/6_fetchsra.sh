#!/bin/bash
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G
#SBATCH --cpus-per-task=1

echo `hostname`
echo "Current dir=$PWD"

module load sratoolkit/2.8.1

sra_file="$1"
dir="$2"

cd $dir

count=0
while read LINE
do
    echo $LINE
    defline='@$sn[_$rn]/$ri'
    script="fastq-dump --defline-seq '$defline' --split-files $LINE"
    eval "$script"
    if [[ "$?" != 0 ]]; then
      echo "Current directory = $PWD"
      echo "Running $script failed"
      exit 1
    fi

    script="vdb-validate $LINE"
    eval "$script"
    if [[ "$?" != 0 ]]; then
      echo "Current directory = $PWD"
      echo "Running $script failed"
      exit 1
    fi

    let count++
done < $sra_file

echo -e "\nTotal lines read = $count"

touch "pipeline_completed"
