#!/bin/bash
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G
#SBATCH --cpus-per-task=1

module load hisat2/2.2.0

genome="$1"
species_name="$2"

cd index/hisat

script="hisat2-build -f $genome ${species_name}.built"

eval "$script"

if [[ "$?" != 0 ]]; then
  echo "Current directory = $PWD"
  echo "Running $script failed"
  exit 1
else
  touch "pipeline_completed"
fi
