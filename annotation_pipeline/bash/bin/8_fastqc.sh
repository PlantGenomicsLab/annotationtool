#!/bin/bash
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G
#SBATCH --cpus-per-task=4

module load fastqc/0.11.7

trimmed_dir="$1"
output_dir="$2"

str="$trimmed_dir/*.fastq"

if [[ "$str" = "" ]];then
  echo "ERR: Fastqc: No fastq's found in $PWD"
  exit 1
fi

cd $output_dir
script="fastqc -t 4 $str"

eval "$script"

if [[ "$?" != 0 ]]; then
  echo "Current directory = $PWD"
  echo "Running $script failed"
  exit 1
else
  touch "pipeline_completed"
fi
