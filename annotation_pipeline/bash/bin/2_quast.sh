#!/bin/bash
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=30G
#SBATCH --cpus-per-task=1

module load quast/5.0.2
module load python/3.6.3

genome="$1"

cd metrics/quast

script="python /isg/shared/apps/quast/5.0.2/quast.py $genome -o quast_o"

eval "$script"

if [[ "$?" != 0 ]]; then
  echo "Current directory = $PWD"
  echo "Running $script failed"
  exit 1
else
  touch "pipeline_completed"
fi
