#!/bin/bash
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G
#SBATCH --cpus-per-task=1

#build gmap index
module load gmap/2019-06-10
#-D /path/to/where/to/save/make/gmap/index/dir -d name_of_gmap_index_dir genome.fa

genome="$1"
species_name="$2"
indexdir="$3"

cd $indexdir

script="gmap_build -D $PWD -d $species_name $genome"

eval "$script"

if [[ "$?" != 0 ]]; then
  echo "Current directory = $PWD"
  echo "Running $script failed"
  exit 1
else
  touch "pipeline_completed"
fi
