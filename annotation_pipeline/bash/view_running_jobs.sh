# Helpful script that expands running/stalled job names to see if a dependency failed, where it might have gone wrong
# Also just easier format to see job names as they are running

squeue -u `whoami` --format="%.18i %35j %.2t %.10M %R"
