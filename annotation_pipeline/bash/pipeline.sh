#!/bin/bash

# Terminate on nonzero command exit
set -e
# Do not run any command with unset variables
set -u
# Cover exception of set -e on last program terminating
set -o pipefail

function usage() {
    cat <<EOF
SYNOPSIS
  run.sh speciesName softmaskedGenome sra_list_original sra_list_related buscoDatabase relatedSpeciesName    - run annotation pipeline
  run.sh help - display this help message
DESCRIPTION
  Accepts a softmasked genome and short read information from original and 
  closely related species. Performs de-novo genome annotation pipeline.
  Details related to the steps of the pipeline, as well as example runs and 
  output are on the PlantComputationalGenomics GitHub.
  See: https://gitlab.com/PlantGenomicsLab/annotationtool/-/tree/master/pipeline

AUTHOR
  Jeremy Bennett
  jeremy.bennett@uconn.edu

EOF
}

function info() {
    echo "INFO: $@" >&2
}
function error() {
    echo "ERR:  $@" >&2
}
function fatal() {
    echo -e "ERR:  $@" >&2
    exit 1
}

function get_sbatch_id () {
  if [[ "$@" =~ Submitted\ batch\ job\ ([0-9]+) ]]; then
    echo "${BASH_REMATCH[1]}"
    exit 0
  else
    fatal "Failed parsing jobid from sbatch output: $@"
  fi
}

# Create a (hopefully) unique prefix for the names of all jobs in this 
# particular run of the pipeline. This makes sure that runs can be
# identified unambiguously
run=$(uuidgen | tr '-' ' ' | awk '{print $1}')

if [ "$#" -lt 6 ]; then usage; exit; fi

################################################################################
#                                 check input                                  #
################################################################################
speciesName=$1
softmaskedGenome=$2 #all paths need to be full
speciesSraFilename=$3
relatedSraFilename=$4
relatedSpeciesName=$5
buscoDatabase=$6

mkdir -p log genome index evidence alignments annotation metrics

# Check genome exists and follows softmasking naming convention
if [[ ! -e $softmaskedGenome ]]; then
  otherLocation=genome/$(basename $softmaskedGenome)
  if [[ ! -e $otherLocation ]]; then
    fatal "Genome file does not exist with provided path: $softmaskedGenome , or in pipeline location $otherLocation"
  else
    info "Sanity check: you are re-running this pipeline, make sure you havent moved any important files (that the pipeline didnt move itself)"
  fi
else
  info "This is the first time you are running this pipeline, directory structure will be auto-generated"
  mv $softmaskedGenome genome
fi
softmaskedGenome=$PWD/genome/$(basename $softmaskedGenome)

if [[ ! $softmaskedGenome =~ .*sm.fasta ]]; then
  error "Genome does not follow expected naming convention: $softmaskedGenome, please double check this is a softmasked fasta file"
fi

# Check sra files exist
if [[ ! -e $speciesSraFilename ]]; then
  fatal "Same species SRA list does not exist with provided path: $speciesSraFilename"
fi

if [[ ! -e $relatedSraFilename ]]; then
  fatal "Related species SRA list does not exist with provided path: $relatedSraFilename"
fi

# Read sra files into variable array
readarray -t speciesSra < $speciesSraFilename
readarray -t relatedSra < $relatedSraFilename

################################################################################
#                               run the pipeline                               #
################################################################################
info "Softmasked genome location is $softmaskedGenome"

## Step 1: filter genome for seqs <500 bp
if [[ -e "$softmaskedGenome-filtered" ]];then 
  info "Genome has already been filtered for seqs <500bp, skipping step"
  filterId=1
else
  script="bin/1_filter_genome.sh $softmaskedGenome"
  info "Running: $script"
  jobString=$(sbatch --job-name=$speciesName.filtergen --output=log/$run.filtergenome $script)
  filterId="$(get_sbatch_id $jobString)"
fi

## Step 2: quast on filtered genome
mkdir -p metrics/quast

if [[ -e "metrics/quast/pipeline_completed" ]];then
  info "quast has already been run, skipping step"
  quastId=1
else
  script="bin/2_quast.sh $softmaskedGenome"
  info "Running: $script"
  jobString=$(sbatch --job-name=$speciesName.quast --output=log/$run.quast --dependency=afterok:$filterId $script)
  quastId="$(get_sbatch_id $jobString)"
fi

## Step 3: busco on filtered genome
mkdir -p metrics/busco

if [[ -e "metrics/busco/pipeline_completed" ]];then
  info "busco has already been run, skipping step"
  buscoId=1
else
  script="bin/3_busco.sh $softmaskedGenome $buscoDatabase"
  info "Running: $script"
  jobString=$(sbatch --job-name=$speciesName.busco --output=log/$run.busco --dependency=afterok:$filterId $script) 
  buscoId=$(get_sbatch_id $jobString)
fi

## Step 4: create gmap index file
gmapIndexDirectory="index/gmap"
mkdir -p $gmapIndexDirectory

if [[ -e "$gmapIndexDirectory/pipeline_completed" ]];then
  info "gmap index has already been created, skipping step"
  gmapBuildid=1
else
  script="bin/4_gmapBuild.sh $softmaskedGenome $speciesName $gmapIndexDirectory"
  info "Running: $script"
  jobString=$(sbatch --job-name=gmapBuildIndex --output=log/$run.gmapBuildIndex --dependency=afterok:$filterId $script)
  gmapBuildid="$(get_sbatch_id $jobString)"
fi

## Step 5: create hisat index file
mkdir -p index/hisat

if [[ -e "index/hisat/pipeline_completed" ]]; then
  info "hisat index has already been created, skipping step"
  hisatBuildid=1
else
  script="bin/5_hisatBuild.sh $softmaskedGenome $speciesName"
  info "Running: $script"
  jobString=$(sbatch --job-name=hisatBuildIndex --output=log/$run.hisatBuildIndex --dependency=afterok:$filterId $script)
  hisatBuildid=$(get_sbatch_id $jobString)
fi

## Step 6: download and validate short read data
speciesRawReadsDir=$PWD/evidence/raw_reads/$speciesName
mkdir -p $speciesRawReadsDir

if [[ -e "$speciesRawReadsDir/pipeline_completed" ]]; then
  info "$speciesName short reads have already been downloaded, skipping step"
  fetchsraSameId=1
else
  script="bin/6_fetchsra.sh $speciesSraFilename $speciesRawReadsDir"
  info "Running: $script"
  jobString=$(sbatch --job-name=$speciesName.fetchsra --output=log/$run.$speciesName.fetchSRA $script)
  fetchsraSameId=$(get_sbatch_id $jobString)
fi

relatedRawReadsDir=$PWD/evidence/raw_reads/$relatedSpeciesName
mkdir -p $relatedRawReadsDir

if [[ -e "$relatedRawReadsDir/pipeline_completed" ]]; then
  info "related species short reads have already been downloaded, skipping step"
  fetchsraRelatedId=1
else
  script="bin/6_fetchsra.sh $relatedSraFilename $relatedRawReadsDir"
  jobString=$(sbatch --job-name=$relatedSpeciesName.downloadrawreads --output=log/$run.$relatedSpeciesName.fetchSRA $script)
  fetchsraRelatedId=$(get_sbatch_id $jobString)
fi

## Step 7: fastQC on raw reads
speciesFastqcRawDir=$PWD/metrics/fastqc/raw/$speciesName
mkdir -p $speciesFastqcRawDir

if [[ -e "$speciesFastqcRawDir/pipeline_completed" ]];then
  info "$speciesName reads have already been fastqc'd, skipping step"
  speciesFastqcId=1
else
  script="bin/8_fastqc.sh $speciesRawReadsDir $speciesFastqcRawDir"
  info "Running: $script"
  jobString=$(sbatch --job-name=$speciesName.fastqc --output=log/$run.$speciesName.fastqc --dependency=afterok:$fetchsraSameId $script)
  speciesFastqcRawId=$(get_sbatch_id $jobString)
fi

relatedFastqcRawDir=$PWD/metrics/fastqc/raw/$relatedSpeciesName
mkdir -p $relatedFastqcRawDir

if [[ -e "$relatedFastqcRawDir/pipeline_completed" ]];then
  info "$relatedSpeciesName reads have already been fastqc'd, skipping step"
  relatedFastqcRawId=1
else
  script="bin/8_fastqc.sh $relatedRawReadsDir $relatedFastqcRawDir"
  info "Running: $script"
  jobString=$(sbatch --job-name=$relatedSpeciesName.fastqc.raw --output=log/$run.$relatedSpeciesName.fastqc.raw --dependency=afterok:$fetchsraRelatedId $script)
  relatedFastqcRawId=$(get_sbatch_id $jobString)
fi

## Step 8: trim reads with sickle
speciesTrimmedDir=$PWD/evidence/trimmed_reads/$speciesName
mkdir -p $speciesTrimmedDir

if [[ -e "$speciesTrimmedDir/pipeline_completed" ]];then
  info "$speciesName reads have already been trimmed, skipping step"
  speciesSickleId=1
else
  script="bin/7_sickle.sh $speciesRawReadsDir $speciesTrimmedDir"
  info "Running: $script"
  jobString=$(sbatch --job-name=$speciesName.sickle --output=log/$run.$speciesName.sickle --dependency=afterok:$fetchsraSameId $script)
  speciesSickleId=$(get_sbatch_id $jobString)
fi

relatedTrimmedDir=$PWD/evidence/trimmed_reads/$relatedSpeciesName
mkdir -p $relatedTrimmedDir

if [[ -e "$relatedTrimmedDir/pipeline_completed" ]];then
  info "$relatedSpeciesName reads have already been trimmed, skipping step"
  relatedSickleId=1
else
  script="bin/7_sickle.sh $relatedRawReadsDir $relatedTrimmedDir"
  info "Running: $script"
  jobString=$(sbatch --job-name=$relatedSpeciesName.sickle --output=log/$run.$relatedSpeciesName.sickle --dependency=afterok:$fetchsraRelatedId $script)
  relatedSickleId=$(get_sbatch_id $jobString)
fi

## Step 9: fastQC on trimmed reads
speciesFastqcTrimmedDir=$PWD/metrics/fastqc/trimmed/$speciesName
mkdir -p $speciesFastqcTrimmedDir

if [[ -e "$speciesFastqcTrimmedDir/pipeline_completed" ]];then
  info "$speciesName reads have already been fastqc'd, skipping step"
  speciesFastqcTrimmedId=1
else
  script="bin/8_fastqc.sh $speciesTrimmedDir $speciesFastqcTrimmedDir"
  info "Running: $script"
  jobString=$(sbatch --job-name=$speciesName.fastqc --output=log/$run.$speciesName.fastqc --dependency=afterok:$speciesSickleId $script)
  speciesFastqcTrimmedId=$(get_sbatch_id $jobString)
fi

relatedFastqcTrimmedDir=$PWD/metrics/fastqc/trimmed/$relatedSpeciesName
mkdir -p $relatedFastqcTrimmedDir

if [[ -e "$relatedFastqcTrimmedDir/pipeline_completed" ]];then
  info "$relatedSpeciesName reads have already been fastqc'd, skipping step"
  relatedFastqcTrimmedId=1
else
  script="bin/8_fastqc.sh $relatedTrimmedDir $relatedFastqcTrimmedDir"
  info "Running: $script"
  jobString=$(sbatch --job-name=$relatedSpeciesName.fastqc --output=log/$run.$relatedSpeciesName.fastqc --dependency=afterok:$relatedSickleId $script)
  relatedFastqcTrimmedId=$(get_sbatch_id $jobString)
fi

## Step 10: run Trinity separately on each library, for each species
speciesTrinityDir=$PWD/evidence/trinity/$speciesName
mkdir -p $speciesTrinityDir

# First check if we successfully created ALL trinity TSA's
trinityPreviouslyRan=1
for f in ${speciesSra[@]}; do
  completed="$speciesTrinityDir/$f/pipeline_completed"
  if [[ ! -e completed ]];then
    trinityPreviouslyRan=0
    break
  fi
done

speciesTrinityIds=()
if [[ $trinityPreviouslyRan -eq 1 ]];then
  info "$speciesName TSA has already been created using trinity; skipping step"
  speciesTrinityIds=( 1 )
else
  # run trinity separately for each SRA number in input
  for f in ${speciesSra[@]}; do
    mkdir -p $speciesTrinityDir/$f
    pe_1="$speciesTrimmedDir/trimmed_${f}_1.fastq"
    pe_2="$speciesTrimmedDir/trimmed_${f}_2.fastq"
    outdir=$speciesTrinityDir/$f
    script="bin/9_trinity.sh $pe_1 $pe_2 $outdir"
    info "Running: $script"
    jobString=$(sbatch --job-name=$speciesName.trinity.$f --output=log/$run.$speciesName.trinity.$f --dependency=afterok:$speciesSickleId $script)
    speciesTrinityIds+=( $(get_sbatch_id $jobString) )
  done
fi

relatedTrinityDir=$PWD/evidence/trinity/$relatedSpeciesName
mkdir -p $relatedTrinityDir

relatedTrinityPreviouslyRan=1
for f in ${speciesSra[@]}; do
  completed="$speciesTrinityDir/$f/pipeline_completed"
  if [[ ! -e completed ]];then
    relatedTrinityPreviouslyRan=0
    break
  fi
done

relatedTrinityIds=()
if [[ $relatedTrinityPreviouslyRan -eq 1 ]];then
  info "$relatedSpeciesName TSA has already been created using trinity; skipping step"
  speciesTrinityIds=( 1 )
else
  # run trinity separately for each SRA number in input
  for f in ${relatedSra[@]}; do
    mkdir -p $relatedTrinityDir/$f
    pe_1="$relatedTrimmedDir/trimmed_${f}_1.fastq"
    pe_2="$relatedTrimmedDir/trimmed_${f}_2.fastq"
    outdir=$relatedTrinityDir/$f
    script="bin/9_trinity.sh $pe_1 $pe_2 $outdir"
    info "Running: $script"
    jobString=$(sbatch --job-name=$relatedSpeciesName.trinity.$f --output=log/$run.$relatedSpeciesName.trinity.$f --dependency=afterok:$relatedSickleId $script)
    relatedTrinityIds+=( $(get_sbatch_id $jobString) )
  done
fi


info "Submissions complete"
