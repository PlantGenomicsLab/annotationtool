# As detailed in github and pipeline.sh
# Arguments are as follows:
# species_name
# softmasked_genome (full path)
# sra_same_species (full path)
# sra_related_species (full path)
# related_species_name
# busco_db_name


# IMPORTANT: If you change anything in this file after already running the pipeline at least once
# be sure to delete the files that change is responsible for. This is because the pipeline checks for certain files
# in certain places to see if it can skip steps. So for example, if you change the busco database parameter
# and you want to re-run the busco analysis, make sure the busco output is deleted before you re-run the pipeline.


bash pipeline.sh \
  rosa_chinensis \
  $PWD/rosa_chinensis_sm.fasta \
  $PWD/sra_same_species.txt \
  $PWD/sra_related_species.txt \
  rosa_rugosa \
  embryophyta_odb10


