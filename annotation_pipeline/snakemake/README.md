# Snakemake implementation of annotation pipeline

This pipeline is built using Snakemake 6.0.5, with corresponding documentation [here](https://snakemake.readthedocs.io/en/stable/index.html)

## How to run
1. Install snakemake using conda/mamba:
  * `conda install -n base -c conda-forge mamba`
  * `conda activate base`
  * `mamba create -c conda-forge -c bioconda -n snakemake snakemake`
  * Activate using `conda activate snakemake`
2. Modify config.yaml, species and related species SRA text files for your chosen species. Examples of these files are in the [examples](examples/) directory
  * The names of the SRA files must follow the names of the species in the config.yaml file, with speciesName: ginkgo, corresponding file must be `ginkgo_sra.txt` 
  * To run on the genome (and not in parallel on scaffolds) set chromosomeAssembly: False
3. Place your genome file (which must be named $speciesName.fasta) in the genome/ folder
4. Edit the `all` rule of the Snakefile for the run you would like to execute. To run the Braker pipeline you can uncomment the `runBraker()` line, or to run metrics you can uncomment `runMetrics()`
5. From the root of your pipeline folder, execute `bash run_pipeline.sh`

## How to run in parallel
* Currently the steps that can be run in parallel are: MAKER, GenomeThreader
* This is done by splitting the input fasta into its chromosomes, running on each contig, and then concatenating
1. Set chromosomeAssembly: True in the config.yaml file
2. cd into genome/ and run `filterChromosomes.sh $genomeFasta` to filter it for chromosomes
3. Check the generated chromosomeHeaders.txt file and verify its contents make sense for your fasta file
4. Edit Snakefile for the run you would like to complete, and then `bash run_pipeline`

## Notes
* All jobs are submitted using SLURM, with dependencies handled
* When re-running the pipeline any steps that completed with no errors will not be re-submitted
* Upon pipeline completion a detailed report can be generated via `snakemake --report report.html`

## To modify scripts in the pipeline
* If you can make your changes to the script without affect input and output (like when just changing a Module Load command), you can do so by modifying the script in the bin folder, and the pipeline should work normally
* If you have to modify the input/output, you will have to modify the Snakefile, and the corresponding rule to reflect the changes

