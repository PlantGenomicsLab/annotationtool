#!/bin/bash

# This script is used only if you have specified chromosomeAssembly: True in the config.yaml file
# Run this script on your genome fasta file before running the pipeline, to filter out non-chromosome contigs
# Verify that the script did what you expected by viewing the 'chromosomeHeaders.txt' file generated when the script completes

genome="$1"

curDir=$(basename $PWD)

if [ -z $genome ];then
  echo "ERR: Please supply the genome fasta as an argument to the script. ex: bash filterChromosomes.sh P_trichocarpa.fasta"
  exit 1
fi

if [[ "$curDir" != "genome" ]];then
  echo "ERR: Please run from the genome/ directory of your pipeline folder, this is so the generated chromosomeHeaders.txt goes to the place where the pipeline expects it to be"
  exit 1
fi

fn=$(basename $genome)
newName="${fn%.fasta}_chromosomes.fasta"


# Parse for chromosome headers and keep them, everything else is discarded
# Uses samtools to perform the step where we re-create the fasta
module load samtools/1.9

cd $(dirname $genome)
headerFile="chromosomeHeaders.txt"

echo "Grepping file for chromosome headers"
grep ">.*[cC][hH][rR].*" $genome | sed 's/>//' > $headerFile
echo -e "Done!\n"

if [ ! -s $headerFile ]; then
  echo "ERR: Could not find any chromosomes in the input fasta file"
  echo "Used grep search '>.*[cC][hH][rR].*'"
  echo "If you do not have a chromosome level assembly, be sure to set chromosomeAssembly: False in the config.yaml file"
  exit 1
fi

echo "Indexing genome file"
samtools faidx $genome
echo -e "Done!\n"

echo "WARNING: about to rewrite fasta file $genome with these headers only:"
while read -r line; do
  echo ">$line"
done < $headerFile

contin="0"
echo ""
read -p "Proceed? (1=continue, 0=cancel): " contin
echo ""

if [[ "$contin" == 1 ]];then
  echo "Re-making genome file with chromosomes only"
  samtools faidx $genome -r $headerFile > tmp.tmp
  mv tmp.tmp $genome
  echo -e "Done!\n"
else
  echo "Canceling"
  exit 0
fi
