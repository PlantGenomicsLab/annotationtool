module load hisat2/2.2.1
#export PATH=$PATH:/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/gingko/make_annotation_pipeline/snakemake/bin/hisat2-2.2.1

inputDir="$PWD/$1"
readName="$2"
indexName="$PWD/$3"
outDir="$4"

cd $outDir

f1=$inputDir/trimmed_${readName}_1.fastq
f2=$inputDir/trimmed_${readName}_2.fastq

# Align, and produce .sam
echo "$PWD"
echo "Writing to ${readName}.sam"
echo "-1 $f1 -2 $f2"
hisat2 -q -x $indexName -1 $f1 -2 $f2 > ${readName}.sam

