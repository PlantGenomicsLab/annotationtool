module load trinity/2.8.5
module load bowtie2/2.3.4.3
module load samtools/1.10

PE_1="$PWD/$1"
PE_2="$PWD/$2"
out="$3"

cd $out

outname=trinity

script="Trinity --seqType fq --left $PE_1 --right $PE_2 --min_contig_length 300 --output $outname --full_cleanup --max_memory 150G --CPU 10"

eval "$script"
