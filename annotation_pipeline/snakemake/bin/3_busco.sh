#module load busco/4.1.2
module load busco/5.0.0

augPath=/labs/Wegrzyn/annotationtool/software/Augustus_3.4.0
export PATH=$augPath/bin:$augPath/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$augPath/config

# Script inputs, paths are given relative to Snakemake file
input="$PWD/$1"
busco_db="$2"

# Running directory
dir="$3"
cd $dir

# Check input filetype (genome/proteins/etc)
if [[ $input == *.fasta || $input == *.fasta-filtered ]];then
  script="busco -i $input -l $busco_db -o busco_o -m geno -c 10 -f"
  echo $script
  eval "$script"
elif [[ $input == *.aa ]];then
  script="busco -i $input -l $busco_db -o busco_o -m proteins -c 10 -f"
  echo $script
  eval "$script"
else
  echo "Input filetype not recognized (does not end in .fasta or .aa)"
fi
