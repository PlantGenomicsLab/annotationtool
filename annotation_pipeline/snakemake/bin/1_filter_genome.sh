#!/bin/bash
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=30G
#SBATCH --cpus-per-task=1

module load biopython/1.70
module load numpy/1.6.2

speciesName="$1"
genome=genome/${1}_sm.fasta
out=genome/${1}_sm_filtered.fasta

script="bin/./filter_len.py --fasta $genome --length 500 --out $out"

eval "$script"

if [[ "$?" != 0 ]]; then
  echo "Current directory = $PWD"
  echo "Running $script failed"
  exit 1
fi

