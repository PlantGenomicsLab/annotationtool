#!/bin/bash
#SBATCH --job-name=splitScaffolds
#SBATCH -o %x%j.out
#SBATCH -e %x%j.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=30G

# Splits a genome file into its scaffolds
# Usually only useful for a chromosomal level assembly

module load samtools/1.9

# inputs
genome="$PWD/$1"
outDir="$2"

headerFile="$PWD/genome/chromosomeHeaders.txt"
if [ ! -s $headerFile ]; then
  grep ">" $genome | sed 's/>//' > $headerFile
fi

# Index
echo "Indexing genome before splitting"
samtools faidx $genome

cd $outDir

while read -r line; do
  echo "Splitting Scaffold $line"
  mkdir -p $line
  samtools faidx $genome $line > $line/${line}.fasta
done < $headerFile

# Sanity check that this worked
while read -r line; do
  if [ ! -s $line/${line}.fasta ];then
    echo "We are missing scaffold file $line after our split, check splitScaffolds.sh for debugging"
    exit 1
  fi
done < $headerFile
