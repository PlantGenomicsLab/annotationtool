inputDir="$PWD/$1"
rawReadName="$2"
outDir="$3"

module load sickle/1.33

f1=raw_"${rawReadName}_1.fastq"
f2=raw_"${rawReadName}_2.fastq"

echo "$f1 $f2"

cd $outDir
script="sickle pe -f $inputDir/$f1 -r $inputDir/$f2 -t sanger -o trimmed_"$f1" -p trimmed_"$f2" -s single_trimmed_"${rawReadName}".fastq -q 30 -l 50"
eval "$script"

mv trimmed_$f1 trimmed_${rawReadName}_1.fastq
mv trimmed_$f2 trimmed_${rawReadName}_2.fastq

# Only one read without reverse pair not supported by pipeline
#if [ -f $inputDir/$f2 ]; then
#else
#  script="sickle se -f $inputDir/$f1 -t sanger -o trimmed_"$f1" -q 30 -l 50"
#  eval "$script"
#fi
