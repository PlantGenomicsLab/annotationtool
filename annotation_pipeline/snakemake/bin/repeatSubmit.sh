#!/bin/bash

module load anaconda

speciesName="$1"
maskLib="$2"

mkdir -p log/repeatMasker
mkdir -p genome/pieces/masked

# Split the genome fasta into equal pieces
echo "Splitting Genome"
bin/./splitFasta.py --speciesName $speciesName --pieces 100 --pathOut genome/pieces
echo "Done!"
#
# -W: wait for array job to finish before exiting
sbatch -W bin/repeatMasker.sh $speciesName $maskLib &
wait

cat genome/pieces/*.masked > genome/${speciesName}_sm.fasta
