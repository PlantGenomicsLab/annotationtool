module load genomethreader/1.7.1
module load genometools/1.5.10

filteredPeptide="$PWD/$1"
softmaskedGenome="$PWD/$2"

# add stop codons to filtered peptide
script="gt seqtransform -addstopaminos $filteredPeptide"
eval $script

# Index genome + filtered peptide
script="gth -genomic $softmaskedGenome -protein $filteredPeptide -createindicesonly"
echo $script && eval $script
