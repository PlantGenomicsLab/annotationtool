module load genomethreader/1.7.1

filteredPeptide="$PWD/$1"
softmaskedGenome="$PWD/$2"
outDir="$3"

cd $outDir

gmapOut="gth.gff3"

#-gff3out -skipalignmentout -paralogs -gcmincoverage 80 -prseedlength 20 -prminmatchlen 20 -prhdist 2

script="gth -genomic $softmaskedGenome -protein $filteredPeptide \
  -gff3out \
  -startcodon \
  -gcmincoverage 80 \
  -finalstopcodon \
  -introncutout \
  -dpminexonlen 20 \
  -skipalignmentout \
  -force \
  -skipindexcheck \
  -gcmaxgapwidth 1000000 \
  -o $gmapOut"
echo $script && eval $script
