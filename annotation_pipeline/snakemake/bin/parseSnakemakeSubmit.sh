#!/bin/bash
fmt="%-25s%-12s\n"
while IFS= read -r line; do
  pat='jobName=([A-Za-z0-9_.]+),'
  if [[ $line =~ $pat ]]; then
    printf '%-40s' "jobName=${BASH_REMATCH[1]}"
  fi
  pat2='Submitted batch job ([0-9]+).'
  if [[ $line =~ $pat2 ]]; then
    printf '%-30s\n' "jobID=${BASH_REMATCH[1]}"
  fi
done
