file="$PWD/$1"
outDir="$PWD/genome"
fd=$(dirname $file)
fn=$(basename $file)

export DATADIR=$fd
export SEQFILE=$fn
export DATABASE="RepeatDatabase"


WORKDIR=/scratch/$USER/repeatmodeler
mkdir -p $WORKDIR
cp $DATADIR/$SEQFILE $WORKDIR
cd $WORKDIR

module load RepeatModeler/1.0.8
module load rmblastn/2.2.28
module unload perl/5.28.0
module load perl/5.24.0
export PERL5LIB=/UCHC/PublicShare/szaman/perl5/lib/perl5/


BuildDatabase -name $DATABASE -engine ncbi $SEQFILE
nice -n 10 RepeatModeler -engine ncbi -pa 22 -database $DATABASE
rsync -a ./consensi.fa.classified $DATADIR/$SEQFILE.consensi.fa.classified

# Copy repeat lib to genome folder
cd  "$(\ls -1dt ./*/ | head -n 1)"
cp consensi.fa.classified $outDir

