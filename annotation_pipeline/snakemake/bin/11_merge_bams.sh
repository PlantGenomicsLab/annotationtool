module load samtools/1.7

inputDir="$PWD/$1"
outDir="$2"

cd $outDir

for f in $inputDir/*.sam; do
  outName=$(basename $f)
  script="samtools view -b -@ 16 $f | samtools sort -o sorted_${outName%.sam}.bam -@ 16"
  eval "$script"
done

script=$( echo "samtools merge merged.bam sorted_*.bam" )
eval $script
