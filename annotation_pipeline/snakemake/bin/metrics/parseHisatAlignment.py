from __future__ import print_function
import re,sys

def eprint(*args, **kwargs): # For errors
    print(*args, file=sys.stderr, **kwargs)

# Match the speciesname + SRA name from log filename
#namePat=re.compile("\s+wildcards:.*name=([\w_\d]+),")
nameSraPat=re.compile("hisatAlign_(.*)_([A-Z]RR[0-9]+)")

# Match the overall alignment
alnPat=re.compile("([\d.]+)% overall alignment rate")

# Match the SRA name
#samPat=re.compile("Writing to (\w+\d+).sam")


def parseFile(fn,p): # match one pattern for whole file
  f = open(fn).read()
  m = re.findall(p,f)
  if(len(m)>0): # Log file can contain multiple runs, take the results of the last one
    return m[-1]
  else:
    eprint("ERR: (%s) re %s did not match for fn %s" % (sys.argv[0],str(p),fn))
    exit(1)

if(__name__=="__main__"):
  fns = sys.argv[1:]
  print("{:<40}{:<20}{:<20}".format("Species","SRA","Alignment"))
  for f in fns:
    base = f.split("/")[-1]
    m = nameSraPat.match(base)
    name,sam=("","")
    if(m):
      name,sam = m.groups()
    else:
      eprint("ERR: (%s) Could not match name+sra from log filename: %s" % (sys.argv[0],base))
      exit(1)
    aln = parseFile(f,alnPat)
    print(("{:<40}{:<20}{:<20}").format(name,sam,aln))
