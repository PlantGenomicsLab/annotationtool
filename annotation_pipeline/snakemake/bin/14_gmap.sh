module load gmap/2019-06-10

indexDir="$PWD/$1"
speciesName="$2"
centroidsFiltered="$PWD/$3"
outDir="$4"

cd $outDir

script="gmap -a 1 --cross-species -D $indexDir -d $speciesName -f gff3_gene $centroidsFiltered --fulllength --nthreads=8 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > gmap.gff3"
eval $script
