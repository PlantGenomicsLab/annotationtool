module load hisat2/2.2.0

# Script inputs, paths are given relative to Snakemake file
genome="$PWD/$1"
species_name="$2"

# Running directory
dir="$3"
cd $dir

script="hisat2-build -f $genome ${species_name}"

eval "$script"
