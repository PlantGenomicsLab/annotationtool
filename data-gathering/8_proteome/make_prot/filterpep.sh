#!/bin/bash
#SBATCH --job-name=filterpep
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o filterpep_%j.out
#SBATCH -e filterpep_%j.err

org="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/model/arabidopsis"
fname="GGJX01.1.fsa_nt.transdecoder.pep"
path="$org/evidence/transcriptome/frameselect/bestHit/"
nameList="filter300nameList.txt"
pathList="$org/evidence/transcriptome/filtered"
out="arabidopsis_filtered.pep"

awk 'sub(/^>/, "")' "$pathList"/centroids_arabidopsis_TSA-filtered > "$nameList"
sed -e 's/\s.*$//' "$path$fname" > tmp.pep

python createFasta.py --fasta tmp.pep --nameList "$nameList" --out "$out"
