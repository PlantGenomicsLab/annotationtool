#!/bin/bash
#SBATCH --mem=10G
#SBATCH --job-name=blat
#SBATCH -o blat-%j.output
#SBATCH -e blat-%j.error
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general

# Optional script to reduce the set of proteins that will be run through exonerate

module load Blat/36x2

org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/rose_old_blush_genome
genome=$org/genome/Rosa_chinensis_Old_Blush_homozygous_genome-v2.0.a1.softmasked.fna
pep=$org/evidence/transcriptome/filtered-pep/oldBlush_filtered.pep

species="oldBlush"

blat $genome $pep \
-q=prot -t=dnax \
-minIdentity=80 \
-minScore=500 \
-mask=lower -dots=10000 -out=blast8 \
${species}_hits.psl
