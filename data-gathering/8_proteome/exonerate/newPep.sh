#!/bin/bash
#SBATCH --job-name=newPep
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 6
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G
#SBATCH -o newPep_%j.out
#SBATCH -e newPep_%j.err

# Creates new protein file with transcripts that passed through Blat

module load seqtk/1.2

echo `hostname`

awk '{print $1}' oldBlush_hits.psl | sort -u > hits.txt

org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/rose_old_blush_genome
pep=$org/evidence/transcriptome/filtered-pep/oldBlush_filtered.pep

seqtk subseq $pep hits.txt > hits.pep
