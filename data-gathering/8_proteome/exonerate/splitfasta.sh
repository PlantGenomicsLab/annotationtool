#!/bin/bash
#SBATCH --job-name=splitfasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=5G
#SBATCH -o splitfasta_%j.out
#SBATCH -e splitfasta_%j.err

# Script to split the protein file into 100 pieces

module load anaconda/4.4.0

echo `hostname`

outdir="pieces"
mkdir pieces

script=/labs/Wegrzyn/annotationtool/data-gathering/2_mask/splitfasta.py

echo "Making pieces..."

python $script \
--path ./ \
--fasta hits.pep \
--pathOut $outdir/ \
--pieces 100

echo "Done!"
