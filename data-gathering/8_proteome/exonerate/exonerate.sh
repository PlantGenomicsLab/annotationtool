#!/bin/bash
#SBATCH --mem=50G
#SBATCH --job-name=exonerate
#SBATCH -o output_files/exonerate-%j.output
#SBATCH -e error_files/exonerate-%j.error
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --array=1-100%5

# Run exonerate protein aligner as a Slurm array script on all 100 pieces of the protein file
# Each job may require 50 GB of memory
# Each job takes 2-3 hours to complete

# Make sure to create directories "error_files" and "output_files" before running script (see header)

module load exonerate/2.4.0

org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/rose_old_blush_genome
genome=$org/genome/Rosa_chinensis_Old_Blush_homozygous_genome-v2.0.a1.softmasked.fna
mkdir gffpieces
out=gffpieces/oldBlush_${SLURM_ARRAY_TASK_ID}.exon.gff


pep=pieces/hits.pep${SLURM_ARRAY_TASK_ID}.fa

echo $SLURM_ARRAY_TASK_ID
echo $pep

exonerate --model protein2genome \
--query $pep \
--target $genome \
-n 1 --percent 95 --score 95 --minintron 50 --showalignment no \
--showtargetgff yes --geneseed 250 --forcegtag \
--hspfilter 100 --showvulgar yes --maxintron 1000000 > $out
