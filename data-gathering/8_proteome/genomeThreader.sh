#!/bin/bash
# Submission script for Xanadu
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --mem=10GB
#SBATCH --job-name=gth
#SBATCH -o gth-%j.o
#SBATCH -e gth-%j.e
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general

module load genomethreader/1.6.6
module load genometools/1.5.10

org="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/model/arabidopsis"
pep="/evidence/transcriptome/filtered-pep/arabidopsis_filtered.pep"
genome="/genome/Arabidopsis_filtered.fasta.masked"
out="arabidopsis.gth.gff3"

gt seqtransform -addstopaminos $org$pep

gth -genomic $org$genome -protein $org$pep -gff3out -startcodon -gcmincoverage 80 -finalstopcodon -introncutout -dpminexonlen 20 -skipalignmentout -o $out -force -gcmaxgapwidth 1000000
