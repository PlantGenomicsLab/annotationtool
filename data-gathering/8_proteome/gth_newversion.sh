#!/bin/bash
# Submission script for Xanadu
#SBATCH --mem=10GB
#SBATCH --job-name=gth
#SBATCH -o gth-%j.o
#SBATCH -e gth-%j.e
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general

module load genomethreader/1.7.1
module load genometools/1.6.1

org="/labs/Wegrzyn/annotationtool/testSpecies/non-model/Hymenolepis_microstoma"

pep="/evidence/align_protein/Hymenolepis_filtered.pep"

genome="/genome/Hymenolepsis_microstoma.PRJEB124.WBPS14.genome_sm.fa"

out="Hymenolepis.gth.gff3"

gt seqtransform -addstopaminos $org$pep

gth -genomic $org$genome -protein $org$pep -gff3out -startcodon -gcmincoverage 80 -finalstopcodon -introncutout -dpminexonlen 20 -skipalignmentout -o $out -force -gcmaxgapwidth 1000000