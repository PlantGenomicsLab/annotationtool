#!/bin/bash
#SBATCH --job-name=braker
#SBATCH -o braker-%j.output
#SBATCH -e braker-%j.error
#SBATCH --mail-user=danmonyak@gmail.com
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G

echo `hostname`

org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/wheel_tree
homedir=$org/analysis/annotation/braker
cd $homedir
mkdir tmp


module load python/3.6.3
module load biopython/1.70
module load perl/5.28.1
module load bamtools/2.5.1
module load blast/2.10.0
module load genomethreader/1.7.1

## Copy software to home directory
#cp -r /labs/Wegrzyn/annotationtool/software/BRAKER_2.1.5 ~/
#cp -r /labs/Wegrzyn/annotationtool/software/Augustus_3.3.3 ~/Augustus
#cp -r /labs/Wegrzyn/annotationtool/software/cdbfasta ~/

## Download gm_key and GeneMark-ET software from http://exon.gatech.edu/GeneMark/license_download.cgi
## Save gm_key as $HOME/local_gm_key
## Local GeneMark-Et software should be $HOME/gmes_linux_64
## The license may expire every few months

export PATH=$HOME/BRAKER_2.1.5:$HOME/BRAKER_2.1.5/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/Augustus/config
export AUGUSTUS_BIN_PATH=$HOME/Augustus/bin
export TMPDIR=$homedir/tmp
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.5.1/bin
export BLAST_PATH=/isg/shared/apps/blast/ncbi-blast-2.10.0+/bin
export CDBTOOLS_PATH=$HOME/cdbfasta
export SAMTOOLS_PATH=/isg/shared/apps/samtools/1.9/bin
export GENEMARK_PATH=$HOME/gmes_linux_64

species="wheelTree"
genome=$org/genome/Trochodendron_aralioides_chromosomes.fa-filtered.masked.fa
bam=$org/analysis/alignments/hisat2/samtools/wheelTree_merged.bam

if [[ -d $AUGUSTUS_CONFIG_PATH/species/$species ]]; then rm -r $AUGUSTUS_CONFIG_PATH/species/$species; fi

cp ~/local_gm_key ~/.gm_key
cp ~/local_gm_key $homedir/.gm_key

braker.pl --cores 10 --genome="$genome" --species="$species" --bam="$bam" --softmasking 1 --gff3

