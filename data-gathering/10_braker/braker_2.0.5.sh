#!/bin/bash
# Submission script for Xanadu 
#SBATCH --job-name=brakerAra
#SBATCH -o brakerAra-%j.output
#SBATCH -e brakerAra-%j.error
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G

module load BRAKER/2.0.5
module load bamtools/2.4.1
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/config
export TMPDIR=/home/CAM/$USER/tmp/
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.4.1/bin/
export GENEMARK_PATH=/UCHC/LABS/Wegrzyn/local_software/gm_et_linux_64/gmes_petap/
module rm  perl/5.28.0-clean
module load perl/5.24.0
export PERL5LIB=/UCHC/LABS/Wegrzyn/perl5/lib/perl5/
export PERLINC=/UCHC/LABS/Wegrzyn/perl5/lib/perl5/

org="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/model/arabidopsis"
bam="$org/analysis/alignments/hisat2/samtools/arabidopsis_merged.bam"
genome="$org/genome/Arabidopsis_filtered.fasta.masked.fa"
species="arabidopsisAT"

braker.pl --cores 10 --genome="$genome" --species="$species" --bam "$bam" --GENEMARK_PATH=/UCHC/LABS/Wegrzyn/local_software/gm_et_linux_64/gmes_petap/ --softmasking 1 --gff3
