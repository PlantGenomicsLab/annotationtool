#!/bin/bash
# Submission script for Xanadu 
#SBATCH --job-name=brakerHm
#SBATCH -o brakerHm-%j.output
#SBATCH -e brakerHm-%j.error
#SBATCH --mail-user=vidya.vuruputoor@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G

module load python/3.6.3
module load biopython/1.70
#module rm  perl/5.28.1
module load perl/5.28.1
#export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/
#export PERLINC=/labs/Wegrzyn/perl5/lib/perl5/
module load bamtools/2.5.1
module load blast/2.10.0
export PATH=/home/FCAM/$USER/BRAKER:/home/FCAM/$USER/BRAKER/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/Augustus/config/
export AUGUSTUS_BIN_PATH=$HOME/Augustus/bin
export TMPDIR=/home/FCAM/$USER/tmp/
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.5.1/bin/
export BLAST_PATH=/isg/shared/apps/blast/ncbi-blast-2.10.0+/bin/
export CDBTOOLS_PATH=/home/FCAM/$USER/cdbfasta/
export SAMTOOLS_PATH=/isg/shared/apps/samtools/1.9/bin/
export GENEMARK_PATH=/home/FCAM/vvuruputoor/gmes_linux_64

cp ~/my_gm_key ~/.gm_key

org=/labs/Wegrzyn/annotationtool/testSpecies/non-model/Hymenolepis_microstoma
bam=$org/evidence/align_sra/Hymenolepis_merged.bam
genome=$org/genome/Hymenolepsis_microstoma.PRJEB124.WBPS14.genome_sm.fa-filtered
species=Hymneolepis_microstoma

braker.pl --cores 10 --genome="$genome" --species="$species" --bam="$bam" --softmasking 1 --gff3 --AUGUSTUS_CONFIG_PATH=$HOME/Augustus/config