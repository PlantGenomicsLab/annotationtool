#!/bin/bash
#SBATCH --job-name=gmapHm
#SBATCH -n 8
#SBATCH -N 1
#SBATCH -o gmapHm_%j.o
#SBATCH -e gmapHm_%j.e
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G

module load gmap/2019-06-10

idx=/labs/Wegrzyn/annotationtool/testSpecies/non-model/Hymenolepis_microstoma/genome/index/gmap/

prefix="Hymenolepis_gmap"

fasta=/labs/Wegrzyn/annotationtool/testSpecies/non-model/Hymenolepis_microstoma/align_TSA/centroids_Hm.Trinity.fasta-filtered

# larger genomes will require the gmapl command instead of just 'gmap'

#gmapl -a 1 --cross-species -D "$idx" -d "$prefix" -f gff3_gene "$fasta" --fulllength --nthreads=16 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > "$prefix"_gmap.gff3 2> "$prefix"_gmap.error

gmap -a 1 --cross-species -D $idx -d $prefix -f gff3_gene "$fasta" --fulllength --nthreads=8 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > $prefix.gff3 2> $prefix.error

#gmap -a 1 --cross-species -D "$idx" -d "$prefix" -f gff3_gene "$fasta" --fulllength --nthreads=8 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > "$prefix".gff3 2> "$prefix".error