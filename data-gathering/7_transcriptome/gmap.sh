#!/bin/bash
#SBATCH --job-name=gmapAra
#SBATCH -n 16
#SBATCH -N 1
#SBATCH -o gmapAra_%j.o
#SBATCH -e gmapAra_%j.e
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G

module load gmap/2017-03-17

idx="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/model/arabidopsis/genome/index/gmap"
prefix="arabidopsis"
fasta="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/model/arabidopsis/evidence/transcriptome/filtered/centroids_arabidopsis_TSA-filtered"

# larger genomes will require the gmapl command instead of just 'gmap'
 
gmap -a 1 --cross-species -D "$idx" -d "$prefix" -f gff3_gene "$fasta" --fulllength --nthreads=16 --min-trimmed-coverage=0.95 --min-identity=0.95 -n1 > "$prefix"_gmap.gff3 2> "$prefix"_gmap.error



