#!/bin/bash
#SBATCH --job-name=filter300
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G
#SBATCH -o filter300_%j.out
#SBATCH -e filter300_%j.err

module load biopython/1.70
module load numpy/1.6.2

filename="centroids_arabidopsis_TSA"
basedir="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/model/arabidopsis/evidence/transcriptome"
filedir=$basedir/usearch

./filterLen.py --fasta $filename --path $filedir --length 300 --out $filename-filtered --pathOut $basedir/filtered/ 

