#!/bin/bash                                                                                                        
# Submission script for Xanadu                                                                                     
#SBATCH --job-name=protea                                                                                          
#SBATCH -o trinity-%j.output
#SBATCH -e trinity-%j.error                                                                                        
#SBATCH --mail-user=sumaira.zaman@uconn.edu                                                                        
#SBATCH --mail-type=ALL                                                                                            
#SBATCH --nodes=1                                                                                                  
#SBATCH --ntasks=1                                                                                                 
#SBATCH --cpus-per-task=16                                                                                         
#SBATCH --partition=general
#SBATCH --mem=150G 

module load trinity/2.6.6

#seqType --> type of sequence (fastq)
#left --> forward read (1), right --> reverse read (2)

PE_1="mus_all_R1.fastq"
PE_2="mus_all_R2.fastq"
out="trinity_o"

Trinity --seqType fq --left $PE_1 --right $PE_2 --min_contig_length 300 --output $out --full_cleanup --max_memory 150G --CPU 16
