#!/bin/bash
# Submission script for Xanadu 
###SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=transdecoder
#SBATCH -o transdecoder-%j.output
#SBATCH -e transdecoder-%j.error
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=30G


TSA_dir="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/model/saccharomyces/evidence/transcriptome"
filename=saccharomyces_TSA.fsa_nt #unclustered, either freshly assembled from Trinity or found online

module load hmmer/3.1b2 
module load perl/5.24.0
module load TransDecoder/5.3.0
###Training with longest ORFs
TransDecoder.LongOrfs -t $TSA_dir/$filename #takes 2 mins

###generic prediction only reporting best hit
mkdir bestHit
echo "generic prediction w/ best"
SECONDS=0
TransDecoder.Predict -t $TSA_dir/$filename --single_best_only
echo "time taken for generic prediction w/ best orf"
echo $SECONDS
mv $filename.transdecoder.* bestHit


