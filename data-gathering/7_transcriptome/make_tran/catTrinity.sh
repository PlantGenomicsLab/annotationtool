#!/bin/bash
#SBATCH --job-name=catTrinity
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH -o catTrinity_%j.out
#SBATCH -e catTrinity_%j.err

prefixdir="trinity_prefix"

fname="liriodendron"

cat $prefixdir/*.fasta > ${fname}_merged.Trinity.fasta
