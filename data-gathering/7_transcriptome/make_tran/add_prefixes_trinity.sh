#!/bin/bash
#SBATCH --job-name=addPrefixes
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=5G
#SBATCH -o addPrefixes_%j.out
#SBATCH -e addPrefixes_%j.err

echo `hostname`

ls *.fasta > assemblies.txt

prefixdir="trinity_prefix"
mkdir $prefixdir

while read assembly
do
        lib=${assembly#trinity_}
        lib=${lib%.Trinity.fasta}

        sed "s/>/>${lib}_/g" $assembly > $prefixdir/prefix.${lib}.Trinity.fasta
done < assemblies.txt

rm assemblies.txt


rm $trinitydir/libraries.txt
