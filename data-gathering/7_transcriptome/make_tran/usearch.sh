#!/bin/bash
#SBATCH --job-name=usearchSac
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G
#SBATCH -o usearchSac_%j.out
#SBATCH -e usearchSac_%j.err

module load usearch/9.0.2132

org="saccharomyces_TSA"
concat_file="$org.fsa_nt.transdecoder.cds"

usearch --cluster_fast "../frameselect/bestHit/$concat_file" --centroids "centroids_$org" --uc "centroids_$org.uc" --id 0.98
