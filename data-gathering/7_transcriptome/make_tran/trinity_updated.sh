#!/bin/bash
#SBATCH --job-name=trinity
#SBATCH -o output_files/trinity-%j.output
#SBATCH -e error_files/trinity-%j.error
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=150G
#SBATCH --array=0-9

# Change array number, should be "0-(#libraries - 1)"
# Create directories "output_files" and "error_files" before running script

org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/rose_old_blush_genome

# Get library array from good_SRAs.txt, created earlier by alignRates.sh
libraries=($(cat $org/analysis/alignments/hisat2/good_SRAs.txt))
trimmeddir=$org/evidence/short_read/trimmed_reads

module load trinity/2.8.5
module load bowtie2/2.3.4.3
module load samtools/1.10

#seqType --> type of sequence (fastq)
#left --> forward read (1), right --> reverse read (2)

lib=${libraries[$SLURM_ARRAY_TASK_ID]}

PE_1=$trimmeddir/trimmed_${lib}_1.fastq
PE_2=$trimmeddir/trimmed_${lib}_2.fastq

out=trinity_${lib}

Trinity --seqType fq \
--left $PE_1 \
--right $PE_2 \
--min_contig_length 300 \
--output $out \
--full_cleanup \
--max_memory 150G \
--CPU 10
