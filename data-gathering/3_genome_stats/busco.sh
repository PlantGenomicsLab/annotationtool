#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=5G
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/4.1.2

export PATH=$HOME/Augustus/bin:$HOME/Augustus/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/Augustus/config

# Input FASTA Sequence
SEQ=../Arabidopsis_filtered.fasta.masked

mkdir tmp

# Check lineages at https://busco.ezlab.org/list_of_lineages.html
lineage="embryophyta_odb10"

# Mode should be "geno", "tran", or "prot"
mode="geno"

outdir="busco_o"

busco -i $SEQ -l $lineage -o $outdir -m $mode

