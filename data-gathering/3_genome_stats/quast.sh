#!/bin/bash
#SBATCH --job-name=quast
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 9
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=5G
#SBATCH -o quast_%j.out
#SBATCH -e quast_%j.err

module load quast/5.0.2
module load python/3.6.3

# Input FASTA Sequence
SEQ=../Arabidopsis_filtered.fasta.masked

script=/isg/shared/apps/quast/5.0.2/quast.py
outdir="quast_o"

# Number of threads in the header (-c or --cpus-per-task)
threads=9

python $script $SEQ -o $outdir -t $threads
