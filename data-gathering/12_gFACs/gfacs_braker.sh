#!/bin/bash
#SBATCH --job-name=gfacs-braker
#SBATCH -n 1
#SBATCH -c 6
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=120G
#SBATCH -o gfacs-braker-%j.o
#SBATCH -e gfacs-braker-%j.e

module load perl/5.24.0

#Script assumes that gff3 files for all 3 braker runs have directory structure as follows:
#$org/annotation/braker/braker/specname1/gfffilename.gff3
#$org/annotation/braker2/braker/specname2/gfffilename.gff3
#$org/annotation/braker3/braker/specname3/gfffilename.gff3

###Variables to change
org="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/model/arabidopsis"
specname1="arabidopsisAT"
specname2="AraRelatedSpecProtAT"
specname="AraRelatedSpecProtAT22"
genome="$org/genome/Arabidopsis_filtered.fasta.masked.fa" #Change to match the naming of your genome file
annotation="$org/analysis/annotation"
gfffilename="augustus.hints.gff3"
###

out1="unfiltered_run1_gfacs_o"
out2="unfiltered_run2_gfacs_o"
out3="unfiltered_run3_gfacs_o"
out4="filtered_run1_gfacs_o"
out5="filtered_run2_gfacs_o"
out6="filtered_run3_gfacs_o"

run1="braker/braker/$specname1"
run2="braker2/braker/$specname2"
run3="braker3/braker/$specname3"

script="/UCHC/LABS/Wegrzyn/annotationtool/process/12_gFACs/gFACs.pl"

options="--splice-rescue \
--statistics \
--statistics-at-every-step \
--splice-table \
--create-gtf"

options_filtered="--splice-rescue \
--statistics \
--statistics-at-every-step \
--splice-table \
--get-fasta-without-introns \
--get-fasta-with-introns \
--min-intron-size 10 \
--min-exon-size 10 \
--mins-CDS-size 300 \
--rem-start-introns \
--rem-end-introns \
--get-protein-fasta \
--create-gtf"

mkdir "$out1" "$out2" "$out3" "$out4" "$out5" "$out6"

# braker run 1 unfiltered
perl "$script" \
-f braker_2.05_gff3 \
"$options" \
--fasta "$genome" \
-O "$out1" \
"$annotation/$run1/$gfffilename" &&

# braker run 1 filtered
perl "$script" \
-f braker_2.05_gff3 \
"$options_filtered" \
--fasta "$genome" \
-O "$out4" \
"$annotation/$run1/$gfffilename" &&

# braker run 2 unfiltered
perl "$script" \
-f braker_2.05_gff3 \
"$options" \
--fasta "$genome" \
-O "$out2" \
"$annotation/$run2/$gfffilename" &&

# braker run 2 filtered
perl "$script" \
-f braker_2.05_gff3 \
"$options_filtered" \
--fasta "$genome" \
-O "$out5" \
"$annotation/$run2/$gfffilename" &&

# braker run 3 unfiltered
perl "$script" \
-f braker_2.05_gff3 \
"$options" \
--fasta "$genome" \
-O "$out3" \
"$annotation/$run3/$gfffilename" &&

# braker run 3 filtered
perl "$script" \
-f braker_2.05_gff3 \
"$options_filtered" \
--fasta "$genome" \
-O "$out6" \
"$annotation/$run3/$gfffilename"

