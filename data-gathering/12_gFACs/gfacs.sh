#!/bin/bash
#SBATCH --job-name=gfacs-m
#SBATCH -n 7
#SBATCH -c 7
#SBATCH -N 7
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=5G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o gfacs-m-%j.o
#SBATCH -e gfacs-m-%j.e

module load perl/5.24.0

org="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/model/arabidopsis"

genome="$org/genome/Arabidopsis_filtered.fasta.masked.fa"

out_gmap="gmap_gfacs_o"
out_gth="gth_gfacs_o"

alignment="$org/analysis/alignments"

script="/UCHC/LABS/Wegrzyn/annotationtool/process/10_gFACs/gFACs.pl"

options="--splice-rescue \
--statistics \
--statistics-at-every-step \
--splice-table \
--min-exon-size 20 \
--min-intron-size 20 \
--get-fasta-without-introns \
--get-fasta-with-introns \
--unique-genes-only \
--rem-start-introns \
--rem-end-introns \
--create-gtf"

mkdir "$out_gmap" "$out_gth"

perl "$script" \
-f gmap_2017_03_17_gff3 \
"$options" \
--fasta "$genome" \
-O "$out_gmap" \
"$alignment/gmap/arabidopsis_gmap.gff3"

perl "$script" \
-f genomethreader_1.6.6_gff3 \
"$options" \
--fasta "$genome" \
-O "$out_gth" \
"$alignment/genomeThreader/arabidopsis.gth.gff3"


