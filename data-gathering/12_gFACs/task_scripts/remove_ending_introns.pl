#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
############################################################################################################################################################
# REMOVING GENES THAT END WITH AN INTRON
#	About this script:
#		Sometimes a gene appears to end with an intron and that is not a "thing". This is almost ALWAYS because on the start or end of a
#	scaffold or known sequence space. The evidence may be strong for a gene, but it is incomplete. 
############################################################################################################################################################
# Arguments:
## ARGV[0] == gene table
## ARGV[1] == location
## ARGV[2] == prefix
############################################################################################################################################################
$complete = 0;
$incomplete = 0;
############################################################################################################################################################
open (GENE_TABLE, $ARGV[0]) or die "Cannot open the gene table!!!";
open (OUTPUT, $ARGV[1]);												# Gene table information
	@infile = <GENE_TABLE>;
	$infile = join "", @infile;
	@infile = split "###", $infile;
close GENE_TABLE;
############################################################################################################################################################
$comp = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "complete_genes.txt";
	open (COMPLETE, ">$comp") or die "Cannot create complete_genes.txt at $ARGV[1]!!!";				# Create output
print COMPLETE "###", "\n";
############################################################################################################################################################
	foreach $gene (@infile){
		$gene =~ s/^\s+\n//g;
		$gene =~ s/\s+$//;
		@split_lines  = split "\n", $gene;
		foreach $line (@split_lines){
			@split_tab = split "\t", $line;
			if ($split_tab[0] =~ /gene/){
				$start = "$split_tab[2]";
				$end = "$split_tab[3]";
			}
			if ($split_tab[0] =~ /exon/){
				if ($split_tab[4] =~ /\+/){
					if ($split_tab[3] == $end){
						$count++;
					}
				}
				if ($split_tab[4] =~ /\-/){
                                        if ($split_tab[2] == $start){
                                                $count++;
                                        }
                                }
			}
		}
		if ($count == 1){
			if ($gene =~ /exon/){
				print COMPLETE $gene, "\n", "###";
				$complete++;
			}
		}
		else{
			if ($gene =~ /exon/){
				#print INCOMPLETE $gene, "\n", "###";					# Those that do not pass
				$incomplete++;
			}
		}
		$count = 0;
		$start = 0;
		$end = 0;
}
############################################################################################################################################################
print "Number of genes retained:\t", $complete, "\n";							# Lessons
print "Number of genes lost:\t", $incomplete, "\n";
############################################################################################################################################################
############################################################################################################################################################
# The ballpoint pen was invented in 1938
