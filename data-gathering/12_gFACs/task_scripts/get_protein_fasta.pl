#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
##################################################################################################################################################################
# THE GREAT FAA
#	About this script:
#		We are essentially doing the same thing as creating the nucleotide fasta, void of intron sequences, but we are going to translate it! Steps have
#	been taken before to ensure things are in frame and that exons are in the correct order. Translations are far more unforgiving as errors will be 
#	more notable if you look at the translation. If you specify the need for a start and stop codon, all genes should start with M and end with *!
##################################################################################################################################################################

# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> prefix

use Bio::Index::Fasta;											# Using our good friend bioperl

$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "genes_without_introns.fasta.faa";			# Name the output
open (FASTA_1, ">$output") or die "Cannot create the output script!!!";					# Create the output

$inx = Bio::Index::Fasta->new(-filename => $ARGV[0]);							# Get the index

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";					# Gene table and storage of information
open (GENE_TABLE, "$gene_table") or die "Cannot open $gene_table";
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;
##################################################################################################################################################################
%aacode = (
  TTT => "F", TTC => "F", TTA => "L", TTG => "L",
  TCT => "S", TCC => "S", TCA => "S", TCG => "S",
  TAT => "Y", TAC => "Y", TAA => "\*", TAG => "\*",
  TGT => "C", TGC => "C", TGA => "\*", TGG => "W",
  CTT => "L", CTC => "L", CTA => "L", CTG => "L",
  CCT => "P", CCC => "P", CCA => "P", CCG => "P",
  CAT => "H", CAC => "H", CAA => "Q", CAG => "Q",
  CGT => "R", CGC => "R", CGA => "R", CGG => "R",
  ATT => "I", ATC => "I", ATA => "I", ATG => "M",
  ACT => "T", ACC => "T", ACA => "T", ACG => "T",
  AAT => "N", AAC => "N", AAA => "K", AAG => "K",
  AGT => "S", AGC => "S", AGA => "R", AGG => "R",
  GTT => "V", GTC => "V", GTA => "V", GTG => "V",
  GCT => "A", GCC => "A", GCA => "A", GCG => "A",
  GAT => "D", GAC => "D", GAA => "E", GAG => "E",
  GGT => "G", GGC => "G", GGA => "G", GGG => "G",
);
##################################################################################################################################################################
GENE: foreach $gene (@gene_table){									# Parsing genes
        $gene =~ s/\s+$//g;
        @split_lines = split "\n", $gene;
        foreach $line (@split_lines){
                $line =~ s/\s+$//;
                @split_tab = split "\t", $line;
                        if ($split_tab[0] =~ /gene/){							# Gene line provides the header
                                print FASTA_1 "\>", $split_tab[5], "\n";
				$seq = $inx->fetch("$split_tab[6]"); 
                  	}
			if ($split_tab[0] =~ /exon/){							# Exon sequences are all we care about
                                $start = $split_tab[2];
                                $end = $split_tab[3];

                               										# Get the sequence
                                $string_1 = $seq->subseq("$start", "$end");				
				push @sequence, $string_1;						# Store the sequence
			}
	}                         
      		$join_string = join "", @sequence;							# Join together all the exon sequence
		@string = split "", $join_string;							# Then break it up to nt by nt

	if ($split_tab[4] =~ /\+/){
  	  for ($a = 0; $a < scalar @string; $a = $a +3){							# Parsing all the nucleotides of the CDS numerically
		$codon = $string[$a] . $string[$a+1] . $string[$a+2];					# Make a codon
		$codon =~ tr/atgc/ATGC/;
                push @aa_sequence, $aacode{$codon}; 	                                                # Positive strand doesn't need anything fancy
	  }
	}
	if ($split_tab[4] =~ /\-/){
		for ($a = scalar @string - 1; $a > 0; $a = $a - 3){					# If negative, we are switching and inverting the sequences
			$codon = $string[$a] . $string[$a-1] . $string[$a - 2];
                       	$codon =~ tr/atgc/ATGC/;
			$codon =~ tr/ATGC/TACG/;
			push @aa_sequence, $aacode{$codon};						# Then storing it in reverse for a forward M-->* protein
		}
	}
	print FASTA_1 @aa_sequence;									# Print the translation
        undef (@sequence);
	undef (@aa_sequence);
        print FASTA_1 "\n";
}
close FASTA_1;
##################################################################################################################################################################
##################################################################################################################################################################
# 100lbs at the north pole would weight 99.65lbs at the equator. 

