#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
##################################################################################################################################################################
# IT IS TIME TO FIND START AND STOP CODONS 
#	About this script:
#		This script is designed to find the start and stop codons of a gene and add that information onto the gene table. This is generally a
#	first step in gtf creation since that information is required for the final file. This is nothing too fancy!
##################################################################################################################################################################

# Arguments:
# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> prefix

use Bio::Index::Fasta;												# We're using bioperl!

$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "start_and_stop_gene_table.txt";				# Name the output
open (OUT_TABLE, ">$output") or die "Cannot create the output script!!!";					# Create the output
	print OUT_TABLE "###", "\n";

$inx = Bio::Index::Fasta->new(-filename => $ARGV[0]);								# Get the index

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";						# Get the gene table
open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";			# Open the gene table
	@gene_table = <GENE_TABLE>;										# Parse and store
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;

##################################################################################################################################################################
$start_end = 0;
GENE: foreach $gene (@gene_table){										# Parse the gene table
	#$gene =~ s/^\s+\n//g;
        @split_lines = split "\n", $gene;
        foreach $line (@split_lines){
		$line =~ s/\s+$//;										# Print gene, exon, intron line
                print OUT_TABLE $line, "\n";
                @split_tab = split "\t", $line;
                        if ($split_tab[0] =~ /gene/){								# If we are dealing with a gene
				if ($split_tab[4] =~ /\+/){							# And if this is a positive strand
					$start = $split_tab[2];							# Where start codon starts
					$start_end = $split_tab[2] +2;						# Where is ends
	                                $seq = $inx->fetch("$split_tab[6]");					# Gets scaffold sequence

        	                        $string_1 = $seq->subseq($start, $start_end);				# seq = First three nt
	                                $string_1 =~ tr/ATGC/atgc/;						# Keeps case predictable

					$end = $split_tab[3] -2;						# Where the stop codon begins
					$end_end = $split_tab[3];						# Where stop codon ends

					$length = $seq->length();						# Length of scaffold
                                                
					if ($string_1 =~ /atg/){						# If we have a start codon
						undef ($string_1);						# Value reset (we'll refill it)
							print OUT_TABLE "start_codon", "\t",				# Start codon
								"3", "\t",						# Length (3)
								$start, "\t",						# Start position
								$start_end, "\t",					# End position
								$split_tab[4], "\t",					# Strand
								".", "\t",						# . for info (nothing)
								$split_tab[6], "\n";					# Scaffold
					}
					if ($length > $end_end){						# Step to stop bioperl tantrum
                                              $string_end_1 = $seq->subseq($end, $end_end);			# Get the sequence
                                              $string_end_1 =~ tr/ATGC/atgc/;
					      if ($string_end_1 =~ /taa/ or $string_end_1 =~ /tga/ or $string_end_1 =~ /tag/){		# Stop codon recognition
							undef ($string_end_1);					# Value reset
							print OUT_TABLE "stop_codon", "\t",				# Stop codon
                                                                "3", "\t",						# Length (3)	
                                                                $end, "\t",						# Start position
                                                                $end_end, "\t",						# End position
                                                                $split_tab[4], "\t",					# Strand (+)
                                                                ".", "\t",						# Info
                                                                $split_tab[6], "\n";					# Scaffold
						}
                                        }
				}
				if ($split_tab[4] =~ /\-/){							# Now onto the problem strand
					$start_end =  $split_tab[3];					 	# In reverse due to strandedness
					$start = $split_tab[3] -2;						# We look for atg at the end within the gene.
	
					$end = $split_tab[2];							# And the stop codon as the first 3
					$end_end = $split_tab[2] +2;

					$seq = $inx->fetch("$split_tab[6]");					# Getting the sacffold

					$string_1 = $seq->subseq($start, $start_end);				# Get the seq
                                        $string_1 =~ tr/ATGC/atgc/;						# Lower case
					$string_1 =~ tr/atgc/tacg/;						# Complement
					
					@split_neg = split "",  $string_1;					
					$string_2 = $split_neg[2] . $split_neg[1] . $split_neg[0];		# Reverse

					if ($string_2 =~ /atg/){						# If we have a start
                                                undef ($string_2);
						undef ($string_1);
						print OUT_TABLE "start_codon", "\t",
                                                                "3", "\t",
                                                                $start, "\t",
                                                                $start_end, "\t",
                                                                $split_tab[4], "\t",
                                                                ".", "\t",
                                                                $split_tab[6], "\n";
                                        }
					$length = $seq->length();
                                                if ($end >= 1){							# Same thing as above, but for stop codon reverse	
							$string_end = $seq->subseq($end, $end_end);		# and complementation
							$string_end =~ tr/ATGC/atgc/;
                                		        $string_end =~ tr/atgc/tacg/;
							@split_neg_end = split "",  $string_end;
						
	                                        	@split_neg_end = split "",  $string_end;
							$string_2_end = $split_neg_end[2] . $split_neg_end[1] . $split_neg_end[0];				

						if ($string_2_end =~ /taa/ or $string_2_end =~ /tga/ or $string_2_end =~ /tag/){
							undef($string_end_1);
							undef($string_end);

							print OUT_TABLE "stop_codon", "\t",
                                                                "3", "\t",
                                                                $end, "\t",
                                                                $end_end, "\t",
                                                                $split_tab[4], "\t",
                                                                ".", "\t",
                                                                $split_tab[6], "\n";
        	                                }
					}
				}
			}
		}
		print OUT_TABLE "###";
	}
close OUT_TABLE;
###################################################################################################################################################################
###################################################################################################################################################################
# Another script down. Did you know earth's escape velocity is 25,000 mph. That is 7 miles per second! 

