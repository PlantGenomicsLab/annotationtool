#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
###################################################################################################################################################################
# RESOLVING THOSE PESKY OVERLAPPING EXONS INTO TRANSCRIPTS
#	About this script:
#		Following overlapping_exons.pl, this script takes the overlapping models and asks if they are actually separate isoforms or direct overlap
#	produced by different information. This is figured out by the information column in the gene table and if the exon lines differ. Once they do, they
#	are treated as different genes. Resolved models are given an integer postfix and concatenated onto the gene table.

###################################################################################################################################################################
# Opening the gene table (overlap table)
$overlap = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "overlapping_gene_table.txt";
open (OVERLAP, $overlap) or die "Cannot open $overlap";
	@overlap = <OVERLAP>;
	$infile = join "", @overlap;
	@infile = split "###", $infile;
close OVERLAP;
###################################################################################################################################################################
$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "transcripts_pre.txt";							# Naming our output
open (OUTFILE, ">$output") or die "Cannot create $output";								# Creating our output
	print OUTFILE "###", "\n";
$number_of_genes = 0;													# Number reset
$splice = 0;
foreach $gene (@infile){												# Parse the overlap models
	$gene =~ s/^\s+\n//g;
	if ($gene =~ /gene/){												# Verifying the space
		$number_of_genes++;											# Count
		@split_lines = split "\n", $gene;
		LINE: for ($a = 0; $a < scalar @split_lines; $a++){							# Numerical parsing of gene lines
			@split_current = split "\t", $split_lines[$a];							# Current line
			@split_previous = split "\t", $split_lines[$a-1];						# One less = previous line
			@split_next = split "\t", $split_lines[$a+1];							# One more = next line

			if ($split_current[0] =~ /exon/){								# If we are currently on an exon line
				if ($split_next[5] ne $split_current[5]){						# And the next one has different info
					push @lines, $split_lines[$a];							# Store the line
					$count++;									# INTEGER MODEL #
					@split_tab = split "\t", $split_lines[1];					
					print OUTFILE $split_tab[0], "\t",						# Print the gene line
							$split_tab[1], "\t",
							$split_tab[2], "\t",
							$split_tab[3], "\t",
							$split_tab[4], "\t",
							$split_tab[5], 
							 "\.", $count, "\t",						
							$split_tab[6], "\n";

						foreach $line (@lines){
							print OUTFILE $line, "\n";					# Print the stored lines (all exons)
						}
					print OUTFILE "###", "\n";
					undef (@lines);									# Reset information.
					next LINE;									# Exit to next line in gene
				}
			}
			if ($split_next[5] =~ $split_current[5]){							# If the next exon is in the same theme
                                push @lines, $split_lines[$a];								# Store it
                        }
		}				
	$count = 0;													# Reset models number
	}
}
close OUTFILE;
###################################################################################################################################################################
#REVALUATING INTRONS!!! - Always important. 
		
open (TRANSCRIPTS, "$output") or die "Cannot open $output";								# We are opening the file we just closed!
	@TRANSCRIPTS = <TRANSCRIPTS>;											# And storing it
	$TRANSCRIPTS = join "", @TRANSCRIPTS;
	@transcripts = split "###", $TRANSCRIPTS;
close TRANSCRIPTS;

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "transcripts.txt";							# Naming the output
open (GENE_TABLE, ">$gene_table") or die "Cannot create $gene_table";							# Creating the output
print GENE_TABLE "###", "\n";

foreach $gene (@transcripts){												# Parsing
	if ($gene =~ /gene/){ $splice++;}										# Number of transcripts for results
        $gene =~ s/\s+$//g;
        @lines = split "\n", $gene;

        foreach $line (@lines){
                @split_line = split "\t", $line;

                if ($split_line[0] =~ /gene/){										# If line is a gene, print as is...
                        print GENE_TABLE $split_line[0],                        # gene
				"\t", $split_line[1],				# length
                                "\t", $split_line[2],                           # start
                                "\t", $split_line[3],                           # stop
                                "\t", $split_line[4],                           # strand
                                "\t", $split_line[5],                           # ID
				"\t", $split_line[6], 
                                "\n";
                }
                if ($split_line[0] =~ /exon/){										# Same with intron
                         print GENE_TABLE $split_line[0],                       # exon
                                "\t", $split_line[1],                           # length
                                "\t", $split_line[2],                           # start
                                "\t", $split_line[3],                           # stop
                                "\t", $split_line[4],                           # strand
                                "\t", $split_line[5],                           # ID
				"\t", $split_line[6],
                                "\n";

                        push @exons, $split_line[2];
                        push @exons, $split_line[3];
                }
        }
	 if (scalar @exons >= 4){											# If we have at least 4 coordinates
                @exons_sorted = sort {$a <=> $b} @exons;
                # 10    20      30      40      50      60								# Logic
                #[0]    [1]-----[2]     [3]-----[4]     [5]
                # Intron = [2] - [1] and [4] - [3]

                for ($a = 2; $a < scalar @exons_sorted; $a = $a +2){							# Positions starting at [2] then to [4]
                        $intron_length = $exons_sorted[$a] - $exons_sorted[$a-1] + 1;					# Assuming intron is [2] - [1] and [4] - [3]
			$start = $exons_sorted[$a - 1] +1;								# Value storage
			$stop = $exons_sorted[$a] -1;
			$length =  $stop - $start + 1;									# Intron length
                                print GENE_TABLE "intron",								# Then print that intron
                                        "\t", $length,
                                        "\t", $start,
                                        "\t", $stop,
                                        "\t", $split_line[4],
					"\t", ".",
					"\t", $split_line[6],
                                        "\n";
                }
        }
        if(scalar @exons < 1){												# Monoexonics
                if ($gene =~ /.+/){
                        @split_line = split "\t", $gene;
                        $gene_length = $split_line[3] - $split_line[2] + 1;
                        print GENE_TABLE "exon",                                # gene
                                "\t", $gene_length,                             # length
                                "\t", $split_line[2],                           # start
                                "\t", $split_line[3],                           # stop
                                "\t", $split_line[4],                           # strand
                                "\t", $split_line[5],                           # ID
				"\t", $split_line[6],
                                "\n";
                }
        }
print GENE_TABLE "###", "\n";
undef (@exons);
undef (@introns);
}
close GENE_TABLE;

system "rm $output";													# RM overlap file
###################################################################################################################################################################
# Results prints. X number of genes has become Y transcripts.
print "Results: ", $number_of_genes, " genes has become ", $splice, " transcripts.", "\n";

###################################################################################################################################################################
###################################################################################################################################################################
# Another script down. It's been fun. Did you know Benjamin Franklin wanted the US national bird to be a turkey?? 
