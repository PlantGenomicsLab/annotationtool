#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
######################################################################################################################################################
# WE HAVE TO HAVE TO HAVE A STOP CODON
#	About this script:
#		Have you ever dreamed of making sure your genes have a stop codon? Well this is the script for you. This script will look at the last
#	3 nt of the gene space for your classic stop codons. Now, be aware, that this script does not detect is stop codons are in FRAME. There is 
#	a second script that runs after this that will evaluate genes for divisibility by 3. 
######################################################################################################################################################
# Arguments:
# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> fasta index 
# ARGV[3] -->  prefix
######################################################################################################################################################
$start_end = 0;
use Bio::Index::Fasta;
######################################################################################################################################################
$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "start_and_stop_gene_table.txt";				# Name output
open (OUT_TABLE, ">$output") or die "Cannot create the output script!!!";					# Create output
print OUT_TABLE "###", "\n";

$inx = Bio::Index::Fasta->new(-filename => $ARGV[2]);

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "gene_table.txt";
open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";			# Gene table info
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;
######################################################################################################################################################
GENE: foreach $gene (@gene_table){
	$end_check = 0;
	if ($gene =~ /gene/){$number_of_genes++;}
	$gene =~ s/^\s+\n//g;
        @split_lines = split "\n", $gene;
        foreach $line (@split_lines){
		$line =~ s/\s+$//;										# Print gene, exon, intron line
                $line =~ s/\s+$//;
                @split_tab = split "\t", $line;
                        if ($split_tab[0] =~ /gene/){
				if ($split_tab[4] =~ /\+/){							
	                                $seq = $inx->fetch("$split_tab[6]");					# Gets scaffold sequence
					$end = $split_tab[3] -2;						# In a pos strand, stop is last 3 positions.		
					$end_end = $split_tab[3];
					$length = $seq->length();						# Gets the length of the scaffold
					if ($length > $end_end){						
                                              $string_end_1 = $seq->subseq($end, $end_end);
                                              $string_end_1 =~ tr/ATGC/atgc/;
					      if ($string_end_1 =~ /taa/ or $string_end_1 =~ /tga/ or $string_end_1 =~ /tag/){
							$end_check = 1;
							$start_end++;						# stop codon detected
							undef ($string_end_1);
						}
                                        }
				}
				if ($split_tab[4] =~ /\-/){
					$end = $split_tab[2];							# NEG strand
					$end_end = $split_tab[2] +2;
					$seq = $inx->fetch("$split_tab[6]");
					$length = $seq->length();
                                                if ($end >= 1){							# Rev strand stop is pre [2]	
							$string_end = $seq->subseq($end, $end_end);
							$string_end =~ tr/ATGC/atgc/;
                                		        $string_end =~ tr/atgc/tacg/;
							@split_neg_end = split "",  $string_end;
	                                        	@split_neg_end = split "",  $string_end;
							$string_2_end = $split_neg_end[2] . $split_neg_end[1] . $split_neg_end[0];				
						if ($string_2_end =~ /taa/ or $string_2_end =~ /tga/ or $string_2_end =~ /tag/){
	                                                $end_check = 1;
							$start_end++;
							undef($string_end_1);
							undef($string_end);
        	                                }
					}
				}
			}
		}
		if ($end_check ==1){										# Was there a stop codon?
			print OUT_TABLE $gene, "\n", "###";
			$pass_pass++;
		}
		$start = 0;
		$end_check = 0;
	}
close OUT_TABLE;
######################################################################################################################################################
$lost = $number_of_genes - $pass_pass;
print "Number of genes retained:", "\t", $pass_pass, "\n";
print "Number of genes lost:", "\t", $lost, "\n";
######################################################################################################################################################
######################################################################################################################################################
# In the 1980s, American airlines had a lifetime unlimited first class pass for $250,000 ($600,000 today). 

