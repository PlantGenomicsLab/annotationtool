#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
########################################################################################################################################################
# REMOVE GENES THAT START WITH INTRONIC SPACE
# 	 About this script:
#               Sometimes a gene appears to start with an intron and that is not a "thing". This is almost ALWAYS because on the start or end of a
#        scaffold or known sequence space. The evidence may be strong for a gene, but it is incomplete.
########################################################################################################################################################
$complete = 0;
$incomplete = 0;
########################################################################################################################################################
open (GENE_TABLE, $ARGV[0]) or die "Cannot open the gene table!!!";						# Gene table info
	@infile = <GENE_TABLE>;
	$infile = join "", @infile;
	@infile = split "###", $infile;
close GENE_TABLE;
########################################################################################################################################################
$comp = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "complete_genes.txt";
open (COMPLETE, ">$comp") or die "Cannot create complete_genes.txt at $ARGV[1]!!!";				# Create output
print COMPLETE "###", "\n";
########################################################################################################################################################
	foreach $gene (@infile){					
		$gene =~ s/^\s+\n//g;
		$gene =~ s/\s+$//;
		@split_lines  = split "\n", $gene;
		foreach $line (@split_lines){
			@split_tab = split "\t", $line;
			if ($split_tab[0] =~ /gene/){
				$start = "$split_tab[2]";
				$end = "$split_tab[3]";
			}
			if ($split_tab[0] =~ /exon/){							# Do we have an exon?
				if ($split_tab[4] =~ /\+/){
					if ($split_tab[2] == $start){					# Does start of exon match start of gene
						$count++;
					}
				}
				if ($split_tab[4] =~ /\-/){						# Inverse for negative strand
					if ($split_tab[3] == $end){
						$count++;
					}
				}
			}
		}
		if ($count == 1){									# If we have a match and only 1 starting exon
			if ($gene =~ /exon/){	
				print COMPLETE $gene, "\n", "###";					# Print
				$complete++;
			}
		}
		else{
			if ($gene =~ /exon/){
				$incomplete++;
			}
		}
		$count = 0;
		$start = 0;
		$end = 0;
}
print "Number of genes retained:\t", $complete, "\n";
print "Number of genes lost:\t", $incomplete, "\n";
########################################################################################################################################################
########################################################################################################################################################
# The frisbee was created by the Frisbie Pie Company in Bridgeport, CT. It was their pie tins and kids at Yale in the 1940s that made it popular. 
