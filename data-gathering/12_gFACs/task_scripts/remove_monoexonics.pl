#!usr/bin/perl
##############################################################################################################################################################
# WHO NEEDS MONOEXONIC GENES?
#	About this script:
#		Removes monoexonics. Creates output multiexonic_genes.txt that is then reverted to gene_table.txt. Very easy.
##############################################################################################################################################################
# Arguments:
# ARGV[1] == output
# ARGV[0] == gene table
# ARGV[2] == prefix
##############################################################################################################################################################
open (GENE_TABLE, $ARGV[0]) or die "Cannot open the gene table!!!";				# Gene table information
	@infile = <GENE_TABLE>;
        $infile = join "", @infile;
        @infile = split "###", $infile;
close GENE_TABLE;
##############################################################################################################################################################
$multis = $ARGV[1] . "\/" . "\/" .  $ARGV[2] . "multiexonic_genes.txt";
	open (OUTPUT, ">$multis") or die "Cannot create multiexonic_genes.txt at $ARGV[1]";	# Output creation
print OUTPUT "###", "\n";
##############################################################################################################################################################
	GENE: foreach $gene (@infile){
                $gene =~ s/^\s+\n//g;
			if ($gene =~ /gene/){ $number_of_genes++; }
                @split_lines  = split "\n", $gene;
		foreach $line (@split_lines){
                        @split_tab = split "\t", $line;
                        if ($split_tab[0] =~ /intron/){						# Do we have an intron?
				print OUTPUT $gene, "\n", "###";				# Cool. Save it. 
				$count++;
				next GENE;
			}
		}
	}
##############################################################################################################################################################
$lost = $number_of_genes - $count;								# Lessons
print "Number of genes retained:", "\t", $count, "\n";
print "Number of genes lost:", "\t", $lost, "\n";
close OUTPUT;
##############################################################################################################################################################
##############################################################################################################################################################
# The Band-Aid was invented in 1921 by Earle Dickson as a response to his wife who was accident-prone.
