#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
###################################################################################################################################################################
# DO WE HAVE A SURPLUS OF EXON SPACE?
#       About this script:
#		This script is part 1 of 2 of the task of separating out transcripts. This script is designed to go through gene models and find areas where
# 	exons are overlapping. When they do within a gene, it suggests there is conflicting evidence or perhaps an entire duplication of a model. For example,
#	if two RNAseq reads directly support the same gene, it will be denoted as the same exons and positions occuring twice with a t1 and t2 note on
#	the exon 9th column. Essentially, I am just separating out models that have any overlap. 
###################################################################################################################################################################
# VARIABLE AND NAME MANAGEMENT

$overlapping_gene = 0;														# Setting results values to zero.
$no_overlap = 0;
$overlap = 0;

###################################################################################################################################################################
open (GENE_TABLE, $ARGV[0]) or die "Cannot open the gene table!!!";								# Gene table open and stored.
	@infile = <GENE_TABLE>;
	$infile = join "", @infile;
	@GENE_TABLE = split "###", $infile;
close GENE_TABLE;
###################################################################################################################################################################
$output_nonov = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "non_overlapping_gene_table.txt";						# output names
$output_ov = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "overlapping_gene_table.txt";

open (NON_OVERLAPPING_GENE_TABLE, ">$output_nonov") or die "Cannot create non_overlapping_gene_table.txt at $ARGV[1]";		# output creation
open (OVERLAPPING, ">$output_ov") or die "Cannot create overlapping_gene_table.txt at $ARGV[1]";		

print OVERLAPPING "###", "\n";													# Leading separator
print NON_OVERLAPPING_GENE_TABLE "###", "\n";
###################################################################################################################################################################
foreach $gene (@GENE_TABLE){													# Parse that table
	if ($gene =~ /gene/){
		$gene =~ s/^\s+\n//g;
		$gene =~ s/\s+$//;
		@split_lines = split "\n", $gene;
			foreach $line (@split_lines){
				$line =~ s/\s+$//;
				@tab = split "\t", $line;
				if ($tab[0] =~ /exon/){										# If we are on an exon line
					for ($a = $tab[2]; $a <= $tab[3]; $a++){						# Get all values the exon is on
						$all_spaces++;									# Count em
						if ($array{$a} =~ /.+/){$overlap++;}						# If value alread exists, count.
						else{$array{$a} = "exon";}							# Otherwise, add it to array.
					}
				}
			}
			if ($overlap > 0){											# If overlap is observed
				print OVERLAPPING $gene, "\n", "###";								# Print it here
				$overlapping_gene++;										# And count
			}
			if ($overlap == 0){											# If no overlap is observed
				$no_overlap++;											# Count
				print NON_OVERLAPPING_GENE_TABLE $gene, "\n", "###";						# And print here
			}
		}
	$overlap = 0;														# Value reset
	$all_spaces = 0;
	undef(%array);							
}

print NON_OVERLAPPING_GENE_TABLE "\n###";
print OVERLAPPING "\n###";
###################################################################################################################################################################
close NON_OVERLAPPING_GENE_TABLE;
close OVERLAPPING;

###################################################################################################################################################################
# RESULTS PRINT
print "Number of genes with overlapping exons:\t", $overlapping_gene, "\n";
print "Number of genes without overlapping exons:\t", $no_overlap, "\n";

###################################################################################################################################################################
###################################################################################################################################################################
# THATS ALL FOLKS. Check back in with splice_variants.pl
#	Did you know a single honey bee only creates 1/12 a teaspoon of honey in their entire life?
