#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
################################################################################################################################################################
# WHEN YOU ONLY WANT GT-AG SPLICE SITES
#	About this script:
#		This script will filter gene models to those who only have canonical splice sites. This will only work on multiexonic genes as monoexonics
#	do not have the criteria to be filtered OUT. This is an "innocent until proven guilty" thing with splice sites. Of course, just because your
#	splice site is not gt-ag does NOT mean it is not right! Other splice sites can and do occur, but at a much lower frequency. 
################################################################################################################################################################

# ARGV[0] --> in fasta
# ARGV[1] --> output location

use Bio::Index::Fasta; 												# Using bioperl

$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "canonical_genes.txt";						# Name the output
open (FASTA_1, ">$output") or die "Cannot create the output script!!!";						# Create the output
	print FASTA_1 "###", "\n";

$inx = Bio::Index::Fasta->new(-filename => $ARGV[0]);								# Load the index

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";						# Name the gene table
open (GENE_TABLE, "$gene_table") or die "Cannot open $gene_table";						# Get the gene table
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;

$intron = 0;													# Setting results values
$saved = 0;
$number_of_genes = 0;

################################################################################################################################################################
GENE: foreach $gene (@gene_table){										# Parse the gene table
	$gene =~ s/^\s+\n//g;
	$intron = 0;
	$canonical = 0;
	if ($gene =~ /gene\t/){
		$number_of_genes++;										# Genes in
	}
	$gene =~ s/\s+$//g;
	@split_lines = split "\n", $gene;
	foreach $line (@split_lines){
		$line =~ s/\s+$//;
		@split_tab = split "\t", $line;
			if ($split_tab[0] =~ /intron/){								# If we are looking at an intron
				$intron++;									# Count
				$split_start_2 = $split_tab[2] +1;						# Splice sites are within intron space
				$split_start_1 = $split_tab[2];
				$split_end_1 =  $split_tab[3] -1;
				$split_end_2 = $split_tab[3];

				$seq = $inx->fetch("$split_tab[6]");						# Get the scaffold

				$string_1 = $seq->subseq("$split_start_1", "$split_start_2");			# Get the sequence donor (acc on -)
				$string_1 =~ tr/ATGC/atgc/;							# Lower case
		
				$string_2 = $seq->subseq("$split_end_1", "$split_end_2");			# Get the sequence acceptor (don on -0
				$string_2 =~ tr/ATGC/atgc/;

				$splice = "$string_1\_$string_2";						# ID for a splice

				if ($splice =~ /gt_ag/ or $splice =~ /ct_ac/){					# If canonical (+ or - strand recognition)
					$canonical++;								# Count
				}	
			}
		}	
	if ($intron == $canonical){										# If # introns = # canonical splice sites
		if ($gene =~ /./){										# And the gene has substance (formatting)
			print FASTA_1 $gene, "\n", "###", "\n";							# Print this guy
			$saved++;										# And count those that pass
		}
	}
	$canonical = 0;												# Value reset
	$intron = 0;
	}

close FASTA_1;
														# Results print
$lost = $number_of_genes - $saved;
print "Number of genes retained:\t", $saved, "\n";
print "Number of genes lost:\t", $lost, "\n";
################################################################################################################################################################
################################################################################################################################################################
# Canonical only done! Did you know US alcohol commercials legally can't show anyone drinking alcohol? 
