#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
####################################################################################################################################################################
# CDS SIZES DISTRIBUTIONS
#	About this script:
#		Designed to go stepwise through CDS sizes in groups predetermined by the data in the gene table. 
#
#		To look like this:
#
#		CDS_sizes		N
#		0-10			10
#		11-20			45
#		21-30			105
#		etc...
#
####################################################################################################################################################################

#ARGV[0] --> Location of gene table
#ARGV[1] --> step
#ARGV[2] --> pre

$gene_table = $ARGV[0] . "\/" . $ARGV[2] . "gene_table.txt";					# Name the gene table
open (GENE_TABLE, $gene_table) or die "Cannot open $gene_table !!!";				# Open the gene table
	@gene_table = <GENE_TABLE>;								# And process it
	$join = join "", @gene_table;
	@gene_table = split "###", $join;							# Then store it here
close GENE_TABLE;

$output = $ARGV[0] . "\/" . $ARGV[2] . "CDS_lengths_distributions.tsv";				# Name the output
open (OUTPUT, ">$output") or die "Cannot create $output !!!";					# Create the output

	print OUTPUT "CDS_lengths", "\t", "N", "\n";

####################################################################################################################################################################

foreach $gene (@gene_table){
	$gene =~ s/\s+$//;

	@split_lines = split "\n", $gene;
	foreach $line (@split_lines){
		$line =~ s/\s+$//;
		@tab = split "\t", $line;
		if ($tab[0] =~ /exon/){
			$this_gene_CDS = $this_gene_CDS + $tab[1];				# Sum of exons
		}
	}
	$CDS_lengths{$tab[1]}++;								# Length : count
	push @CDS_lengths, $tab[1];								# Storage of those lengths to be sorted later	
		
	$this_gene_CDS = 0;									# Reset
}
####################################################################################################################################################################

@sorted_CDS_lengths = sort {$a<=>$b} @CDS_lengths;
	$max = $sorted_CDS_lengths[scalar @sorted_CDS_lengths - 1];
	$min = $sorted_CDS_lengths[0];								# What values are we looking at

	# So if range is <= 100, steps are by 1. If 101- 1000, steps are 10. 1001-10000, steps are 100. Steps = max / 100. 
	
	$divisor = $max / 100;										# Figuring our our range

	# Max values less than 100nt = divisor < 1
	if ($divisor <= 1){ $step = 1;}

	# Max values between 101 and 1,000 = divisor > 1 but <= 10;
	if ($divisor > 1 and $divisor <= 10){ $step = 10;}

	# Max values between 1,001 and 10,000 = divisor > 10 but <= 100;
	if ($divisor > 10 and $divisor <= 100){ $step = 100;}

	# Max values between 10,001 and 100,000 = divisor > 1,000 but <= 10,000;
        if ($divisor > 100 and $divisor <= 1000){ $step = 1000;}

	# Max values between 1,000,001 and 10,000,000 = divisor > 1,000 but <= 10,000;
        if ($divisor > 1000 and $divisor <= 10000){ $step = 10000;}

if ($ARGV[1] =~ /^\d+$/){
        $step = $ARGV[1];
}

####################################################################################################################################################################

$upper_limit = $max + $step;
for ($a = 0; $a <= $upper_limit; $a = $a + $step){						# From 0 to maximum + step, go up by step.
	$final = ($a + $step) - 1;
	$range_value = $a . "-" . $final;							# 0 - 99 ; 100-199 ; etc...

	for ($b = $a; $b <= $final; $b++){							# For all values in that range
		$lengths = $lengths + $CDS_lengths{$b};	 					# Get numbers from the hash
	}
	print OUTPUT $range_value, "\t", $lengths, "\n";	
	$lengths = 0;
	$finap = 0;
}
close OUTPUT;
####################################################################################################################################################################
####################################################################################################################################################################
# A pound of dimes and a pound of quarters are worth the same. How unlikely! 

