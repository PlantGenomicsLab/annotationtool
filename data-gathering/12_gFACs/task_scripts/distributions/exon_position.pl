#!usr/bin/perl
#################################################################################################################################################################
# 

#ARGV[0] --> location
#ARGV[1] --> prefix

$gene_table = $ARGV[0] . "\/" . $ARGV[1] . "gene_table.txt";
open (GENE_TABLE, $gene_table) or die "Cannot open $gene_table!!!";
	@gene_table = <GENE_TABLE>;
	$join = join "", @gene_table;
	@gene_table = split "###", $join;
close GENE_TABLE;

$output = $ARGV[0] . "\/" . $ARGV[1] . "exon_position_distributions.tsv";
open (OUTPUT, ">$output") or die "Cannot create $output";
	foreach $gene (@gene_table){
		
		$gene =~ s/\s+$//;
		$gene =~ s/^\s+\n//g;

		@lines = split "\n", $gene;
		foreach $line (@lines){
			@tab = split "\t", $line;

			if ($tab[0] =~ /exon/){
				if ($tab[4] =~ /\+/){push @exons, $tab[1];}
				if ($tab[4] =~ /\-/){unshift @exons, $tab[1];}
			}
		}
		foreach $exon_size (@exons){
			$exon_count++;
			push @$exon_count, $exon_size;
			$exon_num_range{$exon_count} = "count";
			$exon_num_sizes{$exon_count} = $exon_num_sizes{$exon_count} +  $exon_size;
		}
		$exon_num_range_gene{scalar @exons}++;
		$exon_count = 0;
		undef (@exons);
	}

print OUTPUT "exon_position", "\t", 
	"n(Individual exons)", "\t",
	"n(Max exons in gene)", "\t", 
	"average_size", "\t", 
	"median_size", "\t", 
	"Min", "\t",
	"Max", "\t";
print OUTPUT "\n";

foreach $exon_size (sort {$a <=> $b} keys %exon_num_range){
	print OUTPUT $exon_size, "\t", 									# Positions starting at 1
		scalar @$exon_size, "\t";								# Num of exons supporting location
	
		if ( $exon_num_range_gene{$exon_size} =~ /./){
			print OUTPUT $exon_num_range_gene{$exon_size}, "\t",				# Exon containment distribution
		}
		else{ print OUTPUT "0", "\t"}
	
		$average = $exon_num_sizes{$exon_size} / scalar @$exon_size;				# Average per position
		$average_rounded = sprintf("%.3f", $average);						# 3 decimals
	
		print OUTPUT $average_rounded, "\t";	

		@sorted_array = sort {$a <=> $b} @$exon_size;
		        $middle_of_sorted_array = scalar @sorted_array / 2;
		        if ($middle_of_sorted_array =~ /\./){
	                	$median_value_int = int($middle_of_sorted_array) ;
		                $median_value_red = int($middle_of_sorted_array)  +1;
	        	        $median_size = ($sorted_array[$median_value_int] + $sorted_array[$median_value_red]) /2;
	        	}
	        	elsif($middle_of_sorted_array =~ /\d+$/){
	                	$median_value = $middle_of_sorted_array;
		                $median_size = $sorted_array[$median_value];
        		}
	
		print OUTPUT $median_size, "\t";


		print OUTPUT $sorted_array[0], "\t", $sorted_array[scalar @sorted_array - 1], "\t";		# Min and Max sizes of exon at position
		print OUTPUT "\n";
}
close OUTPUT;

