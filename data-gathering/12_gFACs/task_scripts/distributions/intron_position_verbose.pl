#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
#################################################################################################################################################################
#ARGV[0] --> location
#ARGV[1] --> prefix
#################################################################################################################################################################
$gene_table = $ARGV[0] . "\/" . $ARGV[1] . "gene_table.txt";
open (GENE_TABLE, $gene_table) or die "Cannot open $gene_table!!!";
	@gene_table = <GENE_TABLE>;
	$join = join "", @gene_table;
	@gene_table = split "###", $join;
close GENE_TABLE;
#################################################################################################################################################################
$output = $ARGV[0] . "\/" . $ARGV[1] . "data_intron_position_distributions.tsv";
open (OUTPUT, ">$output") or die "Cannot create $output";
	foreach $gene (@gene_table){
		$gene =~ s/\s+$//;
		$gene =~ s/^\s+\n//g;
		@lines = split "\n", $gene;
		foreach $line (@lines){
			@tab = split "\t", $line;
			if ($tab[0] =~ /intron/){
				if ($tab[4] =~ /\+/){push @introns, $tab[1];}
				if ($tab[4] =~ /\-/){unshift @introns, $tab[1];}
			}
		}
		foreach $intron_size (@introns){
			$intron_count++;
			push @$intron_count, $intron_size;
			$intron_num_range{$intron_count} = "count";
			$intron_num_sizes{$intron_count} = $intron_num_sizes{$intron_count} +  $intron_size;
		}
		$intron_num_range_gene{scalar @introns}++;
		$intron_count = 0;
		undef (@introns);
	}
#################################################################################################################################################################
foreach $intron_size (sort {$a <=> $b} keys %intron_num_range){	
		print OUTPUT $intron_size, "\t";
		foreach $value (@$intron_size){
			print OUTPUT $value, "\t";
		}
		print OUTPUT "\n";
}
close OUTPUT;
#################################################################################################################################################################
#################################################################################################################################################################
# The highest credit score you can get is 850


