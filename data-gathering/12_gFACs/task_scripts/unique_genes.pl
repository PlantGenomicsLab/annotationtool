#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
##############################################################################################################################################################
# UNIQUE GENES ONLY CLUB
#	About this script:
#		This script is designed to remove directly overlapping genes as well as collapse transcript models into the first or largest transcript if more
#	than one exist. 
##############################################################################################################################################################

##ARGV[0] --> location
##ARGV[1] --> prefix

$gene_table = $ARGV[0] . "\/" . $ARGV[1] . "gene_table.txt";						# Name the gene table
$output = $ARGV[0] . "\/" . $ARGV[1] . "unique_genes.txt";						# Name the output

$transcripts = 0;
$genes_in = 0;
$overlap = 0;

open (GENE_TABLE, $gene_table) or die "Cannot open $gene_table";					# Gene table information storage
	@gene_table = <GENE_TABLE>;
	$join = join "", @gene_table;
	@gene_table = split "###", $join;
close GENE_TABLE;

open (OUTPUT, ">$output") or die "Cannot creat $output!!!";						# Create the output

##############################################################################################################################################################
GENE: foreach $gene (@gene_table){								
	$gene =~ s/^\s+\n//g;
	$gene =~ s/\s+$//g;
	@split_lines = split "\n", $gene;
	foreach $line (@split_lines){
                $line =~ s/\s+$//;
                @tab = split "\t", $line;
                if ($tab[0] =~ /gene/){									# If we have a gene
			$genes_in++;									# Tally genes in the gene table
			if ($tab[5] =~ /(.+)\.\d+$/){
				$gene_name = $1;							# If [5] in the gene line has a .#+
				$transcripts++;								# Count
				if ($transcript_hash{$gene_name} =~ /./){				# If the hash already has a value stored
					if ($tab[1] > $transcript_hash{$gene_name}){			# But this one is bigger
						$transcript_hash{$gene_name} = $tab[1];			# Replace it with gene length
						$gene_hash{$gene_name} = $gene;				# Then store the WHOLE gene in gene hash by gene name
					}
					if ($tab[1] <= $transcript_hash{$gene_name}){			# If smaller or equal
                                                next GENE;						# Move along
                                        }
				}
				else{									# If this is the first time you are seeing the gene
 	                                $transcript_hash{$gene_name} = $tab[1];				# Store the gene length by gene name
				        $gene_hash{$gene_name} = $gene;					# And store all gene by gene name
				}
                        }
			else{										# If this is NOT a transcript
				$gene_hash{$tab[5]} = $gene;						# Store gene by gene name. 
				$non_dup{$tab[5]} = $gene;
                                $count_no_dup++;
			}
		}
	}
}
##############################################################################################################################################################
$overlap_hash = 0;
GENE: foreach $gene (keys %gene_hash){									# Going through unique gene IDs
	@split_lines = split "\n", $gene_hash{$gene};
		foreach $line (@split_lines){
			@tab = split "\t", $line;
			if ($tab[0] =~ /gene/){
				$ID = $tab[6] . "_" . $tab[2] . "_" . $tab[3];				# Scaffold_start_stop
				$gene_ID_hash{$ID} = $gene_hash{$gene};					# Unique storage : will remove different gene IDs that
			}										# Cover 100% of another gene. (Duplicates)
		}
}
##############################################################################################################################################################
GENE: foreach $gene (keys %gene_ID_hash){                                                                  # Going through unique gene IDs
	$gene =~ s/^\s+\n//g;
	$gene =~ s/\s+$//g;
        print OUTPUT "\n###\n", $gene_ID_hash{$gene};
}
##############################################################################################################################################################
$transcripts_resolved = scalar keys %transcript_hash;							# Lessons
	print "Number of genes in: ", $genes_in, "\n";
	print "\t", $transcripts, " were transcripts.", "\n";
	$non_dup = $count_no_dup - scalar keys %non_dup;

	print "\t", $non_dup, " were non-transcript duplicates.", "\n";
$scalar = scalar keys %gene_hash;
$sum = $scalar;
$lost = $genes_in - scalar keys %gene_ID_hash;
	print "Number of unique genes retained:\t", scalar keys %gene_ID_hash, "\n";
	print "Number of genes lost:\t", $lost, "\n"; 
##############################################################################################################################################################
##############################################################################################################################################################
# In the original 1831 version of Goldilocks, it was an old angry woman who visits the bears and the bears try to drown her. 
