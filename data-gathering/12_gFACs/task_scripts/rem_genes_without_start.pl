#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
##########################################################################################################################################################
# WE NEED TO HAVE THAT ATG
#	About this script:
#		This script looks at the gene space and looks in the first three nt for ATG. Right now, no alternate start codons are considered. 
##########################################################################################################################################################
# Arguments:
# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> fasta index 
# ARGV[3] -->  prefix
##########################################################################################################################################################
$start_pass = 0;
use Bio::Index::Fasta;
$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "start_and_stop_gene_table.txt";
	open (OUT_TABLE, ">$output") or die "Cannot create the output script!!!";
print OUT_TABLE "###", "\n";

$inx = Bio::Index::Fasta->new(-filename => $ARGV[2]);
##########################################################################################################################################################
$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "gene_table.txt";							# Gene table info
open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;
##########################################################################################################################################################
GENE: foreach $gene (@gene_table){
	$start_check = 0;
	if ($gene =~ /gene/){$number_of_genes++;}
	$gene =~ s/^\s+\n//g;
        @split_lines = split "\n", $gene;
        foreach $line (@split_lines){
		$line =~ s/\s+$//;
                @split_tab = split "\t", $line;
                        if ($split_tab[0] =~ /gene/){
				if ($split_tab[4] =~ /\+/){
					$start = $split_tab[2];								#1
					$start_end = $split_tab[2] +2;							#3
	                                $seq = $inx->fetch("$split_tab[6]");						#Gets scaffold sequence
        	                        $string_1 = $seq->subseq($start, $start_end);					#seq = 1-3
	                                $string_1 =~ tr/ATGC/atgc/;							#all lower case
					$length = $seq->length();
						if ($string_1 =~ /atg/){
							$start_check = 1;
							$start_pass++;
							undef ($string_1);
						}
				}
				if ($split_tab[4] =~ /\-/){
					$start_end =  $split_tab[3];					#In reverse due to strandedness
					$start = $split_tab[3] -2;
					$seq = $inx->fetch("$split_tab[6]");
					$string_1 = $seq->subseq($start, $start_end);
                                        $string_1 =~ tr/ATGC/atgc/;
					$string_1 =~ tr/atgc/tacg/;
					@split_neg = split "",  $string_1;
					$string_2 = $split_neg[2] . $split_neg[1] . $split_neg[0];		#inverting then rearranging 	
						if ($string_2 =~ /atg/){
        	                                        $start_check = 1;
							$start_pass++;
                        	                        undef ($string_2);
							undef ($string_1);
                                        	}
					$length = $seq->length();
				}
			}
		}
		if ($start_check == 1){
			print OUT_TABLE $gene, "\n", "###\n";
			$pass_pass++;
		}
		$start_check = 0;
	}
close OUT_TABLE;
##########################################################################################################################################################
$lost = $number_of_genes - $pass_pass;							# What we learned
print "Number of genes retained:", "\t", $pass_pass, "\n";
print "Number of genes lost:", "\t", $lost, "\n";
##########################################################################################################################################################
##########################################################################################################################################################
# Hedgehogs are primarily nocturnal. 


