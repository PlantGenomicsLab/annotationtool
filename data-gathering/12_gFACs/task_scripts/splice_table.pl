#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
###############################################################################################################################################################
# YE OLE SPLICE TABLE
#	About this script
#		This script is designed to analyze splice usage of all multiexonic genes. It prints to the log and has splice site denoted as xx_xx
#	always translated to positive forward nts. It has count usage, and overall percentage. Most SHOULD be canonical gt_ag splice usage.
###############################################################################################################################################################
# Arguments:
# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> fasta index
# ARGV[3] --> prefix
###############################################################################################################################################################
use Bio::Index::Fasta; 
$inx = Bio::Index::Fasta->new(-filename => $ARGV[2]);
###############################################################################################################################################################
$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "gene_table.txt";
open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";			# Gene table information
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;
###############################################################################################################################################################
	foreach $gene (@gene_table){
		@split_lines = split "\n", $gene;
		foreach $line (@split_lines){
			$line =~ s/\s+$//;
			@split_tab = split "\t", $line;
			if ($split_tab[0] =~ /gene/){
				$seq = $inx->fetch("$split_tab[6]");						# Grab the sequence
			}
			if ($split_tab[0] =~ /intron/){
				$split_start_2 = $split_tab[2] +1;						# Where we expect splice sites to be
				$split_start_1 = $split_tab[2];
				$split_end_1 =  $split_tab[3] -1;
				$split_end_2 = $split_tab[3];

				$string_1 = $seq->subseq("$split_start_1", "$split_start_2");
				$string_1 =~ tr/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/;		# Not taking any chances here
		
				$string_2 = $seq->subseq("$split_end_1", "$split_end_2");
				$string_2 =~ tr/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/;

				$splice = "$string_1\_$string_2";						# Splice = xx_xx

				if ($split_tab[4] =~ /\-/){							# REVERSE STRAND SHENANIGANS
					$splice =~ tr/atgc/tacg/;						# at_gc = ta_cg
					@splice_split = split "", $splice;					# t a _ c g
					foreach $base (@splice_split){
						unshift @unshift, $base;					# g c _ a t
					}
					$splice = join "", @unshift;						# gc_at
					undef (@unshift);
				}
				$Splice_lookup{$splice}++;
				$sum++;
			}
		}	
	}
foreach $splice (keys %Splice_lookup){										# Making the table
	$percentage = ($Splice_lookup{$splice} / $sum) * 100;
	$percentage_rounded = sprintf("%.3f", $percentage);
	print "\t", $splice, "\t", $Splice_lookup{$splice}, "\t", $percentage_rounded, "\%", "\n";
}
###############################################################################################################################################################
###############################################################################################################################################################
# Ice cream is nearly 4000 years old (2000BCE in China) but the ice cream cone was only invented in 1904!
 

