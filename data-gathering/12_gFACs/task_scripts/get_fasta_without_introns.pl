#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
###############################################################################################################################################################
# FASTAS, BUT JUST THE GOOD STUFF
#	About this script:
#		We are creating a fasta but without the intronic sequence. All business. Just nucleotides. 
##############################################################################################################################################################

# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> prefix

use Bio::Index::Fasta;											# Bioperl

$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "genes_without_introns.fasta";				# Name output
open (FASTA_1, ">$output") or die "Cannot create the output script!!!";					# Create output

$inx = Bio::Index::Fasta->new(-filename => $ARGV[0]);							# Get ya index

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";					# Name the gene table
open (GENE_TABLE, "$gene_table") or die "Cannot open $gene_table";					# Get the info stored
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;

##############################################################################################################################################################
GENE: foreach $gene (@gene_table){									# And we're parsing
	$gene =~ s/\s+$//g;
        @split_lines = split "\n", $gene;
        foreach $line (@split_lines){					
        	$line =~ s/\s+$//;
                @split_tab = split "\t", $line;
			if ($split_tab[0] =~ /gene/){							# Info line is derived from gene line info
				print FASTA_1 "\>", $split_tab[5], "\n";
			}
                	if ($split_tab[0] =~ /exon/){							# Printing the exon sequences
				$start = $split_tab[2];
				$end = $split_tab[3];
				$seq = $inx->fetch("$split_tab[6]");
				$string_1 = $seq->subseq("$start", "$end");
				print FASTA_1 $string_1;
			}
	}
	print FASTA_1 "\n";		
}
close FASTA_1;
##############################################################################################################################################################
##############################################################################################################################################################
# Did you know Celsius and Fahrenheit are equal at -40 degrees. 



