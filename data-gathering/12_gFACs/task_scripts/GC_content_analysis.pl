#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
###############################################################################################################################################################
# FASTAS, BUT JUST THE GOOD STUFF
#	About this script:
#		We are creating a fasta but without the intronic sequence. All business. Just nucleotides. 
##############################################################################################################################################################
# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> prefix
##############################################################################################################################################################
use Bio::Index::Fasta;											# Bioperl

$inx = Bio::Index::Fasta->new(-filename => $ARGV[0]);							# Get ya index

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";					# Name the gene table
open (GENE_TABLE, "$gene_table") or die "Cannot open $gene_table";					# Get the info stored
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;

##############################################################################################################################################################
GENE: foreach $gene (@gene_table){									# And we're parsing
	$gene =~ s/\s+$//g;
        @split_lines = split "\n", $gene;
        foreach $line (@split_lines){					
        	$line =~ s/\s+$//;
                @split_tab = split "\t", $line;
			if ($split_tab[0] =~ /gene/){
				 $seq = $inx->fetch("$split_tab[6]");
			}
                	if ($split_tab[0] =~ /exon/){							# Printing the exon sequences
				$start = $split_tab[2];
				$end = $split_tab[3];
				$string_1 = $seq->subseq("$start", "$end");
					@split = split "", $string_1;
					foreach $nt (@split){
						$countseq++;
						if ($nt =~ /G|g|C|c/){$GC++;}
						if ($nt =~ /A|a|T|t/){$AT++;}
						if ($nt =~ /N|n/){$Nn++;}
					}
			}
	}
}
##############################################################################################################################################################
$GC_content = ($GC / $countseq) *100;
$GC_content_rounded = sprintf("%.3f", $GC_content);
print "GC content:\t", $GC_content_rounded, "\%", "\n";

$AT_content = ($AT / $countseq) *100;
$AT_content_rounded = sprintf("%.3f", $AT_content);
print "AT content:\t", $AT_content_rounded, "\%", "\n";

$Nn_content = ($Nn / $countseq) *100;
$Nn_content_rounded = sprintf("%.3f", $Nn_content);
print "N content:\t", $Nn_content_rounded, "%", "\n";

##############################################################################################################################################################
##############################################################################################################################################################
# The Incas had no written language that we know of. 
