#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
##################################################################################################################################################################
# COMPATIBILITY: SNPEFF
#       About this script:
#               This script will take your gtf as an input and get it all ready and formatted for SnpEff. 
##################################################################################################################################################################
#$ARGV[0] = output location
#$ARGV[1] = prefix

$gtf = $ARGV[0] . "\/" . $ARGV[1] . "out.gtf";
$output =  $ARGV[0] . "\/" . $ARGV[1] . "snpeff_format.gff";

open (GTF, $gtf) or die "Cannot open $gtf!!!";
open (OUTPUT, ">$output") or die "Cannot create $output!!!";

while ($line = <GTF>){
	$line =~ s/\s+$//;
	@split = split "\t", $line;
	if ($split[2] =~ /gene/){
		print OUTPUT $split[0], "\t",
		$split[1], "\t",
		$split[2], "\t",
		$split[3], "\t",
		$split[4], "\t",
		$split[5], "\t",
		$split[6], "\t",
		$split[7], "\t",
		"transcript_ID\=\"",
		$split[8],
		"\"\;", "\n";
	}
	if ($split[2] =~ /CDS/){
                print OUTPUT $split[0], "\t",
                $split[1], "\t",
                "exon", "\t",
                $split[3], "\t",
                $split[4], "\t",
                $split[5], "\t",
                $split[6], "\t",
                $split[7], "\t",
                "transcript_ID\=\"",
                $split[8],
                "\"\;", "\n";
        }
}

close GTF;
close OUTPUT;
##################################################################################################################################################################
##################################################################################################################################################################
# Roald Amundsen was both the first person to reach the south pole and the first person do have definitively reached the north pole.

