#!usr/bin/perl
#########################################################################################################################################################
# I ONLY WANT MONOEXONICS
#	About this script:
#		Removes monoexonics. Creates output multiexonic_genes.txt that is then reverted to gene_table.txt. Easy.
#########################################################################################################################################################
# Arguments:
# ARGV[1] == output
# ARGV[0] == gene table
# ARGV[2] == prefix
#########################################################################################################################################################
open (GENE_TABLE, $ARGV[0]) or die "Cannot open the gene table!!!";					# Gene table information
	@infile = <GENE_TABLE>;
        $infile = join "", @infile;
        @infile = split "###", $infile;
close GENE_TABLE;
#########################################################################################################################################################
$monos = $ARGV[1] . "\/" . "\/" .  $ARGV[2] . "monoexonic_genes.txt";					# Name/crete output
	open (OUTPUT, ">$monos") or die "Cannot create monoexonic_genes.txt at $ARGV[1]";
print OUTPUT "###", "\n";
#########################################################################################################################################################
	GENE: foreach $gene (@infile){
                $gene =~ s/^\s+\n//g;
		if ($gene =~ /gene/){$number_of_genes++;}
                @split_lines  = split "\n", $gene;
                        if ($gene =~ /intron/){next GENE;}
			else{
				print OUTPUT $gene, "\n", "###";
				$count++;
				next GENE;
			}
	}
#########################################################################################################################################################
$lost = $number_of_genes - $count;								# What we learned today
print "Number of genes retained:", "\t", $count, "\n";
print "Number of genes lost:\t", $lost, "\n";
close OUTPUT;
#########################################################################################################################################################
#########################################################################################################################################################
# Tupperware is named after its inventor, Earl S. Tupper. 
