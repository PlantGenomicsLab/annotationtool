#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
#################################################################################################################################################################
# FASTA SEQUENCE, BUT WITH OUR FRIENDS THE INTRONS.
#	About this script:
#		This is the easiest thing to write. Get all the sequence even with the introns. That's it. 
#################################################################################################################################################################

# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> prefix

use Bio::Index::Fasta;												# Summoning bioperl

$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "genes_with_introns.fasta";					# Name the output
open (FASTA_1, ">$output") or die "Cannot create the output script!!!";						# Create the output

$inx = Bio::Index::Fasta->new(-filename => $ARGV[0]);								# Snag the index

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";						# Name the gene table
open (GENE_TABLE, "$gene_table") or die "Cannot open $gene_table";						# Get gene table information
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;
#################################################################################################################################################################
GENE: foreach $gene (@gene_table){
	$gene =~ s/\s+$//g;
        @split_lines = split "\n", $gene;
        foreach $line (@split_lines){
        	$line =~ s/\s+$//;
                @split_tab = split "\t", $line;
                	if ($split_tab[0] =~ /gene/){								# Gene line
				$start = $split_tab[2];
				$end = $split_tab[3];
				$seq = $inx->fetch("$split_tab[6]");						# Scaffold query
				$string_1 = $seq->subseq("$start", "$end");					# Get that sequence
				print FASTA_1 "\>", $split_tab[5], "\n", $string_1, "\n";			# And print it!
			}
	}		
}

close FASTA_1;

#################################################################################################################################################################
#################################################################################################################################################################
# Easy. Breezy. Beautiful. Fastas.
#	Ice technically fits the definition of a mineral. 



