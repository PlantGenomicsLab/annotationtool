#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
#################################################################################################################################################################
# ANOTHER RESPONSE TO THE LACK OF UNIFORMITY IN THESE FORMATS
# 	About this script:
#		Not all formats agree, and some models report exons on the negative strand in order of appearance. So a negative bunch of exons would start
#	with the largest + strand values. I have to fix this so that printing sequence is accurate with the right exons! 
#################################################################################################################################################################	

#ARGV[0] == output destination
#ARGV[1] == prefix

$gene_table = $ARGV[0] . "\/" . $ARGV[1] . "gene_table.txt";							# Name the gene table
$output = $ARGV[0] . "\/" . $ARGV[1] . "checked_gene_table.txt";						# Name the output

open (GENE_TABLE, $gene_table) or die "Cannot open $gene_table!!!";						# Snag the information from the gene table
	@gene_table = <GENE_TABLE>;
	$join = join "", @gene_table;
	@gene_table = split "###", $join;
close GENE_TABLE;

open (OUTPUT, ">$output") or die "Cannot create $output!!!";							# Create the output

#################################################################################################################################################################
foreach $gene (@gene_table){											# Parse
	if ($gene =~ /gene/){											# Is this a gene?
		$gene =~ s/^\s+\n//g;
		@split_lines = split "\n", $gene;
		foreach $line (@split_lines){
			$line =~ s/\s+$//;
			@tab = split "\t", $line;
			if ($tab[0] =~ /gene/){									# Gene starts at tab[2]
				$start = $tab[2];
				$gene_line = $line;
			}
			if ($tab[0] =~ /exon/){									# Same story with exon
				$start = $tab[2];
				$hash{$start} = $line;								# And store the start values
				push @exons, $start;								# In a hash and array!
			}
			if ($tab[0] =~ /intron/){								# Same with introns
				$start = $tab[2];	
				$hash{$start} = $line;
				push @introns, $start;
			}
		}
		@sorted_exons = sort {$a <=> $b} @exons;							# Sort smallest to largest
		@sorted_introns = sort {$a <=> $b} @introns;							# Same thing	

		print OUTPUT "###\n", $gene_line, "\n";								# Print gene line
			
		foreach $key (@sorted_exons){									# Going through exon values
			print OUTPUT $hash{$key}, "\n";								# Get the line
		}
		undef (@exons);											# Resets
		foreach $key (@sorted_introns){									# Repeat with introns
                        print OUTPUT $hash{$key}, "\n";
                }
		undef (@introns);
		undef (%hash);
	}
}				
close OUTPUT;

###############################################################################################################################################################
###############################################################################################################################################################
# No results printed. Did you know Finland drinks the most coffee per capita? Followed closely by the other nordic countries! Hva et drikk! 


