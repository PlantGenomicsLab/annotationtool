#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
##################################################################################################################################################
# THE MOST CLASSIC OF STATISTICS!
#	About this script:
#		You've made it. Youve reached the end of your filtering and now it is time to understand exactly what you input file it saying. 
#	This script produces basic statistics on averages, medians, maximums, minimums, and overall counts on gene features. This includes stats
#	on both mono and multiexonic genes separately AND together. If you only have monoexonic genes, the script will give a smaller version
# 	of statistics, because why would you need to know intron lengths when all your genes are monoexonic? For the full array of statistics, 
#	the input file should be a mix of both mono and multiexonic genes. 

#GENE TABLE########################################################################################################################################
open (GENE_TABLE, $ARGV[0]) or die "Cannot open $ARGV[0]!!!";
	@GENE_TABLE = <GENE_TABLE>;									# Opening and storing gene table information
	$gene_table = join "", @GENE_TABLE;
	@GENE_TABLE = split "###", $gene_table;
close GENE_TABLE;

$number_of_genes = 0;
foreach $gene (@GENE_TABLE){
        $gene =~ s/\s+$//g;
	if ($gene =~ /gene/){$number_of_genes++;							# Broad counts of the number of genes
        @split_lines = split "\n", $gene;
	foreach $line (@split_lines){
		$line =~ s/\s+$//;
		@tab = split "\t", $line;
		if ($tab[0] =~ /exon/){
			$all_genes_CDS_size = $all_genes_CDS_size + $tab[1];				# CDS size add up, is reset for each gene
			$sum_all_genes_CDS = $sum_all_genes_CDS + $tab[1];				# Same as above but does NOT get reset

			push @all_exon_sizes_unbiased_array, $tab[1];					# Pushes exon sizes to array, does not get reset 
			$exon_count_unbiased++;								# Count of exon for overall exon size information
		}
		if ($tab[0] =~ /gene/){
                        $all_genes_gene_size = $all_genes_gene_size + $tab[1];				# Gene size storage but is reset with each gene
			$sum_all_genes_gene = $sum_all_genes_gene + $tab[1];				# Same as above but is not reset
				if ($tab[4] =~ /\+/){$positive_strand_genes++;}                       	# Strand information
		                if ($tab[4] =~ /\-/){$negative_strand_genes++;}
                }
	}
		push @all_genes_CDS, $all_genes_CDS_size;						# Storage of CDS and gene sizes
		push @all_genes_gene, $all_genes_gene_size;
		$all_genes_gene_size =0;								# Value reset
		$all_genes_CDS_size = 0;	

        if ($gene !~ /intron/){										# No intron = monoexonic
		if ($gene =~ /gene/){
	                $monoexonic_gene_count++;                                                       # Number of monoexonic genes
	                @tab = split "\t", $gene;				
	                        if ($tab[4] =~ /\+/){$positive_strand_monoexonicgenes++;}		# Strand information on monos
	                        if ($tab[4] =~ /\-/){$negative_strand_monoexonicgenes++;}
        	        if ($tab[0] =~ /gene/){
                	        $mono_exonic_gene_sizes = $tab[1] + $mono_exonic_gene_sizes;		# Monoexonic gene size storage
                        	push @mono_exonic_gene_sizes, $tab[1];
                	}
		}
        }
	if ($gene =~ /intron/){										# Intron = multiexonic
                $multiexonic_gene_count++;                                                              # Number of multiexonic genes
                        @tab = split "\t", $gene;	
                        if ($tab[4] =~ /\+/){$positive_strand_multiexonicgenes++;}			# Strand information
                        if ($tab[4] =~ /\-/){$negative_strand_multiexonicgenes++;}

                foreach $line (@split_lines){
                        @tab = split "\t", $line;
                        if ($tab[0] =~ /exon/){								# Within a multiexonic gene, if we are looking at an exon
                                $exon_count++;								# Number of exons in this gene are added up (reset)
                                $exon_count_sum++;							# Same as above, but not reset
                                $multiexonic_exons++;							
                                $multiexonic_exon_sizes = $tab[1] + $multiexonic_exon_sizes;		# Storage of sizes
                                push @multi_exonic_exon_sizes, $tab[1];
				$this_genes_CDS = $tab[1] + $this_genes_CDS;				# CDS addition
                        }
                        if ($tab[0] =~ /intron/){							# Dealing with an intron	
                                $intron_count++;						
                                $intron_count_sum++;							# Counting introns
                                $multiexonic_introns++;
                                $multiexonic_intron_sizes = $tab[1] + $multiexonic_intron_sizes;
                                push @multi_exonic_intron_sizes, $tab[1];
                        }
                        if ($tab[0] =~ /gene/){								# Dealing with overall gene (exons + introns)
                                $multiexonic_gene_sizes = $tab[1] + $multiexonic_gene_sizes;
                                push @multi_exonic_gene_sizes, $tab[1];
                        }
                }
        push @exon_count, $exon_count;									# Storage of exon counts
        $exon_count = 0;										# Resets
        push @intron_count, $intron_count;								# Storange of intron counts
        $intron_count = 0;										# Resets
        }
	if ($this_genes_CDS > 0){									# If gene has a CDS (precautionary)
		push @gene_CDS, $this_genes_CDS;							# Store it
		$all_genes_CDS = $all_genes_CDS + $this_genes_CDS;
	}
	$this_genes_CDS = 0;
}
}
#############################################################################################################################################################
# All data has been accounted for, and now we are just parsing arrays and getting the actual statistics. 
# First, we are detecting if this is a monoexonic set.

if ($multiexonic_gene_count == 0){print LOG "\nThis looks like a monoexonic-only set\n";}

if ($monoexonic_gene_count > 0){									# If monoexonic and we HAVE monoexonic genes
$average_monoexonic_gene_size = $mono_exonic_gene_sizes / $monoexonic_gene_count;
        @sorted_mono_exonic_gene_sizes = sort {$a <=> $b} @mono_exonic_gene_sizes;			
        $middle_of_sorted_mono_exonic_gene_sizes = scalar @sorted_mono_exonic_gene_sizes / 2;
        if ($middle_of_sorted_mono_exonic_gene_sizes =~ /\./){
                $median_value_int = int($middle_of_sorted_mono_exonic_gene_sizes) ;
                $median_value_red = int($middle_of_sorted_mono_exonic_gene_sizes)  +1;
                $median_monoexonic_gene_size = ($sorted_mono_exonic_gene_sizes[$median_value_int] + $sorted_mono_exonic_gene_sizes[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_mono_exonic_gene_sizes =~ /\d+$/){
                $median_value = $middle_of_sorted_mono_exonic_gene_sizes;
                $median_monoexonic_gene_size = $sorted_mono_exonic_gene_sizes[$median_value];
        }
}
#############################################################################################################################################################
# Printing all MONOEXONIC relevant information if you only have a monoexonic gene set!

if ($multiexonic_gene_count < 1){
	print "\n\nNumber of genes:\t", $number_of_genes, "\n";
        print "Number of positive strand genes:\t", $positive_strand_genes, "\n";
        print "Number of negative strand genes:\t", $negative_strand_genes, "\n";

		$average_monoexonic_gene_size_rounded = sprintf("%.3f", $average_monoexonic_gene_size);
        print "\nAverage size of mono-exonic genes:\t", $average_monoexonic_gene_size_rounded, "\n";
        print "Median size of mono-exonic genes:\t", $median_monoexonic_gene_size, "\n";
        print "Largest monoexonic gene:\t", $sorted_mono_exonic_gene_sizes[scalar $sorted_mono_exonic_gene_sizes - 1], "\n";
        print "Smallest monoexonic gene:\t", $sorted_mono_exonic_gene_sizes[0], "\n\n\n";


	# THE VAUGE ERROR ##########
        die "No multi-exonic gene stats can be calulated! $number_of_genes genes were found!", "\n", 
	"This is a very vague error. You are seeing this because:", "\n",
	"\tThe gene_table.txt file is empty because of a wrong input file name.", "\n",
	"\tThe gene_tabel.txt file is empty because the input file did not match the format flag.", "\n",
	"\tThe input file was not the last argument in the command... which it should be.", "\n",
	"\tYour set is actually only monoexonic! If so, this is not an error message but rather a completion message.", "\n";
}

#############################################################################################################################################################
# Now we are on to the more exciting world of MULTIEXONICS. I play favorites! 

if ($multiexonic_gene_count > 0 and $multiexonic_introns > 0){

#############################################################################################################################################################
$average_multiexonic_exon_size = $multiexonic_exon_sizes / $multiexonic_exons;
        @sorted_multi_exonic_exon_sizes = sort {$a <=> $b} @multi_exonic_exon_sizes;
        $middle_of_sorted_multi_exonic_exon_sizes = scalar @sorted_multi_exonic_exon_sizes / 2;
        if ($middle_of_sorted_multi_exonic_exon_sizes =~ /\./){
                $median_value_int = int($middle_of_sorted_multi_exonic_exon_sizes) ;
                $median_value_red = int($middle_of_sorted_multi_exonic_exon_sizes)  +1;
                $median_multiexonic_exon_size = ($sorted_multi_exonic_exon_sizes[$median_value_int] + $sorted_multi_exonic_exon_sizes[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_multi_exonic_exon_sizes =~ /\d+$/){
                $median_value = $middle_of_sorted_multi_exonic_exon_sizes;
                $median_multiexonic_exon_size = $sorted_multi_exonic_exon_sizes[$median_value];
        }
#############################################################################################################################################################
$average_exons_per_multiexonic_gene =  $exon_count_sum / $multiexonic_gene_count;
        @sorted_exon_count = sort {$a <=> $b} @exon_count;
        $middle_of_sorted_exon_count = scalar @sorted_exon_count / 2;
        if ($middle_of_sorted_exon_count =~ /\./){
                $median_value_int = int($middle_of_sorted_exon_count) ;
                $median_value_red = int($middle_of_sorted_exon_count)  +1;
                $median_exons_per_multiexonic_gene = ($sorted_exon_count[$median_value_int] + $sorted_exon_count[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_exon_count =~ /\d+$/){
                $median_value = $middle_of_sorted_exon_count;
                $median_exons_per_multiexonic_gene = $sorted_exon_count[$median_value];
        }
#############################################################################################################################################################
        $smallest_exon =  $sorted_multi_exonic_exon_sizes[0];
        $largest_exon =  $sorted_multi_exonic_exon_sizes[scalar @sorted_multi_exonic_exon_sizes - 1];
#############################################################################################################################################################
$average_multiexonic_intron_size = $multiexonic_intron_sizes / $multiexonic_introns;
        @sorted_multi_exonic_intron_sizes = sort {$a <=> $b} @multi_exonic_intron_sizes;
        $middle_of_sorted_multi_exonic_intron_sizes = scalar @sorted_multi_exonic_intron_sizes / 2;
        if ($middle_of_sorted_multi_exonic_intron_sizes =~ /\./){
                $median_value_int = int($middle_of_sorted_multi_exonic_intron_sizes) ;
                $median_value_red = int($middle_of_sorted_multi_exonic_intron_sizes)  +1;
                $median_multiexonic_intron_size = ($sorted_multi_exonic_intron_sizes[$median_value_int] + $sorted_multi_exonic_intron_sizes[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_multi_exonic_intron_sizes =~ /\d+$/){
                $median_value = $middle_of_sorted_multi_exonic_intron_sizes;
                $median_multiexonic_intron_size = $sorted_multi_exonic_intron_sizes[$median_value];
        }
#############################################################################################################################################################
        $smallest_intron =  $sorted_multi_exonic_intron_sizes[0];
        $largest_intron =  $sorted_multi_exonic_intron_sizes[scalar @sorted_multi_exonic_intron_sizes - 1];
#############################################################################################################################################################
$average_introns_per_multiexonic_gene =  $intron_count_sum / $multiexonic_gene_count;
        @sorted_intron_count = sort {$a <=> $b} @intron_count;
        $middle_of_sorted_intron_count = scalar @sorted_intron_count / 2;
        if ($middle_of_sorted_intron_count =~ /\./){
                $median_value_int = int($middle_of_sorted_intron_count) ;
                $median_value_red = int($middle_of_sorted_intron_count)  +1;
                $median_introns_per_multiexonic_gene = ($sorted_intron_count[$median_value_int] + $sorted_intron_count[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_intron_count =~ /\d+$/){
                $median_value = $middle_of_sorted_intron_count;
                $median_introns_per_multiexonic_gene = $sorted_intron_count[$median_value];
        }
#############################################################################################################################################################
$average_multiexonic_gene_size = $multiexonic_gene_sizes / $multiexonic_gene_count;
        @sorted_multi_exonic_gene_sizes = sort {$a <=> $b} @multi_exonic_gene_sizes;
        $middle_of_sorted_multi_exonic_gene_sizes = scalar @sorted_multi_exonic_gene_sizes / 2;
        if ($middle_of_sorted_multi_exonic_gene_sizes =~ /\./){
                $median_value_int = int($middle_of_sorted_multi_exonic_gene_sizes) ;
                $median_value_red = int($middle_of_sorted_multi_exonic_gene_sizes)  +1;
                $median_multiexonic_gene_size = ($sorted_multi_exonic_gene_sizes[$median_value_int] + $sorted_multi_exonic_gene_sizes[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_multi_exonic_gene_sizes =~ /\d+$/){
                $median_value = $middle_of_sorted_multi_exonic_gene_sizes;
                $median_multiexonic_gene_size = $sorted_multi_exonic_gene_sizes[$median_value];
        }
#############################################################################################################################################################
$average_multiexonic_CDS_size = $all_genes_CDS / $multiexonic_gene_count;
	@sorted_gene_CDS = sort {$a <=> $b} @gene_CDS;
	$middle_of_sorted_multi_exonic_CDS_sizes = scalar @sorted_gene_CDS / 2;
	 if ($middle_of_sorted_multi_exonic_CDS_sizes =~ /\./){
                $median_value_int = int($middle_of_sorted_multi_exonic_CDS_sizes) ;
                $median_value_red = int($middle_of_sorted_multi_exonic_CDS_sizes)  +1;
                $median_multiexonic_CDS_size = ($sorted_gene_CDS[$median_value_int] + $sorted_gene_CDS[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_multi_exonic_CDS_sizes =~ /\d+$/){
                $median_value = $middle_of_sorted_multi_exonic_CDS_sizes;
                $median_multiexonic_CDS_size = $sorted_gene_CDS[$median_value];
        }
#############################################################################################################################################################
$average_all_genes_gene_size = $sum_all_genes_gene / $number_of_genes;
	@sorted_all_genes_gene = sort {$a<=>$b} @all_genes_gene;
        $middle_of_sorted_all_genes_gene = scalar @sorted_all_genes_gene / 2;
        if (@middle_of_sorted_all_genes_gene =~ /\./){
                $median_value_int = int($middle_of_sorted_all_genes_gene);
                $median_value_red = int($middle_of_sorted_all_genes_gene) +1;
                $median_all_genes_gene_size = ($sorted_all_genes_gene[$median_value_int] + $sorted_all_genes_gene[$median_value_red]) / 2;
        }
        elsif($middle_of_sorted_all_genes_gene =~/\d+$/){
                $median_value = $middle_of_sorted_all_genes_gene;
                $median_all_genes_gene_size = $sorted_all_genes_gene[$median_value];
        }
#############################################################################################################################################################
$average_all_genes_CDS_size = $sum_all_genes_CDS / $number_of_genes;
	@sorted_all_genes_CDS = sort {$a<=>$b} @all_genes_CDS;
	$middle_of_sorted_all_genes_CDS = scalar @sorted_all_genes_CDS / 2;

	if ($middle_of_sorted_all_genes_CDS =~ /\./){
		$median_value_int = int($middle_of_sorted_all_genes_CDS);
		$median_value_red = int($middle_of_sorted_all_genes_CDS) +1;
		$median_all_genes_CDS_size = ($sorted_all_genes_CDS[$median_value_int] + $sorted_all_genes_CDS[$median_value_red]) / 2;
	}
	elsif($middle_of_sorted_all_genes_CDS =~/\d+$/){
		$median_value = $middle_of_sorted_all_genes_CDS;
		$median_all_genes_CDS_size = $sorted_all_genes_CDS[$median_value];
	}
#############################################################################################################################################################
$average_exon_size_unbiased = $sum_all_genes_CDS / $exon_count_unbiased;
	@sorted_all_exon_sizes_unbiased = sort {$a <=> $b} @all_exon_sizes_unbiased_array;
	$middle_of_sorted_all_exon_sizes_unbiased = scalar @sorted_all_exon_sizes_unbiased / 2;
	
	if ($middle_of_sorted_all_exon_sizes_unbiased =~ /\./){
                $median_value_int = int($middle_of_sorted_all_exon_sizes_unbiased);
                $median_value_red = int($middle_of_sorted_all_exon_sizes_unbiased) +1;
                $median_exon_size_unbiased = ($sorted_all_exon_sizes_unbiased[$median_value_int] + $sorted_all_exon_sizes_unbiased[$median_value_red]) / 2;
        }
        elsif($middle_of_sorted_all_exon_sizes_unbiased =~/\d+$/){
                $median_value = $middle_of_sorted_all_exon_sizes_unbiased;
                $median_exon_size_unbiased = $sorted_all_exon_sizes_unbiased[$median_value];
        }
#############################################################################################################################################################
# STATS SECTION

print "STATS:\n";
print "Number of genes:\t", $number_of_genes, "\n";
print "Number of monoexonic genes:\t", $monoexonic_gene_count, "\n";
print "Number of multiexonic genes:\t", $multiexonic_gene_count, "\n";

print "\nNumber of positive strand genes:\t", $positive_strand_genes, "\n";
        print "Monoexonic:\t",  $positive_strand_monoexonicgenes, "\n";
        print "Multiexonic:\t",  $positive_strand_multiexonicgenes, "\n";
print "\nNumber of negative strand genes:\t", $negative_strand_genes, "\n";
        print "Monoexonic:\t",  $negative_strand_monoexonicgenes, "\n";
        print "Multiexonic:\t",  $negative_strand_multiexonicgenes, "\n\n";

	$rounded_average_all_genes_gene_size = sprintf("%.3f", $average_all_genes_gene_size);
print "Average overall gene size:\t", $rounded_average_all_genes_gene_size, "\n";
print "Median overall gene size:\t", $median_all_genes_gene_size, "\n";
	$rounded_average_all_genes_CDS_size = sprintf("%.3f", $average_all_genes_CDS_size);
print "Average overall CDS size:\t", $rounded_average_all_genes_CDS_size, "\n";
print "Median overall CDS size:\t", $median_all_genes_CDS_size, "\n";
	$rounded_average_exon_size_unbiased = sprintf("%.3f", $average_exon_size_unbiased);
print "Average overall exon size:\t", $rounded_average_exon_size_unbiased, "\n";
print "Median overall exon size:\t", $median_exon_size_unbiased, "\n";

	$average_monoexonic_gene_size_rounded = sprintf("%.3f", $average_monoexonic_gene_size);
print "\nAverage size of monoexonic genes:\t", $average_monoexonic_gene_size_rounded, "\n";
print "Median size of monoexonic genes:\t", $median_monoexonic_gene_size, "\n";
print "Largest monoexonic gene:\t", $sorted_mono_exonic_gene_sizes[scalar $sorted_mono_exonic_gene_sizes - 1], "\n";
print "Smallest monoexonic gene:\t", $sorted_mono_exonic_gene_sizes[0], "\n";

	$average_multiexonic_gene_size_rounded = sprintf("%.3f", $average_multiexonic_gene_size);
print "\nAverage size of multiexonic genes:\t", $average_multiexonic_gene_size_rounded, "\n";
print "Median size of multiexonic genes:\t", $median_multiexonic_gene_size, "\n";
print "Largest multiexonic gene:\t", $sorted_multi_exonic_gene_sizes[scalar $sorted_multi_exonic_gene_sizes - 1], "\n";
print "Smallest multiexonic gene:\t", $sorted_multi_exonic_gene_sizes[0], "\n\n";

	 $average_multiexonic_CDS_size_rounded = sprintf("%.3f", $average_multiexonic_CDS_size);
print "Average size of multiexonic CDS:\t", $average_multiexonic_CDS_size_rounded, "\n";
print "Median size of multiexonic CDS:\t", $median_multiexonic_CDS_size, "\n";
print "Largest multiexonic CDS:\t", $sorted_gene_CDS[scalar @sorted_gene_CDS - 1], "\n";
print "Smallest multiexonic CDS:\t", $sorted_gene_CDS[0], "\n";

	$average_multiexonic_exon_size_rounded = sprintf("%.3f", $average_multiexonic_exon_size);
print "\nAverage size of multiexonic exons:\t", $average_multiexonic_exon_size_rounded, "\n";
print "Median size of multiexonic exons:\t", $median_multiexonic_exon_size, "\n";

	 $average_multiexonic_intron_size_rounded = sprintf("%.3f", $average_multiexonic_intron_size);
print "Average size of multiexonic introns:\t", $average_multiexonic_intron_size_rounded, "\n";
print "Median size of multiexonic introns:\t", $median_multiexonic_intron_size, "\n";

	$average_exons_per_multiexonic_gene_rounded = sprintf("%.3f", $average_exons_per_multiexonic_gene);
print "\nAverage number of exons per multiexonic gene:\t", $average_exons_per_multiexonic_gene_rounded, "\n";
print "Median number of exons per multiexonic gene:\t", $median_exons_per_multiexonic_gene, "\n";
print "Largest multiexonic exon:\t", $largest_exon, "\n";
print "Smallest multiexonic exon:\t", $smallest_exon, "\n";
$scalar = scalar @sorted_exon_count -1;
print "Most exons in one gene:\t", $sorted_exon_count[$scalar], "\n";

	$average_introns_per_multiexonic_gene_rounded = sprintf("%.3f", $average_introns_per_multiexonic_gene);
print "\nAverage number of introns per multiexonic gene:\t", $average_introns_per_multiexonic_gene_rounded, "\n";
print "Median number of introns per multiexonic gene:\t", $median_introns_per_multiexonic_gene, "\n";
print "Largest intron:\t", $largest_intron, "\n";
print "Smallest intron:\t", $smallest_intron, "\n\n";

}

#############################################################################################################################################################
#############################################################################################################################################################
#############################################################################################################################################################
#############################################################################################################################################################
# THANKS FOR READING ME!
#	Did you know Antartica is considered a desert as some inner regions only recieve 2 inches of precipitation (snow or rain) per year! 
#		More rain falls in the Sahara! 
