#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
################################################################################################################################################################
##This script is designed to take in an entap .tsv file and filter the gene table by annotation. 

#ARGV[0] == entap annotation file
#ARGV[1] == output location
#ARGV[2] == prefix


open (ANNOTATION, $ARGV[0]) or die "Cannot open $ARGV[0]";

while ($line = <ANNOTATION>){
	$line =~ s/\s+$//;
	@tab = split "\t", $line;
	$tab[0] =~ s/\.t.+//;
	if ($tab[1] =~ /./ or $tab[20] =~ /./){
		$annotated{$tab[0]} = $line;
		#print $tab[0], "\n";
	}
}
close ANNOTATION;

$gene_table = $ARGV[1] . "\/" . $ARGV[2] . "gene_table.txt";
$output = $ARGV[1] . "\/" . $ARGV[2] . "annotated_gene_table.txt";

open (GENE_TABLE, $gene_table) or die "Cannot open $gene_table";
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;

open (OUTFILE, ">$output") or die "Cannot create $output";

foreach $gene (@gene_table){
	$gene =~ s/^\s+\n//g;	
	if ($gene =~ /gene/){
		$number_of_genes++;
		@split_lines = split "\n", $gene;
		
		foreach $line (@split_lines){
			@tab = split "\t", $line;
			$tab[5] =~ s/.+\.\d+$//;
			if ($tab[0] =~ /gene/){
				if ($annotated{$tab[5]} =~ /./){
					print OUTFILE "###", "\n", $gene;
					$saved++;
				}
			}
		}
	}
}

close OUTFILE;

$lost = $number_of_genes - $saved;
print "Number of genes retained:\t", $saved, "\n";
print "Number of genes lost:\t", $lost, "\n";
