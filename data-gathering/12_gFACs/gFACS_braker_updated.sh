#!/bin/bash
#SBATCH --job-name=gfacs-braker
#SBATCH -n 1
#SBATCH -c 2
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH -o output_files/gfacs-braker-%j.o
#SBATCH -e error_files/gfacs-braker-%j.e
#SBATCH --array=0-5

module load perl/5.28.1

#Script assumes that gff3 files for all 3 braker runs have directory structure as follows:
#$annotation/braker/braker/$gff3filename
#$annotation/braker2/braker/$gff3filename
#$annotation/braker3/braker/$gff3filename

###Variables to change
org=/labs/Wegrzyn/annotationtool/testSpecies/model/arabidopsis
genome=$org/genome/Arabidopsis_filtered.fasta.masked.fa
annotation=$org/analysis/annotation_new
gfffilename="augustus.hints.gff3"
###

out_arr=(
"unfiltered_run1_gfacs_o" \
"filtered_run1_gfacs_o" \
"unfiltered_run2_gfacs_o" \
"filtered_run2_gfacs_o" \
"unfiltered_run3_gfacs_o" \
"filtered_run3_gfacs_o" \
)

run_arr=(
"braker/braker" \
"braker/braker" \
"braker2/braker" \
"braker2/braker" \
"braker3/braker" \
"braker3/braker" \
)

script="/labs/Wegrzyn/gFACs/gFACs.pl"

if [ $(($SLURM_ARRAY_TASK_ID % 2)) -eq 0 ]
then

options="--splice-rescue \
--statistics \
--statistics-at-every-step \
--splice-table \
--create-gtf"

else

options="--splice-rescue \
--statistics \
--statistics-at-every-step \
--splice-table \
--unique-genes-only \
--get-fasta-without-introns \
--get-fasta-with-introns \
--min-intron-size 10 \
--min-exon-size 10 \
--mins-CDS-size 300 \
--rem-start-introns \
--rem-end-introns \
--get-protein-fasta \
--create-gtf"

fi

mkdir ${out_arr[$SLURM_ARRAY_TASK_ID]}


echo $SLURM_ARRAY_TASK_ID

perl $script \
-f braker_2.1.5_gff3 \
$options \
--fasta $genome \
-O ${out_arr[$SLURM_ARRAY_TASK_ID]} \
$annotation/${run_arr[$SLURM_ARRAY_TASK_ID]}/$gfffilename
