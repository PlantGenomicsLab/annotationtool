#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero

### PROKKA WILL ASSUME ALL MONOEXONICS

open (GFF, $ARGV[0])
	or die "No input GFF specified!!!";			# This is the input file specified by gstats.pl
open (OUTPUT, $ARGV[1])
	or die "No out output destination specified!!!";	# This is the output destination specified by gstats.pl

@GFF = <GFF>;
$GFF = join "", @GFF;
@GFF = split "\n", $GFF;						

$temp_loc = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "temp_file.txt";
open (TEMPORARY_OUTFILE, ">$temp_loc");
	foreach $line (@GFF){
		$line =~ s/\s+$//;
		@split_line = split "\t", $line;
		if ($split_line[2] =~ /CDS/){
			print TEMPORARY_OUTFILE "###", "\n",
				$line, "\n";
		}
		else{print TEMPORARY_OUTFILE $line, "\n";}
	}
close TEMPORARY_OUTFILE;
close GFF;

open (GFF, "$temp_loc") or die "Cannot find temp_file.txt at $ARGV[1]!!!";
@GFF = <GFF>;
$GFF = join "", @GFF;
@GFF = split "###", $GFF;

$gene_table = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "gene_table.txt";
open (GENE_TABLE, ">$gene_table") or die "Cannot find gene_table.txt at $ARGV[1]!!!";
print GENE_TABLE "###", "\n";

foreach $gene (@GFF){
	$gene =~ s/\s+$//g;
	@lines = split "\n", $gene;

	foreach $line (@lines){
		@split_line = split "\t", $line;
		if ($split_line[2] =~ /CDS/){
			@colon = split ";", $split_line[8];
			$colon[0] =~ s/ID\=//;
			$gene_length = $split_line[4] - $split_line[3] + 1;

			print GENE_TABLE "gene",			# gene
				"\t", $gene_length,				# length 
				"\t", $split_line[3], 				# start
				"\t", $split_line[4], 				# stop
				"\t", $split_line[6], 				# strand
				"\t", $colon[0], 				# ID
				"\t", $split_line[0],
				"\n";
		
			 print GENE_TABLE "exon",                       # exon
				"\t", $gene_length,				# length
                                "\t", $split_line[3],                           # start
                                "\t", $split_line[4],                           # stop
                                "\t", $split_line[6],                           # strand
                                "\t", $colon[0],                           # ID
				 "\t", $split_line[0],
                                "\n";
		}
	}	
print GENE_TABLE "###", "\n";
}		
close GENE_TABLE;
close GFF;

