#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
#$ARGV[0] --> gene table input
#$ARGV[1] --> output location
#$ARGV[2] --> prefix

open (GENE_TABLE, $ARGV[0]) or die "Cannot open your input!!!";

$temp_loc = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "temp_file.txt";

open (TEMPORARY_OUTFILE, ">$temp_loc") or die "Cannot create $temp_loc";
	while ($line = <GENE_TABLE>){
		print TEMPORARY_OUTFILE $line;
	}

close GENE_TABLE;
close TEMPORARY_OUTFILE;

open (TABLE, "$temp_loc") or die "Cannot find temp_file.txt at $ARGV[1]!!!";
$gene_table = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "gene_table.txt";
	
open (GENE_TABLE, ">$gene_table") or die "Cannot create gene_table.txt at $ARGV[1]";
print GENE_TABLE "###", "\n";

	while ($line = <TABLE>){
		print GENE_TABLE $line;
	}

close TABLE;
close GENE_TABLE;


