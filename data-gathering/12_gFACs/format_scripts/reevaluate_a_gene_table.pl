#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero

open (GENE_TABLE, $ARGV[0]);

@GENE_TABLE = <GENE_TABLE>;
$gene_table = join "", @GENE_TABLE;
@GENE_TABLE = split "###", $gene_table;

foreach $gene (@GENE_TABLE){
        $gene =~ s/\s+$//;
        @split_lines = split "\n", $gene;
        @tab = split "\t", $gene;
                if ($tab[4] =~ /\+/){$positive_strand_genes++;}
                if ($tab[4] =~ /\-/){$negative_strand_genes++;}

        if (scalar @split_lines <= 3){
                $monoexonic_gene_count++;                                                               #Number of mono-exonic genes
                @tab = split "\t", $gene;
                        if ($tab[4] =~ /\+/){$positive_strand_monoexonicgenes++;}
                        if ($tab[4] =~ /\-/){$negative_strand_monoexonicgenes++;}

                if ($tab[0] =~ /gene/){
                        $mono_exonic_gene_sizes = $tab[1] + $mono_exonic_gene_sizes;
                        push @mono_exonic_gene_sizes, $tab[1];
                }

        }
        if (scalar @split_lines > 3){
                $multiexonic_gene_count++;                                                              #Number of multi-exonic genes
                        @tab = split "\t", $gene;
                        if ($tab[4] =~ /\+/){$positive_strand_multiexonicgenes++;}
                        if ($tab[4] =~ /\-/){$negative_strand_multiexonicgenes++;}

                foreach $line (@split_lines){
                        @tab = split "\t", $line;
                        if ($tab[0] =~ /exon/){
                                $exon_count++;
                                $exon_count_sum++;
                                $multiexonic_exons++;
                                $multiexonic_exon_sizes = $tab[1] + $multiexonic_exon_sizes;
                                push @multi_exonic_exon_sizes, $tab[1];
                        }
                        if ($tab[0] =~ /intron/){
                                $intron_count++;
                                $intron_count_sum++;
                                $multiexonic_introns++;
                                $multiexonic_intron_sizes = $tab[1] + $multiexonic_intron_sizes;
                                push @multi_exonic_intron_sizes, $tab[1];
                        }
                        if ($tab[0] =~ /gene/){
                                $multiexonic_gene_sizes = $tab[1] + $multiexonic_gene_sizes;
                                push @multi_exonic_gene_sizes, $tab[1];
                        }
                }
	 push @exon_count, $exon_count;
        $exon_count = 0;
        push @intron_count, $intron_count;
        $intron_count = 0;
        }
}

##############################################################################
# Average and median multiexonic intron, exon, and gene size.


$average_monoexonic_gene_size = $mono_exonic_gene_sizes / $monoexonic_gene_count;
        @sorted_mono_exonic_gene_sizes = sort {$a <=> $b} @mono_exonic_gene_sizes;
        $middle_of_sorted_mono_exonic_gene_sizes = scalar @sorted_mono_exonic_gene_sizes / 2;
        if ($middle_of_sorted_mono_exonic_gene_sizes =~ /\./){
                $median_value_int = int($middle_of_sorted_mono_exonic_gene_sizes) ;
                $median_value_red = int($middle_of_sorted_mono_exonic_gene_sizes)  +1;
                $median_monoexonic_gene_size = ($sorted_mono_exonic_gene_sizes[$median_value_int] + $sorted_mono_exonic_gene_sizes[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_mono_exonic_gene_sizes =~ /\d+$/){
                $median_value = $middle_of_sorted_mono_exonic_gene_sizes;
                $median_monoexonic_gene_size = $sorted_mono_exonic_gene_sizes[$median_value];
        }


$average_multiexonic_exon_size = $multiexonic_exon_sizes / $multiexonic_exons;
        @sorted_multi_exonic_exon_sizes = sort {$a <=> $b} @multi_exonic_exon_sizes;
        $middle_of_sorted_multi_exonic_exon_sizes = scalar @sorted_multi_exonic_exon_sizes / 2;
        if ($middle_of_sorted_multi_exonic_exon_sizes =~ /\./){
                $median_value_int = int($middle_of_sorted_multi_exonic_exon_sizes) ;
                $median_value_red = int($middle_of_sorted_multi_exonic_exon_sizes)  +1;
                $median_multiexonic_exon_size = ($sorted_multi_exonic_exon_sizes[$median_value_int] + $sorted_multi_exonic_exon_sizes[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_multi_exonic_exon_sizes =~ /\d+$/){
                $median_value = $middle_of_sorted_multi_exonic_exon_sizes;
                $median_multiexonic_exon_size = $sorted_multi_exonic_exon_sizes[$median_value];
        }
$average_exons_per_multiexonic_gene =  $exon_count_sum / $multiexonic_gene_count;
        @sorted_exon_count = sort {$a <=> $b} @exon_count;
        $middle_of_sorted_exon_count = scalar @sorted_exon_count / 2;
        if ($middle_of_sorted_exon_count =~ /\./){
                $median_value_int = int($middle_of_sorted_exon_count) ;
                $median_value_red = int($middle_of_sorted_exon_count)  +1;
                $median_exons_per_multiexonic_gene = ($sorted_exon_count[$median_value_int] + $sorted_exon_count[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_exon_count =~ /\d+$/){
                $median_value = $middle_of_sorted_exon_count;
                $median_exons_per_multiexonic_gene = $sorted_exon_count[$median_value];
        }

        $smallest_exon =  $sorted_multi_exonic_exon_sizes[0];
        $largest_exon =  $sorted_multi_exonic_exon_sizes[scalar @sorted_multi_exonic_exon_sizes - 1];



$average_multiexonic_intron_size = $multiexonic_intron_sizes / $multiexonic_introns;
        @sorted_multi_exonic_intron_sizes = sort {$a <=> $b} @multi_exonic_intron_sizes;
        $middle_of_sorted_multi_exonic_intron_sizes = scalar @sorted_multi_exonic_intron_sizes / 2;
        if ($middle_of_sorted_multi_exonic_intron_sizes =~ /\./){
                $median_value_int = int($middle_of_sorted_multi_exonic_intron_sizes) ;
                $median_value_red = int($middle_of_sorted_multi_exonic_intron_sizes)  +1;
                $median_multiexonic_intron_size = ($sorted_multi_exonic_intron_sizes[$median_value_int] + $sorted_multi_exonic_intron_sizes[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_multi_exonic_intron_sizes =~ /\d+$/){
                $median_value = $middle_of_sorted_multi_exonic_intron_sizes;
                $median_multiexonic_intron_size = $sorted_multi_exonic_intron_sizes[$median_value];
        }

        $smallest_intron =  $sorted_multi_exonic_intron_sizes[0];
        $largest_intron =  $sorted_multi_exonic_intron_sizes[scalar @sorted_multi_exonic_intron_sizes - 1];

$average_introns_per_multiexonic_gene =  $intron_count_sum / $multiexonic_gene_count;
        @sorted_intron_count = sort {$a <=> $b} @intron_count;
        $middle_of_sorted_intron_count = scalar @sorted_intron_count / 2;
        if ($middle_of_sorted_intron_count =~ /\./){
                $median_value_int = int($middle_of_sorted_intron_count) ;
                $median_value_red = int($middle_of_sorted_intron_count)  +1;
                $median_introns_per_multiexonic_gene = ($sorted_intron_count[$median_value_int] + $sorted_intron_count[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_intron_count =~ /\d+$/){
                $median_value = $middle_of_sorted_intron_count;
                $median_introns_per_multiexonic_gene = $sorted_intron_count[$median_value];
        }



$average_multiexonic_gene_size = $multiexonic_gene_sizes / $multiexonic_gene_count;
        @sorted_multi_exonic_gene_sizes = sort {$a <=> $b} @multi_exonic_gene_sizes;
        $middle_of_sorted_multi_exonic_gene_sizes = scalar @sorted_multi_exonic_gene_sizes / 2;
        if ($middle_of_sorted_multi_exonic_gene_sizes =~ /\./){
                $median_value_int = int($middle_of_sorted_multi_exonic_gene_sizes) ;
                $median_value_red = int($middle_of_sorted_multi_exonic_gene_sizes)  +1;
                $median_multiexonic_gene_size = ($sorted_multi_exonic_gene_sizes[$median_value_int] + $sorted_multi_exonic_gene_sizes[$median_value_red]) /2;
        }
        elsif($middle_of_sorted_multi_exonic_gene_sizes =~ /\d+$/){
                $median_value = $middle_of_sorted_multi_exonic_gene_sizes;
                $median_multiexonic_gene_size = $sorted_multi_exonic_gene_sizes[$median_value];
        }




###################################################################################################################################################################
# STATS SECTION

###################################################################################################################################################################
# STATS SECTION

print "GTF STATS:\n";
print "Number of mono-exonic genes:\t", $monoexonic_gene_count, "\n";
print "Number of multi-exonic genes:\t", $multiexonic_gene_count, "\n";

print "\nNumber of positive strand genes:\t", $positive_strand_genes, "\n";
        print "\tMonoexonic:\t",  $positive_strand_monoexonicgenes, "\n";
        print "\tMultiexonic:\t",  $positive_strand_multiexonicgenes, "\n";
print "\nNumber of negative strand genes:\t", $negative_strand_genes, "\n";
        print "\tMonoexonic:\t",  $negative_strand_monoexonicgenes, "\n";
        print "\tMultiexonic:\t",  $negative_strand_multiexonicgenes, "\n";

print "\nAverage size of mono-exonic genes:\t", $average_monoexonic_gene_size, "\n";
print "Median size of mono-exonic genes:\t", $median_monoexonic_gene_size, "\n";
print "Largest monoexonic gene:\t", $sorted_mono_exonic_gene_sizes[scalar $sorted_mono_exonic_gene_sizes - 1], "\n";
print "Smallest monoexonic gene:\t", $sorted_mono_exonic_gene_sizes[0], "\n";


print "\nAverage size of multi-exonic genes:\t", $average_multiexonic_gene_size, "\n";
print "Median size of multi-exonic genes:\t", $median_multiexonic_gene_size, "\n";
print "Largest multiexonic gene:\t", $sorted_multi_exonic_gene_sizes[scalar $sorted_multi_exonic_gene_sizes - 1], "\n";
print "Smallest multiexonic gene:\t", $sorted_multi_exonic_gene_sizes[0], "\n";


print "\nAverage size of multi-exonic exons:\t", $average_multiexonic_exon_size, "\n";
print "Median size of multi-exonic exons:\t", $median_multiexonic_exon_size, "\n";
print "Average size of multi-exonic introns:\t", $average_multiexonic_intron_size, "\n";
print "Median size of multi-exonic introns:\t", $median_multiexonic_intron_size, "\n";


print "\nAverage number of exons per multiexonic gene:\t", $average_exons_per_multiexonic_gene, "\n";
print "Median number of exons per multiexonic gene:\t", $median_exons_per_multiexonic_gene, "\n";
print "Largest multiexonic exon:\t", $largest_exon, "\n";
print "Smallest multiexonic exon:\t", $smallest_exon, "\n";
$scalar = scalar @sorted_exon_count -1;
print "Most exons in one gene:\t", $sorted_exon_count[$scalar], "\n";

print "\nAverage number of introns per multiexonic gene:\t", $average_introns_per_multiexonic_gene, "\n";
print "Median number of introns per multiexonic gene:\t", $median_introns_per_multiexonic_gene, "\n";
print "Largest intron:\t", $largest_intron, "\n";
print "Smallest intron:\t", $smallest_intron, "\n\n\n";


