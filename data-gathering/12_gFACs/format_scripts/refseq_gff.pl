#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
#################################################################################################################################################################

#ARGV[1] --> output destination
#ARGV[2] --> prefix

open (GFF, $ARGV[0]) or die "No input gff specified!!!";                   
	@GFF = <GFF>;
	$GFF = join "", @GFF;
	@GFF = split "\n", $GFF;							
close GFF;

$temp_loc = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "temp_file.txt";
open (TEMPORARY_OUTFILE, ">$temp_loc");
	foreach $line (@GFF){
		$line =~ s/\s+$//;
		if ($line =~ /^#/){ next;}
                else{
			@split_line = split "\t", $line;
			#used to be ^RNA$
			if ($split_line[2] =~ /^gene$/ or $split_line[2] =~ /^RNA$/){
					print TEMPORARY_OUTFILE "\n", "###", "\n", $line;		
			}
			if ($split_line[2] =~ /CDS/){
				print TEMPORARY_OUTFILE "\n", $line;
			}	                        
                }		
	}
close TEMPORARY_OUTFILE;
#################################################################################################################################################################
open (GFF, "$temp_loc") or die "Cannot find temp_file.txt at $ARGV[1]!!!";
	@GFF = <GFF>;
	$GFF = join "", @GFF;
	@GFF = split "###", $GFF;
close GFF;

$gene_table = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "gene_table.txt";
open (GENE_TABLE, ">$gene_table") or die "Cannot find gene_table.txt at $ARGV[1]!!!";
	print GENE_TABLE "###", "\n";

GENE: foreach $gene (@GFF){ if ($gene =~ /\tCDS\t/){
	$gene =~ s/^\s+$//g;
	$gene =~ s/\s+$//g;
	@lines = split "\n", $gene;

	foreach $line (@lines){
		@split_line = split "\t", $line;
		@split_colon = split ";", $split_line[8];
		$ID_split_colon = $split_colon[0];
		if ($split_line[2] =~ /gene/){	
			$info = $split_colon[0] . $split_colon[1];
			$strand = $split_line[6];
			$scaff = $split_line[0];	
			$start = $split_line[3];
			$stop = $split_line[4];
		}
		if ($split_line[2] =~ /CDS/){
			$exon_length = $split_line[4] - $split_line[3] + 1;

			$split_line[8] =~ s/transcript_id\s\"//;
			$split_line[8] =~ s/\"\;\sgene_id.+$//;
			$CDS_line = "exon" .                       # exon
				"\t" . $exon_length .				# length
                                "\t" . $split_line[3] .                           # start
                                "\t" . $split_line[4] .                           # stop
                                "\t" . $split_line[6] .                         # strand
                                "\t" . $split_colon[1] .                           # ID
				"\t" . $split_line[0] .
                                "\n";

			push @exons, $split_line[3];
			push @exons, $split_line[4];
				
				if ($split_line[6] =~ /\+/){
					push @CDS_lines, $CDS_line;
				}
				if ($split_line[6] =~ /\-/){
                                        unshift @CDS_lines, $CDS_line;
                                }
		}
	}
	@exons_sorted = sort {$a <=> $b} @exons;
	$gene_length = $exons_sorted[scalar @exons_sorted - 1] - $exons_sorted[0] +1;
	if ($gene =~ /\d/ and scalar @CDS_lines > 0){
		print GENE_TABLE "gene",
		"\t", $gene_length,
		"\t", $exons_sorted[0],
		"\t", $exons_sorted[scalar @exons_sorted - 1],
		"\t", $strand,
		"\t", $info,
		"\t", $scaff,
		"\n";
	}
	if (scalar @CDS_lines ==0){
		 if ($gene =~ /gene/){
			$gene_length = $stop - $start +1;
			 print GENE_TABLE "gene",
                        "\t", $gene_length,
                        "\t", $start,
                        "\t", $stop,
                        "\t", $strand,
                        "\t", $info,
                        "\t", $scaff,
                        "\n";
		
			print GENE_TABLE "exon",
        	        "\t", $gene_length,
                	"\t", $start,
	                "\t", $stop,
        	        "\t", $strand,
                	"\t", ".",
	                "\t", $scaff,
        	        "\n", "###", "\n";
			next GENE;
		}
	}
	foreach $CDS (@CDS_lines){
		print GENE_TABLE $CDS;
	}
	undef(@CDS_lines);
	if (scalar @exons >= 4){
                @exons_sorted = sort {$a <=> $b} @exons;
                # 10    20      30      40      50      60
                #[0]    [1]-----[2]     [3]-----[4]     [5]
                # Intron = [2] - [1] and [4] - [3]

                for ($a = 2; $a < scalar @exons_sorted; $a = $a +2){
			$end = $exons_sorted[$a] - 1;
                        $start = $exons_sorted[$a-1] + 1;
                        $intron_length = $end - $start + 1;
	                        print GENE_TABLE "intron",
	                                "\t", $intron_length,
	                                "\t", $start,
	                                "\t", $end,
        	                        "\t", $split_line[6],
					"\t", ".",
					"\t", $split_line[0],
	                                "\n";
                }
	}
	if(scalar @exons < 1){
		if ($gene =~ /.+/){
		 	@split_line = split "\t", $gene;
                        $gene_length = $split_line[4] - $split_line[3] + 1;
                        print GENE_TABLE "exon",	                        # gene
                                "\t", $gene_length,                             # length
                                "\t", $split_line[3],                           # start
                                "\t", $split_line[4],                           # stop
                                "\t", $split_line[6],                           # strand
                            	"\t", "\.",
				"\t", $scaff,                           # scaff
                                "\n";
		}
	}
			
print GENE_TABLE "###", "\n";
undef (@exons);
undef (@introns);
}		
}
close GENE_TABLE;

