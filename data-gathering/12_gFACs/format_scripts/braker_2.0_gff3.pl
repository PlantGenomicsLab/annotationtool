#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
open (GFF3, $ARGV[0])
	 or die "No input gff3 specified!!!";                    # This is the input file specified by gstats.pl
open (OUTPUT, $ARGV[1])
        or die "No out output destination specified!!!";        # This is the output destination specified by gstats.pl

@GFF3 = <GFF3>;
$GFF3 = join "", @GFF3;
@GFF3 = split "\n", $GFF3;							

#$temp_loc = "$ARGV[1]\/temp_file.txt";
$temp_loc = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "temp_file.txt";

open (TEMPORARY_OUTFILE, ">$temp_loc");
	foreach $line (@GFF3){
		$line =~ s/\s+$//;
		@split_line = split "\t", $line;
		if ($split_line[2] =~ /gene/){
			print TEMPORARY_OUTFILE "###", "\n",
				$line, "\n";
		}
		else{print TEMPORARY_OUTFILE $line, "\n";}
	}
close TEMPORARY_OUTFILE;
close GFF3;

open (GFF3, "$temp_loc") or die "Cannot find temp_file.txt at $ARGV[1]!!!";
@GFF3 = <GFF3>;
$GFF3 = join "", @GFF3;
@GFF3 = split "###", $GFF3;

$gene_table = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "gene_table.txt";

open (GENE_TABLE, ">$gene_table") or die "Cannot find gene_table.txt at $ARGV[1]!!!";
print GENE_TABLE "###", "\n";

foreach $gene (@GFF3){
	$gene =~ s/\s+$//g;
	@lines = split "\n", $gene;

	foreach $line (@lines){
		@split_line = split "\t", $line;
		
		if ($split_line[2] =~ /gene/){
			$gene_length = $split_line[4] - $split_line[3] + 1;
			$split_line[8] =~ s/\;//;
			$split_line[8] =~ s/ID\=//;

			print GENE_TABLE $split_line[2],			# gene
				"\t", $gene_length,				# length 
				"\t", $split_line[3], 				# start
				"\t", $split_line[4], 				# stop
				"\t", $split_line[6], 				# strand
				"\t", $split_line[8], 				# ID
				"\t", $split_line[0],
				"\n";
		}
		if ($split_line[2] =~ /exon/){
			$exon_length = $split_line[4] - $split_line[3] + 1;

			$split_line[8] =~ s/\.exon.+$//;
			$split_line[8] =~ s/ID\=//;
			 print GENE_TABLE $split_line[2],                       # exon
				"\t", $exon_length,				# length
                                "\t", $split_line[3],                           # start
                                "\t", $split_line[4],                           # stop
                                "\t", $split_line[6],                           # strand
                                "\t", $split_line[8],                           # ID
				"\t", $split_line[0],
                                "\n";

			push @exons, $split_line[3];
			push @exons, $split_line[4];
			$Exon_hash{$split_line[3]} = "exon";
			$Exon_hash{$split_line[4]} = "exon";
		}
	}
	if (scalar @exons >= 4){
                @exons_sorted = sort {$a <=> $b} @exons;
                # 10    20      30      40      50      60
                #[0]    [1]-----[2]     [3]-----[4]     [5]
                # Intron = [2] - [1] and [4] - [3]

                for ($a = 2; $a < scalar @exons; $a = $a +2){
			$end = $exons_sorted[$a] - 1;
                        $start = $exons_sorted[$a-1] + 1;
                        $intron_length = $end - $start + 1;
			
	                        print GENE_TABLE "intron",
	                                "\t", $intron_length,
	                                "\t", $start,
	                                "\t", $end,
        	                        "\t", $split_line[6],
					"\t", ".",
					"\t", $split_line[0],
	                                "\n";
			
                        }
                }	
print GENE_TABLE "###", "\n";
undef (@exons);
undef (%Exon_hash);
undef (@introns);
}		
close GENE_TABLE;
close GFF3;

