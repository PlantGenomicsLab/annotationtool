#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
open (GFF3, $ARGV[0])
	or die "No input gff3 specified!!!";                    # This is the input file specified by gstats.pl
open (OUTPUT, $ARGV[1])
        or die "No out output destination specified!!!";        # This is the output destination specified by gstats.pl

@GFF3 = <GFF3>;
$gff3 = join "", @GFF3;
@GFF3 = split "###", $gff3;

#$temp_loc = "$ARGV[1]\/temp_file.txt";
$temp_loc = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "temp_file.txt";
open (TEMP, ">$temp_loc");
close TEMP;

$gene_table =  $ARGV[1] . "\/" . "\/" . $ARGV[2] . "gene_table.txt";
open (GENE_TABLE, ">$gene_table") or die "Cannot find gene_table.txt at $ARGV[1]!!!";
print GENE_TABLE "###", "\n";

foreach $gene (@GFF3){
	$gene =~ s/\s+$//g;
	@lines = split "\n", $gene;

	foreach $line (@lines){
		@split_line = split "\t", $line;
		
		if ($split_line[2] =~ /gene/){
			$gene_length = $split_line[4] - $split_line[3] + 1;
			$split_line[8] =~ s/^.+Name\=//;
			print GENE_TABLE $split_line[2],			# gene
				"\t", $gene_length,				# length 
				"\t", $split_line[3], 				# start
				"\t", $split_line[4], 				# stop
				"\t", $split_line[6], 				# strand
				"\t", $split_line[8], 				# ID
				"\t", $split_line[0],
				"\n";
		}
		if ($split_line[2] =~ /exon/){
			$exon_length = $split_line[4] - $split_line[3] + 1;
			 $split_line[8] =~ s/ID\=//;
	 		 $split_line[8] =~ s/\.exon\d+\;Name\=.+$//;
			 $exon_line = $split_line[2] .                        # exon
				"\t" . $exon_length .				# length
                                "\t" . $split_line[3] .                           # start
                                "\t" . $split_line[4] .                           # stop
                                "\t" . $split_line[6] .                           # strand
                                "\t" . $split_line[8] .                           # ID
				"\t" . $split_line[0] .
                                "\n";

			if ($split_line[6] =~ /\+/){
				push @exon_lines, $exon_line;
			}
			if ($split_line[6] =~ /\-/){
                                unshift @exon_lines, $exon_line;
                        }
			push @exons, $split_line[3];
			push @exons, $split_line[4];
		}
	}
	foreach $exon (@exon_lines){
		print  GENE_TABLE $exon;
	}
	undef (@exon_lines);
	if (scalar @exons >= 4){
		@exons_sorted = sort {$a <=> $b} @exons;
		# 10	20	30	40	50	60
		#[0]	[1]-----[2]	[3]-----[4]	[5]	
		# Intron = [2] - [1] and [4] - [3]
			
		for ($a = 2; $a < scalar @exons_sorted; $a = $a +2){
			$end = $exons_sorted[$a] - 1;
                        $start = $exons_sorted[$a-1] + 1;
                        $intron_length = $end - $start + 1;
				
			print GENE_TABLE "intron",
				"\t", $intron_length,
				"\t", $start,
				"\t", $end,
				"\t", $split_line[6],
				"\t", ".",
				"\t", $split_line[0],
				"\n";
			}
		}
	
print GENE_TABLE "###", "\n";
undef (@exons);
}		
close GENE_TABLE;
close GFF3;
