#!usr/bin/perl
######################################################################################################################################################################
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
#    This file is part of gFACs.
#
#    gFACs is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    gFACs is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with gFACs.  If not, see <https://www.gnu.org/licenses/>.
######################################################################################################################################################################
######################################################################################################################################################################
$datestring = localtime();
$version = "07\/06\/2018";
######################################################################################################################################################################
#	gFACs - Gene filtering, analysis, and conversion. 
#	Madison.Caballero@uconn.edu
#
#	About this script:
#		This script is designed to provide statistics based off annotation outputs such as .gft, .gff3, .gff2, and .gff3 files. To do this, the input 
#		script is evaluated and provided the proper supporting script to create accurate statistics. 
#
######################################################################################################################################################################
######################################################################################################################################################################
$INPUT = join " ", @ARGV;									# This is the command
$infile = scalar @ARGV - 1;									# Infile is last in command
$output = scalar @ARGV - 2;									# Output location is second to last in command
$unique_done = 0;										# Value set
######################################################################################################################################################################
######################################################################################################################################################################
# THIS IS THE MANUAL that prints to command line
if (scalar @ARGV == 0){
	print 
		"gFACs\.\pl\n",
		"Contact\:\tMadison.Caballero\@uconn\.edu\n\n",
	        "Version:", $version, "\tUpdated:09/13/2018\n",
		"-------------------------------------------------------------GFACS MANUAL-------------------------------------------------------------\n\n",
		"   Welcome to gFACs, the one stop destination to understand what your annotation or alignment is secretly telling you. The script gFACs.pl\n",
		"is a task-assigning script that calls upon many different scripts to analyze your .gff3, .gff, or .gtf files by creating a median format\n",
		"called gene_table.txt. From here, statisitcs can be calculated and distribution tables created. Mutliple filter paramteters can be used to\n",
		"create an annotation or alignment you can be proud of.",
		"\n\n",

		"To run this script\:\n",
		"\tperl gFACs\.pl \-f \<format\> \[options\] \-O \<output directory\> \<input\_file\>\n\n",

		"Mandatory inputs\:\n",
		"\t\-f \<format\>\t\t I need to know the format of your input. All gff/gtf/gff3 files are different so I need to call\n",
			"\t\t\t\t\tthe right script.\n",
		"\t\-O \<output directory\>\t I need to know where to put the output files since the script runs in a shared space. Please write\n",
			"\t\t\t\t\tthe output ending with a \/ as the SECOND TO LAST argument\n",
		"\t\<input\_file\>\t\t There needs to be an input .gff3, .gtf, or .gff as the LAST argument.\n",		

		"\nOutput files always created:\n",
		"\tgFACs_log.txt : What's happening\n",
		"\tgene_table.txt : A very readable table with information from your input",

		"\n\nSupported formats\:\n\n",
	
		"\t\-f \[format\]\t",
		"Specifying a format\: A mandatory step to call upon the right script.\n",	
		"\t\tAvailable formats\:\n",
	"\t\tBRAKER:\n",
		"\t\t\tbraker_2.05_gtf\n",						# augustus version 3.2.3 gft for braker	
		"\t\t\tbraker_2.05_gff\n",	 					# augustus version 3.2.3 gff for braker
		"\t\t\tbraker_2.05_gff3\n",                                             # augustus version 3.2.3 gff3 for braker
		"\t\t\tbraker_2.0_gff3\n",						# braker version 2.0 gff3
		"\t\t\tbraker_2.0_gff\n",						# braker_2.0_gff
		"\t\t\tbraker_2.0_gtf\n",						# braker_2.0_gtf
		#"\t\t\taugustus_unknown_gtf\n",					# augustus_unknown_gtf
	"\t\tMAKER:\n",
		"\t\t\tmaker_2.31.9_gff\n",						# maker 2.31.9 gff
	"\t\tPROKKA:\n",
		"\t\t\tprokka_1.11_gff\n",
	"\t\tGMAP:\n",
                "\t\t\tgmap_2017_03_17_gff3\n",                                         # gmap version 2017/03/17 gff3 for braker
	"\t\tGENOME THREADER:\n",
		"\t\t\tgenomethreader_1.6.6_gff3\n",					# genomethreader_1.6.6_gff3
	"\t\tGFFREAD:\n",
		"\t\t\tgffread_0.9.12_gff3\n",						# gffread_0.9.12_gff3
	"\t\tEXONERATE:\n",
		"\t\t\texonerate_2.4.0_gff\n",						# exonerate_2.4.0_gff
	"\t\tEVIDENCE MODELER:\n",						
		"\t\t\tEVM_1.1.1_gff3\n",
	"\t\tGFACS:\n",
		"\t\t\tgFACs_gene_table\n",
		"\t\t\tgFACs_gtf\n",
	"\t\tNCBI:\n",
		"\t\t\trefseq_gff\n",							# GFF / GFF3
		"\t\t\tgenbank_gbff - UNDER REPAIR\n",					# Unique format
		"\n\nAdditional parameters you can include\:\n\n",

######FLAGS######

		"\t-p [prefix]\t\tAdds a prefix to every file created.",
		"\n\n",

		"\t\--statistics\t\tCreates the file statistics.txt file full of all sorts of relevant information. Statistics will always be\n",
		"\t\t\t\tperformed on the final gene set as directed by the flags and filters chosen.",
		"\n\n",

		"\t--statistics-at-every-step\tPerforms and prints statistics after every filtering step into the log. For the final statistics.txt file,\n",
		"\t\t\t\t\tbe sure to also include --statisitcs in your command.",
		"\n\n",

		"\t\-\-splice-rescue\t\tGenes with overlapping transcripts that are splice variants or incompletes will be retrieved and added\n",
		"\t\t\t\tinto gene_table.txt modified with a .version.",
		"\n\n",

		"\t--rem-start-introns\tGenes that begin (5' end) with an intron are removed. Remaining genes are kept in gene_table.txt",
		"\n\n",

		"\t--rem-end-introns\tGenes that end (3' end) with an intron are removed. Remaining genes are kept in gene_table.txt",
		"\n\n",

		"\t--rem-extra-introns\tPerforms the tasks of --rem-start-introns and --rem-end-introns",
		"\n\n",

		"\t\-\-rem\-monoexonics\tMonoexonic genes are removed from gene_table.txt. Monoexonic genes are then not reflected in statistics,\n",
		"\t\t\t\tdistributions, or any fasta commands.",
		"\n\n",

		"\t--rem-multiexonics\tMultiexonic genes are removed from gene_table.txt. Multiexonic genes are then not reflected in statistics,\n",
		"\t\t\t\tdistributions, or any fasta commands.",
		"\n\n",
	
		"\t--min-exon-size [number]\tRemoves genes that have exons smaller than a provided size. Default is 20. Remaining genes are kept in gene_table.txt",
		"\n\n",

		"\t--min-intron-size [number]\tRemoves genes that have introns smaller than a provided size. Default is 20. Remaining genes are kept in gene_table.txt",
                "\n\n",
		
		"\t--min-CDS-size [number]\t\tRemoves genes that have a coding sequence (CDS) smaller than a provided size. Default is 74. Remaining genes are kept\n",
		"\t\t\t\t\tin gene_table.txt",
		"\n\n",

		"\t--unique-genes-only\tTranscripts representing the same gene are reduced to one unique gene. The largest transcript is chosen, if available,\n",
                "\t\t\t\totherwise the first transcript is chosen. In the case of originally separate genes, the first one is chosen.",
                "\n\n",

	"\nFlags that require that you input an entap tsv file:\n\n",
		
		"\t--entap-annotation \[path/to/your/entap/annotation.tsv\]\tCurrently only operational for braker-derived annotations. Must match your input\n",
		"\t\t\t\t\t\t\t\tfile otherwise nothing will pass.",
		"\n\n",

		"\tFlags you can include once entap table is specified:",
		"\n\n",

		"\t\t--annotated-ss-genes-only\tOnly genes that have an associated similarity search entap annotation will remain. Passing genes are\n",
		"\t\t\t\t\t\tkept in gene_table.txt.",
		"\n\n",

		"\t\t--annotated-all-genes-only\tOnly genes that have either an associated similarity search or EggNOG entap annotation will remain.\n",
		"\t\t\t\t\t\tPassing genes are kept in gene_table.txt.",
                "\n\n",

	"\nFlags that require that you input a fasta file because seqeunce is being involved:",
		"\n\*\*\*NOTE: DO NOT RUN FASTA FLAGS WITHOUT --splice-rescue. Genetic coordinates may disrupt bioperl commands!\*\*\*",
		"\n\n",

		"\t--fasta \[/path/to/your/nucleotide/fasta.fasta\]\t\tThis must be the same fasta your gff3/gff/gtf is based on otherwise flags will fail!\n",
		"\t\t\t\t\t\t\t\tThe index will be created where the fasta is and have the postfix .idx.",		
		"\n\n",

	"\tFlags you can include once fasta is specified:\n\n",

		"\t\t--splice-table\t\tIntron splice types are surveyed and printed to a splice:count table called splice_table.txt.",		
		"\n\n",
		
		"\t\t--canonical-only\tGenes without canonical (gt-ag) splice sites are filtered out. Monoexonics will remain (to remove them,\n",
			"\t\t\t\t\tuse --rem-monoexonics).",
		"\n\n",
		
		"\t\t--rem-genes-without-start-codon\t\tGenes that have a start codon are kept in gene_table.txt. Those that do not are discarded.",
		"\n\n",

		"\t\t--rem-genes-without-stop-codon\t\tGenes that have a stop codon are kept in gene_table.txt. Those that do not are discarded.",
		"\n\n",

		"\t\t--allowed-inframe-stop-codons [number]\t\tRemoves genes that have more than the specified inframe stop codons. Default is 0, as the last codon\n",
                "\t\t\t\t\t\t\t\tdoes not count. Remaining genes are kept in gene_table.txt",
                "\n\n",

		"\t\t--nt-content\tGC/AT and N content statistics are printed to the log on CDS sequence.",
		"\n\n",

		"\t\t--get-fasta-with-introns\tNucleotide fasta creator, introns included. Makes the file genes_with_introns.fasta from the final filtered", "\n",
		"\t\t\t\t\t\tversion of gene_table.txt. Header names are the fifth column of gene_table.txt (typically the gene ID).",
		"\n\n",

                "\t\t--get-fasta-without-introns\tNucleotide fasta creator, introns excluded. Makes the file genes_without_introns.fasta from the final filtered", "\n",
                "\t\t\t\t\t\tversion of gene_table.txt. Header names are the fifth column of gene_table.txt (typically the gene ID).",
		"\n\n",

		"\t\t--get-protein-fasta\tProtein fasta creator, introns obviously excluded. Makes the file genes_without_introns.fasta.faa from the final filtered\n",
		"\t\t\t\t\tversion of gene_table.txt. Header names are the fifth column of gene_table.txt (typically the gene ID).",
		"\n\n",

		"\t\t--create-gtf\tA gtf file called out.gtf will be created with CDS, intron, gene, and start/stop codon information.\n",
		"\n\n",

 "\nOutput compatibility options:",
		"\n\n",
		
		"\t--compatibility \[option\] \[option\] etc\.\.\.\tAllows the creation of software compatible files. Available format options are:",
                "\n\n",

		"\t\tSnpEff\t\: A gff file called snpeff_format.gff will be created that can be used for SnpEff build.\n",
                "\n\n",

 "\nDistribution table creation:",
		"\n\n",
	
		"\t--distributions \[option\] \[option\] etc\.\.\.\tTurns on the creation of distribution tsv files. Available distribution options are:",
		"\n\n",
	
		"\t\texon_lengths\t\: Creates exon_lengths_distributions.tsv with exon lengths range and the corresponding representation.",
		"\n\n",

                "\t\tintron_lengths\t\: Creates intron_lengths_distributions.tsv with intron lengths range and the corresponding representation.",
		"\n\n",
	
		"\t\tCDS_lengths\t\: Creates CDS_lengths_distributions.tsv with CDS lengths range and the corresponding representation.",
                "\n\n",

                "\t\tgene_lengths\t\: Creates gene_lengths_distributions.tsv with gene lengths range and the corresponding representation. Intron sequences are included.",
                "\n\n",

		"\t\texon_position\t\: Creates exon_position_distributions.tsv with information pertaining to exon order in a gene.",
		"\n\n",
		
                "\t\texon_position_data\t\: Creates data_exon_position_distributions.tsv with raw data on exon order in a gene.",
                "\n\n",

		"\t\tintron_position\t\t\: Creates intron_position_distributions.tsv with information pertaining to intron order in a gene.",
		"\n\n",

		"\t\tintron_position_data\t\: Creates data_intron_position_distributions.tsv with raw data on intron order in a gene.",
                "\n\n",
	
		"\tAdvanced: To control level of zoom on distributions, a value after exon_lengths, intron_lengths, CDS_lengths, and gene_lengths can be added.",
		"\n\t\tExample: exon_lengths 10",

		"\n\t\tResults: Changes the stepwise progression of groups to 10. (0-10, 11-20, 21-30). Default settings choose step based off range of data provided.",
		"\n\n\n\#\#\#\n\n",
	
		"Copyright (C) 2018 Madison Caballero. This file is part of gFACs.\n",	
		"\tgFACs is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by\n",
	    	"\tthe Free Software Foundation, either version 3 of the License, or (at your option) any later version. gFACs is distributed in the hope that\n",
		"\tit will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n",
		"\tSee the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with gFACs.\n",
		"\tIf not, see <https://www.gnu.org/licenses/>.",

	"\n\n";
		die "\n\n";
} 
###############################################################################################################################################################
###############################################################################################################################################################
# ACTUAL gFACs stuff:
# Prefix

if ($INPUT =~ /\s\-p\s+(.+?)\s+/){							# Specifying where the prefix is predicted to be
	$prefix = $1;
	$pre = $prefix . "_";
}
$log = "$ARGV[$output]\/\/" . $pre . "gFACs_log.txt";					# Naming the log
open (LOG, ">$log");									# Creating the log > style
close LOG;										# Closing the > log style log

open (LOG, ">>$log");									# Reopening the log >> style ('tis better)
	print LOG "gFACs LOG", "\n\n", "Version\: ", $version, "\n";			# Print log and version
	print LOG "Time of run: ", $datestring, "\n\n";					# Time stamp
	print LOG "Command:", $0, " ", $INPUT, "\n\n"; 					# Command

if ($prefix =~ /./){print LOG "You have specified a prefix\:\t", $prefix, "\n\n";}	# Prefix note
if ($INPUT =~ /\-f\s*(.+)\s*/){ print LOG "You have specfied a format\:\t";		# Format specifications

#############FORMAT DECISIONS#########################################################################################################
	
	if ($1 =~ /gmap_2017_03_17_gff3/){						# gmap_2017_03_17_gff3
		print LOG "GMAP version 2017\/03\/17 in gff3 format\n";
		$format = "gmap_2017_03_17_gff3";		
	}
	if ($1 =~ /braker\_2\.05\_gtf/){                                             	# braker_2.05_gtf
                print LOG "BRAKER version 2\.05 in gtf format\n";
		$format = "braker\_2\.05\_gtf";		
        }
	if ($1 =~ /braker\_2\.05\_gff\s/){                                             	# braker_2.05_gff
                $format = "braker\_2\.05\_gff";		
    		print LOG "BRAKER version 2\.05 in gff format\n";
	}
	if ($1 =~ /braker\_2\.05\_gff3/){                                  		# braker_2.05_gff3
               	  print LOG "BRAKER version 2\.05 in gff3 format\n";
        	  $format = "braker\_2\.05\_gff3";
	}
	if ($1 =~ /braker\_2\.0\_gff3/){						# braker_2.0_gff3
		print LOG "BRAKER version 2\.0 in gff3 format\n";
                $format = "braker\_2\.0\_gff3";
	}
	if ($1 =~ /augustus\_unknown\_gtf/){						# augustus unknown gtf
		print LOG "AUGUSTUS version UNKNOWN in gtf format\n";
                $format = "augustus\_unknown\_gtf";
	}
	if ($1 =~ /genomethreader\_1\.6\.6\_gff3/){					# genomethreader_1.6.6_gff3
		print LOG "GENOMETHREADER version 1.6.6 in gff3 format\n";
		$format = "genomethreader\_1\.6\.6\_gff3";
	}
	if($1 =~ /gffread\_0\.9\.12\_gff3/){						# gffread_0.9.12_gff3
		print LOG "GFFREAD version 0.9.12 in gff3 format\n";
		$format = "gffread\_0\.9\.12\_gff3";	
	}
	if ($1 =~ /braker\_2\.0\_gff\s/){
		print LOG "BRAKER version 2\.0 in gff format\n";			# braker_2.0_gff
		$format = "braker_2\.0_gff";
	}
	if ($1 =~ /braker\_2\.0\_gtf\s/){						# braker_2.0_gtf
		print LOG "BRAKER version 2\.0 in gtf format\n";		
		$format = "braker\_2\.0\_gff";
	}
	if ($1 =~ /exonerate\_2\.4\.0\_gff/){						#exonerate_2.4.0_gff
		print LOG "EXONERATE version 2\.4\.0 in gff format\n";
		$format = "exonerate\_2\.4\.0\_gff";
	}
	if ($1 =~ /maker\_2\.31\.9\_gff/){
		print LOG "MAKER version 2.31.9 in gff format\n";
		$format = "maker_2.31.9_gff";
	}
	if ($1 =~ /gFACs_gene_table/){
		print LOG "GFACS all version in gene_table.txt format\n";
		$format = "gFACs_gene_table";
	}
	if ($1 =~ /gFACs_gtf/){
		print LOG "GFACS all version in out.gtf format\n";
		$format = "gFACs_gtf";
	}
	if ($1 =~ /EVM_1.1.1_gff3/){
		print LOG "EVIDENCE MODELER version 1.1.1 in gff3 format\n";
		$format = "EVM_1.1.1_gff3";
	}
	if ($1 =~ /refseq_gff/){
		print LOG "NCBI REFSEQ GFF/GFF3 file format\n";
		$format = "refseq_gff";
	}
	if ($1 =~ /genbank_gbff/){
		print LOG "NCBI GENBANK GBFF file format\n";
		$format = "genbank_gbff";
	}
	if ($1 =~ /prokka_1.11_gff/){
		print LOG "PROKKA version 1.11 in gff format. Assuming only monoexonics!!\n";
		$format = "prokka_1.11_gff";
	}
	##########################################################################################################################
}
if ($INPUT !~ /\-f\s*(.+)\s*/){									# No format error and die
	print LOG "You have not specified a format.";
	die "You must specify a format!";
}
if ($INPUT =~ /\-O\s*$ARGV[$output]/){									# Output destination set up
	if ($ARGV[$output] =~ /.+$/){ print LOG "\nYou have specified an output directory: $ARGV[$output]\n";}
}
if ($INPUT !~ /\-O\s*$ARGV[$output]/){die "You must specify an output directory using the -O command. It must also be before your input file.\n";} 

#LOG OF FORMAT###################################################################################################################################################
#COMMAND#########################################################################################################################################################
#CONVERSION TO GENE TABLE

$0 =~ s/(.+)gFACs.pl//;
$loc = $1;
$infile = scalar @ARGV - 1;									
$gene_table = $ARGV[$output] . "\/" . "\/" . $pre . "gene_table.txt";				# Create the gene table name

push @command, "perl ";							# MANDATORY
push @command, "$loc";
push @command, "format_scripts\/";					# MANDATORY --> LOCATION OF FORMAT SCRIPTS
push @command, $format;							# MANDATORY --> FORMAT
push @command, "\.pl ";							# MANDATORY
push @command, "$ARGV[$infile] ";					# MANDATORY --> INFILE
push @command, "$ARGV[$output] ";					# MANDATORY --> outdir
push @command, "$pre ";							# MANDATORY --> prefix

$command = join "", @command;
system "$command";							# Doing the format command

print LOG "\n", "Format command:\t", $command, "\n";			# PRINT TO LOG THE COMMAND

## THE OUTPUT gene_table.txt HAS BEEN CREATED AT THIS POINT at the specified location!

###############################################################################################################################################################
## FLAG TO SCRIPT STATS

print LOG "\n\nFlags:\n\n";

if ($INPUT =~ /\-\-splice-rescue/){
	print LOG "--splice-resuce has been activated. I will look for overlapping gene space and see if it is the result of multiple transcripts mapping to the same location. Splice variants will be labeled as [gene].1, [gene].2, etc... and added to the end of gene_table.txt.";

	$string = "perl $loc" . "task_scripts/overlapping_exons.pl $gene_table $ARGV[$output] $pre >> $log";
        print LOG "\n\tCommand: ", "$string", "\n";
	print LOG "Results:\n";

	system ($string);
	$name = $ARGV[$output] . "\/" . "\/" . $pre . "non_overlapping_gene_table.txt";
	$string = "mv $name $gene_table";
	system ($string);

	$string = "perl $loc" . "task_scripts/splice_variants.pl $gene_table $ARGV[$output] $pre >> $log";

	print LOG "\tCommand: ", $string, "\n";
        system ($string);
	
	$name = $ARGV[$output] . "\/" . "\/" . $pre . "transcripts.txt";
        system "cat $name >> $gene_table";
	system "rm $name";
	#$name = $ARGV[$output] . "\/" . "\/" . $pre . "non_overlapping_gene_table.txt";
	#system "rm $name";
	$name = $ARGV[$output] . "\/" . "\/" . $pre . "overlapping_gene_table.txt";
	system "rm $name";
	
	if ($INPUT =~ /\-\-statistics-at-every-step/){
	        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
	        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
		print LOG "\n";
	        system ($string);
	}
}
#############################################################################################################################################################
#FORMAT CHECK
$string = "perl $loc" . "task_scripts/gene_table_fixer.pl $ARGV[$output] $pre";
$output_name = $ARGV[$output] . "\/" . $pre . "checked_gene_table.txt";

        print LOG "\nFormat Check!\n\tCommand: ", $string, "\n";
        system ($string);
        system "mv $output_name $gene_table";
#############################################################################################################################################################
if ($INPUT =~ /--rem-start-introns/){
	print LOG "\n--rem-start-introns has been activated. Genes that start with intronic space are removed. Those that do not are kept in gene_table.txt.\n";
	$string = "perl $loc" . "task_scripts/remove_starting_introns.pl $gene_table $ARGV[$output] $pre >> $log";
	
	print LOG "\tCommand: ", $string, "\n";
	print LOG "Results:", "\n";
	system ($string);

	$name = $ARGV[$output] . "\/" . "\/" . $pre . "complete_genes.txt";
	system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}
if ($INPUT =~ /--rem-end-introns/){
        print LOG "\n--rem-end-introns has been activated. Genes that end with intronic space are removed. Those that do not are kept in gene_table.txt.\n";
        $string = "perl $loc" . "task_scripts/remove_ending_introns.pl $gene_table $ARGV[$output] $pre >> $log";

        print LOG "\tCommand: ", $string, "\n";
        print LOG "Results:", "\n";
        system ($string);

	$name = $ARGV[$output] . "\/" . "\/" . $pre . "complete_genes.txt";
        system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}
if ($INPUT =~ /--rem-extra-introns/){
	print LOG "\n--rem-start-introns has been activated. Genes that start with intronic space are removed. Those that do not are kept in gene_table.txt.\n";
        $string = "perl $loc" . "task_scripts/remove_starting_introns.pl $gene_table $ARGV[$output] $pre >> $log";

        print LOG "\tCommand: ", $string, "\n";
        print LOG "Results:", "\n";
        system ($string);

        $name = $ARGV[$output] . "\/" . "\/" . $pre . "complete_genes.txt";
        system "mv $name $gene_table";

        if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
	 print LOG "\n--rem-end-introns has been activated. Genes that end with intronic space are removed. Those that do not are kept in gene_table.txt.\n";
        $string = "perl $loc" . "task_scripts/remove_ending_introns.pl $gene_table $ARGV[$output] $pre >> $log";

        print LOG "\tCommand: ", $string, "\n";
        print LOG "Results:", "\n";
        system ($string);

        $name = $ARGV[$output] . "\/" . "\/" . $pre . "complete_genes.txt";
        system "mv $name $gene_table";

        if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}
	
############################################################################################################################################################
if ($INPUT =~ /\-\-rem\-monoexonics/){
	print LOG "\n--rem-monoexonics has been activated. Genes that don't have an intron (2+ exons) will be removed from the gene_table.txt.\n";
	$string = "perl $loc" . "task_scripts/remove_monoexonics.pl $gene_table $ARGV[$output] $pre >> $log";
	print LOG "\tCommand: ", $string, "\n";
	print LOG "Results:", "\n";	

	system ($string);

	$multi = $ARGV[$output] . "\/" . "\/" . $pre . "multiexonic_genes.txt";
	system "mv $multi $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}

if ($INPUT =~ /\-\-rem\-multiexonics/){
        print LOG "\n--rem-multiexonics has been activated. Genes that have an intron (2+ exons) will be removed from the gene_table.txt.\n";
        $string = "perl $loc" . "task_scripts/remove_multiexonics.pl $gene_table $ARGV[$output] $pre >> $log";
        print LOG "\tCommand: ", $string, "\n";
        print LOG "Results:", "\n";

        system ($string);

        $mono = $ARGV[$output] . "\/" . "\/" . $pre . "monoexonic_genes.txt";
        system "mv $mono $gene_table";

        if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}
#############################################################################################################################################################
if ($INPUT =~ /\-\-min-exon-size/){
	if ($INPUT =~ /--min-exon-size\s+(\d+?)\s/){$min = $1;}
	else{$min = 20;}

	print LOG "\n--min-exon-size has been activated. Genes with an exon that is less than $min will be removed from gene_table.txt";
	$string = "perl $loc" . "task_scripts/minimum_exon.pl $gene_table $ARGV[$output] $min  $pre >> $log";
	print LOG "\n\tCommand: ", $string, "\n";
	print LOG "Results:\n";
	system ($string);

	$name = $ARGV[$output] . "\/" . "\/" . $pre . "minumum_exons_gene_table.txt";
	system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}
if ($INPUT =~ /\-\-min-intron-size/){
        if ($INPUT =~ /--min-intron-size\s+(\d+?)\s/){$min = $1;}
        else{$min = 20;}

        print LOG "\n--min-intron-size has been activated. Genes with an intron that is less than $min will be removed from gene_table.txt";
        $string = "perl $loc" . "task_scripts/minimum_intron.pl $gene_table $ARGV[$output] $min $pre >> $log";
        print LOG "\n\tCommand: ", $string, "\n";
        print LOG "Results:\n";
        system ($string);

        $name = $ARGV[$output] . "\/" . "\/" . $pre . "minumum_introns_gene_table.txt";
        system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}
if ($INPUT =~ /\-\-min-CDS-size/){
	if ($INPUT =~ /--min-CDS-size\s+(\d+?)\s/){$min = $1;}
        else{$min = 74;}

	print LOG "\n--min-CDS-size has been activated. Genes with a CDS that is less than $min will be removed from gene_table.txt";
        $string = "perl $loc" . "task_scripts/CDS_size.pl $gene_table $ARGV[$output] $min $pre >> $log";
        print LOG "\n\tCommand: ", $string, "\n";
        print LOG "Results:\n";
        system ($string);

	$name = $ARGV[$output] . "\/" . "\/" . $pre . "minumum_CDS_gene_table.txt";
        system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}

#############################################################################################################################################################
#############################################################################################################################################################
# ENTAP AREA

if ($INPUT =~ /--entap-annotation\s*.+\.tsv\s/ and $format =~ /braker\_2\.05\_g/){
        print LOG "\nYou have specified an entap annotation! Good choice!\n";                                                     
                for ($a = 0; $a < scalar @ARGV; $a++){if ($ARGV[$a] =~ /--entap-annotation/){$entap = $ARGV[$a+1];}}
	if ($INPUT =~ /--annotated-all-genes-only/){                                                                           
		print LOG "\n--annotated-all-gene-only has been activated. Only genes that have been annotated by similarity search or EggNOG hit will be kept. Passed genes will be found in gene_table.txt\n";
		$string = "perl $loc" . "task_scripts/annotated_all_genes_only.pl $entap $ARGV[$output] $pre >> $log";
		print LOG "\tCommand: ", $string, "\n";
		print LOG "Results:\n";
		system ($string);
	
		$name = $ARGV[$output] . "\/" . "\/" . $pre . "annotated_gene_table.txt";
	        system "mv $name $gene_table";

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                        print LOG "\n";
                        system ($string);
                }
	}
	 if ($INPUT =~ /--annotated-ss-genes-only/){
                print LOG "\n--annotated-all-genes-only has been activated. Only genes that have been annotated by similarity search will be kept. Passed genes will be found in gene_table.txt\n";
                $string = "perl $loc" . "task_scripts/annotated_ss_genes_only.pl $entap $ARGV[$output] $pre >> $log";
		print LOG "\tCommand: ", $string, "\n";
                print LOG "Results:\n";
                system ($string);

                $name = $ARGV[$output] . "\/" . "\/" . $pre . "annotated_gene_table.txt";
                system "mv $name $gene_table";

                if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                        print LOG "\n";
                        system ($string);
                }
        }

}
#############################################################################################################################################################
#############################################################################################################################################################
# FASTA FANCY
#
use Bio::Index::Fasta;
use Bio::Seq;
use File::Basename;

if ($INPUT !~ /--fasta\s*.+\.fasta\s/ or $INPUT !~ /--fasta\s*.+\.fa\s/){
	print LOG "\nNO FASTA OR NO VALID FASTA HAS BEEN SPECIFIED \-\- all fasta-required commands cannot be executed. If you did specify a fasta,\n",
		"\tmake sure it ends in .fa or .fasta. See manual for more details on fasta commands and specifications!\n";
}
if ($INPUT =~ /--fasta\s*.+\.fasta\s/ or $INPUT =~ /--fasta\s*.+\.fa\s/){
	print LOG "\nYou have specified a fasta! Indexing it...\n";							# INDEXING FASTA
		for ($a = 0; $a < scalar @ARGV; $a++){if ($ARGV[$a] =~ /--fasta/){
			$fasta = $ARGV[$a+1];
			$fasta_idx = $ARGV[$output] . "/" . fileparse($fasta) . ".idx";}}
		$string = "perl $loc" . "task_scripts/index.pl $fasta $fasta_idx >> $log";
		system ($string);	

	if ($INPUT =~ /\-\-canonical-only\s/){
		print LOG "\n--canonical-only has been activated. Genes without canonical gt-ag splice sites are filterd out. Monoexonics remain if not removed with flag.\n";
		$string = "perl $loc" . "task_scripts/canonical_only.pl $fasta_idx $ARGV[$output] $pre >> $log";
		print LOG "\tCommand: ", $string, "\n";
		print LOG "Results:\n";
		system ($string);

		$canonical = $ARGV[$output] . "\/" . "\/" . $pre . "canonical_genes.txt";
		system "mv $canonical $gene_table";

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                	$statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
        	        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
	                print LOG "\n";
                	system ($string);
        	}
	}
	if ($INPUT =~ /--rem-genes-without-start-codon/){
		print LOG "\n--rem-genes-without-start-codon has been activated. Genes that have a start codon are kept in gene_table.txt.\n";
		$string = "perl $loc" . "task_scripts/rem_genes_without_start.pl $fasta $ARGV[$output] $fasta_idx $pre >> $log";
		print LOG "\tCommand: ", $string, "\n";
		print LOG "Results:\n";
		system ($string);
		$name = $ARGV[$output] . "\/" . "\/" . $pre . "start_and_stop_gene_table.txt";
		$string = "mv $name $gene_table";
		system ($string);

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                        print LOG "\n";
                        system ($string);
                }

	}
	if ($INPUT =~ /--rem-genes-without-stop-codon/){
                print LOG "\n--rem-genes-without-stop-codon has been activated. Genes that have a stop codon are kept in gene_table.txt.\n";
                $string = "perl $loc" . "task_scripts/rem_genes_without_end.pl $fasta $ARGV[$output] $fasta_idx $pre >> $log";
                print LOG "\tCommand: ", $string, "\n";
                print LOG "Results:\n";
                system ($string);
                $name = $ARGV[$output] . "\/" . "\/" . $pre . "start_and_stop_gene_table.txt";
                $string = "mv $name $gene_table";
                system ($string);

		print LOG "\nPerforming a frame check to guarentee stop codon is in frame. Genes that pass are kept in gene_table.txt.\n";
		$string = "perl $loc" . "task_scripts/frame_detection.pl $gene_table $ARGV[$output] $pre >> $log";
		print LOG "\tCommand: ", $string, "\n";
                print LOG "Results:\n";
                system ($string);

		$file_name = $ARGV[$output] . "\/" . "\/" . $pre . "frame_checked_gene_table.txt";
		system "mv $file_name $gene_table";

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                        print LOG "\n";
                        system ($string);
                }
        }
	if ($INPUT =~ /--allowed-inframe-stop-codons/){
		if ($INPUT =~ /--allowed-inframe-stop-codons\s+(\d+?)\s/){$min = $1;}
	        else{$min = 0;}

	        print LOG "\n--allowed-inframe-stop-codons has been activated. Genes with more than $min inframe stop codons will be removed from gene_table.txt";
        	$string = "perl $loc" . "task_scripts/inframe_stop.pl $fasta $ARGV[$output] $min $fasta_idx $pre >> $log";
	        print LOG "\n\tCommand: ", $string, "\n";
        	print LOG "Results:\n";
	        system ($string);

        	$name = $ARGV[$output] . "\/" . "\/" . $pre . "inframe_stop_filtered_gene_table.txt";
	        system "mv $name $gene_table";
	
		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                        print LOG "\n";
                        system ($string);
                }
	}
	if ($INPUT =~ /--unique-genes-only/){
		$unique_done = 1;
		print LOG "\n--unique-genes-only has been activated. The longest transcript is chosen if present. Otherwise, the first transcipt is chosen.\n";
		$string = "perl $loc" . "task_scripts/unique_genes.pl $ARGV[$output] $pre >> $log";

		print LOG "\tCommand: ", $string, "\n";
		print LOG "Results:\n";
		system ($string);
		$name =  $ARGV[$output] . "\/" . "\/" . $pre . "unique_genes.txt";		
		system "mv $name $gene_table";
		
		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                        print LOG "\n";
                        system ($string);
                }
	}
#############################################################################################################################################################
# OUTPUTS
	if ($INPUT =~ /\-\-splice\-table\s/){
                print LOG "\n--splice-table has been activated. Creating splice_table.txt catalog.";
                $string = "perl $loc" . "task_scripts/splice_table.pl $fasta $ARGV[$output] $fasta_idx $pre >> $log";
                print LOG "\n\tCommand: ", "$string", "\n";
		print LOG "Results:\n";
                system ($string);
        }
	if($INPUT =~ /--get-fasta-with-introns/){
		print LOG "\n--get-fasta-with-introns has been activated. Nucleotide fasta will be created called genes_with_introns.fasta.";
		$string = "perl $loc" . "task_scripts/get_fasta_with_introns.pl $fasta_idx $ARGV[$output] $pre";
		print LOG "\n\tCommand: ", $string, "\n";
		system ($string);
	}
	if ($INPUT =~ /--get-fasta-without-introns/){
                print LOG "\n--get-fasta-without-introns has been activated. Nucleotide fasta will be created called genes_without_introns.fasta.";
                $string = "perl $loc" . "task_scripts/get_fasta_without_introns.pl $fasta_idx $ARGV[$output] $pre";
                print LOG "\n\tCommand: ", $string, "\n";
                system ($string);
        }
	if ($INPUT =~ /--get-protein-fasta/){
		print LOG "\n--get-protein-fasta has been activated. Protein fasta will be created called genes_without_introns.fasta.faa.";
                $string = "perl $loc" . "task_scripts/get_protein_fasta.pl $fasta_idx $ARGV[$output] $pre";
                print LOG "\n\tCommand: ", $string, "\n";
                system ($string);
	}
	if ($INPUT =~ /\-\-create\-gtf/){
		print LOG "\n--create-gtf has been activated. A gtf file called out.gtf will be created.";
		$string = "perl $loc" . "task_scripts/add_start_stop_to_gene_table.pl $fasta_idx $ARGV[$output] $pre";
		print LOG "\n\tCommand: ", $string, "\n";
		system ($string);

		$start_stop_table = $ARGV[$output] . "\/" . "\/" . $pre . "start_and_stop_gene_table.txt";

		$string = "perl $loc" . "task_scripts/gtf_creator.pl $start_stop_table $ARGV[$output] $ARGV[$infile] $pre";
		print LOG "\tCommand: ", $string, "\n";
		system ($string);

		system "rm $start_stop_table";

		if ($INPUT =~ /--compatibility/){
			print "--compatibility has been activated! You have chosen:\n";
		
			if ($INPUT =~ /SnpEff/){
				print LOG "\tSnpEff\n";
               			$string = "perl $loc" . "task_scripts/compatibility/SnpEff.pl $ARGV[$output] $pre";
		                print LOG "\tCommand: ", $string, "\n";
                		system ($string);
                		print LOG "\tCreated $pre" . "snpeff_format.gff", "\n\n";
			}
		}
	}
	if ($INPUT =~ /--nt-content/){
		print LOG "\n--nt-content has been activated. GC/AT and N content statistics on CDS sequence will be printed to the log.";
		$string = "perl $loc" . "task_scripts/GC_content_analysis.pl $fasta_idx $ARGV[$output] $pre >> $log";
		print LOG "\n\tCommand: ", $string, "\n";
		print LOG "\nResults:\n";
		system ($string);
	}		
}
###########################################################################################################################################################
 if ($INPUT =~ /--unique-genes-only/ and $unique_done == 0){
                print LOG "\n--unique-genes-only has been activated. The longest transcript is chosen if present. Otherwise, the first transcipt is chosen.\n";
                $string = "perl $loc" . "task_scripts/unique_genes.pl $ARGV[$output] $pre >> $log";

                print LOG "\tCommand: ", $string, "\n";
                print LOG "Results:\n";
                system ($string);
                $name =  $ARGV[$output] . "\/" . "\/" . $pre . "unique_genes.txt";
                system "mv $name $gene_table";

                if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table >> $log";
                        print LOG "\n";
                        system ($string);
               	}
}
###########################################################################################################################################################
if ($INPUT =~ /\-\-statistics\s/){

	$statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
        $string =  "perl $loc" . "task_scripts/classic_stats.pl $gene_table > $statistics";
        system ($string);

        print LOG  "\n--statistics has been activated. Statistics will be printed to statistics.txt\n";
        print LOG "\tCommand: ", $string, "\n";
}
############################################################################################################################################################
if ($INPUT =~ /\-\-distributions/){
        print LOG "\n--distributions have been activated! You have chosen:\n";

        if ($INPUT =~ /exon_lengths/){
                print LOG "\texon_lengths\n";
			if ($INPUT =~ /exon_lengths\s+(\d+)\s+/){ $step = $1;}
			else{ $step = "TBD";}
		print LOG "\tStep set to $step", "\n";
                $string = "perl $loc" . "task_scripts/distributions/exon_lengths.pl $ARGV[$output] $step $pre";
		print LOG "\tCommand: ", $string, "\n";
                system ($string);
		print LOG "\tCreated exon_lengths_distributions.tsv", "\n\n";
		undef ($step);
        }
        if ($INPUT =~ /intron_lengths/){
                print LOG "\tintron_lengths\n";
			if ($INPUT =~ /intron_lengths\s+(\d+)\s+/){ $step = $1; }
			else{ $step = "TBD";}
		print LOG "\tStep set to $step", "\n";
                $string = "perl $loc" . "task_scripts/distributions/intron_lengths.pl $ARGV[$output] $step $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated intron_lengths_distributions.tsv", "\n\n";
		undef ($step);
        }
	if ($INPUT =~ /CDS_lengths/){
                print LOG "\tCDS_lengths\n";
                        if ($INPUT =~ /CDS_lengths\s+(\d+)\s+/){ $step = $1; }
			else{ $step = "TBD";}
                print LOG "\tStep set to $step", "\n";
                $string = "perl $loc" . "task_scripts/distributions/CDS_lengths.pl $ARGV[$output] $step $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated CDS_lengths_distributions.tsv", "\n\n";
		undef ($step);
        }
	if ($INPUT =~ /gene_lengths/){
                print LOG "\tgene_lengths\n";
                        if ($INPUT =~ /gene_lengths\s+(\d+)\s+/){ $step = $1; }
			else{ $step = "TBD";}
                print LOG "\tStep set to $step", "\n";
                $string = "perl $loc" . "task_scripts/distributions/gene_lengths.pl $ARGV[$output] $step $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated gene_lengths_distributions.tsv", "\n\n";
		undef ($step);
        }
	if ($INPUT =~ /exon_position/){
		print LOG "\texon_position\n";
		$string = "perl $loc" . "task_scripts/distributions/exon_position.pl $ARGV[$output] $pre";
		print LOG "\tCommand: ", $string, "\n";
		system ($string);
		print LOG "\tCreated exon_position_distributions.tsv\n\n";
	}
	if ($INPUT =~ /exon_position_data/){
                print LOG "\texon_position_data\n";
                $string = "perl $loc" . "task_scripts/distributions/exon_position_verbose.pl $ARGV[$output] $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated data_exon_position_distributions.tsv\n\n";
        }
	 if ($INPUT =~ /intron_position/){
                print LOG "\tintron_position\n";
                $string = "perl $loc" . "task_scripts/distributions/intron_position.pl $ARGV[$output] $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated intron_position_distributions.tsv\n\n";
        }
	if ($INPUT =~ /intron_position_data/){
                print LOG "\tintron_position_data\n";
                $string = "perl $loc" . "task_scripts/distributions/intron_position_verbose.pl $ARGV[$output] $pre";
                print LOG "\tCommand: ", $string, "\n";
                system ($string);
                print LOG "\tCreated data_intron_position_distributions.tsv\n\n";
        }


}	
###########################################################################################################################################################

print LOG "\nCompleted! Have a great day!\n\n";

$temp = $ARGV[$output] . "\/" . "\/" . $pre . "temp_file.txt";
system "rm $temp";

close LOG;

###########################################################################################################################################################
###########################################################################################################################################################
###########################################################################################################################################################
# ALL gFACs scripts have a fun fact at the end as a reward for code reading. Did you know the Incas DID know about the wheel but never used it functionally?
