#!/bin/bash
#SBATCH --job-name=gfacs-m
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=5G
#SBATCH -o gfacs-m-%j.o
#SBATCH -e gfacs-m-%j.e


module load perl/5.28.1

org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/liriodendron
org_relat=$org/related_species/L_tulipifera

genome=$org/genome/LIR.pbjelly.reN.final.fasta

out_gmap="gmap_gfacs_o"
out_gth="gth_gfacs_o"

gmap_file=$org_relat/alignments/gmap/liriodendron_gmap.gff3
gth_file=$org_relat/alignments/genomeThreader/liriodendron.gth.gff3

script="/labs/Wegrzyn/gFACs/gFACs.pl"

options="--splice-rescue \
--statistics \
--statistics-at-every-step \
--splice-table \
--min-exon-size 3 \
--min-intron-size 3 \
--get-fasta-without-introns \
--unique-genes-only \
--create-gtf \
--get-protein-fasta"

mkdir "$out_gmap" "$out_gth"

perl "$script" \
-f gmap_2017_03_17_gff3 \
"$options" \
--fasta "$genome" \
-O "$out_gmap" \
$gmap_file

perl "$script" \
-f genomethreader_1.6.6_gff3 \
"$options" \
--fasta "$genome" \
-O "$out_gth" \
$gth_file
