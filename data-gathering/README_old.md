### **OUTDATED** README:

### Step 1: Download/gather genome resources
#### Model organisms
1. Download whole gemones and related files from Ensemble (note version)
    * Softmasked genome
    * gff
    * gtf
    * cDNA
    * pep


_If you need to run softmasking, please do Step 3 before Step 2. If there's an existing soft-masked genome, skip ahead to Step 3_
### Step 2: Soft-mask genome  
1. Run repeatModeler.sh which load RepeatModeler/1.0.8 to make the repeat library
2. Run splitfasta.sh which loads anaconda and breaks the filtered genome into 100 pieces
3. Run repeatmasker.sh which loads RepeatMasker/4.0.6
Important: Check version before running this program- some genomes are already softmasked, and so this script can be skipped.

### Step 3: Filter  
1. Use filtersubmit.sh to remove sequences less than 500bp
    * filtersubmit.sh calls filterLen.py (custom script, no version no.)
    * --length 500 # sets the minimum sequence length
    * --fasta $filename # filename = the name of your input fasta
    * --out $filename-filtered --pathOut $filedir # sets the output filename by adding '-filtered' to the end of your input filename
    * --pathOut # sets the output directory
    * output is .fasta files with 'filtered' in the file name
2. Check quality of filtered genome with Quast
    * quast.sh calls quast 5.0.2
    * input file path/name of masked, filtered genome
    * -o # set directory for output files
    * output is outputdir/report.txt (put N50, #contigs, total length in tracking sheet)
3. Check quality of filtered genome with BUSCO
    * busco.sh calls busco/4.1.2 
    * unload augustus/3.2.3 and blast/2.7.1 to run current config
    * input file path/name of masked, filtered genome
    * input path to database of appropriate busco lineage
    * -o # set directory for output files
    * -m genome # set 'mode' as 'genome' (as opposed to tran or prot)
    * add the BUSCO score (from the short summary txt file) to the tracking sheet

### Step 4: Create alignment indexes from the genome
1. Gmap index
    * gmap_index.sh calls gmap/2019-06-10 gmap_build
    * -D # sets the output directory
    * -d sets the index prefix
    * pass in the masked, filtered genome fasta file last
    * output files end in .chromsome, .chromosome.iit, .chrsubset, .contig, .contig.iit, .genomebits128, .genomecomp, .maps, .ref153offsets64meta, .ref153offsets64strm, .ref153positions, .sachildexc, .sachildguide1024, .saindex64meta, .saindex64strm, .salcpchilddc, .salcpexc, .salcpguide1024, .sarray, .version
2. Hisat index
    * hisatBuild.sh calls hisat2/2.2.0 hisat-build
    * -f sets the input .fasta file
    * set the output directory/prefix last 
    * output files are incrementing numbers like so: Arabidopsis_genome.1.ht2
    
### Step 5: Gather evidence  
1. Find reads in Genebank (NCBI)
2. Use the fetchSRA.sh script to get reads from NCBI.  
    * Make a .txt file containg the SSR numbers of the reads to download.  
    * fetchSRA.sh calls sratoolskit/2.8.1
    * In fetchSRA.sh, change the FILENAME to match your .txt. 
    * output is .fastq files in the same directory. 
    * move .fastq files to organism
3. Validate the retrieved reads using validateSRA.sh
    * validateSRA.sh calls sratoolskit/2.8.1
    * FILENAME # sets input file
    * output is logged to *.err
    * move reads to raw_reads dir
5. Trim reads using sickle
    * sickle.sh calls sickle/1.33
    * rawreadsdir # sets input dir
    * output is .fastq files, two for each pair (_1 and _2) that begin with trimmed_ plus single_trimmed_ for singles created by sickle.
6. Check quality of trimmed reads
    * fastqc.sh loads fastqc/0.11.7
    * readsdir # input directory
    * output *_fastqc.html and *_fastqc.zip for each run
7. Retrieve TSA file *OR*, if none available, create transcriptome with short reads and Trinity
    * Guidelines for handling short reads for Trinity input:
        1. Option —-split-fastq to separate pairs into their respective left and right files while fetching.
	    2. Changes header formats so they will be acceptable in Trinity (see trinityfetchSRA.sh).
            * fastq-dump --defline-seq '@$sn[_$rn]/$ri' --split-files $LINE
        3. Concatenate runs where necessary if they are from the same library, but just different lanes.  Lanes are just a byproduct of the ways things are separated during the sequencing process, but it’s all a part of the same sequencing library. Sometimes these are loaded into NCBI as separate runs. Library name can be found on the experiment (SRX) page. Lane info can be seen sometimes on the SRX page or by clicking on the SRR id links from the experiment page to go to the SRA Run Browser MetaData tab.
        4. Do not concatenate replicants. There are two kinds of replicants: sample and technical.
            * Sample: Run these separately in Trinity.
            * Technical: If a sample has multiple runs from the same library and same lane, it might be a technical replicant.  If there is already enough read depth, we don’t need multiple replicants of this kind.
    * If the previous details are not clear in NCBI, referring to the published study may help clarify.
8. Create transcriptome with Trinity (if there is no usable TSA already available)
    * trinity.sh calls trinity/2.6.6
    * using the trimmed raw reads, concatenate all _1 files into one file and _2 into another
    * PE_1 # sets the *all_1.fastq
    * PE_2 # sets the *all_2.fastq
    * out # sets the output directory
    * min_contig_length is 300 
9. Repeat steps 1-6 for a related species

### Step 6: Align short-read evidence to genome
1. Run the hisat2 aligner
    * hisat.sh calls hisat2/2.2.0
    * orgdir/trimmeddir # set input directory (dir containing trimmed fastq)
    * output is .sam files, one for each run (SRR file)
2. Convert, sort and merge the human readable .sam file to compressed machine readable .bam
    * samtools.sh calls samtools/1.7
    * input location of .sam files
    * output is one file with .bam extension

### Step 7: Align transcriptome evidence to genome
1. Frameselect transcriptome
    * concatenate all TSA files for a species into one file.
    * frameselect.sh runs TransDecoder/5.3.0
    * clustered_TSA # sets directory that contains the TSA file
    * filename # sets TSA file name
    * Note: frameselect.sh requires the transcriptome to have a certain number of sequences to work (not sure what number but > 31)
    * output files are but in directory 'bestHit' (.bed, .cds, .gff3, .pep)
2. Cluster frameselected transcriptome
    * usearch.sh runs usearch/9.0.2132
    * percent id is 0.98
    * concat_file # sets the input file (.cds from previous step)
    * output centroids_$concat_file and centroids_$concat_file.uc
3. Remove Remove short genes (less than 300bps) from the centroids file
    * Run filter300.sh which calls filterLen.py
4. Run the gmap aligner
    * gmap.sh calls gmap/2019-06-10
    * idx # sets the location of the index
    * prefix # sets the prefix of the index files
    * fasta sets the location of the frameselected, clustered, filtered TSA as input
    * -a 1 # Translate codons from given nucleotide (1-based)
    * --cross-species # Use a more sensitive search for canonical splicing, which helps especially for cross-species alignments and other difficult cases
    * -D # sets idx
    * -d # sets prefix
    * -f gff3_gene # sets the format
    * --fulllength # Assume full-length protein, starting with Met
    * --min-trimmed-coverage=0.95 # Do not print alignments with trimmed coverage less than this value
    * --min-identity=0.95 # Do not print alignments with identity less than this value
    * -n1 # set maximum number of paths to show. If set to 1, GMAP will not report chimeric alignments, since those imply two paths.  If you want a single alignment plus chimeric alignments, then set this to be 0.
    * output files are $prefix_gmap.gff3 and $prefix_gmap.error

### Step 8: Align the protein evidence to genome
1. Filter the .pep file created from frameselection
    * run filterpep.sh to filter the proteins so that you only have the same sequences that correspond to the genes left after filtering in Step 7.3.
    * filterpep.sh trims headers if there is additional info after id, makes a list of headers from filtered TSA, and  and then calls CreateFasta.py
    * output is *_filtered.pep
2.  Run the genomeThreader aligner
    * genomeThreader.sh calls genomeThreader/1.7.1 and genometools/1.6.1
    * gt adds stop amino acids *
    * gth aligns the protein sequences

### Step 9: Repeat step 6,7,8 with related species ###
    * keep separate from related species


### Step 10: Braker ###

***There is an update to the BRAKER version, this section would be updated accordingly***

1. Run 1 - braker.sh:
    * for later scripts, use path: annotations/*braker*/braker.sh 
    * short reads from species of interest
    * genome from species of interest
        * braker.sh calls BRAKER/2.0.5, augustus, genemark, bamtools
        * augustus must be in your local directory (should be there from Step 3.3)
        * make a directory in your home called 'tmp'
        * softmasking 1 # indicates the genome has been softmasked
        * copies of perl modules from the lab directory root are included for specific dependencies
        * The merged .bam file from Step is is used as input.
2. Run 2 - braker_protein.sh: 
    * use path annotations/*braker2*/braker_prot.sh
    * proteins from species of interest AND related species
    * short reads from species of interest
    * genome from species of interest
        * braker_protein.sh is for including related protein evidence
        * like braker.sh but adds: 
            * --prot_aln : adds protein seqs, 
            * --prg=gth : this and the following param are for adding proteins of shorter evolutionary distance
            * --gth2traingenes
3. Run 3 - braker_protein.sh:
    * use path annotations/*braker3*/braker.sh
    * read set from related species
    * genome from species of interest
    * protein from related species 
### Step 11: Maker

***There is an update to the MAKER version, this section would be updated accordingly***

1. Run 1 - first_round_maker/maker.sh
    * fill in the "EST evidence" section of maker_opts.ctl with the files produced so far, genome files and alignment files.
    * Run the first round of maker
    * After round 1 is complete, run snap.sh. SNAP is a gene-finding program based on HMM models, and it basically breaks the annotates, and breaks the genome into one gene per sequence (1000 bp on each end), and finally builds a hmm.
    * Train augustus with the maker run as well
2. Run 2
    * fill in the "re-annotate with maker" section in maker_opts.ctl
    * add snap and augustus outputs in maker_opt.ctl
    * use the output of maker, and re-run augustus and snap
3. Run 3
    * use the outputs of augustus and snap to run maker

After all runs, run "maker_analysis.sh" to get FASTA files for transcripts and proteins, and a gff file for annotations. The third run is the final run, and that output is the final annotation from MAKER.

### Step 12: gFACs ###  
1. Run transcriptome alignments through gFACs
    * gfacs.sh calls gFACs.pl and related files in annotationtool/process/12_gFACs
    * gmap output is used as input
    * output directory should exist before running gfacs, so the script should make it first
2. Run output of the all three braker runs through gFACs, both filtered and unfiltered  
 * 2a. Filtered 
    * -f braker_2.05_gff3 
    * --splice-rescue 
    * --statistics 
    * --statistics-at-every-step 
    * --splice-table 
    * --get-fasta-without-introns 
    * --get-fasta-with-introns 
    * --min-intron-size 10 
    * --min-exon-size 10 
    * --mins-CDS-size 300 
    * --rem-start-introns 
    * --rem-end-introns 
    * --get-protein-fasta 
    * --create-gtf 
 * 2b. Unfiltered	 
    * --splice-rescue 
    * --statistics 
    * --statistics-at-every-step 
    * --splice-table 
    * --create-gtf 

### Step 13: EnTAP ###
    * database information at https://bioinformatics.uconn.edu/databases/
    * Run EnTAP - entap.sh
    * can be run with BAM/SAM file or braker output (augustus.hints.aa) 
    * BAM file uses flags --ontology 0, -a <BAMFILE> 
    * contaminants specified by -c flags, should contaminants be specific to species of interest?
    * see https://entap.readthedocs.io/en/latest/basic_usage.html#id3 for additional flags 
    * should have multipule .dmnd files (at least 2, refseq and uniprot)

