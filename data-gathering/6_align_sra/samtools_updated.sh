#!/bin/bash
#SBATCH --job-name=samtools
#SBATCH -o samtools-%j.output
#SBATCH -e samtools-%j.error
#SBATCH --mail-user=danmonyak@gmail.com
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=100G

module load samtools/1.7

fname=rugosa

readsdir=../reads

# convert each file from .sam to .bam and sort individually
for f in $readsdir/*.sam
do
        f2=${f#"$readsdir/"}

        # Only convert and sort if SRA is in good_SRA.txt (i.e. has mapping rate >= 90%)
        if grep -qw ${f2%.sam} ../good_SRAs.txt
        then
                samtools view -b -@ 16 $f | samtools sort -o sorted_"${f2%.sam}.bam" -@ 16
        fi
done

# merge sorted .bam files into one
samtools merge "$fname"_merged.bam sorted_*.bam
