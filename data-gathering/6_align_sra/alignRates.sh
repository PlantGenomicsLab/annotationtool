#!/bin/bash
#SBATCH --job-name=alignRates
#SBATCH -o alignRates-%j.out
#SBATCH -e alignRates-%j.err
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --mem=5G
#SBATCH --partition=general
#SBATCH --qos=general

# Script to create files showing each hisat2 alignment (mapping) rate for each SRA library


# Make sure these files don't already exist
rm rates.txt good_SRAs.txt good_rates.txt

# aray of SRA acessions
uniq_file=($( cat hisat2-*.out | sed "/fastq/d" ))

# array of hisat2 alignment rates
rates_list=($(grep "overall" hisat2-*err | sed "s/ overall.*//g"))

# length of arrays
rates_len=${#rates_list[@]}
SRR_len=${#uniq_file[@]}

if [[ $rates_len -eq $SRR_len ]]
then
        # Iterate through each SRA-rate pair and print them to rates.txt
        # If rate>=90%, print SRA to good_SRAs.txt and print rate to good_rates.txt
        for i in $(seq 0 $(($rates_len-1)))
        do
                SRR=${uniq_file[$i]}
                rate=${rates_list[$i]}

                echo -e "${SRR}\t${rate}" >> rates.txt

                if (( $(echo "${rate%\%} >= 90" | bc -l) ));
                then
                        echo $SRR >> good_SRAs.txt
                        echo $rate >> good_rates.txt
                fi
        done
else
        "Something went wrong..."
fi
