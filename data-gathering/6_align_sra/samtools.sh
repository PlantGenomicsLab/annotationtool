#!/bin/bash
# Submission script for Xanadu 
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
#SBATCH --job-name=samtools
#SBATCH -o samtools-%j.output
#SBATCH -e samtools-%j.error
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1
#SBATCH --nodes=1 
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G

module load samtools/1.7

fname=arabidopsis

# convert each file from .sam to .bam and sort individually
for f in ../*.sam
do
	f2=${f#"../"}
        samtools view -b -@ 16 $f | samtools sort -o sorted_"${f2%.sam}.bam" -@ 16
done

# merge sorted .bam files into one
samtools merge "$fname"_merged.bam sorted_*.bam
