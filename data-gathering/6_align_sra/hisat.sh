#!/bin/bash
#SBATCH --job-name=hisatAra
#SBATCH -o hisatAra-%j.out
#SBATCH -e hisatAra-%j.err
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#SBATCH --qos=general

module load hisat2/2.1.0

orgdir="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/model/arabidopsis"
trimmeddir=$orgdir"/evidence/short_read/trimmed_reads"
pe_arr=()

for f in $trimmeddir/trimmed*.fastq
do 
	pe_arr+=($(echo "$f" | awk -F'[/_]' '{ print $15 }'))
done

uniq_file=($(echo "${pe_arr[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

for i in ${uniq_file[@]}
do
	echo $i
	f1=$trimmeddir"/trimmed_"$i"_1.fastq"
	if [ -f $i"_2.fastq" ]; then
		f2=$trimmeddir"/trimmed_"$i"_2.fastq"
		echo $f1
		hisat2 -q -x $orgdir/genome/index/hisat2/Arabidopsis_genome -1 $f1 -2 $f2 -S $i.sam
	else
		hisat2 -q -x $orgdir/genome/index/hisat2/Arabidopsis_genome -U $f1 -S $i.sam
		echo $f2
	fi
done
