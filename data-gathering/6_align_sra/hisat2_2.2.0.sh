#!/bin/bash
#SBATCH --job-name=hisatHm
#SBATCH -o hisathm-%j.out
#SBATCH -e hisathm-%j.err
#SBATCH --mail-user=vidya.vuruputoor@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#SBATCH --qos=general

module load hisat2/2.2.0

orgdir="/labs/Wegrzyn/annotationtool/testSpecies/non-model/Hymenolepis_microstoma"
trimmeddir=$orgdir"/evidence/transcriptome/trimmed_reads"
pe_arr=()

for f in $trimmeddir/trimmed*.fastq
do
        pe_arr+=($(echo "$f" | awk -F'[/_]' '{ print $14 }'))
done

uniq_file=($(echo "${pe_arr[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

for i in ${uniq_file[@]}
do
        echo $i
        f1=$trimmeddir"/trimmed_"$i"_1.fastq"
        if [ -f $trimmeddir"/trimmed_"$i"_2.fastq" ]; then
                f2=$trimmeddir"/trimmed_"$i"_2.fastq"
                echo $f1
                hisat2 -q -x $orgdir/evidence/align_sra/hisatBuild/Hymenolepis_genome.built -1 $f1 -2 $f2 -S $i.sam
        else
                hisat2 -q -x $orgdir/evidence/align_sra/hisatBuild/Hymenolepis_genome.built -U $f1 -S $i.sam
                echo $f2
        fi
done