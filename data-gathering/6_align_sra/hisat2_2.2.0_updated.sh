#!/bin/bash
#SBATCH --job-name=hisat2
#SBATCH -o hisat2-%j.out
#SBATCH -e hisat2-%j.err
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=16
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general

module load hisat2/2.2.0

org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/wheel_tree
trimmeddir=$org/evidence/short_read/trimmed_reads
out_dir=$org/analysis/alignments/hisat2/reads
mkdir -p $out_dir

pe_arr=()

for f in $trimmeddir/trimmed*.fastq
do 
	pe_arr+=($(echo "$f" | sed "s|.*/trimmed_||g" | sed "s|_.*||g"))
done

uniq_file=($(echo "${pe_arr[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

hisat2Prefix=$org/genome/index/hisat2/wheelTree_genome.built
cd $out_dir

for i in ${uniq_file[@]}
do
	echo $i
	f1=$trimmeddir/trimmed_"$i"_1.fastq
	f2=$trimmeddir/trimmed_"$i"_2.fastq
	if [ -f $f2 ]; then
		echo $f1; echo $f2
		hisat2 -q -x $hisat2Prefix -1 $f1 -2 $f2 -S "$i".sam
	else
		echo $f1
		hisat2 -q -x $hisat2Prefix -U $f1 -S "$i".sam
	fi
done
