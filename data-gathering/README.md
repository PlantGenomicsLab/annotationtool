Benchmarking Protocol Here - https://www.protocols.io/view/benchmarking-protocol-for-plant-genomes-bsjknckw

- [Current Benchmark spreadsheet](https://docs.google.com/spreadsheets/d/1rITL0nSpJQzyq_3AepJunn_tv1QnGP67DRkTasigylA/edit#gid=1628684765)
- [Old Benchmark spreadsheet](https://docs.google.com/spreadsheets/d/1Effvbj77kkjbGRoloRnbzyKjeFk6qPmrdB4UIm2opQI/edit#gid=1281612658)
