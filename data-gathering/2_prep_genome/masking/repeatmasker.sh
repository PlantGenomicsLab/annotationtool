#!/bin/bash
#SBATCH --mem=180GB
#SBATCH --job-name=repeatmasker
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH -o repeatmasker-%j.output
#SBATCH -e repeatmasker-%j.error
#SBATCH --partition=general
#SBATCH --qos=general

echo `hostname`

module load perl/5.28.1
export PATH=$HOME/RepeatMasker/4.0.6:$PATH

SEQ=Arabidopsis_thaliana.TAIR10.dna.toplevel.fa
filename=$PWD/$SEQ
DATABASE=Arabidopsis

basedir=/scratch/$USER/repeatmasker_${DATABASE}
mkdir -p $basedir/outputfiles

echo "Running RepeatMasker..."
RepeatMasker $filename -lib ${DATABASE}.consensi.fa.classified -pa 4 -a -noisy -low -xsmall | tee $basedir/outputfiles/output_${SEQ}.sm.fa.txt
echo "Done!"
