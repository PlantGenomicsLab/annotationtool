#!/bin/bash
#SBATCH --job-name=RepeatMasker
#SBATCH -o %x%j.out
#SBATCH -e %x%j.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=20
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=100G

module unload perl/5.28.0
module load perl/5.24.0
export PERL5LIB=/UCHC/PublicShare/szaman/perl5/lib/perl5/
module load RepeatModeler/2.01
module load genometools/1.6.1
module load mafft/7.471
export LTRRETRIEVER_PATH=$HOME/LTR_retriever
module load cdhit/4.8.1
module load ninja/0.95

org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/P_trichocarpa/p_trichocarpa_pipeline/example_run/

BuildDatabase -name P_trichocarpa $org/genome/P_trichocarpa.fasta

nohup RepeatModeler -database P_trichocarpa -pa 20 -LTRStruct 
