#!/bin/bash
#SBATCH --job-name=repContent
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH -o repContent_%j.out
#SBATCH -e repContent_%j.err

# Provide genome file as command line input
genome=$1
bases=${genome}.bases.txt

echo "Using genome: "$genome
echo "Combining all sequences into a single line in file: "$bases

echo $(sed '/>/d' $genome |
while read LINE; do

echo -n $LINE 
done) > $bases

echo "Calculating repeat content in file..."; echo

script=/labs/Wegrzyn/annotationtool/data-gathering/2_prep_genome/masking/rep_content.py
python3 $script --filename $genome

rm $bases
