import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--filename', type=str, required=True)

args = parser.parse_args()
filename=args.filename

bases=filename + ".bases.txt"
outfile=filename + ".rep_content.txt"

with open(bases) as f:
        bases = f.read()


dna_length = len(bases)
vals = np.array(list(map(lambda b : b.islower(), bases)))
cont = vals.astype(int).mean()

print("Total length of genome: %i" % dna_length)
print("Fraction of genome that is masked: %f" % cont)
