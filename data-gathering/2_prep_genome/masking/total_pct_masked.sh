#!/bin/bash
#SBATCH --job-name=totalPctMasked
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=1G
#SBATCH -o totalPctMasked_%j.out
#SBATCH -e totalPctMasked_%j.err

# Provide path of directory with .tbl files as command line input 
tbl_dir=$1

work_file="temp_file.txt"
if [[ -f $work_file ]]; then rm $work_file; fi

masked=0
total=0

for f in $tbl_dir/*.tbl; do

temp_masked=$(grep "bases masked" $f | sed "s/.*: *//" | sed "s/ bp *(.*//")
temp_total=$(grep "total length" $f | sed "s/.*: *//" | sed "s/ bp *(.*//")

let "masked += temp_masked"
let "total += temp_total"

done

echo "Fraction of genome that is masked"
echo "$masked/$total"
bc -l <<< "$masked/$total"

