#!/bin/bash
#SBATCH --job-name=splitfasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=200G
#SBATCH -o splitfasta_%j.out
#SBATCH -e splitfasta_%j.err

module load anaconda

echo `hostname`
echo $PWD

file=/home/CAM/jbennett/benchmark_ginkgo/genome/Ginkgo_biloba.HiC.genome.fasta-filtered

python splitfasta.py --fasta $file --pieces 100
