#!/bin/bash
#SBATCH --mem=50G
#SBATCH --job-name=repeatModeler
#SBATCH -o repeatModeler-%j.output
#SBATCH -e repeatModeler-%j.error
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH --partition=general
#SBATCH --qos=general

fasta=Arabidopsis_thaliana.TAIR10.dna.toplevel.fa
org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/arabidopsis

export homedir=$PWD
export DATADIR=$org/genome/test_masking
export SEQFILE=$fasta
export DATABASE=Arabidopsis


WORKDIR=/scratch/$USER/repeatmodeler_${DATABASE}
mkdir -p $WORKDIR; cd $WORKDIR
cp $DATADIR/$SEQFILE ./

module load RepeatModeler/2.01
module load rmblastn/2.6.0
module load perl/5.24.0


BuildDatabase -name $DATABASE -engine ncbi $SEQFILE
nice -n 10 RepeatModeler -engine ncbi -pa 10 -database $DATABASE

line="Using output directory"
outdir=$(grep "$line" $homedir/repeatModeler-*.output | sed "s/.* = //")
rsync -a $outdir/consensi.fa.classified $homedir/${DATABASE}.consensi.fa.classified
