#!/bin/bash
#SBATCH --job-name=repeatmasker
#SBATCH -o output_files/repeatmasker-%j.out
#SBATCH -e error_files/repeatmasker-%j.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10GB
#SBATCH --array=1-100%5

# Run as an array job
# Each job runs on a separate piece of the genome that was already split up into a max of 100 pieces (some may be empty)
# Make sure to create directories "output_files" and "error_files" (see SLURM header)

module load perl/5.28.1
export PATH=$HOME/RepeatMasker/4.0.6:$PATH

# Run the program                 
echo `hostname`

org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/liriodendron

SEQ=LIR.pbjelly.reN.final.fasta
filename=$SEQ${SLURM_ARRAY_TASK_ID}.fa
DATABASE=Liriodendron

basedir=/scratch/$USER/repeatmasker_${DATABASE}
mkdir -p $basedir/outputfiles

maskeddir=$org/genome/masking/masked
mkdir -p $maskeddir

piecesdir=$org/genome/pieces

if [ -s $piecesdir/$filename ]
then
        echo "Running RepeatMasker..."
        RepeatMasker $piecesdir/$filename -lib ${DATABASE}.consensi.fa.classified -pa 4 -gff -a -noisy -low -xsmall | tee $basedir/outputfiles/output_${SEQ}.sm"$SLURM_ARRAY_TASK_ID".fa.txt
        echo "Done!"
fi

mv $piecesdir/${filename}.* $maskeddir/

# Run after all jobs of the array to comine all masked outputs into a final combined masked genome
#cat $maskeddir/*masked > $org/genome/${SEQ}.masked.fa
