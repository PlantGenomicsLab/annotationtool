#!/bin/bash
#SBATCH --job-name=filter500
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G
#SBATCH -o filter500_%j.out
#SBATCH -e filter500_%j.err

module load biopython/1.70
module load numpy/1.6.2

filename="GCF_002288925.1_ASM228892v2_genomic.fna"
basedir="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/non-model/Delphinapterus_leucas"
filedir=$basedir/genome
scriptsdir=$basedir/scripts_used

$scriptsdir/filter500/./filterLen.py --fasta $filename --path $filedir --length 500 --out $filename-filtered --pathOut $filedir
