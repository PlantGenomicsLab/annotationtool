#!/bin/bash
#SBATCH --job-name=minimap2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 9
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G
#SBATCH -o output_files/minimap2_%j.out
#SBATCH -e error_files/minimap2_%j.err
#SBATCH --array=0-1

echo `hostname`

read=$1
echo $read


#### Uncomment the right one

#source="PacBio"
source="OxfordNanopore"

declare -A preset_dict=( ["PacBio"]="splice:hq -uf" ["OxfordNanopore"]="splice -uf -k14")
preset=${preset_dict[$source]}

####


echo $source


module load minimap2/2.17

org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/arabidopsis
genome=$org/genome/Arabidopsis_filtered.fasta.masked


# Creates array of library FASTA files, chooses one based on the job number
#libs=($(ls reads))
#read=${libs[$SLURM_ARRAY_TASK_ID]}


prefix=${read%_1.fasta*}
prefix=${prefix#reads/}

outdir="sam"; mkdir $outdir

minimap2 -ax $preset $genome ../minimap2/reads/$read -o $outdir/${prefix}.sam -t 9
