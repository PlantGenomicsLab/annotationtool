#!/bin/bash
#SBATCH --job-name=fetchSRA
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH -o fetchSRA_%j.out
#SBATCH -e fetchSRA_%j.err

echo `hostname`

module load sratoolkit/2.8.1

outdir="reads"
mkdir $outdir

# File with SRA accessions
FILENAME="fetchSRA_input.txt"

count=0

while read LINE
do
	let count++
	
	# Fetch SRA library in FASTA format, using the "--fasta" option
	fastq-dump --split-files $LINE --fasta -O $outdir
	echo "$LINE"

done < $FILENAME
echo -e "\nTotal lines read = $count"
