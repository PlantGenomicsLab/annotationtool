#!/bin/bash
#SBATCH --job-name=mergeBam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G
#SBATCH -o mergeBam_%j.out
#SBATCH -e mergeBam_%j.err

echo `hostname`

module load samtools/1.9

prefix="arabidopsis"

for file in ../sam/*.sam
do
	fname=${file#../sam/}
	samtools view -b -@ 16 $file | samtools sort -o sorted_${fname%.sam}.bam -@ 16

done


samtools merge ${prefix}.merged.bam sorted_*.bam
