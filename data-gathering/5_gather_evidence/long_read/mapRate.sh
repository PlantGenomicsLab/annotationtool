#!/bin/bash
#SBATCH --job-name=mapRateNew
#SBATCH -o mapRate-%j.output
#SBATCH -e mapRate-%j.error
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=5G

echo `hostname`
module load samtools/1.10

#grep "Read" ../flair/fetchSRA_3006460.out | sed "s/Read //" | sed "s/ spots for//" > read_nums.txt

readsdir="reads"
rd_fi="read_nums.txt"
if [[ -f $rd_fi ]]; then rm $rd_fi; fi

for f in $readsdir/*.fasta; do

SRA=$(echo $f | sed "s|$readsdir/||" | sed "s/_1\.fasta//")
n=$(grep ">" $f | wc -l)
echo "$n $SRA" >> $rd_fi

done 


ls sam/*.sam | sed "s/.sam//" | sed "s|sam/||" |
while read S
do
	echo $S
	aln=$(samtools view -F 0x904 -c sam/${S}.sam)
	reads=$(grep $S $rd_fi | sed "s/ .*//")
	
	echo $aln/$reads
	echo "$aln/$reads" | bc -l ; echo
done
