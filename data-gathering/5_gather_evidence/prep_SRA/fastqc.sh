#!/bin/bash
# Submission script for Xanadu 
#SBATCH --job-name=fastqc
#SBATCH -o fastqc_%j.out
#SBATCH -e fastqc_%j.err
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=4
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general

readsdir="/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/model/arabidopsis/evidence/short_read"

module load fastqc/0.11.7


str=""
for f in $readsdir/trimmed_reads/*.fastq
do 
	str="$str $f"
done

fastqc -t 4 -o $readsdir/fastQC $str
