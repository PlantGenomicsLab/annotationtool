#!/bin/bash
#SBATCH --job-name=newfetchSRA
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G
#SBATCH -o newfetchSRA_%j.out
#SBATCH -e newfetchSRA_%j.err

echo `hostname`
echo "Current dir=$PWD"

module load  sratoolkit/2.8.1

FILENAME="fetchSRA_input_assembly.txt"
count=0

while read LINE
do
    let count++
    fastq-dump --defline-seq '@$sn[_$rn]/$ri' --split-files $LINE
    echo "$LINE"

done < $FILENAME
echo -e "\nTotal lines read = $count"
