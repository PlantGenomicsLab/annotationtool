#!/bin/bash
# Submission script for Xanadu 
#SBATCH --job-name=sickle
#SBATCH -o sickle_%j.out
#SBATCH -e sickle_%j.err
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=2
#SBATCH --mem=5G
#SBATCH --partition=general
#SBATCH --qos=general


### This sickle script is designed for PAIRED end reads and is path dependent!!!
### variable myarr tries to capture all of the accession numbers (eg. SRR3723923_1.fastq & SRR3723923_2.fastq)
### variable uniq_file tries to capture all of the UNIQUE accession numbers (eg. SRR3723923)
### the unique accession numbers are then used to specify the forward (var f1) files and reverse (var f2) files
### the forward and reverse file names are used to run sickle 


#run this script in the directory you would like the output to be

org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/wheel_tree
rawreadsdir=$org/evidence/short_read/raw_reads
outdir=$org/evidence/short_read/trimmed_reads

module load sickle/1.33

myarr=()
for f in $rawreadsdir/*.fastq
do 
	myarr+=($(echo "$f" | sed "s|.*/||g" | sed "s|_.*||g"))
done

uniq_file=($(echo "${myarr[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

cd $outdir

for i in ${uniq_file[@]}
do
	echo $i
	f1="$i"_1.fastq
  f2="$i"_2.fastq
	if [ -f $rawreadsdir/$f2 ]; then
		echo $f1
		echo $f2
		sickle pe -f $rawreadsdir/$f1 -r $rawreadsdir/$f2 -t sanger -o trimmed_"$f1" -p trimmed_"$f2" -s single_trimmed_"$i".fastq -q 30 -l 50
    sed -i 's/_.*/\/1/g' trimmed_${i}_1.fastq
    sed -i 's/_.*/\/2/g' trimmed_${i}_2.fastq
	else
		echo $f1
		sickle se -f $rawreadsdir/$f1 -t sanger -o trimmed_"$f1" -q 30 -l 50
    sed -i 's/_.*/\/1/g' trimmed_${i}_1.fastq
	fi
done

