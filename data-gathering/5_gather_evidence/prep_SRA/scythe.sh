#!/bin/bash
#SBATCH --job-name=scythe
#SBATCH -o scythe_%j.out
#SBATCH -e scythe_%j.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --qos=general

# Script for running Scythe to trim adaptor content from short-read FASTQ files


org=/labs/Wegrzyn/annotationtool/testSpecies/PlantSet/rose_old_blush_genome
readsdir=$org/evidence/short_read/raw_reads

module load scythe/0.994

SRAs=($(cat bad_SRAs.txt))

# Adaptor sequences in Scythe folder
adapter=/isg/shared/apps/scythe/0.994/illumina_adapters.fa

for SRR in ${SRAs[@]}
do
        read1=$readsdir/${SRR}_1.fastq
        read2=$readsdir/${SRR}_2.fastq

        scythe -a $adapter -o trimmed_${SRR}_1.fastq $read1
        scythe -a $adapter -o trimmed_${SRR}_2.fastq $read2
done

# Outputs trimmed sequences in current directory
