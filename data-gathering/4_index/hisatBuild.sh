#!/bin/bash
#SBATCH --job-name=hisatB
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=60G
#SBATCH -o hisatB_%j.out
#SBATCH -e hisatB_%j.err

###build hisat index
#module load hisat2/2.0.5
module load hisat2/2.1.0

basedir=/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/non-model/Delphinapterus_leucas/genome
masked500bpgenome=beluga_genome.filtered.fna.fa.masked

hisat2-build -f $basedir/$masked500bpgenome  beluga_genome.built

