#!/bin/bash
#SBATCH --job-name=gmapB
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=40G
#SBATCH -o gmapB_%j.out
#SBATCH -e gmapB_%j.err

#build gmap index
module load gmap/2017-03-17
#-D /path/to/where/to/save/make/gmap/index/dir -d name_of_gmap_index_dir genome.fa
indexdir=/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/non-model/Delphinapterus_leucas/genome/index/gmap
genomepath=/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/non-model/Delphinapterus_leucas/genome/beluga_genome.filtered.fna.fa.masked
gmap_build -D $indexdir -d beluga_gmap $genomepath 

