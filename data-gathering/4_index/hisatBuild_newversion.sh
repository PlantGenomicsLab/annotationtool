#!/bin/bash
#SBATCH --job-name=hisatH
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=6G
#SBATCH -o hisatB_%j.out
#SBATCH -e hisatB_%j.err

###build hisat index
#module load hisat2/2.0.5
module load hisat2/2.2.0

basedir=/labs/Wegrzyn/annotationtool/testSpecies/non-model/Hymenolepis_microstoma/genome/
masked500bpgenome=Hymenolepsis_microstoma.PRJEB124.WBPS14.genome_sm.fa-filtered

hisat2-build -f $basedir/$masked500bpgenome  Hymenolepis_genome.built