#!/bin/bash
#SBATCH --job-name=gmapH
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=4G
#SBATCH -o gmapH_%j.out
#SBATCH -e gmapH_%j.err


#build gmap index
module load gmap/2019-06-10
#-D /path/to/where/to/save/make/gmap/index/dir -d name_of_gmap_index_dir genome.fa

indexdir=/labs/Wegrzyn/annotationtool/testSpecies/non-model/Hymenolepis_microstoma/genome/index/gmap

genomepath=/labs/Wegrzyn/annotationtool/testSpecies/non-model/Hymenolepis_microstoma/genome/Hymenolepsis_microstoma.PRJEB124.WBPS14.genome_sm.fa-filtered

gmap_build -D $indexdir -d Hymenolepis_gmap $genomepath