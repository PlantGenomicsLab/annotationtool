#!/bin/bash
#SBATCH --job-name=maker_analysis
#SBATCH --mail-user=vidya.vuruputoor@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=8
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH -o maker-%j.out
#SBATCH -e maker-%j.err
#SBATCH --mem=50G

#maker round 1
#EST: Trinity, gff from gth added related species
module load perl/5.28.1

#/home/FCAM/vvuruputoor/maker/bin/maker -base first_iter maker_opts.ctl maker_bopts.ctl maker_exe.ctl

MAKERDIR="first_iter"

/home/FCAM/vvuruputoor/maker/bin/maker -base ${MAKERDIR} -fix_nucleotides -dsindex
/home/FCAM/vvuruputoor/maker/bin/gff3_merge  -d ${MAKERDIR}.maker.output/${MAKERDIR}_master_datastore_index.log
/home/FCAM/vvuruputoor/maker/bin/fasta_merge -d ${MAKERDIR}.maker.output/${MAKERDIR}_master_datastore_index.log

grep -c '>' ${MAKERDIR}.all.gff | awk {"print $1"} >  gene_annotations.txt
grep -c -P "\tgene\t" ${MAKERDIR}.all.gff | awk {"print $1"} > number_of_genes.txt
