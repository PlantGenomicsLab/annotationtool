#!/bin/bash
#SBATCH --job-name=augustus_training
#SBATCH --mail-user=vidya.vuruputoor@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=12
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err
#SBATCH --mem=50G

module load perl/5.28.1

export AUGUSTUS_CONFIG_PATH=$HOME/Augustus/config/
export AUGUSTUS_BIN_PATH=$HOME/Augustus/bin
export PATH=/programs/augustus/bin:/programs/augustus/scripts:$PATH

MAKERDIR=/home/FCAM/vvuruputoor/maker/bin
MAKERROUNDDIR=/labs/Wegrzyn/annotationtool/testSpecies/model/elegans/analysis/annotation_new_version/maker/1_round_maker
MAKERROUND=first_iter
species=c.elegans.maker

if [[ -d $AUGUSTUS_CONFIG_PATH/species/$species ]]; then rm -r $AUGUSTUS_CONFIG_PATH/species/$species; fi

#take only the maker annotations
awk '{if ($2=="maker") print }' $MAKERROUNDDIR/$MAKERROUND.all.gff > maker_rnd1.gff

/home/FCAM/vvuruputoor/Augustus/scripts/gff2gbSmallDNA.pl maker_rnd1.gff /labs/Wegrzyn/annotationtool/testSpecies/model/elegans/genome/elegans_genome_sm.fa 1000 $MAKERROUND.gb

/home/FCAM/vvuruputoor/Augustus/scripts/randomSplit.pl $MAKERROUND.gb 100

/home/FCAM/vvuruputoor/Augustus/scripts/new_species.pl --species=$species
