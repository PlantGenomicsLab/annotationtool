#!/bin/bash
#SBATCH --job-name=maker
#SBATCH --mail-user=vidya.vuruputoor@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=12
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH -o maker-%j.out
#SBATCH -e maker-%j.err
#SBATCH --mem=50G

#maker round 1
#EST: Trinity, gff from gth added related species
module load perl/5.28.1

/home/FCAM/vvuruputoor/maker/bin/maker -base first_iter maker_opts.ctl maker_bopts.ctl maker_exe.ctl
