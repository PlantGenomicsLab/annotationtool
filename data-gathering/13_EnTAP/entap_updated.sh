#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=70G
#SBATCH --qos=general
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general

module load anaconda/2.4.0
module load perl/5.24.0
module load diamond/0.9.36
module load interproscan/5.25-64.0

prot=../braker/augustus.hints.aa

/labs/Wegrzyn/EnTAP/EnTAP_v0.10.4/EnTAP/EnTAP --runP \
--ini entap_config.ini \
-i $prot \
-d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd \
-d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.205.dmnd \
--threads 16
