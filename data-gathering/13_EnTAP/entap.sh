#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --mem=100G
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load anaconda/4.4.0
module load perl/5.28.1
module load diamond/0.9.19
module load eggnog-mapper/0.99.1
module load interproscan/5.25-64.0


speciesdir=/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/non-model/Delphinapterus_leucas
brakerdir=$speciesdir/analysis/annotation/braker/run1/braker/Beluga #We will be using augustus.hints.aa from this folder
outdir=$speciesdir/analysis/annotation/entap

/UCHC/LABS/Wegrzyn/EnTAP/EnTAP --runP -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.92.dmnd -i $brakerdir/augustus.hints.aa --out-dir $outdir -c bacteria -c archaea -c fungi --threads 32

