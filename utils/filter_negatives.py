import re

kozak_regex = re.compile(r'[ag][ca][ga]atg')
stop_codons = ['tag', 'taa', 'tga']

def filter_negatives(info, start_pad, end_pad):
  if info['label'] != '0':
    return False
  seq = info['seq']
  start = seq[start_pad:start_pad + 3]
  assert(len(start) == 3)
  if start != start.lower():
    return False
  kozak_section = seq[start_pad - 3:start_pad + 3]
  assert(len(kozak_section) == 6)
  if kozak_regex.match(kozak_section) is not None:
    return False
  current_pos = start_pad + 3
  close_stop = False
  while current_pos <= start_pad + 50:
    current_codon = seq[current_pos:current_pos + 3]
    if current_codon.lower() in stop_codons:
      close_stop = True
      break
    current_pos += 3
  if close_stop:
    return True

