from __future__ import print_function
import sys, os

threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
if __name__ == '__main__':
  print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(inter_op_threads)
tf.config.threading.set_intra_op_parallelism_threads(intra_op_threads)

def get_languages():
  base_keys = tf.constant(['0', 'A', 'a', 'C', 'c', 'G', 'g', 'T', 't', 'N', 'n'])
  base_vals = tf.constant([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], dtype=tf.int32)
  input_language = tf.lookup.StaticHashTable(
    tf.lookup.KeyValueTensorInitializer(base_keys, base_vals), 0)
  
  type_keys = tf.constant(['0', '1', '2', '3', '4', '5', '6', '7', '8', '<start>', '<end>'])
  type_vals = tf.constant([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], dtype=tf.float32)
  output_language = tf.lookup.StaticHashTable(
    tf.lookup.KeyValueTensorInitializer(type_keys, type_vals), 0)
  return input_language, output_language

def get_samples(path, batch_size, inp_lang=None, out_lang=None, take=None, skip=None, cache=None, shuffle=None, get_positions=False):

  def format_data(batch):
    if get_positions:
      return tf.map_fn(_format_data, batch, dtype=(tf.int32, tf.bool, tf.string, tf.int32))
    batch = tf.map_fn(_format_data, batch, dtype=((tf.int32, tf.float32), tf.bool))
    return batch

  def _format_data(line):
    items = tf.strings.split(line, ',')
    label = tf.cast(tf.strings.to_number(items[2]), tf.bool)
    seq = items[1]
    struct = items[6]
    #seq = tf.map_fn(lambda x: inp_lang.lookup(x), tf.strings.bytes_split(seq), dtype=tf.int32)
    seq = tf.strings.to_number(tf.strings.split(seq, '|'), tf.int32)
    struct = tf.strings.to_number(tf.strings.split(struct, '|'), tf.float32)
    if get_positions:
      return (seq, struct, label, items[3], tf.cast(tf.strings.to_number(items[4]), tf.int32))
    return ((seq, struct), label)

  if inp_lang is None or out_lang is None:
    inp_lang, out_lang = get_languages()

  if not isinstance(path, list):
    path = [path]
  ds = tf.data.Dataset.from_tensor_slices(path)
  ds = ds.interleave(lambda x: tf.data.TextLineDataset(x), num_parallel_calls=tf.data.experimental.AUTOTUNE)
  # Now all samples from all files are in ds
  if shuffle is not None:
    ds = ds.shuffle(shuffle)

  # Batch ds
  if batch_size is not None:
    ds = ds.batch(batch_size, drop_remainder=True)

  if skip is not None:
    ds = ds.skip(skip)
  if take is not None:
    ds = ds.take(take)
  # Now ds has only the desired samples

  # Map batches from sequence strings to integer tensors
  if batch_size is not None:
    ds = ds.map(format_data, num_parallel_calls=tf.data.experimental.AUTOTUNE)
  else:
    ds = ds.map(_format_data, num_parallel_calls=tf.data.experimental.AUTOTUNE)

  # Cache ds
  if cache is not None:
    ds = ds.cache(cache)

  # Shuffle samples
  #if shuffle is not None:
  #  ds = ds.shuffle(shuffle)

  return ds

def get_split_samples(paths, batch_size, test_batches, cache_dir=None):
  test_cache = train_cache = ''
  if cache_dir is not None:
    test_cache = cache_dir + '/test'
    train_cache = cache_dir + '/train'

  ds = get_samples(paths, None).shuffle(2000, seed=batch_size)#1500)
  test_data = ds.take(test_batches * batch_size)
  train_data = ds.skip(test_batches * batch_size)

  if cache_dir is not None:
    test_data = test_data.cache(test_cache)
    train_data = train_data.cache(train_cache)

  test_data = test_data.batch(batch_size, drop_remainder=True)
  train_data = train_data.shuffle(500).batch(batch_size, drop_remainder=True)
  
  test_data = test_data.repeat().prefetch(tf.data.experimental.AUTOTUNE)
  train_data = train_data.repeat().prefetch(tf.data.experimental.AUTOTUNE)

  return train_data, test_data

if __name__ == '__main__':
  paths = ['../data/train_tis.txt']
  train, test = get_split_samples(paths, 128, 1, 'ds')
  total = train_total = 0
  for _, labels in train.take(15):
    for j in labels:
      if j:
        train_total += 1
  for _, labels in test.take(1):
    for i in labels:
      if i:
        total += 1
  print(total)
  print(train_total)
