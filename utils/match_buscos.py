import os

def grab_gffs(gff_dir):
  gffs = []
  for root, dirs, files in os.walk(gff_dir):
    for name in files:
      gffs.append(os.path.join(root, name))
  return gffs

def add_starts(gff, starts, label, desc):
  with open(gff, 'r') as fn:
    for line in fn:
      if line.startswith('#'):
        continue
      chrom, _, typ, start, end, _, strand = line.split("\t")[:7]
      if (typ == "start_codon"):
        tis = {
          'chrom': chrom,
          'start': int(start),
          'end': int(end),
          'strand': strand,
          'label': label,
          'desc': desc,
        }
        try:
          starts[chrom].append(tis)
        except KeyError:
          starts[chrom] = [tis]

def match_buscos(source, busco_gffs_dir, matches_handle):
  stats = {
    'total_buscos': 0,
    'num_matches': 0,
    'num_misses': 0,
    'total_source': 0,
  }
  matches = {}
  starts_dict = {}
  add_starts(source, starts_dict, '1', 'Ensembl')
  busco_starts = {}
  gff_files = grab_gffs(busco_gffs_dir)
  for gff in gff_files:
    add_starts(gff, busco_starts, '1', 'BUSCO')

  stats['total_source'] = sum(len(i) for i in starts_dict.values())
  idx = 0
  for chrom, start_list in busco_starts.items():
    matches[chrom] = []
    for start in start_list:
      stats['total_buscos'] += 1
      closest_distance = min(abs(start['start']-i['start']) for i in starts_dict[chrom])
      stat_name = 'num_misses'
      if closest_distance == 0:
        if matches_handle is not None:
          matches_handle.write('{},{},{},{}\n'.format(start['chrom'], start['start'], start['end'], start['strand']))
        matches[chrom].append(start)
        stat_name = 'num_matches'
      stats[stat_name] += 1
      idx+=1
  return matches, starts_dict, stats

