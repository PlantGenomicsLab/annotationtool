import re, time
from collections import deque
from canonical import *
from reverse_comp import *
from translate import *
from filter_negatives import *

def write_data(out_fn, info):
  line = '{},{},{},{},{},{}'.format(info['strand'], info['formatted_seq'], info['label'], info['chrom'], info['start'] - 1, info['desc'])
  out_fn.write(line + '\n')

def get_tis(seq_buff, current_pos, info, line_len, start_pad, end_pad):
  site = info['start'] - 1
  strand = info['strand']
  buffer_items = len(seq_buff)
  buff_pos = current_pos - (line_len * buffer_items)
  if strand == '+':
    tis_start = site - start_pad
    tis_end = site + end_pad + 3
  else:
    tis_start = site - end_pad
    tis_end = site + start_pad + 3
  tis_seq = ''
  for line in seq_buff:
    if tis_start >= buff_pos + line_len:
      pass
    elif tis_start > buff_pos and tis_start < (buff_pos + line_len):
      diff = tis_start - buff_pos
      tis_seq += line[diff:]
    elif tis_end < (buff_pos + line_len):
      diff = tis_end - buff_pos
      tis_seq += line[:diff]
    else:
      tis_seq += line
    if len(tis_seq) == 3 + start_pad + end_pad:
      break
    buff_pos += line_len
  info['seq'] = reverse_comp(tis_seq, strand)
  info['formatted_seq'] = format_seq(info['seq'])
  return info

def parse_chrom(out_fn, fasta, f_line, line_len, starts, chrom, start_pad, end_pad, stats):
  buffer_items = int(((max(start_pad, end_pad) / line_len) * 2) + 2)
  seq_num = 0
  current_pos = 0
  prev_start = 0
  starts[chrom] = sorted(starts[chrom], key=lambda x: x['start'])
  seq_buff = deque()
  for _ in range(buffer_items):
    seq_buff.append('')
  filtered_starts = []

  for tis_info in starts[chrom]:
    if tis_info['start'] == prev_start:
      continue
    prev_start = tis_info['start']
    if tis_info['strand'] == '+':
      tis_end = tis_info['end'] + end_pad
    else:
      tis_end = tis_info['end'] + start_pad
    label = tis_info['label']
    desc = tis_info['desc']

    while f_line and (tis_end > current_pos):
      current_pos += line_len
      f_line = fasta.readline().strip()
      seq_buff.append(f_line)
      seq_buff.popleft()

    info = get_tis(seq_buff, current_pos, tis_info, line_len, start_pad, end_pad)
    if (len(info['seq']) != 3 + start_pad + end_pad):
      continue
    assert(len(info['seq']) == 3 + start_pad + end_pad)
    if canonical(info['seq'], start_pad, end_pad):
      write_data(out_fn, info)
      stats['canonical'] += 1
    else:
      stats['non-canonical'] += 1
      continue
    seq_num += 1
    stats['total'] += 1
    filtered_starts.append(info)
  starts[chrom] = filtered_starts
  return f_line

def get_positions(current_chrom, seq_buff, current_pos, line_len, buffer_items, label, desc):
  positions = []
  line_pos = int(current_pos - (line_len * (buffer_items / 2))) + 1
  current_line = seq_buff[int(buffer_items / 2)]
  prev_line = seq_buff[int((buffer_items / 2) - 1)]
  template = {
    'chrom': current_chrom,
    'label': label,
    'desc': desc,
  }
  info = template.copy()

  roll1 = prev_line[-2:] + current_line[0]
  roll2 = prev_line[-1] + current_line[:2]

  if roll1.lower() == 'atg':
    info['start'] = line_pos - 2
    info['end'] = line_pos
    info['strand'] = '+'
    positions.append(info)
    info = template.copy()
  elif roll1.lower() == 'cat':
    info['start'] = line_pos - 2
    info['end'] = line_pos
    info['strand'] = '-'
    positions.append(info)
    info = template.copy()
  if roll2.lower() == 'atg':
    info['start'] = line_pos - 1
    info['end'] = line_pos + 1
    info['strand'] = '+'
    positions.append(info)
    info = template.copy()
  elif roll2.lower() == 'cat':
    info['start'] = line_pos - 1
    info['end'] = line_pos + 1
    info['strand'] = '-'
    positions.append(info)
    info = template.copy()

  for i in range(line_len - 2):
    tis = current_line[i:i+3]
    if tis.lower() == 'atg':
      info['start'] = line_pos
      info['end'] = line_pos + 2
      info['strand'] = '+'
      positions.append(info)
      info = template.copy()
    elif tis.lower() == 'cat':
      info['start'] = line_pos
      info['end'] = line_pos + 2
      info['strand'] = '-'
      positions.append(info)
      info = template.copy()
    line_pos += 1

  return positions

def encode_text(out_fn, fasta, handle, file_format, label, desc, start_info, line_len, start_pad, end_pad):
  check_rank = False
  if 'end' not in file_format.keys():
    file_format['end'] = None
  if 'rank' in file_format.keys():
    check_rank = True
  line = handle.readline().strip()
  while line:
    parts = line.split(',')
    if check_rank and parts[file_format['rank']] != '1':
      line = handle.readline().strip()
      continue
    chrom = parts[file_format['scaffold']]
    start = int(parts[file_format['start']])
    strand = parts[file_format['strand']]
    end = None
    if file_format['end'] is not None:
      end = int(parts[file_format['end']])
    if strand == '1' or strand == '+':
      strand = '+'
      end = start + 2
    else:
      strand = '-'
      if end is None:
        end = start
        start = start - 2
      else:
        start = end - 2
    if chrom not in start_info.keys():
      start_info[chrom] = []
    start_info[chrom].append({
      'chrom': chrom,
      'start': start,
      'end': end,
      'strand': strand,
      'label': label,
      'desc': desc,
    })
    line = handle.readline().strip()
  return encode_dict(out_fn, fasta, start_info, line_len, start_pad, end_pad)

def encode_gff(out_fn, fasta, handle, label, desc, start_info, line_len, start_pad, end_pad):
  line = handle.readline().strip()
  while line:
    if (not line.startswith('#')) and (re.match('.*start_codon', line) is not None):
      parts = line.split('\t')
      info = {
        'chrom': parts[0],
        'strand': parts[6],
        'start': int(parts[3]),
        'end': int(parts[4]),
        'label': label,
        'desc': desc,
      }
      if info['chrom'] not in start_info.keys():
        start_info[info['chrom']] = []
      start_info[info['chrom']].append(info)
    line = handle.readline().strip()
  return encode_dict(out_fn, fasta, start_info, line_len, start_pad, end_pad)

def encode_genome(out_fn, filter_fn, fasta, label, desc, line_len, start_pad, end_pad):
  filtered = {}
  start = time.time()
  seq_buff = deque()
  buffer_items = int(((max(start_pad, end_pad) / line_len) * 2) + 2)
  for _ in range(buffer_items):
    seq_buff.append('')
  f_line = fasta.readline().strip()
  n_lines = 0
  while f_line:
    n_lines += 1
    if f_line[0] == '>':
      current_chrom = f_line.split(' ')[0][1:]
      filtered[current_chrom] = []
      print('chrom ' + current_chrom)
      current_pos = line_len
      f_line = fasta.readline().strip()
      seq_buff.append(f_line)
      seq_buff.popleft()
      continue
    if current_pos < (line_len * buffer_items):
      current_pos += line_len
      f_line = fasta.readline().strip()
      seq_buff.append(f_line)
      seq_buff.popleft()
      continue
    positions = get_positions(current_chrom, seq_buff, current_pos, line_len, buffer_items, label, desc)
    for info in positions:
      get_tis(seq_buff, current_pos, info, line_len, start_pad, end_pad)
      write_data(out_fn, info)
      if filter_negatives(info, start_pad, end_pad):
        write_data(filter_fn, info)
        filtered[current_chrom].append(info)
    current_pos += line_len
    f_line = fasta.readline().strip()
    if not f_line:
      break
    if f_line[0] != '>' and len(f_line) < line_len:
      f_line = fasta.readline().strip()
    seq_buff.append(f_line)
    seq_buff.popleft()
    if n_lines % 100000 == 0:
      print('line number {}...'.format(n_lines))
  print('scan complete! time elapsed: {:.4f}s'.format(time.time() - start))
  return filtered

def encode_dict(out_fn, fasta, starts, line_len, start_pad, end_pad):
  stats = {
    'total': 0,
    'canonical': 0,
    'non-canonical': 0,
  }
  f_line = fasta.readline().strip()
  current_chrom = f_line.split(' ')[0][1:]
  current_pos = 0
  checked_chroms = 0
  while f_line and (checked_chroms < len(starts.keys())):
    if current_chrom in starts.keys():
      f_line = parse_chrom(out_fn, fasta, f_line, line_len, starts, current_chrom, start_pad, end_pad, stats)
      checked_chroms += 1
    else:
      f_line = fasta.readline().strip()
    while f_line and f_line[0] != '>':
      f_line = fasta.readline().strip()
    current_chrom = f_line.split(' ')[0][1:]
  fasta.seek(0)
  return starts, stats

