## Utilities

This directory contains helpful utilities that may be used by all of the steps of the EASEL software.

Some of these scripts are not used anymore, but the majority of them are still in use, especially for the [data preprocessing](../data_preprocessing) step.

### Data Preprocessing

- `add_entropy.py`: Adds RNAfolding data to a dataset file.
- `chunkify.py`: Separate one file into many chunks.
- `encode_data.py`: Converts data from various sources (FASTA, GFF, Ensembl pseudogenes output text file, etc.) to encoded data.
- `filter_negatives.py`: Determines whether an encoded window is one of our "true negatives" based on kozak, early stop codons, soft-masking, etc.
- `get_all_seqs.py`: Get all sequences from an encoded file.
- `get_entropy.py`: Calculate entropy values for multiple sequences.
- `match_buscos.py`: Finds BUSCOs that perfectly match Ensembl locations.
- `reverse_comp.py`: Return the reverse compliment of a sequence based on the base pairs and the strand.
- `select_samples.py`: Randomly chooses samples from a dataset of encoded data.
- `translate.py`: Translate a sequence from our encoding to bases and vice versa.

### TensorFlow

- `dataset.py`: Utilities for converting our encoded data into a [`tf.data.Dataset` object](https://www.tensorflow.org/api_docs/python/tf/data/Dataset).

### Other

- `canonical.py`: Determine whether or not a sequence is canonical.
- `kozak.py`: Calculate some statistics on kozak structure of sequences in an encoded data text file.
- `mfe_stats.py`: Calculate some statistics on entropy encodings using estimate characters from RNAfold (`(){}|.,`).
- `soft_mask.py`: Calculate some statistics on which sequences are soft-masked in an encoded data text file.
- `time_analyze.py`: Some debugging on time bottlenecks for RNAfold python bindings.

