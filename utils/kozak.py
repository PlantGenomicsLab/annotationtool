import operator, re

input_language = {
  '0': '0',
  '1': 'A',
  '2': 'a',
  '3': 'C',
  '4': 'c',
  '5': 'G',
  '6': 'g',
  '7': 'T',
  '8': 't',
  '9': 'N',
  '10': 'n'
}

def top_dict(d, top=3):
  d = sorted(d.items(), key=operator.itemgetter(1))
  d = d[-1 * top:]
  for i in range(len(d)):
    idx = len(d) - i - 1
    print('{}: {}'.format(d[idx][0], d[idx][1]))

def get_kozaks(path, label_column=2, seq_column=1):
  true_genes = total = true_count = pseudo_count = count = 0
  handle = open(path, 'r')
  line = handle.readline().strip()
  non_kozaks = {}
  while line:
    parts = line.split(',')
    true_gene = (parts[label_column] == '1')
    if true_gene:
      true_genes += 1
    seq = translate(parts[seq_column], start=234, stop=245).lower()
    if kozak_regex.match(seq) is not None:
      count += 1
      if true_gene:
        true_count += 1
      else:
        pseudo_count += 1
    else:
      if seq not in non_kozaks.keys():
        non_kozaks[seq] = 0
      non_kozaks[seq] += 1
      assert(len(seq) == 11)
    line = handle.readline().strip()
    total += 1
  return total, true_genes, count, true_count, pseudo_count, non_kozaks

def get_prefixes(path, top=3, label_column=2, seq_column=1):
  busco_prefixes = {}
  pseudo_prefixes = {}
  handle = open(path, 'r')
  line = handle.readline().strip()
  while line:
    parts = line.split(',')
    true_gene = (parts[label_column] == '1')
    seq = translate(parts[seq_column], start=294, stop=305).lower()
    prefix = seq
    if true_gene:
      if prefix not in busco_prefixes.keys():
        busco_prefixes[prefix] = 0
      busco_prefixes[prefix] += 1
    else:
      if prefix not in pseudo_prefixes.keys():
        pseudo_prefixes[prefix] = 0
      pseudo_prefixes[prefix] += 1
    line = handle.readline().strip()
  print('\nbusco:')
  top_dict(busco_prefixes, top)
  print('\npseudo:')
  top_dict(pseudo_prefixes, top)

if __name__ == '__main__':
  from translate import *
  from get_all_seqs import *
  #kozak_regex = re.compile(r'[gc].[cg][ag][ca][cga]atg[ga][ca]')
  #kozak_regex = re.compile(r'...[ag][ca][cga]atg..')
  #kozak_regex = re.compile(r'...[ag][ca][ga]atg[ta].')
  #print('using pattern: \'{}\'\n'.format(kozak_regex.pattern))

  #path = '../data/celegans/full_anno.txt'
  path = '../data/celegans/all_atg_skew.txt'

  get_all_seqs(path)#, start=234, stop=246, true_only=True)
  quit()

  total, true_genes, count, true_count, pseudo_count, non_kozaks = get_kozaks(path)
  pseudogenes = total - true_genes
  print('total kozaks:      {} ({:>4.2f}%)'.format(count, 100. * (count / total)))
  print('true gene kozaks:  {} ({:>4.2f}%)'.format(true_count, 100. * (true_count / true_genes)))
  print('pseudogene kozaks: {} ({:>4.2f}%)\n'.format(pseudo_count, 100. * (pseudo_count / pseudogenes)))
  print('non kozaks:')
  top_dict(non_kozaks)
  #get_prefixes(path)

