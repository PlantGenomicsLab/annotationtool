import argparse
import multiprocessing
import time, math, shutil
from subprocess import Popen, PIPE
import RNA
from translate import *

parser = argparse.ArgumentParser(description='adds RNAfolding data to a dataset file')
parser.add_argument('--dataset', '--in_file', nargs=1, type=str, required=True,
    help='the location of the training data set')
parser.add_argument('--temp_dir', nargs=1, type=str, default='/tmp',
    help='a directory where we can keep temporary files')
parser.add_argument('--out_file', nargs=1, type=str, default=None,
    help='an optional output file. If this argument is not provided, changes will be made to the training data set file.')
parser.add_argument('--finish_incomplete', '--incomplete', action="store_true",
    help='set this option to finish adding entropy to an existing temp directory. This is useful for when a job is killed before it can be completed.')

def chunkify(path, temp_dir, num_chunks):
  cmdstr = "wc -l %s" % path
  proc = Popen(cmdstr.split(), stdout=PIPE, stdin=PIPE)
  stdout, stdin = proc.communicate()
  size = int(stdout.split()[0])
  items_per_chunk = math.ceil(size / num_chunks)
  fn = open(path, 'r')
  print('{} chunks, {} items per chunk'.format(num_chunks, items_per_chunk))

  line = fn.readline()
  chunk_items = 0
  chunk_num = 0
  chunk_path = '{}/chunk_{}.txt'.format(temp_dir, chunk_num)
  chunk_handle = open(chunk_path, 'w')
  while line:
    if chunk_items >= items_per_chunk:
      chunk_num += 1
      chunk_path = '{}/chunk_{}.txt'.format(temp_dir, chunk_num)
      chunk_handle = open(chunk_path, 'w')
      chunk_items = 0
    chunk_handle.write(line)
    chunk_items += 1
    line = fn.readline()
  fn.close()

def combine(out_file, chunk):
  try:
    chunk_handle = open(chunk, 'r')
  except FileNotFoundError:
    return
  line = chunk_handle.readline()
  while line:
    out_file.write(line)
    line = chunk_handle.readline()

def add_entropy(n):
  chunk_path = '{}/chunk_{}.txt'.format(temp_dir, n)
  entropy_path = '{}/entropy_{}.txt'.format(temp_dir, n)
  try:
    chunk_handle = open(chunk_path, 'r')
  except FileNotFoundError:
    return
  print('Working on chunk {}...'.format(n))

  if not args.finish_incomplete:
    entropy_handle = open(entropy_path, 'w')
    start_line_num = 0
  else:
    with open(entropy_path) as f:
      for i, l in enumerate(f):
        pass
    start_line_num = i + 1
    entropy_handle = open(entropy_path, 'a')

  line = chunk_handle.readline().strip()
  line_num = 0
  while line:
    if line_num >= start_line_num:
      fc = RNA.fold_compound(translate(line.split(',')[1]))
      fc.pf()
      entropies = fc.positional_entropy()[1:]
      entropy_handle.write('{},{}\n'.format(line, '|'.join([str(i) for i in entropies])))
    line_num += 1
    line = chunk_handle.readline().strip()

  return n

if __name__ == '__main__':
  args = parser.parse_args()
  num_cpus = multiprocessing.cpu_count()
  in_file = args.dataset[0] if isinstance(args.dataset, list) else args.dataset
  temp_dir = args.temp_dir[0] if isinstance(args.temp_dir, list) else args.temp_dir
  out_file = args.out_file[0] if isinstance(args.out_file, list) else args.out_file
  start = time.time()

  print('Using {} cpus'.format(num_cpus))
  if not args.finish_incomplete:
    print('Splitting file into chunks...')
    chunkify(in_file, temp_dir, num_cpus)

  print('Calculating entropy data...')
  pool = multiprocessing.Pool()
  result = pool.map(add_entropy, range(num_cpus))
  temp_handle = open(temp_dir + 'combine.txt', 'w')
  print('Combining entropy results...')
  for i in range(num_cpus):
    combine(temp_handle, '{}/entropy_{}.txt'.format(temp_dir, i))
  temp_handle.close()
  if out_file is None:
    out_file = in_file
  shutil.move(temp_dir + 'combine.txt', out_file)
  print('Entropy calculations complete!')
  print('Time elapsed: {:.4f}s'.format(time.time() - start))
  print('Results will appear in {}'.format(out_file))


