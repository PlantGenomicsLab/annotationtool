
rev = {
  'a': 't',
  'c': 'g',
  'g': 'c',
  't': 'a',
  'A': 'T',
  'C': 'G',
  'G': 'C',
  'T': 'A',
  'n': 'n',
  'N': 'N',
}

def reverse_comp(seq, strand='-'):
  # Return the reverse compliment of a sequence based on the base pairs and the strand
  if strand == '+':
    return seq
  new_seq = ''
  for char in seq:
    new_seq += rev[char]
  return new_seq[::-1]

