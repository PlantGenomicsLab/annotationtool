import numpy as np
from matplotlib import pyplot as plt
import RNA
import time

species = 'drosophila'
start_pad = 240
end_pad = 60
width = 1
struct_fig = species + '_structure.png'
status = True

encoding_map = {
  '0': 0,
  '1': '.',
  '2': ',',
  '3': '|',
  '4': '{',
  '5': '}',
  '6': '(',
  '7': ')',
}

input_language = {
  '0': '0',
  '1': 'A',
  '2': 'a',
  '3': 'C',
  '4': 'c',
  '5': 'G',
  '6': 'g',
  '7': 'T',
  '8': 't',
  '9': 'N',
  '10': 'n'
}

seq = [0] * (start_pad + end_pad + 3)
accum = seq.copy()

ind = np.arange(-1 * start_pad, end_pad + 3)
mountains = []

path = '../data/' + species + '/train_tis_structure_encoded.txt'
handle = open(path, 'r')
line = handle.readline().strip()
line_num = 0
start = time.time()
while line:
  line_num += 1
  if line_num % 100 == 0:
    print('on line {}...'.format(line_num))
  seq = ''.join([input_language[i] for i in line.split(',')[1].split('|')])
  source = line.split(',')[5]
  if source == 'true_genes':
    color = 'blue'
  elif source == 'pseudogenes':
    color = 'green'
  else:
    color = 'red'
  fc = RNA.fold_compound(seq)
  fc.pf()
  entropies = fc.positional_entropy()[1:]
  plt.plot(ind, entropies, color=color, linewidth='.2')
  line = handle.readline().strip()

#plt.plot(ind, np.transpose(mountains), color='black')
print('structure plot complete! time elapsed: {}'.format(time.time() - start))

plt.savefig(struct_fig)

