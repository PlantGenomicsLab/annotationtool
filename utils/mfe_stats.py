
encoding_map = {
  '0': 0,
  '1': 0.16,
  '2': 0.5,
  '3': 0.83,
  '4': 0.5,
  '5': 0.5,
  '6': 0.83,
  '7': 0.83,
}

mfe_path = '../data/celegans/train_tis_struct_skew2.txt'
mfe_handle = open(mfe_path, 'r')
line = mfe_handle.readline().strip()
total = 0
count = 0
while line:
  parts = line.split(',')
  if parts[2] == '0':
    total += sum([encoding_map[i] for i in parts[6].split('|')])
    count += len(parts[0])
  line = mfe_handle.readline().strip()
print('total:   {}'.format(total))
print('average: {}'.format(total / count))
