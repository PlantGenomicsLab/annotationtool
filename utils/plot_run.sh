#!/bin/bash
#SBATCH --job-name=plot_struct
#SBATCH -o plot_struct.out
#SBATCH -e plot_struct.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=32
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=20G

echo `hostname`
source ~/.bashrc
cd /labs/Wegrzyn/annotationtool/utils
python -u plot_structure_avg.py
