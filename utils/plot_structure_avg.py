import numpy as np
from matplotlib import pyplot as plt
import RNA
import time

species = 'celegans'
start_pad = 240
end_pad = 60
width = 1
pad_str = str(start_pad) + '_' + str(end_pad)
data_dir = '/scratch/par12005/data/' + species + '/' + pad_str
struct_fig = species + '_structure_avg.png'
status = True

seq = [0] * (start_pad + end_pad + 3)
accum = seq.copy()

ind = np.arange(-1 * start_pad, end_pad + 3)
mountains = {
  'BUSCO': seq.copy(),
  'pseudogenes': seq.copy(),
  'all_atg': seq.copy(),
  'Ensembl': seq.copy()
}
totals = {
  'BUSCO': 0,
  'pseudogenes': 0,
  'all_atg': 0,
  'Ensembl': 0,
}

include_files = [
  data_dir + '/train_set_entropy.txt',
  data_dir + '/val_set_entropy.txt',
]

start = time.time()
for path in include_files:
  handle = open(path, 'r')
  line = handle.readline().strip()
  while line:
    entropies = line.split(',')[6].split('|')
    source = line.split(',')[5]
    for i in range(len(entropies)):
      mountains[source][i] += float(entropies[i])
    line = handle.readline().strip()
    totals[source] += 1


print('structure plot completed! time elapsed: {}'.format(time.time() - start))

labels = ['BUSCO', 'Ensembl', 'pseudogenes', 'all_atg']
for label in labels:
  for idx in ind:
    if totals[label] != 0:
      mountains[label][idx] = mountains[label][idx] / totals[label]

plt.plot(ind, mountains['BUSCO'], color='blue', linewidth=width)
plt.plot(ind, mountains['Ensembl'], color='purple', linewidth=width)
plt.plot(ind, mountains['pseudogenes'], color='green', linewidth=width)
plt.plot(ind, mountains['all_atg'], color='red', linewidth=width)
plt.savefig(struct_fig)

