from translate import translate

def get_all_seqs(path, label_column=2, seq_column=1, start=None, stop=None, true_only=False):
  handle = open(path, 'r')
  line = handle.readline().strip()
  while line:
    parts = line.split(',')
    true_gene = (parts[label_column] == '1')
    seq = translate(parts[seq_column], start=start, stop=stop)
    seq = seq[:6].upper() + seq[6:9].lower() + seq[9:].upper()
    if not true_only or true_gene:
      print(seq)
    line = handle.readline().strip()

if __name__ == '__main__':
  import argparse
  parser = argparse.ArgumentParser(description='get all sequences from an encoded file')
  parser.add_argument('--encoded_file', '-f', nargs=1, default=None, type=str,
      help='the path to the file with all potential start sites')
  parser.add_argument('--label_column', '-l', nargs=1, default=None, type=int,
      help='the column in the encoded file that contains the label')
  parser.add_argument('--seq_column', '-s', nargs=1, default=None, type=int,
      help='the column in the encoded file that contains the sequence')
  parser.add_argument('--start', nargs=1, default=None, type=int,
      help='the position in each sequence to start with')
  parser.add_argument('--stop', nargs=1, default=None, type=int,
      help='the position in each sequence to stop with')
  parser.add_argument('--true_only', '--true', nargs=1, type=bool,
      help='whether to use true genes only')
  args = parser.parse_args()

  true_only = False
  label_col = 2
  seq_col = 1
  start = None
  stop = None

  if args.label_column is not None:
    label_col = args.label_column
  if args.seq_column is not None:
    seq_col = args.seq_column
  if args.start is not None:
    start = args.start
  if args.stop is not None:
    stop = args.stop
  if args.true_only is not None:
    true_only = True
  path = args.encoded_file[0]
  get_all_seqs(path, label_col, seq_col, start, stop, true_only)
