import RNA
import time
import argparse
import numpy as np

parser = argparse.ArgumentParser(description='calculate entropy values for multiple sequences')
parser.add_argument('--in_file', '-i', nargs=1, default=None, type=str,
    help='the file with the sequences')
parser.add_argument('--out_file', '-o', nargs=1, default=None, type=str,
    help='the results file')

args = parser.parse_args()
in_path = args.in_file[0]
out_path = args.out_file[0]

input_language = {
  '0': '0',
  '1': 'A',
  '2': 'a',
  '3': 'C',
  '4': 'c',
  '5': 'G',
  '6': 'g',
  '7': 'T',
  '8': 't',
  '9': 'N',
  '10': 'n'
}

handle = open(in_path, 'r')
line = handle.readline().strip()
out_handle = open(out_path, 'w')
line_num = 0
start = time.time()
while line:
  line_num += 1
  seq = ''.join([input_language[i] for i in line.split(',')[1].split('|')])
  fc = RNA.fold_compound(seq)
  fc.pf()
  entropies = fc.positional_entropy()[1:]
  out_handle.write('{},{}\n'.format(line, '|'.join([str(i) for i in entropies])))
  line = handle.readline().strip()

total_time = time.time() - start
print('time elapsed: {:.5f}'.format(total_time))

