import RNA
import time
import numpy as np

input_language = {
  '0': '0',
  '1': 'A',
  '2': 'a',
  '3': 'C',
  '4': 'c',
  '5': 'G',
  '6': 'g',
  '7': 'T',
  '8': 't',
  '9': 'N',
  '10': 'n'
}

species = 'celegans'
start_pad = 240
end_pad = 60
width = 1

seq = [0] * (start_pad + end_pad + 3)
accum = seq.copy()

ind = np.arange(-1 * start_pad, end_pad + 3)
mountains = []

path = '../../data/' + species + '/train_tis_structure_encoded.txt'
handle = open(path, 'r')
line = handle.readline().strip()
line_num = 0
start = time.time()
init_time = 0
calc_time = 0
entr_time = 0
md = RNA.md()
md.compute_bpp = 0
while line:
  line_num += 1
  if line_num % 100 == 0:
    break
  seq = ''.join([input_language[i] for i in line.split(',')[1].split('|')])
  source = line.split(',')[5]
  init_start = time.time()
  fc = RNA.fold_compound(seq, md)
  init_time += time.time() - init_start
  calc_start = time.time()
  fc.pf()
  calc_time += time.time() - calc_start
  entr_start = time.time()
  entropies = fc.positional_entropy()[1:]
  entr_time += time.time() - entr_start
  line = handle.readline().strip()

total_time = time.time() - start
print('total time:                      {:.5f}'.format(total_time))
print('initialization  time:            {:.5f}'.format(init_time))
print('probability calculation time:    {:.5f}'.format(calc_time))
print('entropy calculation time:        {:.5f}'.format(entr_time))
