import operator

input_language = {
  '0': '0',
  '1': 'A',
  '2': 'a',
  '3': 'C',
  '4': 'c',
  '5': 'G',
  '6': 'g',
  '7': 'T',
  '8': 't',
  '9': 'N',
  '10': 'n'
}

def get_soft_masks(path, label_column=2, seq_column=1):
  total = true_genes = soft = true_soft = 0
  handle = open(path, 'r')
  line = handle.readline().strip()
  while line:
    parts = line.split(',')
    true_gene = (parts[label_column] == '1')
    if true_gene:
      true_genes += 1
    seq = [input_language[i] for i in parts[seq_column].split('|')[300:303]]
    seq = ''.join(seq)
    if seq != seq.upper():
      soft += 1
      if true_gene:
        true_soft += 1
    total += 1
    line = handle.readline().strip()
  return total, true_genes, soft, true_soft

if __name__ == '__main__':
  #path = '../data/celegans/train_tis_exact_only.txt'
  path = '../data/celegans/full_anno.txt'
  total, true_genes, soft, true_soft = get_soft_masks(path)
  print('total genes + pseudogenes:    {}'.format(total))
  print('total true genes:             {}'.format(true_genes))
  print('total pseudogenes:            {}\n'.format(total - true_genes))
  print('total soft masked:            {}'.format(soft))
  print('soft masked true genes:       {}'.format(true_soft))
  print('soft masked pseudo genes:     {}'.format(soft - true_soft))

