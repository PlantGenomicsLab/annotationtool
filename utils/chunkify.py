import time, argparse, math
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description='separate one file into many chunks')
parser.add_argument('--in_file', '-i', nargs=1, default=None, type=str,
    help='the file to be split into chunks')
parser.add_argument('--chunk_dir', '-d', nargs=1, default='chunks', type=str,
    help='the directory where the chunks should appear')
parser.add_argument('--chunks', '-c', nargs=1, default=None, type=int,
    help='the number of chunks')

args = parser.parse_args()

chunks = args.chunks[0]
chunk_dir = args.chunk_dir[0]
path = args.in_file[0]
chunk_num = 0

cmdstr = "wc -l %s" % path
proc = Popen(cmdstr.split(), stdout=PIPE, stdin=PIPE)
stdout, stdin = proc.communicate()
size = int(stdout.split()[0])
items_per_chunk = math.ceil(size / chunks)

chunk_path = '{}/chunk_{}.txt'.format(chunk_dir, chunk_num)
chunk_handle = open(chunk_path, 'w')

handle = open(path, 'r')
line = handle.readline()
chunk_items = 0
while line:
  if chunk_items >= items_per_chunk:
    chunk_num += 1
    chunk_path = '{}/chunk_{}.txt'.format(chunk_dir, chunk_num)
    chunk_handle = open(chunk_path, 'w')
    chunk_items = 0
  chunk_handle.write(line)
  chunk_items += 1
  line = handle.readline()

