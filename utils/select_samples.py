import numpy as np
from encode_data import write_data
from subprocess import Popen, PIPE

def flatten(info):
  result = []
  for chrom_info in info.values():
    result += chrom_info
  return result

def select_samples_file(choose, path, out_fn, dataset):
  cmdstr = "wc -l %s" % path
  in_fn = open(path, 'r')
  proc = Popen(cmdstr.split(), stdout=PIPE, stdin=PIPE)
  stdout, stdin = proc.communicate()
  size = int(stdout.split()[0])
  choose = min(choose, size)
  choices = np.random.choice(size, choose, replace=False)

  line = in_fn.readline()
  idx = 0
  while line:
    if idx in choices:
      out_fn.write(line)
    idx += 1
    line = in_fn.readline()

def select_samples(choose, info, out_fn, dataset):
  if isinstance(info, str):
    return select_samples_file(choose, info, out_fn, dataset)
  size = sum(len(i) for i in info.values())
  if size == 0:
    return dataset
  choose = min(choose, size)
  flat = flatten(info)
  choices = np.random.choice(flat, choose, replace=False)
  for item in choices:
    write_data(out_fn, item)
  dataset += list(choices)
  return dataset

