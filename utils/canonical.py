
def canonical(seq, start_pad, end_pad):
  # determine whether or not a sequence is canonical
  return seq[start_pad:(-1 * end_pad)].lower() == 'atg'

