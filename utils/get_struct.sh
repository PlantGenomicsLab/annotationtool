#!/bin/bash
#SBATCH --job-name=get_struct
#SBATCH -o get_struct.out
#SBATCH -e get_struct.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=32
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=20G

seq_file='a.txt'
intermediate='all_atg_full_structure.txt'
result='c.txt'

echo `hostname`
source ~/.bashrc
cd /labs/Wegrzyn/annotationtool/utils
python kozak.py > $seq_file
echo 'starting RNAfold...'
~/ViennaRNA-2.4.14/src/bin/RNAfold -p -P DNA --MEA --jobs --noconv --noPS --noDP < $seq_file > $intermediate
echo 'RNAfold complete!'
grep $intermediate -e '\[' > $result
echo 'grep complete: results are in' $result

