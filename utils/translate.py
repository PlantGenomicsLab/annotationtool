
language_to_base = {
  '0': '0',
  '1': 'A',
  '2': 'a',
  '3': 'C',
  '4': 'c',
  '5': 'G',
  '6': 'g',
  '7': 'T',
  '8': 't',
  '9': 'N',
  '10': 'n'
}

language_to_int = {
  '0': '0',
  'A': '1',
  'a': '2',
  'C': '3',
  'c': '4',
  'G': '5',
  'g': '6',
  'T': '7',
  't': '8',
  'N': '9',
  'n': '10'
}

def format_seq(seq):
  # Format a sequence of bases to our encoding
  return '|'.join([language_to_int[i] for i in seq])

def translate(seq, start=None, stop=None):
  # Translate a sequence from our encoding to bases
  if start is not None and stop is not None:
    return ''.join([language_to_base[i] for i in seq.split('|')[start:stop]])
  else:
    return ''.join([language_to_base[i] for i in seq.split('|')])

