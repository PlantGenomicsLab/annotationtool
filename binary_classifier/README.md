## Binary TIS Classifier

The `classifier.py` script contained in this directory creates a binary classifier for transcription start sites, and trains the model based on provided data.

The structure of the model is loosely based on the research conducted in [this paper](https://academic.oup.com/bioinformatics/article/35/7/1125/5089227).

A simplified visualization of the model can be found [here](https://mariusbrataas.github.io/flowpoints_ml/?p=MgJga0Gdaj6E).

### Brief summary of trained model attempts

| Species | Window Size | RNAfold? | # Training BUSCOs | # Training pseudogenes | # Training non-pseudogene true negatives | Overall Accuracy (%) | True Gene Accuracy (%) | Notes |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| celegans | 300/300 | no | 960 | 1136 | 0 | **72.63** | **43.24** |
| celegans | 240/60 | no | 960 | 1136 | 0 | **80.1** | **45.45** |
| celegans | 240/60 | no | 960 | 1136 | 1000 | **82.47** | **37.19** |
| celegans | 240/60 | no | 789 | 500 | 500 | **66.25** | **61.21** |
| celegans | 240/60 | yes (character encodings) | 789 | 500 | 500 | **68.24** | **61.87** |
| celegans | 240/60 | yes (character encodings) | 789 | 200 | 500 | **40.85** | **83.01** |
| celegans | 240/60 | yes (character encodings) | 789 | 200 | 500 | **37.83** | **83.14** | ignore softmasked windows in statistics evalution |
| drosophila | 240/60 | yes (character encodings) | 982 | 73 | 859 | **22.4** | **94.57** |

More detailed trained model attempts and the resulting statistics can be found [here](https://docs.google.com/spreadsheets/d/1RRxci03ixGcMhkssYMn83LOPWzz20Gp04vidIp4lgS4/edit#gid=0).

### Contents of this directory

- `gan_classifier.py`: A script that creates a binary classifier using a GAN model structure. This file contains inline comments that should help explain what different parts of the training loop are for.
- `opt_test.py`: A smaller test version of the `gan_classifier.py` script that can be used for testing/development. This script is a little bit easier to deal with but is not meant for end users/production.
- `classifier.py`: A script that creates a binary classifier for transcription start sites, and trains the model based on provided data.
- `predict.py`: A script that loads a model and a dataset (usually the `all_atg` dataset) and creates predictions based on that dataset.
- `sens_spec.py`: A script that takes a file of predictions and calculates some statistics based on the predictions the model made and the true answers.

### Conda environment packages:
```
$ conda list
# packages in environment at /home/CAM/prichter/miniconda2/envs/tensorflow:
#
# Name                    Version                   Build  Channel
_libgcc_mutex             0.1                        main
absl-py                   0.9.0                    pypi_0    pypi
astor                     0.8.1                    pypi_0    pypi
ca-certificates           2019.11.27                    0
cachetools                3.1.1                    pypi_0    pypi
certifi                   2019.11.28               pypi_0    pypi
chardet                   3.0.4                    pypi_0    pypi
cycler                    0.10.0                   pypi_0    pypi
gast                      0.2.2                    pypi_0    pypi
google-auth               1.9.0                    pypi_0    pypi
google-auth-oauthlib      0.4.1                    pypi_0    pypi
google-pasta              0.1.8                    pypi_0    pypi
grpcio                    1.25.0                   pypi_0    pypi
h5py                      2.10.0                   pypi_0    pypi
idna                      2.8                      pypi_0    pypi
joblib                    0.14.1                   pypi_0    pypi
keras-applications        1.0.8                    pypi_0    pypi
keras-preprocessing       1.1.0                    pypi_0    pypi
kiwisolver                1.1.0                    pypi_0    pypi
libedit                   3.1.20181209         hc058e9b_0
libffi                    3.2.1                hd88cf55_4
libgcc-ng                 9.1.0                hdf63c60_0
libstdcxx-ng              9.1.0                hdf63c60_0
markdown                  3.1.1                    pypi_0    pypi
matplotlib                3.1.2                    pypi_0    pypi
ncurses                   6.1                  he6710b0_1
numpy                     1.17.4                   pypi_0    pypi
oauthlib                  3.1.0                    pypi_0    pypi
openssl                   1.1.1d               h7b6447c_3
opt-einsum                3.1.0                    pypi_0    pypi
pip                       19.3.1                   py37_0
protobuf                  3.11.1                   pypi_0    pypi
pyasn1                    0.4.8                    pypi_0    pypi
pyasn1-modules            0.2.7                    pypi_0    pypi
pyparsing                 2.4.6                    pypi_0    pypi
python                    3.7.5                h0371630_0
python-dateutil           2.8.1                    pypi_0    pypi
readline                  7.0                  h7b6447c_5
requests                  2.22.0                   pypi_0    pypi
requests-oauthlib         1.3.0                    pypi_0    pypi
rsa                       4.0                      pypi_0    pypi
scikit-learn              0.22.1                   pypi_0    pypi
scipy                     1.4.1                    pypi_0    pypi
setuptools                42.0.2                   pypi_0    pypi
six                       1.13.0                   pypi_0    pypi
sklearn                   0.0                      pypi_0    pypi
sqlite                    3.30.1               h7b6447c_0
tensorboard               2.1.0                    pypi_0    pypi
tensorflow-estimator      2.1.0                    pypi_0    pypi
tensorflow-gpu            2.1.0                    pypi_0    pypi
termcolor                 1.1.0                    pypi_0    pypi
tk                        8.6.8                hbc83047_0
urllib3                   1.25.7                   pypi_0    pypi
werkzeug                  0.16.0                   pypi_0    pypi
wheel                     0.33.6                   pypi_0    pypi
wrapt                     1.11.2                   pypi_0    pypi
xz                        5.2.4                h14c3975_4
zlib                      1.2.11               h7b6447c_3
```
