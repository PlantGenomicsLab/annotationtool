import time
from collections import deque
from sklearn.metrics import roc_curve, roc_auc_score
import matplotlib.pyplot as plt
import getpass

def print_breakdown(results):
  total = results['true_neg'] + results['true_pos'] + results['false_neg'] + results['false_pos']
  print('true positive: {:>8} false positive: {:>8}'.format(results['true_pos'], results['false_pos']))
  print('true negative: {:>8} false negative: {:>8}'.format(results['true_neg'], results['false_neg']))
  print('overall accuracy    (true pos + true neg / total):       {:>4.2f}%'.format(100. * (results['true_pos'] + results['true_neg']) / total))
  print('true gene accuracy  (true pos / (true pos + false neg)): {:>4.2f}%'.format(100. * (results['true_pos'] / (results['true_pos'] + results['false_neg']))))
  print('pseudogene accuracy (true neg / (true neg + false pos)): {:>4.2f}%'.format(100. * (results['true_neg'] / (results['true_neg'] + results['false_pos']))))

input_language = {
  '0': '0',
  '1': 'A',
  '2': 'a',
  '3': 'C',
  '4': 'c',
  '5': 'G',
  '6': 'g',
  '7': 'T',
  '8': 't',
  '9': 'N',
  '10': 'n'
}

species = 'celegans'
user = getpass.getuser()
preds_path = species + '_classifier/predictions_entropy.txt'
preds_anno_path = species + '_classifier/annotated_predictions_entropy.txt'
preds_roc_fig = species + '_classifier/roc_figure.png'
all_tis_path = '/scratch/' + user + '/data/' + species + '/all_atg_entropy.txt'
tis_path = '/scratch/' + user + '/data/' + species + '/full_anno.txt'
window_size = 10
include_empty = False
skip_softmask = False

tis_handle = open(tis_path, 'r')
preds_handle = open(preds_path, 'r')
preds_anno = open(preds_anno_path, 'w')
all_handle = open(all_tis_path, 'r')

tis = tis_handle.readline().strip()
pred = preds_handle.readline().strip()
sample = all_handle.readline().strip()

preds_buffer = deque()
buffer_pos = preds_handle.tell()

exact_results = {
  'true_neg': 0,
  'false_neg': 0,
  'false_pos': 0,
  'true_pos': 0
}
fuzzy_results = exact_results.copy()
low_thresh_results = exact_results.copy()
threshold = None

start = time.time()
tis_total = 0
tis_vals = {}
tis = tis_handle.readline().strip()
while tis:
  tis_total += 1
  tis_parts = tis.split(',')
  tis_vals[tis_parts[3] + ':' + tis_parts[4]] = (tis_parts[2] == '1')
  tis = tis_handle.readline().strip()

true_vals = []
pred_vals = []

while pred:
  seq = '' #TODO
  current_pred = preds_handle.tell()
  exact_correct = fuzzy_correct = False

  pred_parts = pred.split('|')
  pred_pos_raw = pred_parts[0]
  pred_chrom = pred_parts[0].split(':')[0]
  pred_pos = int(pred_parts[0].split(':')[1])
  pred_strand = pred_parts[1]
  pred_val = (pred_parts[2] == 'True')
  pred_raw = float(pred_parts[3])

  if skip_softmask:
    sample_seq = ''.join([input_language[i] for i in sample.split(',')[1].split('|')])
    assert(len(sample_seq) == 303)
    if sample_seq == sample_seq.lower():
      sample = all_handle.readline().strip()
      pred = preds_handle.readline().strip()
      continue

  if window_size is not None:
    while len(preds_buffer) > 0 and (preds_buffer[0]['pos'] < (pred_pos - window_size) or preds_buffer[0]['chrom'] != pred_chrom):
      preds_buffer.popleft()

  real_val = False
  if pred_pos_raw in tis_vals.keys():
    real_val = tis_vals[pred_pos_raw]
  #pred_vals.append(pred_raw)
  true_vals.append(real_val)

  if pred_val and real_val:
    exact_results['true_pos'] += 1
    fuzzy_results['true_pos'] += 1
    exact_correct = fuzzy_correct = True
  elif not (pred_val or real_val):
    exact_results['true_neg'] += 1
    fuzzy_results['true_neg'] += 1
    exact_correct = fuzzy_correct = True
  elif (not pred_val) and real_val:
    exact_results['false_neg'] += 1
  elif (not real_val) and pred_val:
    exact_results['false_pos'] += 1

  if threshold is not None:
    if real_val and pred_raw > threshold:
      low_thresh_results['true_pos'] += 1
    elif real_val and pred_raw <= threshold:
      low_thresh_results['false_neg'] += 1
    elif (not real_val) and pred_raw <= threshold:
      low_thresh_results['true_neg'] += 1
    elif (not real_val) and pred_raw > threshold:
      low_thresh_results['false_pos'] += 1

  # if we weren't correct the first time, check the window
  closest_raw = pred_raw
  if not exact_correct and window_size is not None:
    # look backwards
    for item in preds_buffer:
      if item['val'] == real_val:
        fuzzy_correct = True
      if real_val and item['raw'] > closest_raw:
        closest_raw = item['raw']
      if not real_val and item['raw'] < closest_raw:
        closest_raw = item['raw']

    # if backwards not helpful, look forwards
    next_pred = preds_handle.readline().strip()
    while next_pred:
      next_pred_parts = next_pred.split('|')
      next_pred_chrom = next_pred_parts[0].split(':')[0]
      next_pred_pos = int(next_pred_parts[0].split(':')[1])
      next_pred_val = (next_pred_parts[2] == '1')
      next_pred_raw = float(next_pred_parts[3])
      if next_pred_chrom != pred_chrom:
        break
      if next_pred_pos > pred_pos + window_size:
        break
      if next_pred_val == real_val:
        fuzzy_correct = True
      if real_val and next_pred_raw > closest_raw:
        closest_raw = next_pred_raw
      if not real_val and next_pred_raw < closest_raw:
        closest_raw = next_pred_raw
      next_pred = preds_handle.readline().strip()

    if fuzzy_correct and real_val:
      fuzzy_results['true_pos'] += 1
    elif fuzzy_correct and not real_val:
      fuzzy_results['true_neg'] += 1
    elif not (fuzzy_correct or real_val):
      fuzzy_results['false_pos'] += 1
    elif (not fuzzy_correct) and real_val:
      fuzzy_results['false_neg'] += 1

  pred_vals.append(closest_raw)
  preds_anno.write('{}:{}|{}|{}|{}|{}|{}|{}\n'.format(pred_chrom, pred_pos, pred_strand, pred_val, pred_raw, exact_correct, fuzzy_correct, seq))

  if window_size is not None:
    preds_buffer.append({
      'chrom': pred_chrom,
      'pos': pred_pos,
      'val': pred_val,
      'raw': pred_raw,
    })
  preds_handle.seek(current_pred)
  pred = preds_handle.readline().strip()
  sample = all_handle.readline().strip()

#fpr, tpr, thresholds = roc_curve(true_vals, pred_vals)
#auc_score = roc_auc_score(true_vals, pred_vals)
#plt.plot(fpr, tpr)
#plt.axis([0,1,0,1])
#plt.xlabel('False Positive Rate')
#plt.ylabel('True Positive Rate')
#plt.savefig(preds_roc_fig)

print('time elapsed: {:.2f}s'.format(time.time() - start))
total = exact_results['true_neg'] + exact_results['true_pos'] + exact_results['false_neg'] + exact_results['false_pos']
total_pos = exact_results['true_pos'] + exact_results['false_pos']
total_neg = total - total_pos

print('total predictions:    {:>8}'.format(total))
print('positive predicitons: {:>8} ({:.2f}%)'.format(total_pos, 100. * (total_pos / total)))
print('negative predicitons: {:>8} ({:.2f}%)'.format(total_neg, 100. * (total_neg / total)))

print('\nexact matches:')
print_breakdown(exact_results)

if threshold is not None:
  print('\nthreshold of {}:'.format(threshold))
  print_breakdown(low_thresh_results)

if window_size is not None:
  print('\n{}bp window:'.format(window_size))
  print_breakdown(fuzzy_results)

#print('AUC Score: {}'.format(auc_score))

