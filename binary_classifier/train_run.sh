#!/bin/bash
#SBATCH --job-name=train_tis_class
#SBATCH -o slurm_train_tis.out
#SBATCH -e slurm_train_tis.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=4
#SBATCH --nodelist=gpu03
#SBATCH --partition=gpu_v100
#SBATCH --mem=10G
#SBATCH --gres=gpu:1

echo '' > slurm_train_tis.out
echo '' > slurm_train_tis.err
echo `hostname`
source ~/.bashrc
cd ~/annotationtool/binary_classifier

[[ -z "$py_exec" ]] && export py_exec=$py_exec || export py_exec='python3'
echo "using python executable: `which $py_exec`"
#rm -drf logs/
$py_exec -u classifier.py
#tar -czf logs.tar.gz logs/
echo 'training complete'
