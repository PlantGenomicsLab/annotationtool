import sys, os

threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(inter_op_threads)
tf.config.threading.set_intra_op_parallelism_threads(intra_op_threads)

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  try :
    for gpu in gpus:
      tf.config.experimental.set_memory_growth(gpu, True)
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    print(e)

from tensorboard.plugins.hparams import api as hp
from tensorflow.keras.models import Sequential, Model, load_model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.backend import clear_session
from tensorflow.keras import layers
from tensorflow.keras import regularizers as regs
sys.path.append('..')
from utils.dataset import *
from binary_classifier import *
import numpy as np
from subprocess import Popen, PIPE
import math
from tensorflow.keras.callbacks import Callback
import getpass
import time

def train_test_model(hparams=None, run_dir=None):
  global loss, callbacks
  if hparams is not None:
    model = build_model(hparams)
    verbose = 0
    callbacks = [
      tf.keras.callbacks.TensorBoard(run_dir, histogram_freq=10, write_graph=False, write_images=True, embeddings_freq=10),
      tf.keras.callbacks.EarlyStopping(min_delta=0, patience=200),
      hp.KerasCallback(run_dir, hparams)
    ]
  else:
    print('building model')
    model = build_model(None, conv_settings, lstm_settings, l2_reg, drop_rate, embedding_dim)
    verbose = 2
  model.compile(optimizer=opt, loss=loss, metrics=metrics)
  if hparams is None:
    print('model compiled:')
    model.summary()
    print('starting training')
    start = time.time()
  model.fit(train_data,
          verbose=verbose,
          epochs=n_epochs,
          steps_per_epoch=train_batches,
          validation_data=test_data,
          validation_steps=test_batches,
          callbacks=callbacks,
          class_weight=class_weight)
  if hparams is None:
    print('training complete!')
    print('training time: {:.2f}s'.format(time.time() - start))
    print('evaluating model...')
  return model, model.evaluate(test_data, steps=test_batches, verbose=0)

print('tf imported')

species = 'celegans'
start_pad = '240'
end_pad = '240'
pad_str = start_pad + '_' + end_pad
user = getpass.getuser()
data_dir = '/scratch/' + user + '/data/' + species + '/' + pad_str
ds_dir = '/scratch/' + user + '/ds/' + species + '/' + pad_str

paths = [
  data_dir + '/train_set_entropy.txt',
]
val_paths = [
  data_dir + '/val_set_entropy.txt',
]
model_location = species + '_classifier/' + pad_str + '/tis_entropy_classifier.h5'

print('getting number of samples')
total_samples = total_val_samples = 0
train_pos = 0
for path in paths:
  cmdstr = "wc -l %s" % path
  proc = Popen(cmdstr.split(), stdout=PIPE, stdin=PIPE)
  stdout, stdin = proc.communicate()
  total_samples += int(stdout.split()[0])
  cmdstr = "grep -c --regexp=.,.*,1,.*,.*,.*,.*$ %s" % path
  proc = Popen(cmdstr.split(), stdout=PIPE, stdin=PIPE)
  stdout, stdin = proc.communicate()
  train_pos += int(stdout.split()[0])
for path in val_paths:
  cmdstr = "wc -l %s" % path
  proc = Popen(cmdstr.split(), stdout=PIPE, stdin=PIPE)
  stdout, stdin = proc.communicate()
  total_val_samples += int(stdout.split()[0])

tf.keras.backend.clear_session()
batch_size = 128
n_epochs = 500
n_steps = total_samples // batch_size
val_steps = total_val_samples // batch_size
loss = tf.keras.losses.BinaryCrossentropy()
metrics = [
  tf.keras.metrics.BinaryCrossentropy(name='binary_crossentropy'),
  tf.keras.metrics.BinaryAccuracy(name='accuracy'),
  tf.keras.metrics.Precision(name='precision'),
  tf.keras.metrics.Recall(name='recall'),
  tf.keras.metrics.AUC(name='auc'),
]
show_data = False
l2_reg = .0001
drop_rate = .5
embedding_dim = 5
callbacks = [tf.keras.callbacks.ModelCheckpoint(model_location, monitor='val_loss', save_best_only=True)]
try_hp = True

train_batches = n_steps
test_batches = val_steps

opt = 'adam'

print('optimizer:           {}'.format(opt))
print('loss:                {}'.format(loss))
print('metrics              {}'.format(metrics))
print('batch size:          {}'.format(batch_size))
print('epochs:              {}'.format(n_epochs))
print('number of samples:   {}'.format(total_samples))
print('training batches:    {}'.format(train_batches))
print('testing batches:     {}'.format(test_batches))

if not isinstance(metrics, list):
  metrics = [metrics]

print('getting dataset')
train_data = get_samples(data_dir + '/train_set_entropy.txt', batch_size, cache=ds_dir + '/train', shuffle=2000).repeat().prefetch(tf.data.experimental.AUTOTUNE)
test_data = get_samples(data_dir + '/val_set_entropy.txt', batch_size, cache=ds_dir + '/val', shuffle=2000).repeat().prefetch(tf.data.experimental.AUTOTUNE)
print('got dataset')
train_neg = total_samples - train_pos
class_weight = {
  0: (1 / train_neg) * (total_samples) / 2.0,
  1: (1 / train_pos) * (total_samples) / 2.0,
}
print('Weight for class 0: {:.2f}'.format(class_weight[0]))
print('Weight for class 1: {:.2f}'.format(class_weight[1]))

if show_data:
  print('sample batch:')
  for batch in train_data.take(1):
    print(batch)

if not try_hp:
  conv_settings = [
    [8, 3, 1, 3],
    [10, 30, 20, 6]
  ]
  lstm_settings = [
    [16, True],
    [0, False]
  ]
  
  model, results = train_test_model()
  print('evaluation results:')
  for i in range(len(results)):
    print('{}:\t{}'.format(model.metrics_names[i], results[i]))
  quit()

METRIC_LOSS = 'binary_crossentropy'
METRIC_ACCURACY = 'accuracy'

EMBEDDING_DIM = hp.HParam('embedding_dim', hp.Discrete([
  5,
  10,
]))
CONV_SETTINGS1 = hp.HParam('convolution_settings1', hp.Discrete([
  8,
  32,
]))
LSTM_SETTINGS = hp.HParam('lstm_settings', hp.Discrete([
  16,
  32,
]))
LSTM_SETTINGS2 = hp.HParam('lstm_settings2', hp.Discrete([
  0,
  8,
  16,
]))
L2_REG = hp.HParam('l2_reg', hp.Discrete([
  .001,
  .0001,
]))
DROP_RATE = hp.HParam('dropout_rate', hp.Discrete([
  .3,
  .5,
]))

with tf.summary.create_file_writer('logs/hparam_tuning').as_default():
  hp.hparams_config(
    hparams=[
      EMBEDDING_DIM,
      CONV_SETTINGS1,
      LSTM_SETTINGS,
      LSTM_SETTINGS2,
      L2_REG,
      DROP_RATE
    ],
    metrics=[
      hp.Metric(METRIC_LOSS, display_name='Binary Crossentropy'),
      hp.Metric(METRIC_ACCURACY, display_name='Accuracy'),
      hp.Metric('precision', display_name='Precision'),
      hp.Metric('recall', display_name='Recall'),
      hp.Metric('auc', display_name='AUC'),
    ]
  )

for dim in EMBEDDING_DIM.domain.values:
  for l2 in L2_REG.domain.values:
    for conv_set1 in CONV_SETTINGS1.domain.values:
      for lstm_set in LSTM_SETTINGS.domain.values:
        for lstm_set2 in LSTM_SETTINGS2.domain.values:
          for drop in DROP_RATE.domain.values:
            tf.keras.backend.clear_session()
            hparams = {
              'EMBEDDING_DIM': dim,
              'CONV_SETTINGS1': conv_set1,
              'LSTM_SETTINGS': lstm_set,
              'LSTM_SETTINGS2': lstm_set2,
              'L2_REG': l2,
              'DROP_RATE': drop
            }
            run_name = 'embed:{};lstm:{},{};conv:{};drop:{};reg:{}'.format(dim, lstm_set, lstm_set2, conv_set1, drop, l2)
            print('--- Starting trial: %s' % run_name)
            run_dir = 'logs/hparam_tuning/' + run_name
            summary_str = ''
            with tf.summary.create_file_writer(run_dir).as_default():
              hp.hparams(hparams)
              model, results = train_test_model(hparams, run_dir)
              for i in range(len(results)):
                tf.summary.scalar(model.metrics_names[i], results[i], step=1)
                summary_str += '{}: {:>7.6f} '.format(model.metrics_names[i], results[i])
            print(summary_str)
            cmdstr = "tar -czf logs.tar.gz logs/"
            proc = Popen(cmdstr.split(), stdout=PIPE, stdin=PIPE)
            stdout, stdin = proc.communicate()

