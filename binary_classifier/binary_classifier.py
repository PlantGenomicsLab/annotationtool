import tensorflow as tf
from tensorflow.keras import Model, Sequential, layers
from tensorflow.keras import regularizers as regs

def build_model(hparams=None, conv_settings=[], lstm_settings=[], l2_reg=0., drop_rate=0., embedding_dim=None):
  if hparams is not None:
    conv1 = hparams['CONV_SETTINGS1']
    lstm_settings = [[hparams['LSTM_SETTINGS'], True], [hparams['LSTM_SETTINGS2'], False]]
    conv_settings = [[conv1, 3, 1, 3],
                     [10, 30, 20, 6]]
    l2_reg = hparams['L2_REG']
    drop_rate = hparams['DROP_RATE']
    embedding_dim = hparams['EMBEDDING_DIM']

  inputs = layers.Input(shape=(None,))
  structs = layers.Input(shape=(None,))
  embed = layers.Embedding(11, embedding_dim)(inputs)
  struct_embed = layers.Reshape((-1,1))(structs)
  
  layer_num = 1
  conv = pool = drop = None
  for s in conv_settings:
    if conv is None:
      conv = layers.Conv1D(s[0], s[1], s[2], activation='relu', kernel_regularizer=regs.l2(l2_reg), name='{}_conv1d_{}_{}_{}'.format(layer_num, s[0], s[1], s[2]))(embed)
      struct_conv = layers.Conv1D(s[0], s[1], s[2], activation='relu', kernel_regularizer=regs.l2(l2_reg), name='struct_{}_conv1d_{}_{}_{}'.format(layer_num, s[0], s[1], s[2]))(struct_embed)
    else:
      conv = layers.Conv1D(s[0], s[1], s[2], activation='relu', kernel_regularizer=regs.l2(l2_reg), name='{}_conv1d_{}_{}_{}'.format(layer_num, s[0], s[1], s[2]))(drop)
      struct_conv = layers.Conv1D(s[0], s[1], s[2], activation='relu', kernel_regularizer=regs.l2(l2_reg), name='struct_{}_conv1d_{}_{}_{}'.format(layer_num, s[0], s[1], s[2]))(struct_drop)
    layer_num += 1
    pool = layers.MaxPool1D(s[3], name='{}_pool1d_{}'.format(layer_num, s[3]))(conv)
    struct_pool = layers.MaxPool1D(s[3], name='struct_{}_pool1d_{}'.format(layer_num, s[3]))(struct_conv)
    drop = layers.Dropout(drop_rate)(pool)
    struct_drop = layers.Dropout(drop_rate)(struct_pool)
    layer_num += 1
  
  lstm = None
  for s in lstm_settings:
    if s[0] == 0:
      continue
    if lstm is None:
      lstm = layers.Bidirectional(layers.LSTM(s[0], return_sequences=s[1], recurrent_regularizer=regs.l2(l2_reg), kernel_regularizer=regs.l2(l2_reg), name='{}_lstm_{}'.format(layer_num, s[0]), dropout=drop_rate, recurrent_dropout=drop_rate))(drop)
      struct_lstm = layers.Bidirectional(layers.LSTM(s[0], return_sequences=s[1], recurrent_regularizer=regs.l2(l2_reg), kernel_regularizer=regs.l2(l2_reg), name='struct_{}_lstm_{}'.format(layer_num, s[0]), dropout=drop_rate, recurrent_dropout=drop_rate))(struct_drop)
    else:
      lstm = layers.Bidirectional(layers.LSTM(s[0], return_sequences=s[1], recurrent_regularizer=regs.l2(l2_reg), kernel_regularizer=regs.l2(l2_reg), name='{}_lstm_{}'.format(layer_num, s[0]), dropout=drop_rate, recurrent_dropout=drop_rate))(lstm)
      struct_lstm = layers.Bidirectional(layers.LSTM(s[0], return_sequences=s[1], recurrent_regularizer=regs.l2(l2_reg), kernel_regularizer=regs.l2(l2_reg), name='struct_{}_lstm_{}'.format(layer_num, s[0]), dropout=drop_rate, recurrent_dropout=drop_rate))(struct_lstm)
    layer_num += 1

  if lstm is None:
    concat = layers.Concatenate()([drop, struct_drop])
  else:
    concat = layers.Concatenate()([lstm, struct_lstm])
  
  outputs = layers.Dense(1, activation='sigmoid', kernel_regularizer=regs.l2(l2_reg))(concat)
  outputs = layers.Reshape((1,))(outputs)
  model = Model(inputs=[inputs, structs], outputs=outputs)
  return model

