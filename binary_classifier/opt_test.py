import sys, os
threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(inter_op_threads)
tf.config.threading.set_intra_op_parallelism_threads(intra_op_threads)
from tensorflow.keras import Model, Sequential, layers
from tensorflow.keras import regularizers as regs
from tensorflow.keras.optimizers import Adam
import tensorflow.keras.backend as K
sys.path.append('..')
from utils.dataset import *
from subprocess import Popen, PIPE

def build_model(in_size):
  model = Sequential()
  model.add(layers.Dense(dense_size, use_bias=False, input_shape=(in_size,)))
  model.add(layers.BatchNormalization())
  model.add(layers.LeakyReLU())

  model.add(layers.Reshape((dense_size,)))
  assert model.output_shape == (None, dense_size)

  return model

def model_loss(labels, output):
  return cat_cross(labels, output)

def model2_loss(labels, output):
  return cat_cross2(labels, output)

@tf.function
def train_step(data, labels, labels2):
  print('Tracing train_step!')
  with tf.GradientTape() as tape, tf.GradientTape() as tape2:
    output = model(data, training=True)
    output2 = model2(output, training=True)
    loss = model_loss(labels, output2)
    loss2 = model2_loss(labels2, output2)

  grads = tape.gradient(loss, model.trainable_variables)
  opt.apply_gradients(zip(grads, model.trainable_variables))
  grads2 = tape2.gradient(loss2, model2.trainable_variables)
  opt2.apply_gradients(zip(grads2, model2.trainable_variables))
  train_loss(loss)

  return loss, loss2

def train(epochs, train_steps, in_size):
  global train_step
  for epoch in range(epochs):
    # Train one epoch
    for batch_num in range(train_steps):
      batch = tf.random.normal([batch_size, in_size])
      labels2 = tf.random.normal([batch_size, dense_size])
      labels = tf.random.normal([batch_size, dense_size])
      loss, loss2 = train_step(batch, labels, labels2)

    train_loss.reset_states()
  train_step = train_step._clone(None)
  return loss, loss2

if __name__ == '__main__':
  batch_size = 5
  dense_size = 10
  in_size = 3
  n_epochs = 4
  train_steps = 10

  trials = 100

  K.clear_session()

  cat_cross = tf.keras.losses.CategoricalCrossentropy(from_logits=True)
  cat_cross2 = tf.keras.losses.CategoricalCrossentropy(from_logits=True)
  opt = Adam(1e-3)
  opt2 = Adam(1e-3)

  train_loss = tf.keras.metrics.Mean('train_loss', dtype=tf.float32)

  for i in range(trials):
    K.clear_session()
    model = build_model(in_size)
    model2 = build_model(dense_size)
    opt = Adam(1e-3)
    opt2 = Adam(1e-3)
    print('Starting trial {:>3}...'.format(i))
    loss, loss2 = train(n_epochs, train_steps, in_size)
    print('trial {:>3} loss: {:>4.2f}, {:4.2f}'.format(i, loss, loss2))
    #del model
    #del model2
    #del opt
    #del opt2

