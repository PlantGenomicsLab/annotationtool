import sys, os

threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(inter_op_threads)
tf.config.threading.set_intra_op_parallelism_threads(intra_op_threads)

from tensorboard.plugins.hparams import api as hp
from tensorflow.keras.models import Sequential, Model, load_model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.backend import clear_session
from tensorflow.keras import layers
from tensorflow.keras import regularizers as regs
sys.path.append('..')
from utils.dataset import *
from binary_classifier import *
import numpy as np
from subprocess import Popen, PIPE
import math
from tensorflow.keras.callbacks import Callback
import time
import getpass

class BatchNum(Callback):
  def on_predict_batch_end(self, batch, logs=None):
    if batch % 1000 == 0:
      print('batch number {}...'.format(batch))

species = 'drosophila'
user = getpass.getuser()
samples_path = '/scratch/' + user + '/data/' + species + '/all_atg_entropy.txt'
model_location = species + '_classifier/tis_entropy_classifier.h5'
pred_location = species + '_classifier/predictions_entropy.txt'
batch_size = 256
embedding_dim = 5
conv_settings = [
  [16, 3, 1, 3],
  [10, 20, 10, 6]
]
lstm_settings = [
  [8, True],
  [16, False]
]
l2_reg=.0001
drop_rate=.5
ds = get_samples(samples_path, batch_size, cache='/scratch/' + user + '/ds/' + species + '/all_atg_entropy').prefetch(tf.data.experimental.AUTOTUNE)
pred_results = open(pred_location, 'w')

model = build_model(None, conv_settings, lstm_settings, l2_reg, drop_rate, embedding_dim)
model.load_weights(model_location)
model.summary()
print('starting predictions...')
start = time.time()
preds = model.predict(ds, callbacks=[BatchNum()])
print('time elapsed: {}s'.format(time.time() - start))
preds_raw = preds[:,0]
preds = tf.math.greater(preds_raw, tf.constant(.5))
total_pos = tf.reduce_sum(tf.cast(preds, tf.int32)).numpy()
total = preds.shape[0]
total_neg = total - total_pos
print('total positive: {:>10} ({:.2f}%)\n'.format(total_pos, (100. * total_pos / total)))
print('total negative: {:>10} ({:.2f}%)\n'.format(total_neg, (100. * total_neg / total)))
samples_handle = open(samples_path, 'r')
line = samples_handle.readline().strip()
i = 0
while line:
  parts = line.split(',')
  strand = parts[0]
  pos = parts[4]
  chrom = parts[3]
  pred_results.write('{}:{}|{}|{}|{}\n'.format(chrom, pos, strand, preds[i].numpy(), preds_raw[i]))
  i += 1
  if i % 100000 == 0:
    print('working on pred {}...'.format(i))
  line = samples_handle.readline().strip()


