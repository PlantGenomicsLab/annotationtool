import argparse
if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='train a GAN classifier model')
  parser.add_argument('--species', '-s', nargs=1, type=str, required=True,
      help='the species we are training on.')
  parser.add_argument('--start_pad', '--start', nargs=1, type=int, required=True,
      help='upstream padding.')
  parser.add_argument('--end_pad', '--end', nargs=1, type=int, required=True,
      help='downstream padding.')
  parser.add_argument('--epochs', '-e', nargs=1, type=int, required=True,
      help='number of training epochs.')
  parser.add_argument('--batch_size', '-b', nargs=1, type=int, default=128,
      help='training batch size.')
  parser.add_argument('--early_stopping', nargs=1, type=int, default=None,
      help='number of epoch before stopping early.')
  parser.add_argument('--data_dir', '-d', nargs=1, type=str, default=None,
      help='directory which holds the encoded TIS data.')
  parser.add_argument('--cache_dir', nargs=1, type=str, default=None,
      help='directory which holds the cached dataset data.')
  parser.add_argument('--show_sample', action='store_true',
      help='whether to show a sample batch of training data.')
  parser.add_argument('--hparams', action='store_true',
      help='whether we are using hparams or not.')
  args = parser.parse_args()

import sys, os

# Multithreading setup
threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(inter_op_threads)
tf.config.threading.set_intra_op_parallelism_threads(intra_op_threads)

# GPU Setup
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  try :
    for gpu in gpus:
      tf.config.experimental.set_memory_growth(gpu, True)
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    print(e)

from tensorboard.plugins.hparams import api as hp
from tensorflow.keras import Model, Sequential, layers
from tensorflow.keras import regularizers as regs
from tensorflow.keras.optimizers import Adam
import tensorflow.keras.backend as K
sys.path.append('..')
from utils.dataset import *
from subprocess import Popen, PIPE
import getpass
import time

def build_discriminator_model(window_size,
      dense=5,
      conv_settings=None,
      lstm_settings=None,
      regular=0.001,
      drop=0.3):
  # Build a discriminator based on some parameters

  # Default parameters
  if conv_settings is None:
    conv_settings = [
      [8, 3, 1, 3],
      [10, 30, 20, 6],
    ]
  if lstm_settings is None:
    lstm_settings = [
      [16, True],
      [8, False],
    ]

  model = Sequential()
  model.add(layers.Dense(dense, input_shape=(window_size, 11)))

  for setting in conv_settings:
    model.add(layers.Conv1D(setting[0], setting[1], setting[2], padding='same'))
    model.add(layers.LeakyReLU())
    model.add(layers.MaxPool1D(setting[3]))
    model.add(layers.Dropout(drop))

  for setting in lstm_settings:
    model.add(layers.Bidirectional(layers.LSTM(setting[0], return_sequences=setting[1], recurrent_regularizer=regs.l2(regular), kernel_regularizer=regs.l2(regular), recurrent_dropout=drop)))

  model.add(layers.Dense(3, activation='sigmoid'))
  model.add(layers.Reshape((3,)))

  return model

def build_generator_model(window_size, in_size=128, hidden_size=32):
  # Build a generator based on some parameters
  model = Sequential()
  model.add(layers.Dense(in_size * window_size, use_bias=False, input_shape=(window_size,)))
  model.add(layers.BatchNormalization())
  model.add(layers.LeakyReLU())

  model.add(layers.Reshape((window_size, in_size)))
  assert model.output_shape == (None, window_size, in_size)

  model.add(layers.Conv1D(hidden_size, 5, padding='same', use_bias=False))
  assert model.output_shape == (None, window_size, hidden_size)
  model.add(layers.BatchNormalization())
  model.add(layers.LeakyReLU())

  model.add(layers.Conv1D(11, 5, padding='same', use_bias=False, activation='tanh'))
  assert model.output_shape == (None, window_size, 11)

  return model

def discriminator_loss(labels, output, evaluating=False):
  return cat_cross(labels, output)

def discriminator_accuracy(labels, output):
  output = tf.slice(output, [0, 0], [tf.shape(output)[0], 2])
  return cat_acc(labels, output)

def generator_loss(output, placeholder=None):
  fake_output = tf.slice(output, [0, 2], [tf.shape(output)[0], 1])
  return bin_cross(tf.zeros_like(fake_output), fake_output)

# This function decoration tells TF to compile this function once before using it, which speeds up training a lot.
# For more information, check out https://www.tensorflow.org/guide/function and https://www.tensorflow.org/api_docs/python/tf/function
@tf.function
def train_step(data, real_labels, using_hparams=False):
  print('Tracing train_step!')
  # Get data shape
  data_shape = tf.shape(data)
  # Make some noise!
  noise = tf.random.normal([data_shape[0], data_shape[1]])

  # Instantiate a tape to calculate gradients later
  with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
    # Get generator output
    generated_seqs = generator(noise, training=True)

    # Create some fake labels in the correct shape
    fake_labels = tf.ones((data_shape[0],)) * 2

    # Get discriminator output for real and fake data
    real_output = discriminator(data, training=True)
    fake_output = discriminator(generated_seqs, training=True)

    # Calculate losses
    real_loss = discriminator_loss(real_labels, real_output)
    fake_loss = discriminator_loss(fake_labels, fake_output)
    disc_loss = real_loss
    disc_loss = real_loss + fake_loss
    gen_loss = generator_loss(fake_output)

  # Calculate gradients
  disc_grads = disc_tape.gradient(disc_loss, discriminator.trainable_variables)
  gen_grads = gen_tape.gradient(gen_loss, generator.trainable_variables)
  # Apply gradients
  disc_opt.apply_gradients(zip(disc_grads, discriminator.trainable_variables))
  gen_opt.apply_gradients(zip(gen_grads, generator.trainable_variables))

  if using_hparams:
    # Log losses and output in TensorBoard
    train_loss(disc_loss)
    gen_train_loss(gen_loss)
    train_accuracy(real_labels, real_output)
    train_accuracy(fake_labels, fake_output)
  return disc_loss, gen_loss

@tf.function
def test_step(data, real_labels, using_hparams=False):
  print('Tracing test_step!')
  output = discriminator(data, training=False)
  loss = discriminator_loss(real_labels, output)
  accuracy = discriminator_accuracy(real_labels, output)

  if using_hparams:
    val_loss(loss)
    val_accuracy(real_labels, output)
  return loss, accuracy

def train(epochs, train_steps, test_steps, disc_loc=None, gen_loc=None, using_hparams=False, train_writer='', val_writer='', early_stopping=None):
  global train_step, test_step
  best_eval = None
  since_best_eval = 0
  for epoch in range(epochs):
    start = time.time()
    # Train one epoch
    for batch, labels in train_data.take(train_steps):
      labels = tf.cast(labels, dtype=tf.float32)
      batch = tf.one_hot(batch[0], 11, dtype=tf.float32)
      disc_loss, gen_loss = train_step(batch, labels, using_hparams)
    if using_hparams:
      with train_writer.as_default():
        tf.summary.scalar('loss', train_loss.result(), step=epoch)
        tf.summary.scalar('gen_loss', gen_train_loss.result(), step=epoch)
        tf.summary.scalar('accuracy', train_accuracy.result(), step=epoch)

    # Evaluate
    for batch, labels in test_data.take(test_steps):
      labels = tf.cast(labels, dtype=tf.float32)
      batch = tf.one_hot(batch[0], 11, dtype=tf.float32)
      eval_loss, eval_acc = test_step(batch, labels, using_hparams)
    if using_hparams:
      with val_writer.as_default():
        tf.summary.scalar('val_loss', val_loss.result(), step=epoch)
        tf.summary.scalar('val_accuracy', val_accuracy.result(), step=epoch)

    if best_eval is None or eval_loss < best_eval:
      best_eval = eval_loss
      since_best_eval = 0
    elif early_stopping is not None:
      since_best_eval += 1
      if since_best_eval > early_stopping:
        if using_hparams:
          train_loss.reset_states()
          gen_train_loss.reset_states()
          val_loss.reset_states()
          train_accuracy.reset_states()
          val_accuracy.reset_states()
          cat_acc.reset_states()
        return disc_loss, gen_loss, eval_loss, eval_acc

    if disc_loc is not None and gen_loc is not None:
      # Save model if it is better than current best
      if best_eval is None or eval_loss < best_eval:
        best_eval = eval_loss
        discriminator.save(disc_loc)
        generator.save(gen_loc)

    # Print results
    if not using_hparams:
      print("Epoch {:>4}: Discriminator Loss: {:>5.3f} Evaluation: {:>5.3f} Accuracy: {:>5.3f} | Generator Loss: {:>5.3f} Time elapsed: {:>3.2f}s".format(epoch + 1, disc_loss, eval_loss, eval_acc, gen_loss, time.time() - start))

    # Reset metrics
    if using_hparams:
      train_loss.reset_states()
      gen_train_loss.reset_states()
      val_loss.reset_states()
      train_accuracy.reset_states()
      val_accuracy.reset_states()
      cat_acc.reset_states()

  # Rebuild train and test step functions so that they can be used for the next model
  train_step = train_step._clone(None)
  test_step = test_step._clone(None)
  return disc_loss, gen_loss, eval_loss, eval_acc

if __name__ == '__main__':
  # Get user-defined arguments
  species = args.species[0]
  start_pad = args.start_pad[0]
  end_pad = args.end_pad[0]
  pad_str = str(start_pad) + '_' + str(end_pad)
  show_data = args.show_sample

  data_dir = args.data_dir[0] if args.data_dir is not None else None
  # Set default data directory
  if data_dir is None:
    data_dir = '/scratch/' + getpass.getuser() + '/data/' + species + '/' + pad_str
  # Set default caching directory
  ds_dir = args.cache_dir[0] if args.cache_dir is not None else None
  if ds_dir is None:
    ds_dir = '/scratch/' + getpass.getuser() + '/ds/' + species + '/' + pad_str

  # Get total window length
  total_length = start_pad + end_pad + 3

  # Paths for discriminator/generator models
  disc_location = species + '_classifier/' + pad_str + '/gan_discriminator.h5'
  gen_location = species + '_classifier/' + pad_str + '/gan_generator.h5'

  # Paths for training/validation datasets.
  paths = [
    data_dir + '/train_set_entropy.txt',
  ]
  val_paths = [
    data_dir + '/val_set_entropy.txt',
  ]

  # Get total numbers of train/test samples
  print('getting number of samples')
  total_samples = total_val_samples = 0
  for path in paths:
    cmdstr = "wc -l %s" % path
    proc = Popen(cmdstr.split(), stdout=PIPE, stdin=PIPE)
    stdout, stdin = proc.communicate()
    total_samples += int(stdout.split()[0])
  for path in val_paths:
    cmdstr = "wc -l %s" % path
    proc = Popen(cmdstr.split(), stdout=PIPE, stdin=PIPE)
    stdout, stdin = proc.communicate()
    total_val_samples += int(stdout.split()[0])

  K.clear_session()

  # Load more user arguments
  batch_size = args.batch_size[0] if isinstance(args.batch_size, list) else args.batch_size
  n_epochs = args.epochs[0] if isinstance(args.epochs, list) else args.epochs
  train_batches = total_samples // batch_size
  test_batches = total_val_samples // batch_size
  # Decide whether to use tensorboard or not
  use_hparams = args.hparams
  early_stopping = args.early_stopping[0] if isinstance(args.early_stopping, list) else args.early_stopping

  # Define losses and metrics
  cat_cross = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
  cat_acc = tf.keras.metrics.SparseCategoricalAccuracy()
  bin_cross = tf.keras.losses.BinaryCrossentropy(from_logits=True)

  # Define optimizers
  gen_opt = Adam(1e-3)
  disc_opt = Adam(1e-3)

  # Get datasets (see utils/dataset.py for more details)
  print('getting dataset')
  train_data = get_samples(data_dir + '/train_set_entropy.txt', batch_size, cache=ds_dir + '/train', shuffle=2000).repeat().prefetch(tf.data.experimental.AUTOTUNE)
  test_data = get_samples(data_dir + '/val_set_entropy.txt', batch_size, cache=ds_dir + '/val', shuffle=2000).repeat().prefetch(tf.data.experimental.AUTOTUNE)

  print('got dataset')

  if show_data:
    # Display a sample batch
    print('sample batch:')
    for batch in train_data.take(1):
      print(batch)

  # If we are not using TensorBoard, then just do one round of training and then quit
  if not use_hparams:
    generator = build_generator_model(total_length)
    discriminator = build_discriminator_model(total_length, .0001, .1)

    train(n_epochs, train_batches, test_batches, disc_loc=disc_location, gen_loc=gen_location)
    quit()

  # Define losses and metrics
  train_loss = tf.keras.metrics.Mean('train_loss', dtype=tf.float32)
  gen_train_loss = tf.keras.metrics.Mean('gen_train_loss', dtype=tf.float32)
  train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy('train_accuracy')
  val_loss = tf.keras.metrics.Mean('val_loss', dtype=tf.float32)
  val_accuracy = tf.keras.metrics.SparseCategoricalAccuracy('val_accuracy')

  # Establish domains for tensorboard params
  DENSE = hp.HParam('dense', hp.Discrete([
    20,
    10,
    #5,
  ]))
  CONV_SETTINGS1 = hp.HParam('convolution_settings1', hp.Discrete([
    32,
    #8,
  ]))
  LSTM_SETTINGS1 = hp.HParam('lstm_settings1', hp.Discrete([
    128,
    64,
    32,
    #16,
  ]))
  LSTM_SETTINGS2 = hp.HParam('lstm_settings2', hp.Discrete([
    16,
    #8,
  ]))
  L2_REG = hp.HParam('l2_reg', hp.Discrete([
    #.0001,
    .001,
    .005,
  ]))
  DROP_RATE = hp.HParam('dropout_rate', hp.Discrete([
    #.1,
    .3,
    .5,
  ]))
  GEN_SIZE1 = hp.HParam('gen_size1', hp.Discrete([
    256,
    128,
  ]))
  GEN_SIZE2 = hp.HParam('gen_size2', hp.Discrete([
    128,
    64,
    #32,
  ]))
  GEN_LR = hp.HParam('gen_lr', hp.Discrete([
    1e-2,
    1e-3,
  ]))

  # Create a file writer to compare different Hparams in tensorboard
  with tf.summary.create_file_writer('logs/hparam_tuning').as_default():
    hp.hparams_config(
      hparams=[
        DENSE,
        CONV_SETTINGS1,
        LSTM_SETTINGS1,
        LSTM_SETTINGS2,
        L2_REG,
        DROP_RATE,
        GEN_SIZE1,
        GEN_SIZE2,
        GEN_LR,
      ],
      metrics=[
        hp.Metric('val_loss', display_name='Validation Loss'),
        hp.Metric('val_accuracy', display_name='Validation Accuracy'),
      ],
    )

  # Loop through parameter domains and train models
  for dense in DENSE.domain.values:
    for l2 in L2_REG.domain.values:
      for conv_set1 in CONV_SETTINGS1.domain.values:
        for lstm_set1 in LSTM_SETTINGS1.domain.values:
          for lstm_set2 in LSTM_SETTINGS2.domain.values:
            for drop in DROP_RATE.domain.values:
              for gen_size1 in GEN_SIZE1.domain.values:
                for gen_size2 in GEN_SIZE2.domain.values:
                  for gen_lr in GEN_LR.domain.values:
                    K.clear_session()
                    discriminator = build_discriminator_model(total_length,
                          dense=dense,
                          conv_settings=[
                            [conv_set1, 3, 1, 3],
                            [10, 30, 20, 6]
                          ],
                          lstm_settings=[
                            [lstm_set1, True],
                            [lstm_set2, False],
                          ],
                          regular=l2,
                          drop=drop)
                    generator = build_generator_model(total_length,
                          in_size=gen_size1,
                          hidden_size=gen_size2)
                    gen_opt = Adam(gen_lr)
                    disc_opt = Adam(1e-3)

                    discriminator.reset_states()
                    discriminator.reset_metrics()
                    generator.reset_states()
                    generator.reset_metrics()

                    hparams = {
                      DENSE: dense,
                      CONV_SETTINGS1: conv_set1,
                      LSTM_SETTINGS1: lstm_set1,
                      LSTM_SETTINGS2: lstm_set2,
                      L2_REG: l2,
                      DROP_RATE: drop,
                      GEN_SIZE1: gen_size1,
                      GEN_SIZE2: gen_size2,
                      GEN_LR: gen_lr,
                    }
                    run_name = 'dense:{}|conv1:{}|lstm:{},{}|drop:{}|reg:{}|gen:{},{},lr:{}'.format(dense, conv_set1, lstm_set1, lstm_set2, drop, l2, gen_size1, gen_size2, gen_lr)
                    print('--- Starting trial: %s' % run_name)
                    run_dir = 'logs/hparam_tuning/' + run_name
                    train_writer = tf.summary.create_file_writer(run_dir + '/train')
                    val_writer = tf.summary.create_file_writer(run_dir + '/val')
                    start = time.time()
                    with tf.summary.create_file_writer(run_dir).as_default():
                      hp.hparams(hparams)
                      d_loss, g_loss, e_loss, e_acc = train(n_epochs,
                            train_batches,
                            test_batches,
                            using_hparams=True,
                            train_writer=train_writer,
                            val_writer=val_writer,
                            early_stopping=early_stopping)
                      tf.summary.scalar('val_loss', e_loss, step=1)
                      tf.summary.scalar('val_accuracy', e_acc, step=1)
                    print('discriminator loss: {:>6.4f} generator loss: {:>6.4f} evaluation loss: {:>6.4f} evaluation accuracy: {:>6.4f} time elapsed: {:>3.2f}'.format(d_loss, g_loss, e_loss, e_acc, time.time() - start))

