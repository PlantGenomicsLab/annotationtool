## Create Gene Models

This directory contains all of the scripts that EASEL uses to construct gene models from high-likelihood TIS sites identified by the [binary classifier](../binary_classifier).
