#!/bin/bash
#SBATCH --job-name=finish_incomplete_data
#SBATCH -o finish_incomplete_data_480_480.out
#SBATCH -e finish_incomplete_data_480_480.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --nodelist=gpu11
#SBATCH --cpus-per-task=36
#SBATCH --partition=gpu_xtra
#SBATCH --mem=60G

echo `hostname`
source ~/.bashrc
cd ~/annotationtool/data_preprocessing

[[ -z "$py_exec" ]] && export py_exec=$py_exec || export py_exec='python3'
echo "using python executable: `which $py_exec`"

$py_exec --version || ( echo 'Error using python executable!' && exit )

# Species we are building data for
species='celegans'
declare -i chunks=`nproc`
USER=`whoami`
# Directory which holds data
DATA_DIR='/scratch/'$USER

# Source files required for building data
FASTA=$DATA_DIR'/data/'$species'/genome.fa'
GFF_DIR=$DATA_DIR'/data/'$species'/gffs/'
SOURCE_ANNO=$DATA_DIR'/data/'$species'/source_annotation.gtf'
PSEUDOGENES=$DATA_DIR'/data/'$species'/pseudogenes.txt'

# Padding sizes
START_PAD='480'
END_PAD='480'

data_prefix=$DATA_DIR'/data/'$species'/'$START_PAD'_'$END_PAD'/'
temp_dir=$data_prefix'tmp/'
mkdir -p $data_prefix

# Names of output files (these are the ones with folding information)
TRAIN_SET_ENTROPY=$data_prefix'train_set_entropy.txt'
FASTA_ENTROPY=$data_prefix'all_atg_entropy.txt'
VAL_SET_ENTROPY=$data_prefix'val_set_entropy.txt'

# Names of intermediate files
BUSCO_ENCODED=$data_prefix'busco_encoded.txt'
PSEUDO_ENCODED=$data_prefix'pseudogenes_encoded.txt'
ENSEMBL_ENCODED=$data_prefix'ensembl_encoded.txt'
FASTA_ENCODED=$data_prefix'all_atg_encoded.txt'
FILTERED_NEGATIVES=$data_prefix'filtered_negatives.txt'
TRAIN_SET=$data_prefix'train_set.txt'
VAL_SET=$data_prefix'val_set.txt'

$py_exec -u ../utils/add_entropy.py --in_file=$FASTA_ENCODED --temp_dir=$temp_dir --out_file=$FASTA_ENTROPY --finish_incomplete

