import argparse

parser = argparse.ArgumentParser(description='Add structure encoding to an existing TIS encoding site')
parser.add_argument('--tis_file', '-f', nargs=1, default=None, type=str,
    help='the path to the file with the encoded potential start sites')
parser.add_argument('--struct_file', '-s', nargs=1, default=None, type=str,
    help='the path to the file with the start site structures')
parser.add_argument('--out', '-o', nargs=1, default=None, type=str,
    help='an output file')
args = parser.parse_args()

struct_lang = {
  '0': '0',
  '.': '1',
  ',': '2',
  '|': '3',
  '{': '4',
  '}': '5',
  '(': '6',
  ')': '7',
}

def format_seq(seq):
  return '|'.join([struct_lang[i] for i in seq])

if args.struct_file is not None:
  struct_path = args.struct_file[0]
if args.tis_file is not None:
  tis_path = args.tis_file[0]
if args.out is not None:
  new_file_path = args.out[0]

struct_handle = open(struct_path, 'r')
tis_handle = open(tis_path, 'r')
new_file = open(new_file_path, 'w')

struct = struct_handle.readline().strip()
tis = tis_handle.readline().strip()

while struct:
  if len(struct.split(' ')[0].split(':')) == 1:
    struct_seq = format_seq(struct.split(' ')[0])
    new_file.write('{},{}\n'.format(tis, struct_seq))
  
  struct = struct_handle.readline().strip()
  tis = tis_handle.readline().strip()
