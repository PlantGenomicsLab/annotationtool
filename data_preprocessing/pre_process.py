import os, sys, re, argparse, time
from collections import deque

parser = argparse.ArgumentParser(description='Use start codon locations and genome fasta to produce TIS data.')
parser.add_argument('--fasta', '-f', nargs=1, type=argparse.FileType('r'), required=True,
    help='the path to the .FASTA file of the genome')
parser.add_argument('--pseudogenes', '-p', nargs=1, default=None, type=argparse.FileType('r'),
    help='the path to a plaintext file of pseudogenes from Ensembl')
parser.add_argument('--true_genes', '-t', nargs=1, default=None, type=argparse.FileType('r'),
    help='the path to a plaintext file of true genes from Ensembl')
parser.add_argument('--true_genes_format', '--format', nargs=1, default=None, type=str,
    help='the formatting of the column in the true genes Ensembl file')
parser.add_argument('--gffs_dir', '--gffs', '-g', nargs=1, default=None, type=str,
    help='the path to a directory of BUSCO output gff files')
parser.add_argument('--out', '-o', nargs=1, default=sys.stdout, type=argparse.FileType('w'),
    help='an output file')
parser.add_argument('--pad_len', default=300, type=int,
    help='the length of padding around the TIS site (default: 300)')
parser.add_argument('--start_pad', default=None, type=int,
    help='the length of upstream padding around the TIS site (default: None)')
parser.add_argument('--end_pad', default=None, type=int,
    help='the length of downstream padding around the TIS site (default: None)')
parser.add_argument('--line_len', default=60, type=int,
    help='the length of each line in the provided FASTA file (default: 60)')
parser.add_argument('--max_location_len', default=10, type=int,
    help='the maximum length TIS site locations - this helps with sorting TIS locations, which is important for parsing FASTA files (default: 10)')

args = parser.parse_args()

rev = {
  'a': 't',
  'c': 'g',
  'g': 'c',
  't': 'a',
  'A': 'T',
  'C': 'G',
  'G': 'C',
  'T': 'A',
  'n': 'n',
  'N': 'N',
}

input_language = {
  '0': '0',
  'A': '1',
  'a': '2',
  'C': '3',
  'c': '4',
  'G': '5',
  'g': '6',
  'T': '7',
  't': '8',
  'N': '9',
  'n': '10'
}

def format_seq(seq):
  return '|'.join([input_language[i] for i in seq])

def reverse_comp(seq, strand='-'):
  if strand == '+':
    return seq
  new_seq = ''
  for char in seq:
    new_seq += rev[char]
  return new_seq[::-1]

def canonical(seq):
  return seq[start_pad:(-1 * end_pad)].lower() == 'atg'

def parse_chrom(f_line, starts, current_chrom):
  seq_num = 0
  current_pos = 0
  starts.sort()
  seq_buff = deque()
  for _ in range(buffer_items):
    seq_buff.append('')

  for tis_info in starts:
    seq_num += 1
    parts = tis_info.split('|')
    tis_chrom = parts[0]
    strand = parts[3]
    if strand == '+':
      tis_start = int(parts[1]) - start_pad - 1
      tis_end = int(parts[2]) + end_pad
    else:
      tis_start = int(parts[1]) - end_pad - 1
      tis_end = int(parts[2]) + start_pad
    label = parts[4]
    source = parts[5]
    tis_seq = ''

    while f_line and (tis_end > current_pos):
      current_pos += line_len
      f_line = fasta.readline().strip()
      seq_buff.append(f_line)
      seq_buff.popleft()

    seq = get_tis(seq_buff, current_pos, int(parts[1]) - 1, strand)
    if not canonical_only or canonical(seq):
      out_handle.write(strand + ',' + format_seq(seq) + ',' + label + ',' + current_chrom + ',' + str(int(parts[1]) - 1) + ',' + source + '\n')
  return f_line

def get_positions(seq_buff, current_pos):
  positions = []
  line_pos = int(current_pos - (line_len * (buffer_items / 2)))
  current_line = seq_buff[int(buffer_items / 2)]
  prev_line = seq_buff[int((buffer_items / 2) - 1)]

  roll1 = prev_line[-2:] + current_line[0]
  roll2 = prev_line[-1] + current_line[:2]

  if roll1.lower() == 'atg':
    positions.append((line_pos - 2, '+'))
  elif roll1.lower() == 'cat':
    positions.append((line_pos - 2, '-'))
  if roll2.lower() == 'atg':
    positions.append((line_pos - 1, '+'))
  elif roll2.lower() == 'cat':
    positions.append((line_pos - 1, '-'))

  for i in range(line_len - 2):
    tis = current_line[i:i+3]
    if tis.lower() == 'atg':
      positions.append((line_pos, '+'))
    elif tis.lower() == 'cat':
      positions.append((line_pos, '-'))
    line_pos += 1
  
  return positions

def get_tis(seq_buff, current_pos, tis_site, strand):
  buff_pos = current_pos - (line_len * buffer_items)
  if strand == '+':
    tis_start = tis_site - start_pad
    tis_end = tis_site + end_pad + 3
  else:
    tis_start = tis_site - end_pad
    tis_end = tis_site + start_pad + 3
  tis_seq = ''
  for line in seq_buff:
    if tis_start >= buff_pos + line_len:
      pass
    elif tis_start > buff_pos and tis_start < (buff_pos + line_len):
      diff = tis_start - buff_pos
      tis_seq += line[diff:]
    elif tis_end < (buff_pos + line_len):
      diff = tis_end - buff_pos
      tis_seq += line[:diff]
    else:
      tis_seq += line
    if len(tis_seq) == 3 + start_pad + end_pad:
      break
    buff_pos += line_len
  if (len(tis_seq) != 3 + start_pad + end_pad):
    print('incorrect length: {} at {}'.format(len(tis_seq), tis_site))
    print(tis_seq)
  assert(len(tis_seq) == 3 + start_pad + end_pad)
  return reverse_comp(tis_seq, strand)

def scan_fasta():
  seq_buff = deque()
  for _ in range(buffer_items):
    seq_buff.append('')
  f_line = fasta.readline().strip()
  n_lines = 0
  while f_line:
    n_lines += 1
    if f_line[0] == '>':
      current_chrom = f_line.split(' ')[0][1:]
      print('chrom ' + current_chrom)
      current_pos = line_len
      f_line = fasta.readline().strip()
      seq_buff.append(f_line)
      seq_buff.popleft()
      continue
    if current_pos < (line_len * buffer_items):
      current_pos += line_len
      f_line = fasta.readline().strip()
      seq_buff.append(f_line)
      seq_buff.popleft()
      continue
    positions = get_positions(seq_buff, current_pos)
    for pos, strand in positions:
      out_handle.write(strand + ',' + format_seq(get_tis(seq_buff, current_pos, pos, strand)) + ',0,' + current_chrom + ',' + str(pos) + ',' + 'all_atg' + '\n')

    current_pos += line_len
    f_line = fasta.readline().strip()
    if not f_line:
      break
    if f_line[0] != '>' and len(f_line) < line_len:
      f_line = fasta.readline().strip()
    seq_buff.append(f_line)
    seq_buff.popleft()
    if n_lines % 100000 == 0:
      print('line number {}...'.format(n_lines))

def create_tis():
  global start_locs
  f_line = fasta.readline().strip()
  current_chrom = f_line.split(' ')[0][1:]
  current_pos = 0
  checked_chroms = 0
  while f_line and (checked_chroms < len(start_locs.keys())):
    if current_chrom in start_locs.keys():
      f_line = parse_chrom(f_line, start_locs[current_chrom], current_chrom)
      checked_chroms += 1
    else:
      f_line = fasta.readline().strip()
    while f_line and f_line[0] != '>':
      f_line = fasta.readline().strip()
    current_chrom = f_line.split(' ')[0][1:]

def get_starts_busco_dir(busco_dir, label):
  # Retrieve and format start locations based on a busco gff directory path
  global start_locs
  source = 'busco_genes'

  gff_paths = os.listdir(busco_dir)
  
  for path in gff_paths:
    gff = open(busco_dir + path, 'r')
    line = gff.readline().strip()
    while line:
      if re.match('.*start_codon', line) is not None:
        parts = line.split('\t')
        chrom = parts[0]
        strand = parts[6]
        start = ('0' * (max_location_len - len(parts[3]))) + parts[3]
        end = ('0' * (max_location_len - len(parts[4]))) + parts[4]
        line = '|'.join([chrom, start, end, strand, label, source])
        if chrom not in start_locs.keys():
          start_locs[chrom] = []
        start_locs[chrom].append(line)
        break
      line = gff.readline().strip()

def get_starts_text(handle, file_format, label, source):
  global start_locs
  # Retrieve and format start locations based on a text file
  check_rank = False
  if 'end' not in file_format.keys():
    file_format['end'] = None
  if 'rank' in file_format.keys():
    check_rank = True
  line = handle.readline().strip()
  while line:
    parts = line.split(',')
    if check_rank and parts[file_format['rank']] != '1':
      line = handle.readline().strip()
      continue
    chrom = parts[file_format['scaffold']]
    start = int(parts[file_format['start']])
    strand = parts[file_format['strand']]
    end = None
    if file_format['end'] is not None:
      end = int(parts[file_format['end']])
    if end is None:
      if strand == '1' or strand == '+':
        strand = '+'
        end = str(start + 2)
        start = str(start)
      else:
        strand = '-'
        end = str(start)
        start = str(start - 2)
    else:
      if strand == '1' or strand == '+':
        strand = '+'
        end = str(start + 2)
        start = str(start)
      else:
        strand = '-'
        start = str(end - 2)
        end = str(end)
    start = '0' * (max_location_len - len(start)) + start
    end = '0' * (max_location_len - len(end)) + end
    if chrom not in start_locs.keys():
      start_locs[chrom] = []
    start_locs[chrom].append('|'.join([chrom, start, end, strand, label, source]))
    line = handle.readline().strip()

start_locs = {
  '2L': [],
  '2R': [],
  '3L': [],
  '3R': [],
  '4': [],
  'X': [],
  'Y': [],
}

max_location_len = args.max_location_len  
if args.start_pad is not None:
  start_pad = args.start_pad
else:
  start_pad = args.pad_len
if args.end_pad is not None:
  end_pad = args.end_pad
else:
  end_pad = args.pad_len
line_len = args.line_len
buffer_items = int(((max(start_pad, end_pad) / line_len) * 2) + 2)

if isinstance(args.out, list):
  out_handle = args.out[0]
else:
  out_handle = args.out
fasta = args.fasta[0]
canonical_only = True

if args.pseudogenes is not None:
  pseudogenes_handle = args.pseudogenes[0]
  pseudo_format = {
    'scaffold': 0,
    'start': 1,
    'strand': 2,
  }
  #pseudo_format = {
  #  'scaffold': 0,
  #  'start': 1,
  #  'end': 2,
  #  'strand': 3
  #}
  get_starts_text(pseudogenes_handle, pseudo_format, '0', 'pseudogenes')

if args.true_genes is not None:
  true_genes_handle = args.true_genes[0]
  true_genes_format = {
    'scaffold': 0,
    'start': 1,
    'end': 2,
    'strand': 3,
  }
  if args.true_genes_format is not None:
    format_arg = args.true_genes_format[0]
    col_num = 0
    true_genes_format = {}
    for i in format_arg.split(','):
      if i != '_':
        true_genes_format[i] = col_num
      col_num += 1
  get_starts_text(true_genes_handle, true_genes_format, '1', 'true_genes')

if args.gffs_dir is not None:
  get_starts_busco_dir(args.gffs_dir[0], '1')

if args.gffs_dir is not None or args.pseudogenes is not None or args.true_genes is not None:
  create_tis()
else:
  print('starting scan...')
  start = time.time()
  scan_fasta()
  print('scan complete! time: {:.2f}s'.format(time.time() - start))


