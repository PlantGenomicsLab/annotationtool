#!/bin/bash
#SBATCH --job-name=get_tis_data
#SBATCH -o get_tis_data.out
#SBATCH -e get_tis_data.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=256MB

FASTA='../testSpecies/model/arabidopsis/genome/Arabidopsis_assembly.fasta.masked'
GFF_DIR='../testSpecies/model/arabidopsis/genome/busco/run_busco_o/augustus_output/gffs/'
PSEUDOGENES='../data/arabidopsis/pseudogenes.txt'
#OUTPUT='../data/arabidopsis/train_tis.txt'
OUTPUT='a.txt'

echo `hostname`
cd /labs/Wegrzyn/annotationtool/data_preprocessing
python pre_process.py -f=$FASTA -p=$PSEUDOGENES -o=$OUTPUT --pad_len=300
#python pre_process.py -f=$FASTA -g=$GFF_DIR -o=$OUTPUT --pad_len=300

