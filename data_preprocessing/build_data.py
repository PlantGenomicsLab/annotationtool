import sys
sys.path.append('../utils')
import argparse
from translate import *
from encode_data import *
from match_buscos import *
from canonical import *
from select_samples import *

parser = argparse.ArgumentParser(description='use start codon locations and genome fasta to produce TIS data.')
parser.add_argument('--fasta', '-f', nargs=1, type=argparse.FileType('r'), required=True,
    help='the path to the .FASTA file of the genome')
parser.add_argument('--fasta_out', '--atg_out', nargs=1, default=None, type=str,
    help='the output file for the full genome scan')
parser.add_argument('--filtered', '--filtered_atg', nargs=1, default=None, type=str,
    help='the output file containing filtered non-genes for testing')
parser.add_argument('--start_pad', '--start', type=int, required=True,
    help='the length of upstream padding around the TIS site (default: None)')
parser.add_argument('--end_pad', '--end', type=int, required=True,
    help='the length of downstream padding around the TIS site (default: None)')
parser.add_argument('--ensembl', '-t', nargs=1, default=None, type=argparse.FileType('r'),
    help='the path to a plaintext file of true genes from Ensembl')
parser.add_argument('--ensembl_encodings', '--ensembl_encodings', nargs=1, default=None, type=str,
    help='the output file for Ensembl true gene encodings')
parser.add_argument('--pseudogenes', '-p', nargs=1, default=None, type=argparse.FileType('r'),
    help='the path to a plaintext file of pseudogenes from Ensembl')
parser.add_argument('--pseudo_encodings', '--pseudogene_encodings', nargs=1, default=None, type=str,
    help='the output file for pseudogene encodings')
parser.add_argument('--gffs_dir', '--gffs', '-g', nargs=1, type=str, required=True,
    help='the path to a directory of BUSCO output gff files')
parser.add_argument('--source_anno', nargs=1, type=str, required=True,
    help='the source annotation to compare busco against')
parser.add_argument('--train_set', nargs=1, default=None, type=str,
    help='the location of the training data set')
parser.add_argument('--val_set', '--validation_set', nargs=1, default=None, type=str,
    help='the location of the validation data set')
parser.add_argument('--busco_matches', nargs=1, default=None, type=str,
    help='the output file for busco exact matches')
parser.add_argument('--busco_encodings', nargs=1, default=None, type=str,
    help='the output file for busco encodings')
parser.add_argument('--stats', '--stats_file', nargs=1, default=None, type=str,
    help='the output file for statistics on the dataset')
parser.add_argument('--line_len', default=60, type=int,
    help='the length of each line in the provided FASTA file (default: 60)')
parser.add_argument('--train_balance', nargs=3, default=[1000, 200, 500], type=int,
    help='3 numbers describing the number of buscos, pseudogenes, and non-pseudogene negatives in the dataset. If there is not enough data to satisfy the request, then there will be slightly less data in the resulting train set. (default: [1000, 200, 500])')
parser.add_argument('--val_balance', nargs=2, default=[100, 100], type=int,
    help='2 numbers describing the number of ensemble genes and scanned atgs in the validation dataset. If there is not enough data to satisfy the request, then there will be slightly less data in the resulting validation set. (default: [100, 100])')
parser.add_argument('--skip_busco', action='store_true',
    help='whether to skip creation of busco data or not')
parser.add_argument('--skip_pseudo', '--skip_pseudogene', action='store_true',
    help='whether to skip creation of pseudogene data or not')
parser.add_argument('--skip_ensembl', action='store_true',
    help='whether to skip creation of Ensemble true gene data or not')
parser.add_argument('--skip_full_scan', action='store_true',
    help='whether to skip full fasta scan and creation of data or not')

def get_out_file(arg, mode='w'):
  if isinstance(arg, list):
    arg = arg[0]
  if arg is not None:
    return open(arg, mode)
  return None

if __name__ == '__main__':
  args = parser.parse_args()

  # Get configurations from parser
  start_pad = args.start_pad
  end_pad = args.end_pad
  line_len = args.line_len
  buffer_items = int(((max(start_pad, end_pad) / line_len) * 2) + 2)
  canonical_only = True

  # Get data files from parser
  stats_file = args.stats[0] if args.stats is not None else None
  fasta = args.fasta[0]
  gffs_dir = args.gffs_dir[0]
  source_anno = args.source_anno[0] if isinstance(args.source_anno, list) else args.source_anno
  if not args.skip_busco:
    print('Getting exact busco matches...')
    busco_matches = get_out_file(args.busco_matches)
    buscos, ensembls, busco_stats = match_buscos(source_anno, gffs_dir, busco_matches)
    print(busco_stats)
  
    print('Getting busco gene encodings...')
    busco_fn = get_out_file(args.busco_encodings)
    buscos, _ = encode_dict(busco_fn, fasta, buscos, line_len, start_pad, end_pad)

  pseudogenes = {}
  if not args.skip_pseudo:
    print('Getting pseudogene encodings...')
    pseudo_data = args.pseudogenes[0]
    pseudo_fn = get_out_file(args.pseudo_encodings)
    pseudo_format = {
      'scaffold': 0,
      'start': 1,
      'strand': 2,
    }
    pseudogenes, _ = encode_text(pseudo_fn, fasta, pseudo_data, pseudo_format, '0', 'pseudogenes', pseudogenes, line_len, start_pad, end_pad)

  ensembls = {}
  if not args.skip_ensembl:
    print('Getting Ensembl encodings...')
    source_anno = open(source_anno, 'r')
    ensembl_fn = get_out_file(args.ensembl_encodings)
    ensembls, ensembl_stats = encode_gff(ensembl_fn, fasta, source_anno, '1', 'Ensembl', ensembls, line_len, start_pad, end_pad)

  filtered_negatives = {}
  if not args.skip_full_scan:
    print('Getting full genome encodings. This may take a while...')
    fasta_out = get_out_file(args.fasta_out)
    filtered = get_out_file(args.filtered)
    filtered_negatives = encode_genome(fasta_out, filtered, fasta, '0', 'all_atg', line_len, start_pad, end_pad)

  train_fn = get_out_file(args.train_set)
  val_fn = get_out_file(args.val_set)
  num_buscos = args.train_balance[0]
  num_pseudos = args.train_balance[1]
  num_non_pseudos = args.train_balance[2]
  num_val_ensembl = args.val_balance[0]
  num_val_atg = args.val_balance[1]

  train_set = []
  val_set = []

  train_set = select_samples(num_buscos, buscos, train_fn, train_set)
  train_set = select_samples(num_pseudos, pseudogenes, train_fn, train_set)
  train_set = select_samples(num_non_pseudos, filtered_negatives, train_fn, train_set)

  fasta_out.close()
  fasta_fn = args.fasta_out[0] if isinstance(args.fasta_out, list) else args.fasta_out
  val_set = select_samples(num_val_ensembl, ensembls, val_fn, val_set)
  val_set = select_samples(num_val_atg, fasta_fn, val_fn, val_set)





