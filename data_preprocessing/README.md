## Data Preprocessing

This directory contains all of the scripts related to data preprocessing. These scripts will take a genome as an input and will return potential TIS sites which will be fed to the [binary classifier](../binary_classifier).

The main script used for data preprocessing is the `prepare_all_data.sh` script. This bash script will start a slurm job and call the necessary python scripts needed to prepare the data before it can be fed to the binary classifier. It has a number of files which must be found in your data directory:

* Genome fasta: this is the complete fasta file for the genome of the species you wish to train on
* GFF Directory: this is the directory which contains all of the output gff files from a BUSCO run
* Source Annotation: this is the .gtf file which contains the full annotation of the genome from Ensembl
* Pseudogenes file: this is the pseudogenes text file which contains the location of all the reported pseudogenes from Ensembl

The `prepare_all_data.sh` script will pass the location of all of these files, plus the locations of some intermediate files and some additional parameters (such as padding sizes and train set/validation set balance) to the `build_data.py` script. This script will then create three separate datasets: a training set, a validation set, and a full genome set. Each dataset is encoded so that it can be properly interpreted by the binary classifier. The training set will contain TIS sites from BUSCO genes, pseudogenes, and non-pseudogene true negatives. The validation set will contain TIS sites from Ensembl genes and non-pseudogene true negatives. The full genome set will contain all of the potential TIS sites within the genome (or all of the sites within the genome which have the ATG codon).

After these three datasets are created, the `prepare_all_data.sh` script will run the `add_entropy.py` script on each, which will add the RNAfold values to each data point in the dataset. Once that is complete, the data is ready to be fed to the binary classifier.

Some other utilities for the data preparation process exist in the [`utils/`](../utils) folder. More details are available there.
