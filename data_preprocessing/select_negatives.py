import numpy as np
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser(description='Filter out some obvious non-pseudogene true negative start sites.')
parser.add_argument('--filtered_file', '-f', nargs=1, default=None, type=str,
    help='the path to the file with filtered potential start sites')
parser.add_argument('--choose', '-c', nargs=1, default=None, type=int,
    help='the number of filtered negatives to select')

args = parser.parse_args()
choose = args.choose

path = args.filtered_file[0]

cmdstr = "wc -l %s" % path
proc = Popen(cmdstr.split(), stdout=PIPE, stdin=PIPE)
stdout, stdin = proc.communicate()
size = int(stdout.split()[0])
handle = open(path, 'r')

choices = np.random.choice(size, choose, replace=False)
line = handle.readline().strip()
idx = 0
while line:
  if idx in choices:
    print(line)
  idx += 1
  line = handle.readline().strip()

