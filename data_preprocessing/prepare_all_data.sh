#!/bin/bash
#SBATCH --job-name=prepare_all_data
#SBATCH -o prepare_all_data_240_240.out
#SBATCH -e prepare_all_data_240_240.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --nodelist=gpu03
#SBATCH --cpus-per-task=32
#SBATCH --partition=gpu_xtra
#SBATCH --mem=10G

echo `hostname`
source ~/.bashrc
cd ~/annotationtool/data_preprocessing

[[ -z "$py_exec" ]] && export py_exec=$py_exec || export py_exec='python3'
echo "using python executable: `which $py_exec`"

$py_exec --version || ( echo 'Error using python executable!' && exit )

# Species we are building data for
species='celegans'
declare -i chunks=`nproc`
USER=`whoami`
# Root directory for our data
DATA_DIR='/scratch/'$USER

# Source files required to build data
FASTA=$DATA_DIR'/data/'$species'/genome.fa'
GFF_DIR=$DATA_DIR'/data/'$species'/gffs/'
SOURCE_ANNO=$DATA_DIR'/data/'$species'/source_annotation.gtf'
PSEUDOGENES=$DATA_DIR'/data/'$species'/pseudogenes.txt'

# Padding sizes
START_PAD='240'
END_PAD='240'

# Ensure data directory and temp directories exist
data_prefix=$DATA_DIR'/data/'$species'/'$START_PAD'_'$END_PAD'/'
temp_dir=$data_prefix'tmp/'
mkdir -p $data_prefix
mkdir -p $temp_dir

# Names of output files
TRAIN_SET_ENTROPY=$data_prefix'train_set_entropy.txt'
VAL_SET_ENTROPY=$data_prefix'val_set_entropy.txt'
FASTA_ENTROPY=$data_prefix'all_atg_entropy.txt'

# Names of intermediate files
BUSCO_ENCODED=$data_prefix'busco_encoded.txt'
PSEUDO_ENCODED=$data_prefix'pseudogenes_encoded.txt'
ENSEMBL_ENCODED=$data_prefix'ensembl_encoded.txt'
FASTA_ENCODED=$data_prefix'all_atg_encoded.txt'
FILTERED_NEGATIVES=$data_prefix'filtered_negatives.txt'
TRAIN_SET=$data_prefix'train_set.txt'
VAL_SET=$data_prefix'val_set.txt'

# Build data
$py_exec -u build_data.py -f $FASTA -g $GFF_DIR --start $START_PAD --end $END_PAD --source_anno $SOURCE_ANNO --busco_encodings $BUSCO_ENCODED -p $PSEUDOGENES --pseudo_encodings $PSEUDO_ENCODED --ensembl_encodings $ENSEMBL_ENCODED --atg_out $FASTA_ENCODED --filtered $FILTERED_NEGATIVES --train_set $TRAIN_SET --val_set $VAL_SET --train_balance 1000 500 1000

# Get entropy data
rm $temp_dir/*
$py_exec -u ../utils/add_entropy.py --in_file=$TRAIN_SET --temp_dir=$temp_dir --out_file=$TRAIN_SET_ENTROPY
rm $temp_dir/*
$py_exec -u ../utils/add_entropy.py --in_file=$VAL_SET --temp_dir=$temp_dir --out_file=$VAL_SET_ENTROPY
rm $temp_dir/*
$py_exec -u ../utils/add_entropy.py --in_file=$FASTA_ENCODED --temp_dir=$temp_dir --out_file=$FASTA_ENTROPY

