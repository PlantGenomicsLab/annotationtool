## Refine Gene Models

This directory contains all of the scripts that EASEL uses to refine and polish gene models constructed by the [create gene models](../create_gene_models) step.
