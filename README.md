## Current Approach
[diagram](diagrams/EASEL_PAG_final.pdf)

Input: Genome (protein/RNAseq evidence optional)

### Prepare Data
1. Softmask Genome
2. HISAT2
3. StringTie
Output: Assembled transcripts, softmasked genome

### Predict Starts
4. Create test/train set using Pseudogenes as true negatives
5. Train deep learning binary classifier with Sequence and Free energy information
6. Predict likely starts on all canonical sites across genome
Output: List of likely starts in the genome

### Extend Gene Models
7. Train AUGUSTUS on StringTie transcripts
8. Run AUGUSTUS on whole genome provided with possible starts hintsfile
Output: GTF annotation

### Filtering/Refinement
9. Run GTF through gFACs to filter gene models
- full filtering here: (filtering steps)
10. Run EnTAP to filter based on sequence similarity of known protein
11. Final gFACs/BUSCO statistics


## Relevant Scripts
- [binary_classifier](binary_classifier/binary_classifier.py)
- [train_aug](Workspace/Supervised_Training/extend_gene_models/example_scripts/train_aug.sh)
- [filter_gfacs](Workspace/Supervised_Training/extend_gene_models/example_scripts/filt_gfacs.sh)
- [entap](Workspace/Supervised_Training/extend_gene_models/example_scripts/entap.sh)
- [gfacs_stats](Workspace/Supervised_Training/extend_gene_models/example_scripts/gfacs.sh)

## Contents 
- [Weekly Updates](https://docs.google.com/document/d/11ehQTdCwwkWIh_KmhpiNqDzvG8s2QPPE7LzR489-Lw0/edit)
- [Data Information Spreadsheet](https://docs.google.com/spreadsheets/d/1Effvbj77kkjbGRoloRnbzyKjeFk6qPmrdB4UIm2opQI/edit#gid=961477260)
- [Binary Classifier Stats](https://docs.google.com/spreadsheets/d/1RRxci03ixGcMhkssYMn83LOPWzz20Gp04vidIp4lgS4/edit#gid=0)
- [EASEL predictions - Stringtie, Braker, Maker Spreadsheet](https://docs.google.com/spreadsheets/d/1NG_rW-8GbvySx6wY5AfBE3s9-XH3dCxTbdpW-7jPxJc/edit?folder=0AAX8ZdB4HRYIUk9PVA#gid=915834033)
- [BRAKER new version iterations in Moss](https://docs.google.com/spreadsheets/d/1nD2lru3QTsvsn6HtXip9XP7PTvCHfbWFkLESv1-goLE/edit?usp=sharing)
- [Benchmark status](https://docs.google.com/spreadsheets/d/1QjPuc7HekoIH8AgDMuzAiD_fTqDHzQdYPjNz-xvYByI/edit#gid=0)
