#!/bin/bash
#SBATCH --job-name=beluga_busco
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/3.0.2b
module unload augustus/3.2.3
module unload blast/2.7.1

basedir=/UCHC/LABS/Wegrzyn/annotationtool/testSpecies/non-model/Delphinapterus_leucas/


# cp augustus to home to avoid permission error
export PATH=/home/CAM/jbennett/augustus/bin:/home/CAM/jbennett/augustus/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/config

/isg/shared/apps/busco/3.0.2b/bin/run_BUSCO.py -i $basedir/genome/beluga_genome_sm.fa-filtered -l /isg/shared/databases/busco_lineages/laurasiatheria_odb9/ -o busco_o -m geno


