#!/bin/bash
#SBATCH --job-name=uniprot_elegans
#SBATCH -o redone%j.out
#SBATCH -e redone%j.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G

module load Blat/36x2

proteins=uniprot_sprot.fasta
genome=../elegans_genome_sm.fa-filtered

#stepsize=5 is used by default for protein, spacing between tiles
#repmatch=1024 by default.
#Sets the number of repetitions of a tile allowed before it is marked as 
#overused. Typically this is 256 for tileSize 12, 1024 for tile size 11, 
#4096 for tile size 10. Default is 1024. Typically needed only with makeOoc. 
#Also affected by stepSize: when stepSize is halved, repMatch is doubled to compensate

blat -q=prot -t=dna  -minIdentity=70 $genome $proteins output_redone.psl
