#!/bin/bash
#SBATCH --job-name=celegans_busco_exonerate
#SBATCH -o %j.out
#SBATCH -e %j.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=20G

module load exonerate/2.4.0

proteins=celegans_ancestral
genome=elegans_genome_sm.fa-filtered

exonerate -q $proteins -t $genome -m protein2genome
