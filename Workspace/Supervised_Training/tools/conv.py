import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv1D
import numpy as np

def conv_coding_gen(handle, vector_len, batch_size=32):

  #Tokenizer for dna_seq
  dna_tokenizer = {"a":1,"A":2,"c":3,"C":4,"g":5,"G":6,"t":7,"T":8,"n":9,"N":10}

  while(True):
    #input_data = np.zeros((batch_size, vector_len, 11), dtype='float32')
    #target_data = np.zeros((batch_size, vector_len, 8), dtype='float32')
    #for i in range(batch_size):
    #  dna_line = handle.readline().strip()
    #  if (not(dna_line)):
    #    handle.seek(0)
    #    dna_line = handle.readline().strip()
    #  label_line = handle.readline().strip()
    #  handle.readline()
    #  for j in range(vector_len):
    #    if j >= len(dna_line):
    #      input_data[i, j, 0] = 1.
    #      target_data[i, j, 0] = 1.
    #    else:
    #      input_data[i, j, dna_tokenizer[dna_line[j]]] = 1.
    #      target_data[i, j, int(label_line[j])] = 1.
    #yield(input_data, target_data)
    dna_lines = []
    label_lines = []
    longest_line = 0
    for i in range(batch_size):
      dna_line = handle.readline().strip()
      if (not(dna_line)):
        handle.seek(0)
        dna_line = handle.readline().strip()
      label_lines.append(handle.readline().strip())
      handle.readline()
      if (longest_line < len(dna_line)):
        longest_line = len(dna_line)
      dna_lines.append(dna_line)
    input_data = np.zeros((batch_size, longest_line, 11), dtype='float32')
    target_data = np.zeros((batch_size, longest_line, 8), dtype='float32')
    for i in range(batch_size):
      for j in range(longest_line):
        if j >= len(dna_lines[i]):
          input_data[i, j, 0] = 1.
          target_data[i, j, 0] = 1.
        else:
          input_data[i, j, dna_tokenizer[dna_lines[i][j]]] = 1.
          target_data[i, j, int(label_lines[i][j])] = 1.
    yield(input_data, target_data)

def generate_model(longest_line, filters, kernel_size, strides):
  model = Sequential()
  model.add(Conv1D(filters, kernel_size, strides, input_shape=(None, 11), padding="same"))
  model.add(Dense(8, activation='softmax'))
  return model
