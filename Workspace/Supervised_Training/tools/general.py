from tensorflow.keras.callbacks import Callback
from subprocess import Popen,PIPE
from bidirectional import coding_gen, generate_model
import resource

def get_n_samples(filename):
  cmdstr = "wc -l %s" % filename
  proc = Popen(cmdstr.split(), stdout=PIPE, stdin=PIPE)
  stdout, stdin = proc.communicate()
  n_samples = int(stdout.split()[0]) / 3
  print("got number of samples: " + str(n_samples))
  return n_samples

class MemoryCallback(Callback):
  def on_batch_end(self, batch, logs={}):
    print("")
    print("batch " + str(batch) + " memory usage (MB): " + str(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss // 1024))

def train_model(train_filename,
    bs=32,
    lstm_units=16,
    n_epochs=1,
    model=None,
    generator=coding_gen,
    loss_fn='categorical_crossentropy',
    opt_fn='adam',
    metric_fns=['accuracy'],
    callbacks=[MemoryCallback()],
    max_queue=None):
  n_samples = get_n_samples(train_filename)
  n_steps = n_samples // bs
  if max_queue is None:
    max_queue = bs // 2
  
  if model is not None:
    model = keras.models.load_model(model)
  else:
    model = generate_model(lstm_units)
    model.compile(loss=loss_fn, optimizer=opt_fn, metrics=metric_fns)
    model.summary()
    print("model compiled")
  handle = open(train_filename, 'r')
  gen = generator(handle, batch_size=bs)
  model.fit_generator(gen, epochs=n_epochs, steps_per_epoch=n_steps, max_queue_size=max_queue, callbacks=callbacks)
  handle.close()
  print("model trained")
  return model
