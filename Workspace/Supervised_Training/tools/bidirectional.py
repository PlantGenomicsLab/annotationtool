import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import LSTM,Dense,Input,Bidirectional,Concatenate,Embedding,RNN
from tensorflow.contrib.rnn import LSTMBlockCell
from tensorflow.nn import dynamic_rnn
import numpy as np
import math

def kmer_coding_gen(handle, batch_size=32, k_size=3):
  dna_tokenizer = {"a": 0, "A": 1, "c": 2, "C": 3, "g": 4, "G": 5, "t": 6, "T": 7, "n": 8, "N": 9}
  while (True):
    dna_lines = []
    label_lines = []
    longest_line = 0
    for i in range(batch_size):
      dna_line = handle.readline().strip()
      if (not(dna_line)):
        handle.seek(0)
        dna_line = handle.readline().strip()
      label_line = handle.readline().strip()
      handle.readline()
      if longest_line < len(dna_line):
        longest_line = len(dna_line)
      dna_lines.append(dna_line)
      label_lines.append(label_line)
    
    dim = math.ceil(longest_line / k_size)
    encoder_input_data = np.zeros((batch_size, dim, 1 + (10 ** k_size)), dtype='float32')
    decoder_input_data = np.zeros((batch_size, dim, (8 ** k_size)), dtype='float32')
    decoder_target_data = np.zeros((batch_size, dim, (8 ** k_size)), dtype='float32')
    for i in range(batch_size):
      sample_index = 0
      line_index = 0
      while (sample_index < dim):
        dna_item = 0
        label_item = 0
        for j in range(k_size):
          dna_item *= 10
          label_item *= 8
          if line_index < len(dna_lines[i]):
            dna_item += dna_tokenizer[dna_lines[i][line_index]]
            label_item += int(label_lines[i][line_index]) - 1
          line_index += 1
        encoder_input_data[i, sample_index, dna_item] = 1.
        decoder_input_data[i, sample_index, label_item] = 1.
        if sample_index > 0:
          decoder_target_data[i, sample_index - 1, label_item] = 1.
        sample_index += 1
      #for j in range(longest_line):
      #  if j >= len(dna_lines[i]):
      #    encoder_input_data[i, j, 0] = 1.
      #    decoder_input_data[i, j, 0] = 1.
      #    if j > 0:
      #      decoder_target_data[i, j - 1, 0] = 1.
      #  else:
      #    encoder_input_data[i, j, dna_tokenizer[dna_lines[i][j]]] = 1.
      #    decoder_input_data[i, j, int(label_lines[i][j])] = 1.
      #    if j > 0:
      #      decoder_target_data[i, j - 1, int(label_lines[i][j])] = 1.
      #  if ((index + 1) % k_size) == 0:
      #    index += 1
    yield([encoder_input_data, decoder_input_data], decoder_target_data)

  #while (True):
  #  samples = []
  #  labels = []
  #  longest = 0
  #  for i in range(batch_size):
  #    dna_line = handle.readline().strip()
  #    if (not(dna_line)):
  #      handle.seek(0)
  #      dna_line = handle.readline().strip()
  #    label_line = handle.readline().strip()
  #    handle.readline()
  #    if longest_line < math.ceil(len(dna_line) / k_size):
  #      longest_line = math.ceil(len(dna_line) / k_size)
  #    dna_lines.append(dna_line)
  #    label_lines.append(label_line)

  #  encoder_input_data = np.zeros((batch_size, longest_line), dtype='float32')
  #  decoder_input_data = np.zeros((batch_size, longest_line, 8), dtype='float32')
  #  decoder_target_data = np.zeros((batch_size, longest_line, 8), dtype='float32')
  #  for i in range(batch_size):
  #    

def coding_gen(handle, batch_size=32):
  
  dna_tokenizer = {"a":1,"A":2,"c":3,"C":4,"g":5,"G":6,"t":7,"T":8,"n":9,"N":10}
  while (True):
    dna_lines = []
    label_lines = []
    longest_line = 0
    for i in range(batch_size):
      dna_line = handle.readline().strip()
      if (not(dna_line)):
        handle.seek(0)
        dna_line = handle.readline().strip()
      label_line = handle.readline().strip()
      handle.readline()
      if longest_line < len(dna_line):
        longest_line = len(dna_line)
      dna_lines.append(dna_line)
      label_lines.append(label_line)
    
    encoder_input_data = np.zeros((batch_size, longest_line), dtype='float32')
    decoder_input_data = np.zeros((batch_size, longest_line, 8), dtype='float32')
    decoder_target_data = np.zeros((batch_size, longest_line, 8), dtype='float32')
    for i in range(batch_size):
      for j in range(longest_line):
        if j >= len(dna_lines[i]):
          encoder_input_data[i, j] = 0.
          decoder_input_data[i, j, 0] = 1.
          if j > 0:
            decoder_target_data[i, j - 1, 0] = 1.
        else:
          encoder_input_data[i, j] = dna_tokenizer[dna_lines[i][j]]
          decoder_input_data[i, j, int(label_lines[i][j])] = 1.
          if j > 0:
            decoder_target_data[i, j - 1, int(label_lines[i][j])] = 1.
    yield([encoder_input_data, decoder_input_data], decoder_target_data)

def generate_model(lstm_units):
  #main_inputs = Input(shape=(None,), dtype='int32')
  #encoder_inputs = Embedding(input_dim=11, output_dim=10, mask_zero=True)(main_inputs)
  main_inputs = Input(shape=(None, 1 + 10 ** 3), dtype='int32')
  encoder = Bidirectional(LSTM(lstm_units, return_state=True))
  #encoder_outputs, front_h, front_c, back_h, back_c = encoder(encoder_inputs)
  encoder_outputs, front_h, front_c, back_h, back_c = encoder(main_inputs)
  state_h = Concatenate()([front_h, back_h])
  state_c = Concatenate()([front_c, back_c])
  encoder_states = [state_h, state_c]

  decoder_inputs = Input(shape=(None, 8 ** 3))
  decoder_lstm = LSTM(lstm_units * 2, return_sequences=True, return_state=True)
  decoder_outputs, _, _ = decoder_lstm(decoder_inputs, initial_state=encoder_states)
  decoder_dense = Dense(8, activation='softmax')
  decoder_outputs = decoder_dense(decoder_outputs)
  print('test')
  model = Model([main_inputs, decoder_inputs], decoder_outputs)
  return model
