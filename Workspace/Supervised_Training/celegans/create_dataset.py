import subprocess,sys

## input: BUSCO full_table output, directory of busco run
## output: genomic sequence input + labels for each base

smallset=0

busco_table_filename = "full_table_busco_o.tsv"
busco_outputdir = "/labs/Wegrzyn/annotationtool/testSpecies/model/elegans/scripts_used/3_filter/busco/genome_busco/run_busco_o"
genome_path = "/labs/Wegrzyn/annotationtool/testSpecies/model/elegans/genome/elegans_genome_sm.fa-filtered"

def grab_seq(scaffold_name,start,end,strand):
  start = int(start)
  end = int(end)
  with open(genome_path,'r') as rf:
    take_next=0
    for line in rf:
      line = line.strip()
      if(take_next):
        break
      if(line==">"+scaffold_name):
        take_next=1 
#    if((start-1501<0) or (end+1501>len(line))):
#      return "boundaryerror"
    if(strand=="+"):
#      region = line[start-1501:end+1500]
       region = line[start-1:end]
    else:
#      region = line[start-1500:end+1500+1]
       region = line[start:end+1]
  return region

def replace(lst,tup,label):
  if(tup[0]==tup[1]): #replacing nothing
    return
  try:
    assert(tup[0]<tup[1])
    for i in range(tup[0],tup[1]+1):
      lst[i] = label
  except IndexError:
    print("Failed index when replacing %s with %c" % (str(tup),label))
    sys.exit()

def grab_labels(busco_outputdir,busco_id,total_length,gene_start):
  ## Label legend
  label_legend = {"UTR_5":"1","start_codon":"2","exon":"3","DSS":"4","intron":"5","ASS":"6","stop_codon":"7","UTR_3":"8"}

  base_labels = [label_legend["intron"] for i in range(total_length)]

  try:
    with open(busco_outputdir+"/augustus_output/gffs/"+busco_id+".gff",'r') as rf:
  
      ## Gather locations from gff file
      exon_list=[] #(start,stop)
      startloc = ()
      stoploc = ()
      UTR_5 = ()
      UTR_3 = ()
      print("Working with %s" % (busco_id))
      for line in rf:
        line_list = line.split("\t")
        contig,source,name,start,stop,score,strand = line_list[:7]
        start = int(start)
        stop = int(stop)
        region = (start-gene_start,stop-gene_start)
        if(name=="start_codon"):
          startloc = region
        elif(name=="stop_codon"):
          stoploc = region
        elif(name=="CDS"):
          exon_list.append(region)
        elif(name=="5'-UTR"):
          UTR_5 = region
        elif(name=="3'-UTR"):
          UTR_3 = region

      ## Label Exon, DSS/ASS
 
      if(len(exon_list)>1): #multi-exonic
        for i in range(len(exon_list)):
          replace(base_labels,exon_list[i],label_legend["exon"])
          if(i==0):
            base_labels[exon_list[i][1]+1:exon_list[i][1]+3] = label_legend["DSS"]*2
          elif(i==len(exon_list)-1):
            base_labels[exon_list[i][0]-2:exon_list[i][0]] = label_legend["ASS"]*2
          else:
            base_labels[exon_list[i][1]+1:exon_list[i][1]+3] = label_legend["DSS"]*2
            base_labels[exon_list[i][0]-2:exon_list[i][0]] = label_legend["ASS"]*2
      else:
        replace(base_labels,exon_list[0],label_legend["exon"])

      ## Label Start/Stop
      if(startloc):
        replace(base_labels,startloc,label_legend["start_codon"])
      if(stoploc):
        replace(base_labels,stoploc,label_legend["stop_codon"])

      ## Label UTR
      if(UTR_5):
        replace(base_labels,UTR_5,label_legend["UTR_5"])
      if(UTR_3):
        replace(base_labels,UTR_3,label_legend["UTR_3"])

#      if(strand=="-"):
#        temp1 = [label_legend["5UTR"] for i in range(1500)]
#        temp2 = [label_legend["3UTR"] for i in range(1500)]
#      else:
#        temp1 = [label_legend["3UTR"] for i in range(1500)]
#        temp2 = [label_legend["5UTR"] for i in range(1500)]
#      base_labels = temp1+base_labels+temp2
  except IOError:
    return(("filenotfound","","",""))

  return((base_labels,strand,startloc,stoploc))

if(__name__=="__main__"):
  wf = open("Celegans_Labels_New.txt",'w')
  valid_starts = 0
  valid_stops = 0
  total_genes = 0
  invalid_starts = {}
  invalid_stops = {}

  ## Error Counts
  boundaryerror=0
  filenotfound=0

  with open(busco_outputdir+"/"+busco_table_filename,'r') as rf:
    for line in rf:
      if(line.startswith("#")==True):
        continue
      line_list = line.split("\t")
      if((line_list[1] == "Complete") or (line_list[1] == "Duplicated")):
        busco_id,status,contig,start,end = line_list[:5]
        start = int(start)
        end = int(end)
        labels,strand,start_loc,stop_loc = grab_labels(busco_outputdir,busco_id,end-start+1,start)
        seq = grab_seq(contig,start,end,strand)

        ## Error Counts
        if(seq=="boundaryerror"):
          boundaryerror+=1
          continue
        if(labels == "filenotfound"):
          filenotfound+=1
          continue

        ## Sanity checking, small set examples
        if(smallset):
          if(total_genes>19):
            break

        ## Checking starts/stops
        if(start_loc):
          start_loc = start_loc[0]
          start = seq[start_loc:start_loc+3]
          start = start.upper()
          if(start == "ATG"):
            valid_starts+=1
          else:
            invalid_starts[start] = invalid_starts.get(start,0)+1
        if(stop_loc):
          stop_loc = stop_loc[0]#+1500
          stop = seq[stop_loc:stop_loc+3]
          stop = stop.upper()
          if(stop in ["TAA","TAG","TGA"]):
            valid_stops+=1
          else:
            invalid_stops[stop] = invalid_stops.get(stop,0)+1
        total_genes+=1

        print("len(labels) = %d, len(seq) = %d" % (len(labels),len(seq)))
        assert(len(labels)==len(seq))
        wf.write(seq+"\n"+"".join(labels)+"\n\n")
      else:
        continue
  print("valid_starts = %d, valid_stops = %d, total_genes = %d" % (valid_starts,valid_stops,total_genes))
  print("invalid_starts = %s" % str(invalid_starts))
  print("invalid_stops = %s" % str(invalid_stops))
  print("boundaryerror = %d, filenotfound = %d" % (boundaryerror,filenotfound))
  wf.close()
