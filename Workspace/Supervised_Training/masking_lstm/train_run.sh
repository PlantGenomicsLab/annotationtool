#!/bin/bash
#SBATCH --job-name=train_model_less_cores
#SBATCH -o less_cores.out
#SBATCH -e less_cores.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=150G

echo `hostname`
source ~/.bashrc
cd masking_lstm
NUM_EPOCHS=10
python -u masking_lstm.py --model=model.h5 --epochs=$NUM_EPOCHS
echo final epoch: $NUM_EPOCHS
