import sys, os

threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
config = tf.compat.v1.ConfigProto()
config.inter_op_parallelism_threads = inter_op_threads
config.intra_op_parallelism_threads = intra_op_threads
tf.compat.v1.Session(config=config)

from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.backend import clear_session
from tensorflow.keras import layers
import numpy as np
from subprocess import Popen, PIPE
import math
from tensorflow.keras.callbacks import Callback
import resource

class MemoryCallback(Callback):
  def on_batch_end(self, batch, logs={}):
    print("")
    print("batch %d memory usage (MB): %d" % (batch, resource.getrusage(resource.RUSAGE_SELF).ru_maxrss // 1024))

class CharacterTable(object):
  """
  general onehot class
  """

  def __init__(self, chars, pad_value='0'):
    self.chars = sorted(set(chars))
    self.pad_value = pad_value
    self.char_indices = dict((c,i) for i,c in enumerate(self.chars))
    self.indices_char = dict((i,c) for i,c in enumerate(self.chars))

  def encode(self, C, num_rows):
    x = np.zeros((num_rows,len(self.chars)))
    for i,c in enumerate(C):
      if c == self.pad_value:
        continue
      x[i,self.char_indices[c]] = 1
    return x

  def decode(self, x, calc_argmax=True):
    if calc_argmax:
      x = x.argmax(axis=-1)
    return ''.join(self.indices_char[x] for x in x)

def customLoss(ytrue, ypred):
  labels = tf.cast(ytrue, tf.int32)
  bs = tf.shape(labels)[0]
  steps = tf.shape(labels)[1]
  dim = tf.shape(labels)[2]
  shift = tf.roll(labels, [1], [1])

  # Check which of the labels is different from the previous
  changes = tf.math.equal(tf.math.argmax(shift, axis=2), tf.math.argmax(labels, axis=2))
  changes = tf.ones((bs, steps), dtype=labels.dtype) - tf.cast(changes, labels.dtype)

  # The first label of each sequence should never be marked as "different from previous"
  # To ensure this, apply a mask
  mask = tf.one_hot(0*tf.ones((bs,), dtype=tf.int32), steps)
  mask = tf.ones((bs, steps)) - mask
  mult = changes * tf.cast(mask, labels.dtype)

  # Expand the shape of the multiplier to be compatible and apply the multiplier to the old labels
  expanded_mult = tf.broadcast_to(tf.reshape(mult, (bs, steps, 1)), (bs, steps, dim))
  multiplier = tf.cast(expanded_mult, labels.dtype) + tf.ones((bs, steps, dim), dtype=labels.dtype)
  new_labels = labels * multiplier

  # Calculate loss with new labels
  return tf.keras.losses.categorical_crossentropy(new_labels, ypred)

def coding_gen(handle, input_chars, output_chars, batch_size=32, pad_value='0'):
  input_table = CharacterTable(input_chars)
  output_table = CharacterTable(output_chars)
  input_dim = len(input_chars)
  output_dim = len(output_chars)
  while (True):
    longest = 0
    bases = []
    labels = []
    for i in range(batch_size):
      base_line = handle.readline().strip()
      if not base_line:
        handle.seek(0)
        base_line = handle.readline().strip()
      label_line = handle.readline().strip()
      handle.readline()
      if longest < len(base_line):
        longest = len(base_line)
      bases.append(base_line)
      labels.append(label_line)
    x = np.zeros((batch_size, longest, input_dim), dtype=np.bool)
    y = np.zeros((batch_size, longest, output_dim), dtype=np.bool)
    for i,seq in enumerate(bases):
      seq = seq + pad_value*(longest - len(seq))
      x[i] = input_table.encode(seq, longest)
    for i,seq in enumerate(labels):
      seq = seq + pad_value*(longest - len(seq))
      y[i] = output_table.encode(seq, longest)

    yield(x, y)

def main():
  args = sys.argv[1:]
  
  model_location = None
  train_path = "/labs/Wegrzyn/annotationtool/Workspace/Supervised_Training/celegans/Celegans_Labels_New_Train.txt"
  batch_size = 32
  n_layers = 4
  hidden_size = 128
  epochs = 10
  new_model = True
  help_opt = False
  n_steps = None
  init_epoch = 0
  
  for arg in args:
    if arg[:8] == "--model=":
      model_location = arg[8:]
    if arg[:7] == "--file=":
      train_path = arg[7:]
    if arg[:13] == "--batch_size=":
      batch_size = int(arg[13:])
    if arg[:8] == "--steps=":
      n_steps = int(arg[8:])
    if arg[:9] == "--layers=":
      n_layers = int(arg[9:])
    if arg[:13] == "--layer_size=":
      hidden_size = int(arg[13:])
    if arg[:9] == "--epochs=":
      epochs = int(arg[9:])
    if arg[:13] == "--init_epoch=":
      init_epoch = int(arg[13:])
    if arg == "--continue":
      new_model = False
    if arg == "-h":
      help_opt = True

  if help_opt:
    print('Usage: python annotate_rnn_coding_gen.py --model=<model file>')
    print('       [--file=<train data file>]')
    print('       [--batch_size=<batch size>]')
    print('       [--steps=<number of batches per epoch>]')
    print('       [--layers=<number of hidden layers>]')
    print('       [--layer_size=<units per layer>]')
    print('       [--epochs=<number of epochs>]')
    print('       [--continue]')
    print('       [-h]')
    return

  if model_location is None:
    print("please specify a model location: --model=<model file>")
    return

  clear_session()
  print('session cleared')

  print('Gathering data...')
  sys.stdout.flush()
  input_chars = 'AaCcGgTtNn'
  output_chars = '12345678'
  
  # get longest line for padding
  cmdstr = "wc -l %s" % train_path
  proc = Popen(cmdstr.split(),stdout=PIPE,stdin=PIPE)
  stdout,stdin = proc.communicate()
  n_lines=int(stdout.split()[0])

  RNN = layers.LSTM
  n_samples = n_lines // 3
  if n_steps is None:
    n_steps = math.ceil(n_samples / batch_size)

  print('number of samples: ' + str(n_samples))
  print('Batch size: ' + str(batch_size))
  print('Batches per epoch: ' + str(n_steps))
  print('Planned number of epochs: ' + str(epochs - init_epoch))
  print('Hidden layer size: ' + str(hidden_size))
  print('Number of hidden layers: ' + str(n_layers))

  if new_model:
    print('Build model...')
    model = Sequential()
    model.add(layers.Masking(input_shape=(None,len(input_chars))))
    model.add(layers.Bidirectional(RNN(hidden_size, return_sequences=True)))
    for _ in range(n_layers):
      model.add(RNN(hidden_size,return_sequences=True))
    model.add(layers.TimeDistributed(layers.Dense(len(output_chars),activation='softmax')))
    model.compile(loss='categorical_hinge',
    #model.compile(loss=customLoss,
                  optimizer='adam',
                  metrics=['accuracy'])
    model.summary()
    init_epoch = 0
  else:
    print('Load model...')
    model = load_model(model_location)
    model.summary()

  train_handle = open(train_path, 'r')
  model.fit_generator(coding_gen(train_handle, input_chars, output_chars, batch_size=batch_size),
          steps_per_epoch=n_steps,
          epochs=epochs,
          initial_epoch=init_epoch,
          callbacks=[ModelCheckpoint("checkpoint_" + model_location), MemoryCallback()])
  os.rename("checkpoint_" + model_location, model_location)

if __name__ == "__main__":
  main()
