import sys,math
from time import time

args = sys.argv[1:]

model_location = None
test_path = "/labs/Wegrzyn/annotationtool/Workspace/Supervised_Training/celegans/Celegans_Labels_New_Test.txt"
pred_file = "latest_predictions.txt"
bs = 32
predict = True
help_opt = False

for arg in args:
  if arg[:8] == "--model=":
    model_location = arg[8:]
  if arg[:7] == "--file=":
    test_path = arg[7:]
  if arg[:13] == "--batch_size=":
    bs = int(arg[13:])
  if arg == "-e":
    predict = False
  if arg[:6] == "--out=":
    pred_file = arg[6:]
  if arg == "-h":
    help_opt = True

if help_opt:
  print('Usage: python annotate_rnn_coding_gen.py --model=<model file>')
  print('       [--file=<data file>]')
  print('       [--batch_size=<batch size>]')
  print('       [--out=<prediction output file>]')
  print('       [-e]')
  print('       [-h]')
  quit()

if model_location is None:
  print("please specify a model location: --model=<model file>")
  quit()

import tensorflow as tf
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.backend import clear_session
from tensorflow.keras import layers
import numpy as np
from subprocess import Popen, PIPE

class CharacterTable(object):
  """
  general onehot class
  """

  def __init__(self, chars, pad_value='0'):
    self.chars = sorted(set(chars))
    self.pad_value = pad_value
    self.char_indices = dict((c,i) for i,c in enumerate(self.chars))
    self.indices_char = dict((i,c) for i,c in enumerate(self.chars))

  def encode(self, C, num_rows):
    x = np.zeros((num_rows,len(self.chars)))
    for i,c in enumerate(C):
      if c == self.pad_value:
        continue
      x[i,self.char_indices[c]] = 1
    return x

  def encode_flat(self, C, num_rows):
    x = np.zeros((num_rows))
    for i,c in enumerate(C):
      if c == self.pad_value:
        continue
      x[i] = self.char_indices[c]
    return x

  def decode(self, x, calc_argmax=True):
    if calc_argmax:
      x = x.argmax(axis=-1)
    results = []
    for item in x:
      result = ""
      for i in item:
        if str(i) == self.pad_value:
          #return result
          result += self.pad_value
          continue
        result += str(self.indices_char[i])
      results.append(result)
    return results

def coding_gen(handle, input_chars, output_chars, batch_size=32, pad_value='0'):
  input_table = CharacterTable(input_chars)
  output_table = CharacterTable(output_chars)
  input_dim = len(input_chars)
  output_dim = len(output_chars)
  while (True):
    longest = 0
    bases = []
    labels = []
    for i in range(batch_size):
      base_line = handle.readline().strip()
      if not base_line:
        handle.seek(0)
        base_line = handle.readline().strip()
      label_line = handle.readline().strip()
      handle.readline()
      if longest < len(base_line):
        longest = len(base_line)
      bases.append(base_line)
      labels.append(label_line)
    x = np.zeros((batch_size, longest, input_dim), dtype=np.bool)
    y = np.zeros((batch_size, longest, output_dim), dtype=np.bool)
    for i,seq in enumerate(bases):
      seq = seq + pad_value*(longest - len(seq))
      x[i] = input_table.encode(seq, longest)
    for i,seq in enumerate(labels):
      seq = seq + pad_value*(longest - len(seq))
      y[i] = output_table.encode(seq, longest)

    yield(x, y)

def customLoss(ytrue, ypred):
  labels = tf.cast(ytrue, tf.int32)
  bs = tf.shape(labels)[0]
  steps = tf.shape(labels)[1]
  dim = tf.shape(labels)[2]
  shift = tf.roll(labels, [1], [1])

  # Check which of the labels is different from the previous
  changes = tf.math.equal(tf.math.argmax(shift, axis=2), tf.math.argmax(labels, axis=2))
  changes = tf.ones((bs, steps), dtype=labels.dtype) - tf.cast(changes, labels.dtype)

  # The first label of each sequence should never be marked as "different from previous"
  # To ensure this, apply a mask
  mask = tf.one_hot(0*tf.ones((bs,), dtype=tf.int32), steps)
  mask = tf.ones((bs, steps)) - mask
  mult = changes * tf.cast(mask, labels.dtype)

  # Expand the shape of the multiplier to be compatible and apply the multiplier to the old labels
  expanded_mult = tf.broadcast_to(tf.reshape(mult, (bs, steps, 1)), (bs, steps, dim))
  multiplier = tf.cast(expanded_mult, labels.dtype) + tf.ones((bs, steps, dim), dtype=labels.dtype)
  new_labels = labels * multiplier

  # Calculate loss with new labels
  return tf.keras.losses.categorical_crossentropy(new_labels, ypred)

def main():
  global bs
  cmdstr = "wc -l %s" % test_path
  proc = Popen(cmdstr.split(),stdout=PIPE,stdin=PIPE)
  stdout,stdin = proc.communicate()
  n_lines=int(stdout.split()[0])
  
  n_samples = n_lines // 3
  n_steps = (n_samples // bs) + 1
  if n_samples < bs:
    bs = n_samples
    n_steps = 1

  clear_session()
  print('session cleared')
  
  print('Gathering data...')
  input_chars = 'AaCcGgTtNn'
  output_chars = '12345678'
  output_table = CharacterTable(output_chars)
  
  print('Build model...')
  model = load_model(model_location, custom_objects={'customLoss': customLoss})
  test_handle = open(test_path, 'r')
  
  start = time()
  if predict:
    pred_handle = open(pred_file, 'w')
    model.summary(print_fn=lambda x: pred_handle.write(x + "\n"))
    preds = model.predict_generator(coding_gen(test_handle, input_chars, output_chars, batch_size=bs),steps=n_steps)
    test_handle.close()

    pred_handle.write("samples predicted: " + str(n_samples) + "\n")
    pred_handle.write("prediction time: " + str(round(time() - start, 3)) + 's' + "\n")
    predictions = output_table.decode(preds)
    if len(predictions) == 1:
      predictions = predictions[0]
    pred_handle.write("results:" + "\n")
    pred_handle.write(str(predictions))
    pred_handle.write("\n")
    pred_handle.close()
  else:
    model.summary()
    evals = model.evaluate_generator(coding_gen(test_handle, input_chars, output_chars, batch_size=bs),steps=n_steps)
    test_handle.close()

    print("samples evaluated: " + str(n_samples))
    print("evaluation time: " + str(round(time() - start, 3)) + 's')
    print("results:")
    for i, val in enumerate(evals):
      print(model.metrics_names[i] + ': ' + str(round(val, 5)))

if __name__ == "__main__":
  main()

