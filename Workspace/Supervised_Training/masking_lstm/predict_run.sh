#!/bin/bash
#SBATCH --job-name=predict_run
#SBATCH -o continue_rnn.out
#SBATCH -e continue_rnn.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=150G

echo `hostname`
source ~/.bashrc
cd masking_lstm
python -u rnn_predict.py --model=model.h5 --file=predict_data.txt
