from __future__ import print_function
import sys, os

threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
if __name__ == '__main__':
  print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(inter_op_threads)
tf.config.threading.set_intra_op_parallelism_threads(intra_op_threads)

def get_languages():
  base_keys = tf.constant(['0', 'A', 'a', 'C', 'c', 'G', 'g', 'T', 't', 'N', 'n'])
  base_vals = tf.constant([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], dtype=tf.int32)
  input_language = tf.lookup.StaticHashTable(
    tf.lookup.KeyValueTensorInitializer(base_keys, base_vals), 0)
  
  type_keys = tf.constant(['0', '1', '2', '3', '4', '5', '6', '7', '8', '<start>', '<end>'])
  type_vals = tf.constant([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], dtype=tf.float32)
  output_language = tf.lookup.StaticHashTable(
    tf.lookup.KeyValueTensorInitializer(type_keys, type_vals), 0)
  return input_language, output_language

def get_samples(path, batch_size, inp_lang=None, out_lang=None, take=None, skip=None, cache=None, shuffle=None):

  def format_data(batch):
    batch = tf.map_fn(_format_data, batch, dtype=(tf.int32, tf.bool))
    return batch

  def _format_data(line):
    items = tf.strings.split(line, ',')
    label = tf.cast(tf.strings.to_number(items[-1]), tf.bool)
    seq = items[1]
    seq = tf.map_fn(lambda x: inp_lang.lookup(x), tf.strings.bytes_split(seq), dtype=tf.int32)
    return (seq, label)

  if inp_lang is None or out_lang is None:
    inp_lang, out_lang = get_languages()

  if not isinstance(path, list):
    path = [path]
  ds = tf.data.Dataset.from_tensor_slices(path)
  ds = ds.interleave(lambda x: tf.data.TextLineDataset(x), num_parallel_calls=tf.data.experimental.AUTOTUNE)
  # Now all samples from all files are in ds

  # Batch ds
  ds = ds.batch(batch_size, drop_remainder=True)

  if skip is not None:
    ds = ds.skip(skip)
  if take is not None:
    ds = ds.take(take)
  # Now ds has only the desired samples

  # Map batches from sequence strings to integer tensors
  ds = ds.map(format_data, num_parallel_calls=tf.data.experimental.AUTOTUNE)

  # Cache ds
  if cache is not None:
    ds = ds.cache(cache)

  # Shuffle samples
  if shuffle is not None:
    ds = ds.shuffle(shuffle)

  return ds

if __name__ == '__main__':
  paths = ['data/tis.txt', 'data/false_tis.txt']
  s = get_samples(paths, 3)
  for i in s.take(1):
    print(i)
