from __future__ import print_function
import sys, os

threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(inter_op_threads)
tf.config.threading.set_intra_op_parallelism_threads(intra_op_threads)

from tensorflow.keras.models import Sequential, Model, load_model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.backend import clear_session
from tensorflow.keras import layers
from tensorflow.keras import regularizers as regs
from dataset import get_samples, get_languages
import numpy as np
from subprocess import Popen, PIPE
import math
from tensorflow.keras.callbacks import Callback
import time

paths = [
  'data/tis.txt',
  'data/false_tis.txt'
]
model_location = 'tis_classifier.h5'

tf.keras.backend.clear_session()
batch_size = 256
n_epochs = 500
total_samples = 2893
n_steps = total_samples // batch_size
loss = 'binary_crossentropy'
metrics = 'accuracy'
show_data = False
l2_reg = .0001
drop_rate = .1

train_batches = n_steps - (n_steps // 10)
test_batches = n_steps - train_batches

lr_sched = tf.keras.optimizers.schedules.InverseTimeDecay(0.001,
    decay_steps=train_batches * 1000,
    decay_rate=1,
    staircase=False)
opt = tf.keras.optimizers.Adam(lr_sched)

print('optimizer:           {}'.format(opt))
print('loss:                {}'.format(loss))
print('metrics              {}'.format(metrics))
print('batch size:          {}'.format(batch_size))
print('epochs:              {}'.format(n_epochs))
print('number of samples:   {}'.format(total_samples))
print('training batches:    {}'.format(train_batches))
print('testing batches:     {}'.format(test_batches))

if not isinstance(metrics, list):
  metrics = [metrics]

print('getting dataset')
test_data = get_samples(paths, batch_size, take=test_batches, cache='ds/test').repeat().prefetch(tf.data.experimental.AUTOTUNE)
train_data = get_samples(paths, batch_size, skip=test_batches, cache='ds/train', shuffle=100)
#test_data = get_samples(paths, batch_size).take(test_batches).cache('ds/test').repeat()
#train_data = get_samples(paths, batch_size).skip(test_batches).cache('ds/train').shuffle(1000)
train_data = train_data.repeat().prefetch(tf.data.experimental.AUTOTUNE)
print('got dataset')

if show_data:
  print('sample batch:')
  for batch in train_data.take(1):
    print(batch)

conv_settings = [
  [64, 3, 1, 'relu', 3],
  [10, 30, 10, 'relu', 6]
]
lstm_settings = [
  #[16, False]
]

print('building model')
model = Sequential()
model.add(layers.Embedding(10, 10, input_shape=(None,)))

layer_num = 1
for s in conv_settings:
  model.add(layers.Conv1D(s[0], s[1], s[2], activation=s[3], kernel_regularizer=regs.l2(l2_reg), name='{}_conv1d_{}_{}_{}_{}'.format(layer_num, s[0], s[1], s[2], s[3])))
  layer_num += 1
  model.add(layers.MaxPool1D(s[4], name='{}_pool1d_{}'.format(layer_num, s[4])))
  model.add(layers.Dropout(drop_rate))
  layer_num += 1

for s in lstm_settings:
  model.add(layers.LSTM(s[0], return_sequences=s[1], kernel_regularizer=regs.l2(l2_reg), name='{}_lstm_{}_{}'.format(layer_num, s[0], s[1])))
  model.add(layers.Dropout(drop_rate))
  layer_num += 1

model.add(layers.Dense(1, activation='sigmoid', kernel_regularizer=regs.l2(l2_reg)))
model.compile(optimizer=opt, loss=loss, metrics=metrics)
print('model compiled:')
model.summary()

start = time.time()
print('starting training')
model.fit(train_data,
        verbose=2,
        epochs=n_epochs,
        steps_per_epoch=train_batches,
        validation_data=test_data,
        validation_steps=test_batches,
        callbacks=[tf.keras.callbacks.ModelCheckpoint(model_location, monitor='val_loss', save_best_only=True)])
print('training complete!')
print('training time: {:.2f}s'.format(time.time() - start))

print('evaluating model...')
results = model.evaluate(test_data, steps=test_batches, verbose=0)
print('evaluation results:')
for i in range(len(results)):
  print('{}:\t{}'.format(model.metrics_names[i], results[i]))

