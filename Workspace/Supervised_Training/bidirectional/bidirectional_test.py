import tensorflow as tf
from tensorflow import keras
from subprocess import Popen,PIPE
import sys
sys.path.append('..')
from tools.bidirectional import coding_gen
from tools.general import get_n_samples

if(__name__=="__main__"):
  tf.keras.backend.clear_session()
  print("session cleared")

  test_filename = "../celegans/test_set/test_set.txt"
  n_samples = get_n_samples(test_filename)

  ##Model hyper-params
  n_epochs = 1
  bs = 32
  n_steps = n_samples // bs

  #Evaluate model on test data
  model = keras.models.load_model('no_padding_model_more_units.h5')
  handle = open(test_filename,'r')
  print("evaluating model")
  print(model.evaluate_generator(coding_gen(handle, batch_size=bs),steps=n_steps))
  print("model metric names:")
  print(model.metrics_names)
  handle.close()

