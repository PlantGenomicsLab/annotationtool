import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.callbacks import ModelCheckpoint
from subprocess import Popen,PIPE
import sys
sys.path.append('..')
from tools.general import MemoryCallback, get_n_samples, train_model
from tools.bidirectional import *

if(__name__=="__main__"):
  tf.keras.backend.clear_session()
  print("session cleared")

  train_filename = "../celegans/train_set/train_set.txt"
  model = train_model(train_filename, n_epochs=1, lstm_units=64, callbacks=[MemoryCallback(), ModelCheckpoint('bd_no_embed.h5')])
  tf.keras.backend.clear_session()
  quit()

