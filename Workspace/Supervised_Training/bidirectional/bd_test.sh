#!/bin/bash
#SBATCH --job-name=bd_test
#SBATCH -o bd_test.out
#SBATCH -e bd_test.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=150G

echo `hostname`
source ~/.bashrc
cd bidirectional
conda activate tf
python -u bidirectional_test.py > evaluate_bd_more_units.txt
