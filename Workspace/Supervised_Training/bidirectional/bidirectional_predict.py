import tensorflow as tf
from tensorflow import keras
from subprocess import Popen,PIPE
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input
import sys
sys.path.append('..')
from tools.bidirectional import coding_gen
from tools.general import get_n_samples
import numpy as np

def decode_sequence(input_seq, encoder, decoder, max_len=None):
  states_value = encoder.predict(input_seq)
  target_seq = np.zeros((1, 1, 8))
  target_seq[0, 0, 1] = 1.

  decoded_sequence = ''
  while len(decoded_sequence) < input_seq.shape[1]:
    if max_len is not None and (len(decoded_sequence) >= max_len):
      break
    if len(decoded_sequence) % 1000 == 0:
      print("decoding progress: " + str(100. * len(decoded_sequence) / input_seq.shape[1]) + "%")
    output_tokens, h, c = decoder.predict([target_seq] + states_value)
    output_seq = np.argmax(output_tokens[0, -1, :])
    decoded_sequence += str(output_seq)
    target_seq = np.zeros((1, 1, 8))
    target_seq[0, 0, output_seq] = 1.
    states_value = [h, c]
  return decoded_sequence

if(__name__=="__main__"):
  tf.keras.backend.clear_session()
  print("session cleared")

  model = keras.models.load_model('bd_no_embed.h5')
  print("loaded model")
  main_inputs = model.get_layer(name='input_1').input
  enc_state_h = model.get_layer(name='concatenate').output
  enc_state_c = model.get_layer(name='concatenate_1').output
  encoder_states = [enc_state_h, enc_state_c]
  encoder_model = Model(main_inputs, encoder_states)
  print("created encoder")
  
  decoder_state_input_h = Input(shape=(128,), name="decoder_input_h")
  decoder_state_input_c = Input(shape=(128,), name="decoder_input_c")
  decoder_states_inputs = [decoder_state_input_c, decoder_state_input_h]
  decoder = model.get_layer(name='lstm_1')
  decoder_dense = model.get_layer(name='dense')
  decoder_inputs = model.get_layer('input_2').input
  decoder_outputs, state_h, state_c = decoder(decoder_inputs, initial_state=decoder_states_inputs)
  decoder_states = [state_h, state_c]
  decoder_outputs = decoder_dense(decoder_outputs)
  decoder_model = Model([decoder_inputs] + decoder_states_inputs, [decoder_outputs] + decoder_states)
  print("created decoder")
  
  filename = '../celegans/test_set/test_set.txt'
  handle = open(filename, 'r')
  raw_in = handle.readline().strip()
  dna_tokenizer = {"a":1,"A":2,"c":3,"C":4,"g":5,"G":6,"t":7,"T":8,"n":9,"N":10}
  #input_seq = []
  input_seq = np.zeros((1, len(raw_in), 11))
  for i in range(0, len(raw_in)):
    #input_seq.append(dna_tokenizer[i])
    input_seq[0, i, dna_tokenizer[raw_in[i]]] = 1.
  print("starting predictions:")
  prediction = decode_sequence(input_seq, encoder_model, decoder_model)
  print("input: " + raw_in[1450:1600])
  print("prediction:")
  print(prediction[1450:1600])
  print("")
  print(prediction)
  handle.close()
