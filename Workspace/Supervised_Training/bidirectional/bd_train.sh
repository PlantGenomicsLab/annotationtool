#!/bin/bash
#SBATCH --job-name=bd_train
#SBATCH -o bd_train.out
#SBATCH -e bd_train.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=150G

echo `hostname`
source ~/.bashrc
cd bidirectional
conda activate tf
python -u bidirectional_train.py > output_bd_no_embed.txt
