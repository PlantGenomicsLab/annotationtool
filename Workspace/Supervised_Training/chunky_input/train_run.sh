#!/bin/bash
#SBATCH --job-name=chunky_train
#SBATCH -o rnn_chunky_train.out
#SBATCH -e rnn_chunky_train.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mem=10G

echo '' > rnn_chunky_train.out
echo '' > rnn_chunky_train.err
echo `hostname`
source ~/.bashrc
NUM_EPOCHS=300
python -u chunky_train.py --model=model --epochs=$NUM_EPOCHS --layer_size=32 --batch_size=64 --file=new_labels.txt --layers=1
#python -u chunky_train.py --model=smaller_model --epochs=$NUM_EPOCHS --layer_size=32 --batch_size=10 --file=short_labels.txt --layers=1 --no_test
echo final epoch: $NUM_EPOCHS
