from __future__ import print_function
import sys, os

threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
if __name__ == '__main__':
  print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(inter_op_threads)
tf.config.threading.set_intra_op_parallelism_threads(intra_op_threads)
from tensorflow.keras import layers
import numpy as np
import pickle
import time
import resource

class ChunkyModel():
  def __init__(self,
        input_language,
        output_language,
        hidden_size=64,
        n_layers=4,
        batch_size=32,
        conv_args=None,
        optimizer=None,
        type_loss=None,
        length_loss=None,
        trained_epochs=0):
    self.input_language = input_language
    self.output_language = output_language
    self.input_height = self.input_language.size().numpy()
    self.output_height = self.output_language.size().numpy()
    self.hidden_size = hidden_size
    self.n_layers = n_layers
    self.batch_size = batch_size
    self.trained_epochs = trained_epochs
    self.conv_args = conv_args
    if optimizer is None:
      optimizer = tf.keras.optimizers.Adam()
    self.optimizer = optimizer
    if type_loss is None:
      type_loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True, reduction='none')
    self.type_loss = type_loss
    if length_loss is None:
      length_loss = tf.keras.losses.MeanAbsoluteError(reduction='none')
    self.length_loss = length_loss
    self.encoder = ChunkyEncoder(self.input_height,
                                self.hidden_size,
                                self.n_layers,
                                self.batch_size,
                                self.conv_args)
    self.decoder = ChunkyDecoder(self.output_height,
                                self.hidden_size,
                                self.n_layers,
                                self.batch_size)

  def get_config(self):
    return {
      'hidden_size': self.hidden_size,
      'n_layers': self.n_layers,
      'batch_size': self.batch_size,
      'trained_epochs': self.trained_epochs,
      'conv_args': self.conv_args,
    }

  def from_config(cls, input_language, output_language, config):
    return cls(input_language=input_language, output_language=output_language, **config)

  def save(self, save_dir):
    if not os.path.isdir(save_dir):
      os.mkdir(save_dir)
    self.encoder.save_weights(save_dir + '/encoder.h5')
    self.decoder.save_weights(save_dir + '/decoder.h5')
    opt_serial = tf.keras.optimizers.serialize(self.optimizer)
    with open(save_dir + '/optimizer.pkl', 'wb+') as out:
      pickle.dump(opt_serial, out, pickle.HIGHEST_PROTOCOL)
    type_loss = self.type_loss.get_config()
    with open(save_dir + '/type_loss.pkl', 'wb+') as out:
      pickle.dump(type_loss, out, pickle.HIGHEST_PROTOCOL)
    length_loss = self.length_loss.get_config()
    with open(save_dir + '/length_loss.pkl', 'wb+') as out:
      pickle.dump(length_loss, out, pickle.HIGHEST_PROTOCOL)
    config = self.get_config()
    with open(save_dir + '/config.pkl', 'wb+') as out:
      pickle.dump(config, out, pickle.HIGHEST_PROTOCOL)

  def train(self, data, epochs=1, checkpoint=None):
    data = data.cache('ds/samples')
    start = time.time()
    for epoch in range(epochs):
      epoch_start = time.time()
      total_loss = 0
      batch = 1
      print('Epoch {:>3}...'.format(epoch + 1))
      for (inp, targ) in data:
        batch_start = time.time()
        batch_loss, t_loss, l_loss = self.train_step(inp, targ)
        total_loss += batch_loss
        print('Batch {:>3} Loss {:>10.4f} ({:>8.4f} type, {:>8.6f} length) Time {:>8.2f}s Mem {:>5}MB'.format(batch, batch_loss.numpy(), t_loss.numpy(), l_loss.numpy(), time.time() - batch_start, resource.getrusage(resource.RUSAGE_SELF).ru_maxrss // 1024))
        batch += 1
      print('Average Loss {:.2f} Time {:>6.2f}s\n'.format(total_loss / (batch - 1), time.time() - epoch_start))
      self.trained_epochs += 1
      if checkpoint is not None and (((epoch + 1) % checkpoint) == 0):
        self.save('checkpoint')
    print('Final Loss {:.4f} Total Time {:.2f}s Average Time per Epoch {:.2f}s'.format(total_loss, time.time() - start, (time.time() - start) / epochs))

  @tf.function(input_signature=(tf.TensorSpec(shape=[None,None], dtype=tf.int32), tf.TensorSpec(shape=[None,None,None], dtype=tf.float32),))
  def train_step(self, inp, targ):
    inp = tf.reshape(inp, [self.batch_size, -1])
    targ = tf.reshape(targ, [2, self.batch_size, -1])
    print("tracing with input shape", inp.shape, "type", inp.dtype, "and target shape", targ.shape, "type", targ.dtype)
    loss = tf.constant(0, tf.float32)
    types_loss_total = tf.constant(0, tf.float32)
    length_loss_total = tf.constant(0, tf.float32)
    with tf.GradientTape() as tape:
      enc_output, enc_hidden = self.encoder(inp)
      dec_hidden = [enc_hidden, enc_hidden]
      dec_input = tf.expand_dims(targ[:, :, 0], 2)
      for t in tf.range(1, tf.shape(targ)[2] - 1):
        predictions, dec_hidden, _ = self.decoder(dec_input, dec_hidden, enc_output)
        types_loss = self.loss_function(targ[0, :, t], predictions[0], self.type_loss)
        length_loss = self.loss_function(targ[1, :, t], predictions[1], self.length_loss)
        types_loss_total += types_loss
        length_loss_total += length_loss
        loss += types_loss
        loss += length_loss
        dec_input = tf.expand_dims(targ[:, :, t], 2)
    variables = self.encoder.trainable_variables + self.decoder.trainable_variables
    gradients = tape.gradient(loss, variables)
    self.optimizer.apply_gradients(zip(gradients, variables))
    print("tracing complete!")
    return loss, types_loss_total, length_loss_total

  def predict(self, inp, max_len=1000):
    ended = [False] * self.batch_size
    result = []
    enc_output, enc_hidden = self.encoder(inp)
    dec_hidden = [enc_hidden, enc_hidden]
    dec_input = tf.expand_dims(tf.concat([[[self.output_language.lookup(tf.constant('<start>'))],[0]]] * self.batch_size, axis=1), 2)
    for _ in range(1, max_len):
      predictions, dec_hidden, attention_weights = self.decoder(dec_input, dec_hidden, enc_output)
      predicted_type = tf.argmax(predictions[0], axis=1).numpy()
      predicted_length = predictions[1].numpy()
      for i in range(len(predicted_type)):
        if ended[i]:
          predicted_type[i] = 0
          predicted_length[i, 0] = 0
      result.append({'type':predicted_type,'length':predicted_length[:, 0]})
      for i in range(len(predicted_type)):
        if predicted_type[i] == 10:
          ended[i] = True
      dec_input = tf.concat([[tf.cast(tf.expand_dims(predicted_type, 1), tf.float32)], [predicted_length]], axis=0)
    return result

  def loss_function(self, real, pred, obj):
    mask = tf.math.logical_not(tf.math.equal(real, 0))
    loss_ = obj(real, pred)
    mask = tf.cast(mask, dtype=loss_.dtype)
    loss_ *= mask
    return tf.reduce_mean(loss_)

  def summary(self):
    self.encoder.summary()
    self.decoder.summary()

  def set_opt(self, optimizer):
    self.optimizer = tf.keras.optimizers.deserialize(optimizer)

  def set_losses(self, type_loss, length_loss):
    self.type_loss = type_loss
    self.length_loss = length_loss

  def load_weights(self, save_dir):
    inp = tf.zeros((self.batch_size, 100))
    enc_out, enc_hid = self.encoder(inp)
    dec_hidden = [enc_hid, enc_hid]
    starts = tf.cast([[self.output_language.lookup(tf.constant('<start>'))], [0]], tf.float32)
    dec_input = tf.expand_dims(tf.broadcast_to(starts, (2,self.batch_size)),2)
    self.decoder(dec_input, dec_hidden, enc_out)
    self.encoder.load_weights(save_dir + '/encoder.h5', by_name=True)
    self.decoder.load_weights(save_dir + '/decoder.h5', by_name=True)

def load_chunky(input_language, output_language, save_dir):
  with open(save_dir + '/config.pkl', 'rb') as inp:
    config = pickle.load(inp)
  loaded = ChunkyModel(input_language, output_language, **config)
  with open(save_dir + '/optimizer.pkl', 'rb') as inp:
    optimizer = pickle.load(inp)
  loaded.set_opt(optimizer)
  with open(save_dir + '/type_loss.pkl', 'rb') as inp:
    type_loss = pickle.load(inp)
  with open(save_dir + '/length_loss.pkl', 'rb') as inp:
    length_loss = pickle.load(inp)
  loaded.set_losses(type_loss, length_loss)
  loaded.load_weights(save_dir)
  return loaded

class ChunkyEncoder(tf.keras.Model):
  def __init__(self, input_height, hidden_size, n_layers, batch_size, conv_args=None):
    super(ChunkyEncoder, self).__init__()
    self.input_height = input_height
    self.batch_size = batch_size
    self.hidden_size = hidden_size // 2
    self.n_layers = n_layers
    if conv_args is None:
      conv_args = [[50, 30, 10, 3], [20, 10, 2, 2]]
    self.conv_args = conv_args
    self.conv1 = layers.Conv1D(self.conv_args[0][0],
                               self.conv_args[0][1],
                               self.conv_args[0][2],
                               padding='same',
                               activation='relu',
                               name='enc_conv1')
    self.conv2 = layers.Conv1D(self.conv_args[1][0],
                               self.conv_args[1][1],
                               self.conv_args[1][2],
                               padding='same',
                               activation='relu',
                               name='enc_conv2')
    self.pool1 = layers.MaxPool1D(self.conv_args[0][3], name='enc_pool1')
    self.pool2 = layers.MaxPool1D(self.conv_args[1][3], name='enc_pool2')
    self.enc_layers = []
    for i in range(self.n_layers):
      self.enc_layers.append(layers.LSTM(self.hidden_size * 2, return_sequences=True, return_state=True, recurrent_initializer='glorot_uniform', name='enc_lstm_' + str(i)))
    self.bd = layers.Bidirectional(layers.LSTM(self.hidden_size, return_sequences=True, return_state=True, recurrent_initializer='glorot_uniform', name='enc_bd'))
  
  def call(self, x, hidden=None):
    if hidden is None:
      hidden = self.initialize_hidden_state()
    x = tf.cast(x, tf.int32)
    x = tf.one_hot(x, self.input_height, axis=-1, dtype=tf.float32)
    x = self.conv1(x)
    x = self.pool1(x)
    x = self.conv2(x)
    x = self.pool2(x)
    x, f_h, f_c, b_h, b_c = self.bd(x, initial_state=hidden)
    hidden = [layers.Concatenate()([f_h, b_h]), layers.Concatenate()([f_c, b_c])]
    for i in range(len(self.enc_layers)):
      x, h, c = self.enc_layers[i](x, initial_state=hidden)
      hidden = [h, c]
    return x, [h, c]

  def initialize_hidden_state(self):
    shape = (self.batch_size, self.hidden_size)
    return [tf.zeros(shape), tf.zeros(shape), tf.zeros(shape), tf.zeros(shape)]

class ChunkyAttention(layers.Layer):
  def __init__(self, units, name=''):
    super(ChunkyAttention, self).__init__()
    self.W1 = layers.Dense(units, name=name + '_att_w1')
    self.W2 = layers.Dense(units, name=name + '_att_w2')
    self.V = layers.Dense(1, name=name + '_att_v')
  
  def call(self, query, values):
    hide_with_time = tf.expand_dims(query, 1)
    score = self.V(tf.nn.tanh(self.W1(values) + self.W2(hide_with_time)))
    attention_weights = tf.nn.softmax(score, axis=1)
    context_vector = attention_weights * values
    context_vector = tf.reduce_sum(context_vector, axis=1)
    return context_vector, attention_weights

class ChunkyDecoder(tf.keras.Model):
  def __init__(self, output_height, hidden_size, n_layers, batch_size):
    super(ChunkyDecoder, self).__init__()
    self.output_height = output_height
    self.batch_size = batch_size
    self.hidden_size = hidden_size
    self.n_layers = n_layers
    self.type_layers = []
    self.length_layers = []
    for i in range(self.n_layers):
      self.type_layers.append(layers.LSTM(self.hidden_size, return_sequences=True, return_state=True, recurrent_initializer='glorot_uniform', name='type_lstm_' + str(i)))
      self.length_layers.append(layers.LSTM(self.hidden_size, return_sequences=True, return_state=True, recurrent_initializer='glorot_uniform', name='length_lstm_' + str(i)))
    self.types_fc = layers.Dense(output_height, name='type_fc')
    self.length_fc = layers.Dense(1, name='length_fc')
    self.types_att = ChunkyAttention(self.hidden_size, name='type')
    self.length_att = ChunkyAttention(self.hidden_size, name='length')

  def call(self, x, hidden, encoder_output):
    type_hidden, length_hidden = hidden
    types_x = x[0]
    length_x = x[1]
    types_output, types_state, types_attention = self.types_call(types_x, type_hidden, encoder_output)
    length_output, length_state, length_attention = self.length_call(length_x, length_hidden, encoder_output)
    return [types_output, length_output], [types_state, length_state], [types_attention, length_attention]

  def types_call(self, x, hidden, encoder_output):
    att_query = layers.Concatenate()(hidden)
    context_vector, attention_weights = self.types_att(att_query, encoder_output)
    x = tf.cast(x, tf.int32)
    x = tf.one_hot(x, self.output_height, axis=-1, dtype=tf.float32)
    x = tf.concat([tf.expand_dims(context_vector, 1), x], axis=-1)
    state = None
    for i in range(len(self.type_layers)):
      x, h, c = self.type_layers[i](x, initial_state=state)
      state = [h, c]
    out = tf.reshape(x, (-1, x.shape[2]))
    out = self.types_fc(out)
    return out, [h, c], attention_weights

  def length_call(self, x, hidden, encoder_output):
    att_query = layers.Concatenate()(hidden)
    context_vector, attention_weights = self.length_att(att_query, encoder_output)
    x = tf.expand_dims(tf.cast(x, tf.float32),2)
    x = tf.concat([tf.expand_dims(context_vector, 1), x], axis=-1)
    state = None
    for i in range(len(self.length_layers)):
      x, h, c = self.length_layers[i](x, initial_state=state)
      state = [h, c]
    out = tf.reshape(x, (-1, x.shape[2]))
    out = self.length_fc(out)
    return out, [h, c], attention_weights
