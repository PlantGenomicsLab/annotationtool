from __future__ import print_function
import sys, os

threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(inter_op_threads)
tf.config.threading.set_intra_op_parallelism_threads(intra_op_threads)

from tensorflow.keras.models import Sequential, Model, load_model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.backend import clear_session
from tensorflow.keras import layers
from custom_models import ChunkyModel
from dataset import get_samples, get_languages
import numpy as np
from subprocess import Popen, PIPE
import math
from tensorflow.keras.callbacks import Callback
import resource
import time

clear_session()
print('session cleared')

args = sys.argv[1:]

model_location = None
train_path = "/labs/Wegrzyn/annotationtool/Workspace/Supervised_Training/chunky_input/new_labels.txt"
batch_size = 32
n_layers = 4
hidden_size = 128
epochs = 10
new_model = True
use_test = True
help_opt = False
n_steps = None
init_epoch = 0

for arg in args:
  if arg[:8] == "--model=":
    model_location = arg[8:]
  if arg[:7] == "--file=":
    train_path = arg[7:]
  if arg[:13] == "--batch_size=":
    batch_size = int(arg[13:])
  if arg[:8] == "--steps=":
    n_steps = int(arg[8:])
  if arg[:9] == "--layers=":
    n_layers = int(arg[9:])
  if arg[:13] == "--layer_size=":
    hidden_size = int(arg[13:])
  if arg[:9] == "--epochs=":
    epochs = int(arg[9:])
  if arg[:13] == "--init_epoch=":
    init_epoch = int(arg[13:])
  if arg == "--no_test":
    use_test = False
  if arg == "--continue":
    new_model = False
  if arg == "-h":
    help_opt = True

if help_opt:
  print('Usage: python chunky_train.py --model=<model file>')
  print('       [--file=<train data file>]')
  print('       [--batch_size=<batch size>]')
  print('       [--steps=<number of batches per epoch>]')
  print('       [--layers=<number of hidden layers>]')
  print('       [--layer_size=<units per layer>]')
  print('       [--epochs=<number of epochs>]')
  print('       [--init_epoch=<epoch to start with>]')
  print('       [--no_test]')
  print('       [--continue]')
  print('       [-h]')
  quit()

if model_location is None:
  print("please specify a model location: --model=<model file>")
  quit()

print('Gathering data...')
sys.stdout.flush()

# get longest line for padding
cmdstr = "wc -l %s" % train_path
proc = Popen(cmdstr.split(),stdout=PIPE,stdin=PIPE)
stdout,stdin = proc.communicate()
n_lines=int(stdout.split()[0])

n_samples = n_lines // 3
if n_steps is None:
  n_steps = n_samples // batch_size

if use_test:
  test_batches = n_steps // 10
  if test_batches == 0 and n_steps > 1:
    test_batches = 1
else:
  test_batches = 0
train_batches = n_steps - test_batches

print('number of samples: ' + str(n_samples))
print('Batch size: ' + str(batch_size))
print('Batches per training epoch: ' + str(train_batches))
print('Planned number of epochs: ' + str(epochs - init_epoch))
print('Hidden layer size: ' + str(hidden_size))
print('Number of hidden layers: ' + str(n_layers))

input_language, output_language = get_languages()

model = ChunkyModel(input_language,
                    output_language,
                    hidden_size=hidden_size,
                    n_layers=n_layers,
                    batch_size=batch_size)

samples = get_samples(train_path, input_language, output_language, batch_size)
train_samples = samples.take(train_batches)
test_samples = samples.skip(train_batches)

print('starting training...')
model.train(train_samples, epochs, checkpoint=10)
model.save(model_location)
model.encoder.summary()
model.decoder.summary()
print('training complete!')

