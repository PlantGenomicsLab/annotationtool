from __future__ import print_function
import sys, os

threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
if __name__ == '__main__':
  print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(inter_op_threads)
tf.config.threading.set_intra_op_parallelism_threads(intra_op_threads)

def get_languages():
  base_keys = tf.constant(['0', 'A', 'a', 'C', 'c', 'G', 'g', 'T', 't', 'N', 'n'])
  base_vals = tf.constant([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], dtype=tf.int32)
  input_language = tf.lookup.StaticHashTable(
    tf.lookup.KeyValueTensorInitializer(base_keys, base_vals), 0)
  
  type_keys = tf.constant(['0', '1', '2', '3', '4', '5', '6', '7', '8', '<start>', '<end>'])
  type_vals = tf.constant([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], dtype=tf.float32)
  output_language = tf.lookup.StaticHashTable(
    tf.lookup.KeyValueTensorInitializer(type_keys, type_vals), 0)
  return input_language, output_language

def get_samples(path, inp_lang, out_lang, batch_size):
  def is_label(line):
    return tf.strings.regex_full_match(line, ".*\|.*")
  
  def base_map(line):
    return tf.map_fn(lambda x: inp_lang.lookup(x), tf.strings.bytes_split(line), dtype=tf.int32)
  
  def label_items_map(items):
    items = tf.strings.split(items, ':')
    return tf.concat([[out_lang.lookup(items[0])], [tf.strings.to_number(items[1], tf.float32)]], axis=0)
  
  def label_map(line):
    items = tf.strings.split(line, '|')
    result = tf.concat([starts, tf.map_fn(label_items_map, items, dtype=tf.float32), ends], axis=0)
    types = tf.expand_dims(result[:, 0], -1)
    lengths = result[:, 1]
    tot = tf.broadcast_to(tf.expand_dims(tf.reduce_sum(lengths, axis=0), 0), tf.shape(lengths))
    lengths = tf.expand_dims(lengths / tot, -1)
    return tf.concat([types, lengths], axis=-1)
  
  def label_split(batch):
    return tf.concat([[batch[:,:,0]], [batch[:,:,1]]], axis=0)
  
  def is_bases(line):
    return (not is_label(line)) and tf.cast(tf.strings.length(line), tf.bool)

  starts = tf.cast([[out_lang.lookup(tf.constant('<start>')), 0]], tf.float32)
  ends = tf.cast([[out_lang.lookup(tf.constant('<end>')), 0]], tf.float32)

  ds = tf.data.TextLineDataset(path)
  labels = ds.filter(is_label).map(label_map)
  labels = labels.padded_batch(batch_size, padded_shapes=(None,2))
  labels = labels.map(label_split)
  bases = ds.filter(is_bases).map(base_map)
  bases = bases.padded_batch(batch_size, padded_shapes=(None,), drop_remainder=True)
  return tf.data.Dataset.zip((bases, labels))

