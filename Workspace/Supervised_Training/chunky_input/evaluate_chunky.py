from __future__ import print_function
import sys, os

threads = int(os.getenv('SLURM_CPUS_PER_TASK',1))
print("using %d threads" % threads)

inter_op_threads = 1

assert threads % inter_op_threads == 0

intra_op_threads = threads // inter_op_threads
os.environ['OMP_NUM_THREADS'] = str(intra_op_threads)

import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(inter_op_threads)
tf.config.threading.set_intra_op_parallelism_threads(intra_op_threads)

from tensorflow.keras.models import Sequential, Model, load_model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.backend import clear_session
from tensorflow.keras import layers
from custom_models import ChunkyModel, load_chunky
from dataset import get_languages, get_samples
import numpy as np
from subprocess import Popen, PIPE
import math
from tensorflow.keras.callbacks import Callback
import resource
import time

clear_session()
print('session cleared')

input_language, output_language = get_languages()

model_dir = 'model'

model = load_chunky(input_language, output_language, model_dir)
print('model loaded successfully!')
model.summary()

test_path = 'short_labels.txt'
batch_size = model.batch_size

samples = get_samples(test_path, input_language, output_language, batch_size)
max_len = 100
n_steps = 2

for (inp, _) in samples.take(n_steps):
  result = model.predict(inp, max_len)
  for item in result:
    print('Type: {} Length: {}'.format(item['type'], item['length']))

print('predictions complete')
