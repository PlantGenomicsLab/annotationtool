## Chunky Input LSTM

In this implementation, we are trying to process input as a list of label:length items. This should greatly reduce the size of our output/labels. We think that the most prominent issue with this approach will be finding a loss function that accurately evaluates our predictions/labels.

The structure of this model is based off of the model featured in [this guide](https://www.tensorflow.org/tutorials/text/nmt_with_attention)

