#!/bin/bash
#SBATCH --job-name=train_aug
#SBATCH -o %j.out
#SBATCH -e %j.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=30G

source /home/CAM/jbennett/miniconda2/etc/profile.d/conda.sh
conda activate aug

genome=drosophila.fasta
stringtie_gff=stringtie.gff 
species=easel_drosophila

# create template parameter files
#new_species.pl --species=$species

# create busco training set
#buscodir=/labs/Wegrzyn/annotationtool/testSpecies/model/drosophila/scripts_used/run_busco_o/augustus_output/gffs
#outfile=busco_cat.gff
#touch $outfile
#for i in $buscodir/*.gff
#do
#  cat $i >> $outfile
#done

#to generate gb file from gff, gff must contain only 1 transcript per gene to avoid overlapping gb file
avg_len=2882

training_genes=busco_stringtie.gff

#only columns with CDS are translated into gb format
gff2gbSmallDNA.pl $training_genes $genome $avg_len stringtie_genes.gb
etraining --species=$species stringtie_genes.gb > training.out

#find genes that lead to errors
etraining --species=$species stringtie_genes.gb 2>&1 | grep "in sequence" | perl -pe 's/.*n sequence (\S+):.*/$1/' | sort -u > bad.lst


#filter genes that lead to errors
filtered_gb=stringtie_genes.f.gb
filterGenes.pl bad.lst stringtie_genes.gb > $filtered_gb

#Create holdout test set
#randomSplit.pl $filtered_gb 600
#mv ${filtered_gb}.test test.gb
#mv ${filtered_gb}.train train.gb

etraining --species=$species $filtered_gb > etrain.out

#results
augustus --species=$species $genome > augustus_retrained.out

#optimize meta-params
#$scripts/optimize_augustus.pl --species=new --kfold=8 --cpus=8 train.gb > optimize.out
