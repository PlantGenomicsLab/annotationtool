#!/bin/bash
#SBATCH --job-name=entap_filter
#SBATCH -o %j.out
#SBATCH -e %j.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=40G

## get .aa file from augustus prediction
source /home/CAM/jbennett/miniconda2/etc/profile.d/conda.sh
conda activate aug

aug=../augustus_output/augustus_nohints.gff
#getAnnoFasta.pl $aug

output=augustus_nohints.aa

module load anaconda/4.4.0
module load perl/5.28.1
module load diamond/0.9.19
module load eggnog-mapper/0.99.1
module load interproscan/5.25-64.0

outdir=$PWD

echo $outdir

/labs/Wegrzyn/EnTAP/EnTAP --runP -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -i $output --out-dir $outdir --taxon arthropoda --threads 32
