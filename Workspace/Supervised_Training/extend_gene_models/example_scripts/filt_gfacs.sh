#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH -o gfacs-%j.o
#SBATCH -e gfacs-%j.e

module load perl

genome="../../drosophila.fasta"
alignment="../augustus_output/augustus_hints.gff"
script="/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f braker_2.1.2_gtf \
--fasta "$genome" \
--statistics \
--statistics-at-every-step \
--get-protein-fasta \
--unique-genes-only \
--canonical-only \
--min-intron-size 9 \
--min-exon-size 11 \
--create-gtf \
-O hints_gfacs_filtered \
"$alignment"
