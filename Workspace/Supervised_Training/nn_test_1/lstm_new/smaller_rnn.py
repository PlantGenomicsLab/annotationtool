import sys
from pympler import asizeof
from keras.models import Sequential
from keras import layers
import numpy as np
from subprocess import Popen, PIPE
import itertools

class CharacterTable(object):
  """
  general onehot class
  """

  def __init__(self, chars):
    self.chars = sorted(set(chars))
    self.char_indices = dict((c,i) for i,c in enumerate(self.chars))
    self.indices_char = dict((i,c) for i,c in enumerate(self.chars))

  def encode(self, C, num_rows):
    x = np.zeros((num_rows,len(self.chars)))
    for i,c in enumerate(C):
      x[i,self.char_indices[c]] = 1
    return x

  def decode(self, x, calc_argmax=True):
    if calc_argmax:
      x = x.argmax(axis=-1)
    return ''.join(self.indices_char[x] for x in x)

class kmer_encoding:

  def __init__(self,alphabet,kmer_length):
    self.alphabet = alphabet
    self.kmers = [''.join(i) for i in itertools.product(alphabet, repeat = kmer_length)]
    self.kmer_length = kmer_length
    self.kmer_indices = dict((c,i) for i,c in enumerate(self.kmers))
    self.indices_kmer = dict((i,c) for i,c in enumerate(self.kmers))
    
  def encode(self,sequence,num_rows):
    assert(len(sequence)%self.kmer_length==0)
    x = np.zeros((num_rows,len(self.kmers)))
    for i in range(0,len(x)):
      temp = i*self.kmer_length
      slice = sequence[temp:temp+self.kmer_length]
      x[i,self.kmer_indices[slice]] = 1
    return x

  def decode(self,x,calc_argmax=True):
    if calc_argmax:
      x = x.argmax(axis=-1)
    return ''.join(self.indices_kmer[x] for x in x)


# Import and convert data to onehot

print('Gathering data...')
sys.stdout.flush()

KMER_LEN=3

chars_input = 'ACGTN '
ctable_input = kmer_encoding(chars_input,KMER_LEN)

chars_output = '12345678 '
ctable_output = kmer_encoding(chars_output,KMER_LEN)

dataset_path = "/labs/Wegrzyn/annotationtool/Workspace/Supervised_Training/celegans/Celegans_Labels_New.txt"

questions = []
expected = []

# get longest line for padding
cmdstr = "wc -L %s" % dataset_path
proc = Popen(cmdstr.split(),stdout=PIPE,stdin=PIPE)
stdout,stdin = proc.communicate()
MAXLEN=round(int(stdout.split()[0])/KMER_LEN)*KMER_LEN
print("got longest line: " + str(MAXLEN))

with open(dataset_path,'r') as readf:
  temp=0
  while(True):
    bases = readf.readline().strip()
    if(not(bases)):
      print("total lines read = %d" % temp)
      break
    temp+=1
    labels = readf.readline().strip()
    readf.readline()

    bases_padded = bases.upper()+' '*(MAXLEN-len(bases))
    labels_padded  = labels+' '*(MAXLEN-len(labels))
    questions.append(bases_padded)
    expected.append(labels_padded)

print('Total seqs to annotate:',len(questions))

print('Vectorization...')
arr_size = MAXLEN/KMER_LEN
x = np.zeros((len(questions),arr_size,len(chars_input)),dtype=np.bool)
y = np.zeros((len(expected),arr_size,len(chars_output)),dtype=np.bool)
for i,seq in enumerate(questions):
  x[i] = ctable_input.encode(seq,arr_size)
for i,seq in enumerate(expected):
  y[i] = ctable_output.encode(seq,arr_size)

split_at = len(x)-len(x) //10
(x_train, x_val) = x[:split_at], x[split_at:]
(y_train, y_val) = y[:split_at], y[split_at:]

print('Size of train_dataset = %d' % asizeof.asizeof(x_train))

print('Training Data:')                                                       
print(x_train.shape)                                                         
print(y_train.shape)                                                        
                                                                           
print('Validation Data:')                                                 
print(x_val.shape)                                                       
print(y_val.shape) 

RNN = layers.LSTM                                                       
HIDDEN_SIZE = 128                                                     
BATCH_SIZE = 128                                                   
LAYERS = 1                                                           
                                                                    
print('Build model...')                                            
model = Sequential() 
model.add(RNN(HIDDEN_SIZE,input_shape=(MAXLEN,len(chars_input))))
model.add(layers.RepeatVector(MAXLEN))
for _ in range(LAYERS):
  model.add(RNN(HIDDEN_SIZE,return_sequences=True))
model.add(layers.TimeDistributed(layers.Dense(len(chars_output),activation='softmax')))
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
model.summary()

for iteration in range(1, 20):
    print()
    print('-' * 50)
    print('Iteration', iteration)
    model.fit(x_train, y_train,
              batch_size=BATCH_SIZE,
              epochs=1,
              validation_data=(x_val, y_val))

model.evaluate(x_val,y_val)
