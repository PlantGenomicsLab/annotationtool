import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import LSTM,Dense,Input
from tensorflow.keras.preprocessing.text import Tokenizer
import numpy as np
import sys
from subprocess import Popen,PIPE

def coding_gen(handle, vector_len, batch_size=32):
  
  dna_tokenizer = {"a":1,"A":2,"c":3,"C":4,"g":5,"G":6,"t":7,"T":8,"n":9,"N":10}
  while (True):
    encoder_input_data = np.zeros((batch_size, vector_len, 11), dtype='float32')
    decoder_input_data = np.zeros((batch_size, vector_len, 8), dtype='float32')
    decoder_target_data = np.zeros((batch_size, vector_len, 8), dtype='float32')
    for i in range(batch_size):
      dna_line = handle.readline().strip()
      if (not(dna_line)):
        handle.seek(0)
        dna_line = handle.readline().strip()
      label_line = handle.readline().strip()
      handle.readline()
      for j in range(vector_len):
        if j >= len(dna_line):
          encoder_input_data[i, j, 0] = 1.
          decoder_input_data[i, j, 0] = 1.
          if j > 0:
            decoder_target_data[i, j - 1, 0] = 1.
        else:
          encoder_input_data[i, j, dna_tokenizer[dna_line[j]]] = 1.
          decoder_input_data[i, j, int(label_line[j])] = 1.
          if j > 0:
            decoder_target_data[i, j - 1, int(label_line[j])] = 1.
    yield([encoder_input_data, decoder_input_data], decoder_target_data)

if(__name__=="__main__"):
  filename = "BUSCO_Input_Data.txt"

  #Find length of longest line in file so we know how long our onehot vectors need to be
  cmdstr = "wc -L %s" % filename
  proc = Popen(cmdstr.split(),stdout=PIPE,stdin=PIPE)
  stdout,stdin = proc.communicate()
  longest_line=int(stdout.split()[0])
  print("got longest line: " + str(longest_line))

  #Model hyper-params
  n_samples = 3654
  n_epochs = 1
  bs = 4
  n_steps = n_samples // bs
  
  encoder_inputs = Input(shape=(None, 11))
  encoder = LSTM(32, return_state=True)
  encoder_outputs, state_h, state_c = encoder(encoder_inputs)
  encoder_states = [state_h, state_c]

  decoder_inputs = Input(shape=(None, 8))
  decoder_lstm = LSTM(32, return_sequences=True, return_state=True)
  decoder_outputs, _, _ = decoder_lstm(decoder_inputs, initial_state=encoder_states)
  decoder_dense = Dense(8, activation='softmax')
  decoder_outputs = decoder_dense(decoder_outputs)

  train_filename = "train_set/train_set.txt"
  handle = open(train_filename,'r')

  model = Model([encoder_inputs, decoder_inputs], decoder_outputs)
  model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
  model.summary()
  print("model compiled")
  model.fit_generator(coding_gen(handle, longest_line, batch_size=bs), epochs=n_epochs, steps_per_epoch=n_steps, max_queue_size=2)
  print("model trained")
  model.save('model.h5')
  handle.close()
  quit()

  #Evaluate model on test data
  test_filename = "test_set/test_set.txt"
  handle = open(test_filename,'r')
  test_gen = minibatch_gen(handle, longest_line, bs)
  model.evaluate_generator(test_gen,steps=115)

