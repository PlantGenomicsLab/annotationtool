## Weekly Progress

### 9/26

Completed:
* Model now predicts "genic regions", sequence of introns and exons
  * [model_output](Supervised_Training/nn_test_1/temp/tf_hinge_predictions2.txt)
* Model uses bidirectional lstm and a masking layer to avoid predicting padding
  * [model_architecture](Supervised_Training/nn_test_1/temp/rnn_predict.py)
* alignment of celegans_buscoprots -> genome using BLAT and then exonerate to refine underway
  * [blat](Supervised_Training/check_alignments/uniprot/blat_elegans_uniprot.sh)
  * [exonerate](Supervised_Training/check_alignments/exonerate_elegans_busco.sh)

To-Do:
* Implement KMER encoding of input
  * 20, 40-mers may hold too much memory?
  * 20-mer = alphabet size of 1E12, not possible with one-hot?
* Usearch + unique representation of 60% of aligned busco proteins to genome
