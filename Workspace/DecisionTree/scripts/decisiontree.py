#!/bin/python

import numpy as np
from sklearn import tree
import graphviz

feature_names = ["GC Content","Kozak-4","Kozak-3","Kozak-2","Kozak-1","Kozak0","Kozak1","Kozak2","Kozak3","SpliceScore","RepeatContent"]

train_features=[]
train_labels = []

test_features = []
test_labels = []

train_features = open("train_features.txt",'r').readlines()
train_labels = open("train_labels.txt",'r').readlines()

test_features = open("test_features.txt",'r').readlines()
test_labels = open("test_labels.txt",'r').readlines()

for i,e in enumerate(train_features):
  train_features[i] = e.split("\t")[:-1]

for i,e in enumerate(test_features):
  test_features[i] = e.split("\t")[:-1]

test_labels = [i.strip() for i in test_labels]
train_labels = [i.strip() for i in train_labels]
  
#train_features = np.array(train_features)
#train_labels = np.array(train_labels).reshape(-1,1)

#test_features = np.array(test_features)
#test_labels = np.array(test_labels).reshape(-1,1)

clf = tree.DecisionTreeClassifier(max_depth=6,min_samples_leaf=200)
clf.fit(train_features,train_labels)

### Visualize trained DTC
dot_data = tree.export_graphviz(clf,out_file=None,feature_names=feature_names,filled=True,rounded=True)
graph = graphviz.Source(dot_data)
graph.render("dtc_maxdepth6_minsamples200")

### Predict

#train set

print("Train set:")
predict = clf.predict(train_features)

TP=0
TN=0
FP=0
FN=0

for i in range(len(predict)):
  if(predict[i]==train_labels[i]):
    if(predict[i]=='1'):
      TP+=1
    else:
      TN+=1
  else:
    if(predict[i]=='1'):
      FP+=1
    else:
      FN+=1

print("TP = %d, TN = %d, FP = %d, FN = %d" % (TP, TN, FP, FN))
print("Sensitivity = %0.3f, Specificity = %0.3f" % (TP/(TP+FN),TN/(TN+FP)))

#test set
print("Test set:")
predict = clf.predict(test_features)

TP=0
TN=0
FP=0
FN=0

for i in range(len(predict)):
  if(predict[i]==test_labels[i]):
    if(predict[i]=='1'):
      TP+=1
    else:
      TN+=1
  else:
    if(predict[i]=='1'):
      FP+=1
    else:
      FN+=1

print("TP = %d, TN = %d, FP = %d, FN = %d" % (TP, TN, FP, FN))
print("Sensitivity = %0.3f, Specificity = %0.3f" % (TP/(TP+FN),TN/(TN+FP)))
