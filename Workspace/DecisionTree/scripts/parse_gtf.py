#!/bin/python

import re
import sys

class Gene:
  def __init__(self):
    self.source = ""
    self.id = ""
    self.transcripts = []
    self.chrom = ""
    self.linenum=0
  def __len__(self):
    return 0

class Transcript:
  def __init__(self):
    self.source = ""
    self.id = ""
    self.strand = ""
    self.cds_list = []
    self.frame_list = []
    self.valid = 0
    self.chrom = ""
    self.biotype = ""
  def __len__(self): #calling len(Transcript) returns in bp and also sets valid bit
    totallen = 0
    for i in self.cds_list:
      curlen = (i[1]-i[0])+1
      totallen+=curlen
    if((totallen%3)==0):
      self.valid=1
    return totallen


def parse_gene(file_handle,line_offset=0): #must be the line number where feature=="gene"
  #Chrom  Source  Feature  Start  Stop  Score  Strand  Frame  Attribute
  regstr = "[\t]".join("(\w+) (\w+) (\w+) (\d+) (\d+) [0-9.]+ ([+-.]) ([012.]) (.*)\n".split())
  reg = re.compile(regstr)
  gene = Gene()
  transcripts = []
  cds_list = []
  if(line_offset):
    for i in range(line_offset-1):
      file_handle.readline()
  startpos = file_handle.tell()
  flag=0
  noncoding_flag=0

  while(True):
    curpos=file_handle.tell()
    line = file_handle.readline()
    if(line==""): #end of file
      gene.transcripts = transcripts
      cur_t.cds_list = cds_list
      cur_t.frame_list = frame_list 
      return (gene,1)
    try:
      matchlist = reg.match(line).groups()
    except AttributeError:
      print("Failed to match line:")
      print(line)
      return 
    chrom,source,feature,start,stop,strand,frame,attrs = matchlist
    if(curpos == startpos):
      if(feature!="gene"):
        print("Called parse_gene on a line that is not the start of a gene")
        print("Line tried to parse:")
        print(line)
        return
    if(feature=="gene"): #end of gene
      if(flag==1): #Second time we are in this check
        gene.transcripts = transcripts
        cur_t.cds_list = cds_list
        cur_t.frame_list = frame_list
        file_handle.seek(curpos) #go back one line, to allow multiple gene readings
        break
      else:
        g_attrs = parse_attributes(attrs)
        gene.source = source
        gene.chrom = chrom
        gene.linenum = startpos
        try:
          gene.source = g_attrs["gene_source"]
        except KeyError:
          pass #value will remain as "none" in class
        try:
          gene.id = g_attrs["gene_id"]
        except KeyError:
          gene.id = g_attrs["id"]
        flag=1
    elif(feature=="transcript"):
      if(cds_list): 
        cur_t.cds_list = cds_list
        cur_t.frame_list = frame_list
      cur_t = Transcript()
      transcripts.append(cur_t)
      t_attrs = parse_attributes(attrs)
      cds_list = []
      frame_list = []
      cur_t.source = source
      cur_t.strand = strand
      cur_t.chrom = chrom
      try:
        cur_t.id = t_attrs["transcript_id"]
      except KeyError:
        cur_t.id = t_attrs["id"]
      try:
        cur_t.biotype = t_attrs["transcript_biotype"]
        if(cur_t.biotype=="pseudogene"):
          noncoding_flag=1
      except KeyError:
        pass
    elif(feature=="CDS" or (noncoding_flag and (feature=="exon"))):
      cds_list.append((int(start),int(stop)))
      frame_list.append(frame)
  return gene

def parse_gtf(filename,dictionary=False):
  print("Parsing gtf file %s for genes: \n[" % filename,end="")
  if(dictionary):
    genes = {}
  else:
    genes = []
  fn = open(filename,"r")
  file_len=0
  while(fn.read(20000)):  pass
  file_len=fn.tell()
  fn.seek(0)
  x=1
  regstr = "[\t]".join("\w+ \w+ (\w+)".split())
  reg = re.compile(regstr)
  curpos = fn.tell()
  line = fn.readline()
  while(True):
    match = reg.match(line)
    if(match):
      if(match.groups()[0]=="gene"): #at start of first gene in file
        break
    curpos = fn.tell()
    line = fn.readline()
  fn.seek(curpos) #back up one line
  while(True):
    if(fn.tell()>.1*x*file_len):
      print("*",end="")
      x+=1
      sys.stdout.flush()
    ret = parse_gene(fn)
    if(len(ret)==2): #reached end of file
      if(dictionary):
        try:
          genes[ret[0].chrom].append(ret[0])
        except KeyError:
          genes[ret[0].chrom] = [ret[0]]
        break
      else:
        genes.append(ret[0])
        break
    else:
      if(dictionary):
        try:
          genes[ret.chrom].append(ret)
        except KeyError:
          genes[ret.chrom] = [ret]
      else:
        genes.append(ret)
  print("*]")
  fn.close()
  return genes
  


def parse_attributes(attribute_string): #Currently works for Ensembl annotated gtf
  dict = {}
  attrs = attribute_string.split(";")
  if(attrs[-1]==""):
    del attrs[-1] #GTF formats that end lines with ; create a list with one too many elements
  for i in attrs:
    i=i.lstrip()
    i = i.split()
    if(len(i)==1): #attr list not in {key "value"} format, dont know how to parse
      return {"id":i[0]}
    dict[i[0]] = i[1].strip('"') #get rid of double quotes
  return dict
