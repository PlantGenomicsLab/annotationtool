#!/bin/python

#Using biopython/1.65
from Bio.Seq import Seq
from parse_gtf import *
import numpy as np
from Bio import SeqIO
from Bio.SeqUtils import GC
from parse_pseudopipe import parse_pseudopipe

fasta_filename = "Arabidopsis_thaliana.TAIR10.dna_sm.toplevel.fa"
true_filename = "Arabidopsis_thaliana.TAIR10.43.gff3"
chroms = {}

for i in SeqIO.parse(fasta_filename, "fasta"):
  chroms[i.id] = i.seq

def grab(transcript,option=""):
  chrom = transcript.chrom
  cds_list = transcript.cds_list
  strand = transcript.strand
  ## Re-order cds_list from low to high if necessary
  if((len(cds_list)>1) and (cds_list[1][0] < cds_list[0][0])):
    cds_list = sorted(cds_list,key=lambda x:x[0])
  seq = Seq("")
  if(option=="kozak"): #assumes first exon contains start codon
    if(strand=="+"):
      seq+=chroms[chrom][cds_list[0][0]-5:cds_list[0][0]+3]
    else:
      seq+=chroms[chrom][cds_list[-1][1]-4:cds_list[-1][1]+4]
  if(option=="splits"):
    if(len(cds_list)==1):
      return "monoexonic"
    for i in range(len(cds_list)-1): #get canonical splice sites for introns
      seq+=chroms[chrom][cds_list[i][1]:cds_list[i][1]+2]
      seq+=chroms[chrom][cds_list[i+1][0]-3:cds_list[i+1][0]-1]
  if(option==""): #grab cds
    for i in cds_list:
      seq+=chroms[chrom][i[0]-1:i[1]]
  if(strand=="-"):
    seq = seq.reverse_complement()
  return seq



def create_pwm(sequence):
  #A C G T
  matrix =np.zeros((len(sequence),4))
  for i,base in enumerate(sequence):
    if(base == "A"):
      matrix[i][0] = 1
    elif(base=="C"):
      matrix[i][1] = 1
    elif(base=="G"):
      matrix[i][2] = 1
    elif(base=="T"):
      matrix[i][3] = 1
  return matrix

def extract_features(transcript):
  #float GC,8 binary kozak,float splice_sites,float repeat_content, binary inframe_stop, binary valid length
  feat = np.zeros(13)

  #GC score
  seq_full = grab(transcript)
  feat[0] = GC(seq_full)

  #kozak score
  seq = grab(transcript,"kozak")
  pwm = create_pwm(seq)*kozak
  pwm = np.sum(pwm,axis=1)
  feat[1:9] = pwm
  
  #splice site score, normalized with # introns, max score=4
  numintrons = len(transcript.cds_list)-1
  if(numintrons==0):
    feat[9] = 1
  else:
    seq = grab(transcript,"splits")
    pwm1 = create_pwm(seq)
    np.where(pwm1==0,-1,pwm1)
    pwm2 = create_pwm("GTAG"*numintrons)
    res = pwm1*pwm2
    res = np.sum(res) / numintrons
    feat[9] = res

  #repeat content
  tot_len = len(seq_full)
  num_masked=0
  for i in seq_full:
    if(i.islower()):
      num_masked+=1
  feat[10] = num_masked/tot_len

  #inframe stop
  global prot_count1,pseudo_count1,prot_count2,pseudo_count2
  prot = seq_full.translate()
  if(("*" in prot and (prot[-1]!="*")) or (prot.count("*")>1)):
    feat[11] = 1
    if(transcript.biotype=="protein_coding"):
      prot_count1+=1
    elif(transcript.biotype=="pseudogene"):
      pseudo_count1+=1
  else:
    feat[11] = 0

  #valid length
  len(transcript)
  feat[12] = transcript.valid
  if(transcript.valid==0):
    if(transcript.biotype=="protein_coding"):
      prot_count2+=1
    elif(transcript.biotype=="pseudogene"):
      pseudo_count2+=1
  
  return feat

def coverage(gene_list):
  res = 0
  invalid = 0
  for i in gene_list:
    for j in i.transcripts:
      res+=len(j)
      if(j.valid == 0):
        invalid+=1
  print("num invalid = %d" % invalid)
  return res

def write_files(features,labels,feature_handle,label_handle): 
  for i in range(len(features)):
    feat = extract_features(features[i])
    for k in feat:
      feature_handle.write(str(k)+"\t")
    feature_handle.write("\n")
    label_handle.write(str(labels[i])+"\n")
  feature_handle.close()
  label_handle.close()

def create_fasta(transcript_list,handle,protein=False):
  for i in transcript_list:
    handle.write(">"+i.id+"\n")
    if(not protein):
      handle.write(str(grab(i))+"\n")
    else:
      handle.write(str(grab(i).translate())+"\n")
  handle.close()

if __name__ == "__main__":
  from parse_gtf import *
  kozak = np.array([[25,53,15,7],[61,2,36,1],[27,49,13,11],[15,55,21,9],[97,0,0,0],[0,0,0,97],[0,0,97,0],[23,16,46,15]])
  pseudo_filename = "Arabidopsis_thaliana.TAIR10.27.77_pgene.txt"
  true = parse_gtf(true_filename)
  truepos = []
  for i in true:
    for j in i.transcripts:
      if(j.biotype=="protein_coding"):
        truepos.append(j)
  trueneg = parse_pseudopipe(pseudo_filename)

  print("%d collected proteincoding and %d collected pseudogene" % (len(truepos),len(trueneg))) 

  #Balance dataset
  truepos=truepos[:len(trueneg)]
 
  #Create train and test set 
  length = len(trueneg)
  train_part = round((2/3.0)*length)
  test_part = round((1/3.0)*length)
  train_features = truepos[:train_part]+trueneg[:train_part]
  train_labels = [1]*train_part + [0]*train_part
  test_features = truepos[train_part:]+trueneg[train_part:]
  test_labels = [1]*test_part + [0]*test_part
  assert(len(test_labels) == len(test_features))

  wf1 = open("train_prot.fasta",'w')
  wf2 = open("test_prot.fasta",'w')
  create_fasta(train_features,wf1,protein=True)
  create_fasta(test_features,wf2,protein=True)
  
  
  trfeat = open("train_features.txt",'w')
  trlabe = open("train_labels.txt",'w')
  tefeat = open("test_features.txt",'w')
  telabe = open("test_labels.txt",'w')

  pseudo_count1,prot_count1,pseudo_count2,prot_count2=[0]*4
  write_files(train_features,train_labels,trfeat,trlabe)
  write_files(test_features,test_labels,tefeat,telabe)

  print("Inframe stop = %d, %d" % (prot_count1,pseudo_count1))
  print("Valid length = %d, %d" % (prot_count2,pseudo_count2))
