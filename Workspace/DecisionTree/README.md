### Key Results

* Most informative features were Kozak, GC, and SpliceScore
* Sensitivity of .873 and Specificity of .686 were reached with max_depth=5, using train/dev set
* above results shown in pdf [here](results/dtc_maxdepth3.pdf)
* Bias due to more real genes than pseudogenes accounted for by limiting #realgenes=#pseudogenes in dataset
* Many Ensembl pseudogenes contain in-frame stops, making them very easily separable from real genes in a gene set, however above scores were calculated without using this feature
* gff last column highly divergent depending on sources

