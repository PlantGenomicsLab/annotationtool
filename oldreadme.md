Plant Computational Genomics Lab @ UConn  

# Non-Model Genome Annotation Tool
**We are designing a genome annotation tool that integrates deep learning approaches to build upon existing approaches.**  

### 1. Assessment of Existing Tools
We pushed twelve model & non-model species through existing annotation pipelines to assess the current state of whole genome annotation. These organisms include plants in two phyla, animals in 4 phyla and a single fungus.

##### Organism used for study  
The binomial name, common name, general taxonomic classification, genome size and current state of genomic resources for that system are displayed in the table below.

Organism | Common Name | Taxonomy | Genome size (Mbp) | Characterization 
-------- | ----------- | -------- | ----------- | ----------------
_Apis mellifera_ | Honey bee | Insect | 235 | Well studied
_Arabidopsis thaliana_ | Mouse-ear (thale) cress | Flowering Plant | 135 | Model system
_Caenorhabditis elegans_ | C. elegans | Nematode | 101 | Model system
_Delphinapterus leucas_ | Beluga whale | Mammal | 2320 | Limited
_Drosophila melanogaster_ | Fruit Fly | Insect | 175 | Model system
_Funaria hydgrometrica_ | Bonfire moss | Bryophyte | **???** | Limited
_Juglans regia_ | Persian (English) walnut | Flowering Plant | 650 | Intermediate
_Saccharomyces cerevisiae_ | Brewer's yeast | Ascomycete | 12.1 | Model system
_Mus musculus_ | House mouse | Mammal | 2670 | Model system
_Notamacropus eugenii_ | Tammar wallaby | Mammal | 3070 | Limited 
_Pterocarya stenoptera_ | Chinese wingnut | Flowering Plant | 955 | Limited
A tapeworm? | ? | ? | ? | ?



