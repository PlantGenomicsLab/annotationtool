## Full pipeline used
1. Genome softmasked and filtered
2. Stringtie transcript created using StringTie version 1, input genome + reads
3. BUSCO ran with default parameters and correct taxa on genome
4. True genes extracted from BUSCO, False genes gathered/inferred
5. Sequences 300bp upstream/downstream of tss extracted from all genes
6. Sequences folded and fold alphabet converted to integers
7. DNA sequence and folding information used to train TiS classifier
8. TiS classifier run on full genome sequence to identify possible starts
9. Hintsfile created from starts
10. Augustus run with/without hintsfile
11. Output gff filtered by EnTAP sequence similarity hit
12. gFACs filtering/statistics


## Links
- [data_spreadsheet](https://docs.google.com/spreadsheets/d/1NG_rW-8GbvySx6wY5AfBE3s9-XH3dCxTbdpW-7jPxJc/edit?folder=0AAX8ZdB4HRYIUk9PVA#gid=1339513429)
- [binary_classifier](https://docs.google.com/spreadsheets/d/1RRxci03ixGcMhkssYMn83LOPWzz20Gp04vidIp4lgS4/edit#gid=0)

## Iterations tried
{stringtie training small/large,hints/nohints/truehints,gFACs filters,pretrained}
Notable results
* Truehints show the best results, but hints in general have small impact, presented with the M=manual notation (obey hint notation)
* Stringtie transcripts show high sensitivity/specificity
* Results approach braker results, and are higher than maker in general
* Nucleotide sensitivity is high, transcript sensitifity is low
* Underprediction without hints, overprediction with hints, but similar scores for both
* Predict more genes with truehints than with normal hints
* EnTAPfiltered reduce number of genes back to nohints level, with only small improvements that are mostly in specificity


## Room for improvement
* Pass folding information in differently than integer representation of complex symbols
* Either pass hints in differently or create new hmm for problem specific solution
* Determine if different/more filtering can be done to increase specificity
* Different window sizes to find optimal region for TiS classifier to work with
* New StringTie Version

