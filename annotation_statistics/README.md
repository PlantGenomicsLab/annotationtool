## Annotation Statistics

This directory will contain scripts/external tools used for measuring performance of annotations.

# What I've done
1. Train augustus on busco transcripts for celegans
* path on cluster
2. Perform 3 runs on chromosome 1 using different hint files. 
* nohints:
* hints:
* truehints:
3. Produce results and plots using script comparison_statistics.py

# Issues noticed and fixed
* Augustus does not include stop site in prediction, ensembl does, resulting in all stop sites to be -3 or +3 off

# Relevant results
* TBD


