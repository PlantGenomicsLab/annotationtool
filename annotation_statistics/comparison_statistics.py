import sys
import numpy as np
import re
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

fn = sys.argv[1]

counts = []

start_base_err=[]
stop_base_err=[]
cds_err = []

perfect_starts=[]
perfect_stops=[]
percent_cds=[]

num_pred_per_comp=[]


startreg = re.compile(".*reference transcripts:.*")
stopreg = re.compile(".*prediction transcripts:.*")

wf = open("comparisons.txt",'w')

with open(fn) as rf:
  l = rf.readline()
  ln = 1
  while(l):
    if(not(startreg.match(l))):
      l = rf.readline()
      ln+=1
      continue
    wf.write("Found comparison on line %d\n" % ln)
    count = 0
    l = rf.readline()
    ln+=1
    while(not(stopreg.match(l))):
      count+=1
      l = rf.readline()
      ln+=1
    counts.append(count)
    if(count>1):
      wf.write("Ignoring multiple ref transcripts at Line %d\n\n" % ln)
      continue
    while(not(l.startswith('##gff-version 3'))):
      l = rf.readline()
      ln+=1
    l = rf.readline()
    ln+=1
# read ref transcript
#    print("reading reference transcript:")
    ref_stop=0
    ref_start=0
    ref_cds=0
    while(not(l.startswith('###'))):
      chrom,source,typ,start = l.strip().split("\t")[:4]
      if(typ=="start_codon"):
        ref_start=start
      elif(typ=="stop_codon"):
        ref_stop=start
      elif(typ=="CDS"):
        ref_cds+=1
      l = rf.readline()
      ln+=1
    pred_stops = []
    pred_starts = []
    pred_cds = []
    cur_cds=0
# skip | predictive genes, and ###gff-version
    l = rf.readline()
    l = rf.readline()
    l = rf.readline()
#    print("Reading pred transcript:")
#    print(l,end="") # print start of pred gene 
    ln+=3
    while(not(l.startswith(' |'))):
      if(l.startswith('###')):
#        print("end of pred transcript")
        pred_cds.append(cur_cds)
        cur_cds=0
        l = rf.readline()
        ln+=1
        continue
      chrom,source,typ,start = l.strip().split("\t")[:4]
      if(typ=="start_codon"):
        pred_starts.append(start)
      elif(typ=="stop_codon"):
        pred_stops.append(start)
      elif(typ=="CDS"):
        cur_cds+=1
      l = rf.readline()
      ln+=1
#    print("Done parsing comparison")
    wf.write("\tReference\tPredicted\n")
    wf.write("Starts:\t%s\t\t%s\n" % (ref_start,str(pred_starts)))
    wf.write("Stops:\t%s\t\t%s\n" % (ref_stop,str(pred_stops)))
    wf.write("#CDS:\t%s\t\t%s\n\n" % (ref_cds,str(pred_cds)))
    assert(len(pred_cds)==len(pred_starts))
    num_pred_per_comp.append(len(pred_cds))
    for i in range(len(pred_starts)):
      sta = int(pred_starts[i])
      sto = int(pred_stops[i])
      sta_diff = sta-int(ref_start)
      sto_diff = sto-int(ref_stop)
      start_base_err.append(sta_diff)
      stop_base_err.append(sto_diff)
      if(sta_diff==0):
        perfect_starts.append(1)
      else:
        perfect_starts.append(0)
      if(sto==ref_stop):
        perfect_stops.append(1)
      else:
        perfect_stops.append(0)
      perc_error = (pred_cds[i]-ref_cds)/ref_cds
      percent_cds.append(perc_error)
      cds_err.append(pred_cds[i]-ref_cds)
    l = rf.readline()
    ln+=1


def plot_hist(starterr,stoperr):
  bins = [-10000,-1000,-100,-50,-3,-2,-1,0,1,2,3,4,50,100,1000,10000]
  fig,axs = plt.subplots(2,1,figsize=(20,15))
  for a,data in enumerate([starterr,stoperr]):
    hist, bin_edges = np.histogram(np.clip(data,bins[0],bins[-1]),bins)
#    print(hist)
    ax = axs[a]
    tick_label= ['<%d' % bins[0]] + \
    ['[{}, {})'.format(bins[i],bins[i+1]) for i in range(1,4)] + \
    [str(i) for i in range(-3,4)] + \
    ['[{}, {})'.format(bins[i],bins[i+1]) for i in range(11,len(bins)-2)] + \
    ['>%d' % bins[-1]]
    ax.bar(range(len(hist)),hist,width=1,align='center',tick_label=tick_label)
    ax.text(len(hist)-1,max(hist)+1,"Total=%d" % sum(hist),fontsize=14,ha='center')
    for i,v in enumerate(hist):
      ax.text(i,v+1,str(v),color='black',fontweight='bold',fontsize=12,ha='center')
      
  axs[0].set_ylabel("Start error (bp)",fontsize=20)
  axs[1].set_ylabel("Stop error (bp)",fontsize=20)
  fig.tight_layout(pad=3.0)
  fig.savefig("startstop.png",dpi=100)
#  plt.setp(axs[0].xaxis.get_majorticklabels(), rotation=90)

def plot_cds_hist(cds_err):
  fig, axs = plt.subplots(1,1,figsize=(20,15))
  nbins=20
  hist = plt.hist(cds_err,bins=nbins)
  rightcoord = len([i for i in hist[1] if i>0])
  plt.text(rightcoord-5,max(hist[0])+1,"Total = %d" % sum(hist[0]),fontsize=14,ha='center')
#  axs.set_title("Normalized cds error (abs(pred_num_cds - true_num_cds) / true_num_cds)",fontsize=14,ha='center')
  axs.set_title("Number of CDS error (pred_num_cds - true_num_cds)",fontsize=14,ha='center')
  for i in range(nbins):
    plt.text(hist[1][i]+0.25,hist[0][i],str(hist[0][i]),fontsize=12)
  fig.savefig("cds.png",dpi=100)

      

perfect_starts=np.array(perfect_starts)
perfect_stops=np.array(perfect_stops)
percent_cds=np.array(percent_cds)
num_pred_per_comp=np.array(num_pred_per_comp)
counts = np.array(counts)


start_base_err = np.array(start_base_err)
stop_base_err = np.array(stop_base_err)
cds_err = np.array(cds_err)


plot_hist(start_base_err,stop_base_err)
plot_cds_hist(cds_err)

print("General Statistics:")
print("%d total comparisons" % len(counts))
print("avg num reference transcripts per comparison = %0.3f" % counts.mean())
print("avg num predicted transcripts per comparison = %0.3f" % num_pred_per_comp.mean())

print()
print("Start/Stop Statistics")
print("%2.1f%% Predicted starts match perfectly with reference" % (perfect_starts.mean()*100))
print("%2.1f%% Predicted stops match perfectly with reference" % (perfect_stops.mean()*100))
print()

print("CDS Statistics")
#print("average percent error of predicted # CDS = %0.3f" % percent_cds.mean())
print("Average # CDS error = %0.3f" % cds_err.mean())
