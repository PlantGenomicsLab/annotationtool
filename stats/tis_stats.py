import operator

total = 0
total_labels = {
  '0': 0,
  '1': 0
}
counts = {}
strand_counts = {}
canon = 0
canon_labels = {
  '0': 0,
  '1': 0
}
masked = 0
masked_labels = {
  '0': 0,
  '1': 0
}

pad_len = 600
path = '../data/train_tis.txt'
handle = open(path, 'r')
line = handle.readline().strip()
while line:
  items = line.split(',')
  strand = items[0]
  label = items[2]
  tis = items[1][pad_len:(-1 * pad_len)]
  assert(len(tis) == 3)
  total += 1
  total_labels[label] += 1
  if tis != tis.upper():
    masked += 1
    masked_labels[label] += 1
  if tis.lower() == 'atg':
    canon += 1
    canon_labels[label] += 1
  else:
    strand_key = strand + tis.upper()
    key = tis.upper()
    if not key in counts.keys():
      counts[key] = 0
    counts[key] += 1
    if not strand_key in strand_counts.keys():
      strand_counts[strand_key] = 0
    strand_counts[strand_key] += 1
  line = handle.readline().strip()

if canon != total:
  top = 3
  sort_counts = sorted(counts.items(), key=operator.itemgetter(1))
  print('Top {} non-canonical:'.format(top))
  sort_counts = sort_counts[-1 * top:]
  for i in range(len(sort_counts)):
    index = len(sort_counts) - i - 1
    print('{}: {}'.format(sort_counts[index][0], sort_counts[index][1]))
  
  sort_counts = sorted(strand_counts.items(), key=operator.itemgetter(1))
  print('Top {} (strand-specific) non-canonical:'.format(top))
  sort_counts = sort_counts[-1 * top:]
  for i in range(len(sort_counts)):
    index = len(sort_counts) - i - 1
    print('{}: {}'.format(sort_counts[index][0], sort_counts[index][1]))

print('tis file:        {}'.format(path))
print('\nAll tis\'s:')
print('Total tis\'s:     {}'.format(total))
print('Canonical:       {} ({:.2f}%)'.format(canon, 100. * canon / total))
print('Masked:          {} ({:.2f}%)'.format(masked, 100. * masked / total))

print('\n=============================')

print('\nBy label:')
print('True Positives:')
print('Total tis\'s:     {}'.format(total_labels['1']))
print('Canonical:       {} ({:.2f}%)'.format(canon_labels['1'], 100. * canon_labels['1'] / total_labels['1']))
print('Masked:          {} ({:.2f}%)'.format(masked_labels['1'], 100. * masked_labels['1'] / total_labels['1']))

print('\nTrue Negatives:')
print('Total tis\'s:     {}'.format(total_labels['0']))
print('Canonical:       {} ({:.2f}%)'.format(canon_labels['0'], 100. * canon_labels['0'] / total_labels['0']))
print('Masked:          {} ({:.2f}%)'.format(masked_labels['0'], 100. * masked_labels['0'] / total_labels['0']))
