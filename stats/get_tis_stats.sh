#!/bin/bash
#SBATCH --job-name=get_tis_stats
#SBATCH -o get_tis_stats.out
#SBATCH -e get_tis_stats.err
#SBATCH --ntasks=1 
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=256MB

echo `hostname`
cd /labs/Wegrzyn/annotationtool/stats
python tis_stats.py > tis_stats.txt

