# Results and Temporary Storage for Thesis on Augustus Analysis

## Intro
Augustus is a widely popular, efficient, and accurate gene structure prediction software. Using an underlying Hidden Markov Model algorithm for its prediction, 
Augustus can be trained on extrinsic data for a boost in prediction performance. It has been benchmarked on many model species with impressive results, and 
according to the makers, "usually belongs to the most accurate programs for the species it is trained for". Augustus serves as the backbone for more complex 
gene prediction software Maker and Braker, which continue to dominate the field in efficiency and accuracy. However, augustus has been around since at least 
2006, and its age is noticible. Many features of the software are vague and confusing, and in what circumstances it excels and fails have not been outlined 
in recent years. This study will take a deep look at augustus, why it works so well and the issues it currently has in practice, so researchers may gain insight 
on best practices with the software, as well as a look into some ways it could be improved as a whole.

## Questions to answer
* When does augustus do best? What types of input from which sources perform best according to well known models and some insight as to why. For the bioinformatician 
who wants to get the best prediction possible using augustus, what preprocessing, if any, do they need to do to their input? 
* What tradeoffs are present in the software? How sensitive is the software to changes in its parameters when it comes to accuracy/precision/recall/etc
* When does augustus struggle? What types of input make it very difficult for the software to output sensible results?
* Can the benchmarks for the software currently out in the wild be recreated? If not, why? If yes, how?
* What things can be improved about the implementation of augustus, in terms of results and prediction performance?
* What things can be improved about the implementation in terms of usability and understandability of the output/understanding the errors when it fails?


## Thesis work outline


1. Re-Understanding how augustus works and all of its parts
* Not necessarily at the code level, reading papers and article on how it works creating a document to explain its parts and why they are necessary
2. Recreate the benchmarks found online
* Using celegans,fly,arabidopsis
3. Prediction runs
* Using model and non-model species, get runs done that highlight a wide range of accuracy and performance of augustus
* Goal is to get experimental results of when augustus does very well and when it does poorly
* full documentation
4. Analysis into prediction runs
* With advice from 3rd parties, analyze exactly why these situations occured
* Breakdown situations into component parts, which parts are responsible for performance
5. Test analysis
* Using identified parts, see if you can predict which runs will be great and bad by altering parameters you decided as important
6. Conclusions
* What can my results do to aid the community, what was valuable and what is still up for grabs

